//
//  InfoCell.h
//  RMA
//
//  Created by Alexander Roode on 2/17/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RMATableViewCell.h"

@interface InfoCell : RMATableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
+(CGFloat)heightWithDetailText:(NSString*)detailText width:(CGFloat)width;
@property (weak, nonatomic) IBOutlet UIButton *button;
@end
