//
//  FormApprovalViewController.m
//  RMA
//
//  Created by Michael Beteag on 10/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormApprovalViewController.h"

@interface FormApprovalViewController ()

@end

@implementation FormApprovalViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(close)];
	self.navigationItem.leftBarButtonItem = closeButton;
    
    self.clientNameField.delegate = self;
}

-(void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sign:(id)sender {
    if(self.clientNameField.text.length > 0) {
        Image *image = [Image imageWithImage:self.signatureView.signatureImage size:self.signatureView.frame.size];
        //[self.navigationController popViewControllerAnimated:YES];
        [self.delegate signWithImage:image clientName:self.clientNameField.text];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter client name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

- (IBAction)clear:(id)sender {
    [self.signatureView erase];
}


- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(![textField isKindOfClass:[RMATextField class]]) {
        return YES;
    }
    
    int maxWidth = [[textField valueForKey:@"maxWidth"] intValue];
    if(maxWidth > 0) {
        UIFont *font = [UIFont systemFontOfSize:14];
        CGSize newSize = CGSizeZero;
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        CGRect rect = [newText boundingRectWithSize:CGSizeMake(MAXFLOAT, 20)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:font}
                                            context:nil];
        newSize = rect.size;

        return newSize.width < maxWidth;
    }
    
    int maxLength = [[textField valueForKey:@"maxLength"] intValue];
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= maxLength || returnKey;
}

-(void)textFieldDidChange:(UITextField*)textField {
    textField.backgroundColor = [UIColor whiteColor];
    [textField removeTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

@end
