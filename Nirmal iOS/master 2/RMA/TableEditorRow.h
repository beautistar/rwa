//
//  TableEditorRow.h
//  RMA
//
//  Created by Michael Beteag on 12/18/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TableEditorRow : NSObject

@property (nonatomic, retain) NSArray* fixedValues;

+(TableEditorRow*)tableEditorRowWithFixedValues:(NSArray*)fixedValues;

@end
