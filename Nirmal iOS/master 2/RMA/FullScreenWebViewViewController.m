//
//  FullScreenWebViewViewController.m
//  RMA
//
//  Created by Michael Beteag on 12/18/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FullScreenWebViewViewController.h"

@interface FullScreenWebViewViewController ()

@end

@implementation FullScreenWebViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webView.delegate = self;
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    if(self.username.length > 0 && self.password.length > 0) {
        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById('MainContent_LoginUser_UserName').value = '%@';",self.username]];
        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById('MainContent_LoginUser_Password').value = '%@';",self.password]];
        [self.webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('MainContent_LoginUser_LoginButton').click();"];
    }
    
}

@end
