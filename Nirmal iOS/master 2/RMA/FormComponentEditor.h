//
//  FormComponentEditor.h
//  RMA
//
//  Created by Michael Beteag on 7/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

typedef enum : NSUInteger {
    validationTypeNone,
    validationTypeAll
} validationType;

#import <Foundation/Foundation.h>
#import "UISegmentedControl+Text.h"
#import "FormValidationError.h"
#import "RMATextView.h"
#import "DatePickerViewController.h"

@interface FormComponentEditor : UIView <UITextFieldDelegate,UITextViewDelegate, DatePickerDelegate>

@property (nonatomic, retain) IBOutlet UIView *view;
@property (nonatomic, retain) UISegmentedControl *enabler;
@property (nonatomic) BOOL isEnabled;
@property (nonatomic, strong) NSArray *requiredTaskCodes;
@property validationType validationType;

// If standalone is YES, editor gets its own section with type sectiontType.
// Currently used only for additional remarks.
@property BOOL standalone;
@property (nonatomic, strong) NSString *standaloneSectionType;

@property (strong, nonatomic) NSMutableDictionary *LabSamples;
@property (strong, nonatomic) IBOutlet UITableView *LabSamplesTableView;

//-(void)highlightRequiredFields;
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

-(BOOL)validates:(NSMutableArray**)validationErrors referenceView:(UIView*)referenceView;
-(NSMutableDictionary*)getDataForRendering;
-(NSMutableDictionary*)getData;
-(NSMutableDictionary*)getSharedData;
-(void)setData:(NSMutableDictionary*)data;
-(BOOL)requireAllFields:(NSArray*)views validationErrors:(NSMutableArray**)validationErrors referenceView:(UIView*)referenceView;

-(void)addEnablerWithText:(NSString*)text reversed:(BOOL)reversed;
-(void)setEnabled:(BOOL)enabled forTagsBetween:(int)firstTag and:(int)lastTag;
-(void)setControls:(NSArray*)controls enabled:(BOOL)enabled;
-(void)setFormEnabled:(BOOL)enabled;
-(BOOL)forcesNonCompliance;
-(void)reevaluateForcedNonComplaince;
- (IBAction)toggleButtonSelected:(id)sender;
-(void)didEnterDate:(NSString *)date;

-(NSDictionary*)dictionary;

@end
