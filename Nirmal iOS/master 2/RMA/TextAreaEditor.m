//
//  TextAreaEditor.m
//  RMA
//
//  Created by Michael Beteag on 7/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "TextAreaEditor.h"
#import "RMATextView.h"
#import "Constants.h"

@implementation TextAreaEditor

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.view = self;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame name:(NSString*)name key:(NSString*)key
{
    self = [self initWithFrame:frame];
    
    int topPadding = kPaddingY;
    int topHeight = kLabelHeight;
    
    if (self.view == nil) {
        NSLog(@"View is nil!");
    }
    self.name = name;
    self.key = key;
    self.textAreaLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPaddingX,
                                                                   topPadding,
                                                                   self.frame.size.width - 2*kPaddingX,
                                                                   kLabelHeight)];
    self.textAreaLabel.text = self.name;
    [self.view addSubview:self.textAreaLabel];
    
    self.textArea = [[RMATextView alloc] initWithFrame:CGRectMake(kPaddingX,
                                                                  2*kPaddingY + topHeight,
                                                                  self.frame.size.width - 2*kPaddingX,
                                                                  self.frame.size.height - 3*kPaddingY - topHeight)];

    self.textArea.delegate = (FormComponentEditor*)self.view;
    
    [self.view addSubview:self.textArea];
    
    return self;
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    NSString *text = @"";
    
    if(self.textArea.text != nil) {
        text = self.textArea.text;
    }
    [data setObject:[Paragraph paragraphWithText:text] forKey:self.key];

    return data;
    
}
-(void)setData:(NSMutableDictionary*)data {
    NSString *text = @"";
    if(((Paragraph*)[data objectForKey:self.key]).text != nil) {
        text = ((Paragraph*)[data objectForKey:self.key]).text;
    }
    self.textArea.text = text;
}

-(NSDictionary*)dictionary {
    return @{self.key: self.textArea.text};
}

@end
