//
//  ConcretePageSetBreakPointsEditor.h
//  RMA
//
//  Created by Michael Beteag on 11/12/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"

@class ConcretePageSet,RMATextField;
@interface ConcretePageSetBreakPointsEditor : FormComponentEditor <UITextFieldDelegate>

@property (nonatomic, retain) RMATextField *numberOfSamplesField;
@property (nonatomic, retain) NSMutableArray *breakPointSegmentedControls;

+(ConcretePageSetBreakPointsEditor*)concretePageSetBreakPointsEditor;
-(NSMutableDictionary*)getPartialConcreteSet;
-(void)setConcretePageSet:(ConcretePageSet*)set;

@end
