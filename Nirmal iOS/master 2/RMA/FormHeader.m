//
//  FormHeader.m
//  RMA
//
//  Created by Michael Beteag on 10/7/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormHeader.h"
#import "GRMustache.h"

@implementation FormHeader

+(FormHeader*)headerWithType:(NSString*)headerType {
    FormHeader *header = nil;
    if([headerType isEqualToString:@"nonDSAHeader"]) {
        header = [[FormHeader alloc] init];
        header.templateName = headerType;
        header.height = 170;
    } else if([headerType isEqualToString:@"DSAPage1Header"]) {
        header = [[FormHeader alloc] init];
        header.templateName = headerType;
        header.height = 230;
    } else if([headerType isEqualToString:@"nonDSAPage1Header"]) {
        header = [[FormHeader alloc] init];
        header.templateName = headerType;
        header.height = 196;
    } else if([headerType isEqualToString:@"blankHeader"]) {
        header = [[FormHeader alloc] init];
        header.height = 150;
        header.templateName = headerType;
        
    }
    return header;
}

-(NSString*)HTMLwithData:(NSMutableDictionary*)data {
    return [GRMustacheTemplate renderObject:data
                               fromResource:self.templateName
                                     bundle:nil
                                      error:NULL];
}


@end
