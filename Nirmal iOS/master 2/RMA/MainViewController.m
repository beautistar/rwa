//
//  MainViewController.m
//  RMA
//
//  Created by Michael Beteag on 7/12/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "MainViewController.h"
#import "GRMustache.h"
#import "SharepointUtility.h"
#import "AppData.h"
#import "PDFViewer.h"

#import "ProjectDashboardViewController.h"
#import "XMLDictionary.h"

#import "LoginManager.h"

#import "MenuViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
    _usernameField.text = @"500lep";
    _passwordField.text = @"6279";
    
    UIImage *logoImage = nil;
    
#ifdef GEOSCIENCE
    logoImage = [UIImage imageNamed:@"rma-geosciences"];
#endif
#ifdef CVT
    logoImage = [UIImage imageNamed:@"rma-cvt"];
#endif
#ifdef SITESCAN
    logoImage = [UIImage imageNamed:@"sitescan"];
#endif
    
    if (logoImage != nil) {
        CGPoint center = self.logoView.center;
        self.logoView.image = logoImage;
        self.logoView.frame = CGRectMake(center.x - logoImage.size.width / 2,
                                         center.y - logoImage.size.height / 2,
                                         logoImage.size.width,
                                         logoImage.size.height);
    }
    
    PDFViewer *viewer = [PDFViewer sharedViewer];
    viewer.navigationController = self.navigationController;
    
    // Removed for app store build
    //[[SharepointUtility sharedUtilty] updateDocuments];

}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    __weak typeof(self) weakSelf = self;
    loginSuccessObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"LoginDidSucceed"
                                                                 object:nil
                                                                  queue:nil
                                                             usingBlock:^(NSNotification* notification){
                                                                 [SVProgressHUD dismiss];
                                                                 /*
                                                                 ProjectDashboardViewController *projectDashboardViewController = [[ProjectDashboardViewController alloc] init];
                                                                 projectDashboardViewController.technicianId = weakSelf.usernameField.text;
                                                                 
                                                                 AppData *appData = [AppData sharedData];
                                                                 appData.currentTechnicianId = weakSelf.usernameField.text;
                                                                 if(weakSelf.selectedProject != nil) {
                                                                     projectDashboardViewController.project = weakSelf.selectedProject;
                                                                     
                                                                 }
                                                                 [weakSelf.navigationController pushViewController:projectDashboardViewController animated:YES];*/
                                                                 
                                                                 // for test
                                                                 [self gotoReport];
                                                                 
                                                             }];
    loginFailureObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"LoginDidFail"
                                                                             object:nil
                                                                              queue:nil
                                                                         usingBlock:^(NSNotification* notification){
                                                                             [SVProgressHUD showErrorWithStatus:notification.object];
                                                                         }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:loginFailureObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:loginSuccessObserver];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setEmpId:nil];
    [super viewDidUnload];
}

- (void) gotoReport {
    
    __weak typeof(self) weakSelf = self;
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Report" bundle:[NSBundle mainBundle]];
    MenuViewController * menuVC = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    [weakSelf.navigationController pushViewController:menuVC animated:YES];
    
}

-(void)didSelectProject:(NSDictionary*)projectDict {
    if(projectDict == nil) {
        self.selectedProjectLabel.text = @"All";
    } else {
        self.selectedProject = projectDict;
        self.selectedProjectLabel.text = [projectDict objectForKey:@"Descr"];
        CGFloat width = self.selectedProjectLabel.frame.size.width;
        [self.selectedProjectLabel sizeToFit];
        [self.selectedProjectLabel setFrame:CGRectMake(self.selectedProjectLabel.frame.origin.x,
                                                      self.selectedProjectLabel.frame.origin.y,
                                                      width,
                                                      self.selectedProjectLabel.frame.size.height)];
    }
    [self.projectPopoverController dismissPopoverAnimated:YES];
}

- (IBAction)selectProjectButtonPressed:(id)sender {
    ProjectSelectionViewController *projectSelectionViewController = [[ProjectSelectionViewController alloc] init];
    projectSelectionViewController.delegate = self;
    self.projectPopoverController = [[UIPopoverController alloc] initWithContentViewController:projectSelectionViewController];
    
    [self.projectPopoverController presentPopoverFromRect:self.projectSelectButton.frame inView:self.projectSelectButton.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)downloadForms:(id)sender {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:@"Logging in..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    
    LoginManager *manager = [LoginManager loginManagerWithUsername:self.usernameField.text password:self.passwordField.text];
    [manager login];
    
    return;
}

- (IBAction)testButtonPressed:(id)sender {
    NSString *testRecords = @"<Records><Record><DispID>2222222</DispID><Project>12-823-0</Project><Scope>03</Scope><Descr>Kaiser Antelope Valley Specialty MOB</Descr><TaskCode>H2211</TaskCode><DailyForm>Building Inspection Report</DailyForm><Page2Form></Page2Form><Page3Form>                         </Page3Form><TechnicianId>200KET</TechnicianId><TechnicianName> Erik Klunk</TechnicianName><CallNotes/><DRRemarks/><Hours>8.00</Hours><StartDate>2013-11-20T00:00:00</StartDate><StartTime>1900-01-01T07:00:00</StartTime><ClientID>ADMIN112973631599</ClientID><ClientName>Kaiser Foundation Health Plan, Inc</ClientName><ContactId>YKEFFELEW1328217118603</ContactId><ContactName>Prater, Randy</ContactName><ContactPhone>(323) 783-7374</ContactPhone><Address1>Kaiser Lancaster</Address1><Address2>604 West Avenue L</Address2><City>Lancaster</City><State>CA</State></Record>"
    @"<Record><DispID>11111111</DispID><Project>12-823-0</Project><Scope>03</Scope><Descr>Kaiser Antelope Valley Specialty MOB</Descr><TaskCode>H2211</TaskCode><DailyForm>Public Works Daily Report</DailyForm><Page2Form></Page2Form><Page3Form>                         </Page3Form><TechnicianId>200KET</TechnicianId><TechnicianName> Erik Klunk</TechnicianName><CallNotes/><DRRemarks/><Hours>8.00</Hours><StartDate>2013-11-20T00:00:00</StartDate><StartTime>1900-01-01T07:00:00</StartTime><ClientID>ADMIN112973631599</ClientID><ClientName>Kaiser Foundation Health Plan, Inc</ClientName><ContactId>YKEFFELEW1328217118603</ContactId><ContactName>Prater, Randy</ContactName><ContactPhone>(323) 783-7374</ContactPhone><Address1>Kaiser Lancaster</Address1><Address2>604 West Avenue L</Address2><City>Lancaster</City><State>CA</State></Record></Records>";

    NSDictionary *responseData = [NSDictionary dictionaryWithXMLString:testRecords];
    if([responseData objectForKey:@"body"] != nil) {
    } else {
        if([responseData objectForKey:@"Record"] != nil) {
            [[AppData sharedData] updateForms:[responseData objectForKey:@"Record"]];
        }
    }
}

@end
