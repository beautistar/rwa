//
//  FormPreviewViewController.m
//  RMA
//
//  Created by Michael Beteag on 7/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormPreviewViewController.h"

@interface FormPreviewViewController ()
@property (nonatomic, strong) PDFGenerator *generator;
@property (nonatomic, strong) SubmissionManager *submissionManager;
@end

@implementation FormPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.webView.delegate = self;
    self.submissionManager = [[SubmissionManager alloc] initWithDelegate:self];
    
    self.PDFFilename = [self.form.localFilename stringByReplacingOccurrencesOfString:@".rma" withString:@".pdf"];
    self.PDFPath = [[AppData sharedData] pathToDocumentNamed:self.PDFFilename];

    // Check if form has already been completed
    if (![self.form isEditable]) {
        [self removeApprovalButton];
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:self.PDFPath]]];
        return;
    }
    
    if (self.disableApproval) {
        [self removeApprovalButton];
    }
    
#ifdef DEBUG
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Submit Data" style:UIBarButtonItemStylePlain target:self action:@selector(submitDatabuttonPressed:)];
    NSMutableArray * toolbarItems = [self.toolbar.items mutableCopy];
    [toolbarItems addObject:nextButton];
    self.toolbar.items = toolbarItems;
#endif
    
    [self createPDF];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    if (self.submitting) {
        [self submitData];
    }
}

-(void)createPDF {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:@"Creating form..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];

    self.generator = [[PDFGenerator alloc] init];
    self.generator.delegate = self;
    [self.generator generatePDFFromFormBuilder:self.formBuilder outputPath:self.PDFPath];
}

-(void)PDFGenerationDidFail:(NSString *)error {
    [SVProgressHUD showErrorWithStatus:error];
}

-(void)PDFGenerationDidSucceed:(NSString *)path {
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
    [SVProgressHUD dismiss];
    
    if (self.submitting) {
        NSMutableArray *newViewControllerStack = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [newViewControllerStack removeObjectAtIndex:3];
        self.navigationController.viewControllers = newViewControllerStack;
        
        if ([self.form.status isEqual:@(FormStatusResubmit)]) {
            [[AppData sharedData] setFormStatus:FormStatusCompletedAfterResubmit forFormId:self.form.dispatchId];
        } else {
            [[AppData sharedData] setFormStatus:FormStatusComplete forFormId:self.form.dispatchId];
        }
        
        [self removeApprovalButton];
        return;
    }
}

-(void)removeApprovalButton {
    NSMutableArray *newToolbarItems = [self.toolbar.items mutableCopy];
    [newToolbarItems removeObject:[newToolbarItems lastObject]];
    self.toolbar.items = newToolbarItems;
}

- (IBAction)signToComplete:(id)sender {
    FormApprovalViewController *formApprovalViewController = [[FormApprovalViewController alloc] init];
    formApprovalViewController.delegate = self;

    UINavigationController *modalNavigationController = [[UINavigationController alloc] initWithRootViewController:formApprovalViewController];
    [self presentViewController:modalNavigationController animated:YES completion:nil];
}

-(void)signWithImage:(Image *)image clientName:(NSString *)clientName {
    [self dismissViewControllerAnimated:YES completion:^{
        NSMutableDictionary *extras = [[NSMutableDictionary alloc] init];
        [extras setObject:image forKey:@"clientSignature"];
        [extras setObject:clientName forKey:@"clientAuthorization"];
        FormSection *mainSection = self.formBuilder.sections[0];
        
        if (mainSection.extraData == nil) {
            mainSection.extraData = extras;
        } else {
            [mainSection.extraData addEntriesFromDictionary:extras];
        }
        
        self.submitting = YES;
        [self createPDF];
    }];
}

- (IBAction)email:(id)sender {
    [self openEmail];
}

-(void)openEmail {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
    
        [mailViewController setToRecipients:[self.formBuilder toEmails]];
        [mailViewController setCcRecipients:[self.formBuilder ccEmails]];
        [mailViewController setSubject:[NSString stringWithFormat:@"%@",[self.formBuilder.metadata objectForKey:@"displayName" ]]];
        
        NSData *pdfData = [NSData dataWithContentsOfFile:self.PDFPath];
        NSString *attachmentFilename = [NSString stringWithFormat:@"LIMS_%@.pdf", self.form.dispatchId];
        
        [mailViewController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:attachmentFilename];
        [self presentViewController:mailViewController animated:YES completion:nil];
    } else {
        
        [SVProgressHUD showErrorWithStatus:@"Email not available. Please configure an email account on your device"];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
	if (result == MFMailComposeResultFailed) {
		[SVProgressHUD showErrorWithStatus:@"Message not sent - check your internet connection"];
	}
	else if (result == MFMailComposeResultSent) {
        [SVProgressHUD showSuccessWithStatus:@"Message sent"];
	}
}

- (IBAction)submitDatabuttonPressed:(id)sender {
    [self submitData];
}

// Sumbits form data (hours and XML representation of input) to various webservices
-(void)submitData {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:@"Submitting hours..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    NSLog(@"%@", self.formBuilder);
    [self.submissionManager submitHours:self.formBuilder];
}

-(void)xmlSubmissionDidSucceed {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    [SVProgressHUD dismiss];
    [self openEmail];
}

-(void)hoursSubmissionDidSucceed {
    [SVProgressHUD showWithStatus:@"Submitting data..."];
    [self.submissionManager submitXml:self.formBuilder];
}

-(void)submissionDidFail:(NSError *)error {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    [SVProgressHUD showErrorWithStatus:@"Submission failed - check your internet connection"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

@end
