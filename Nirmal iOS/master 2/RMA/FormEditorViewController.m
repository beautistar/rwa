//
//  FormEditorViewController.m
//  RMA
//
//  Created by Michael Beteag on 7/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormEditorViewController.h"
#import "SmallTextInputViewController.h"
#import "RMASegmentedControl.h"
#import "Form+Extensions.h"
#import "SectionEnablerEditor.h"
#import "AdditionalRemarksEditor.h"
#import "NonDSAFooterEditor.h"

@interface FormEditorViewController ()
@property BOOL pagePopupMode;
@end

@implementation FormEditorViewController {
    id sizeDidChangeObserver;
    id sectionEnabledObserver;
    id sectionDisabledObserver;
    id reevaluateNonComplianceObserver;
    id setComplianceObserver;
    id setNonComplianceObserver;
    id otherPopupObserver;
    id editChecklistsObserver;
}

@synthesize popoverController;

- (void)viewDidLoad
{
    [super viewDidLoad];

    if([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
#ifdef DEBUG
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(preview)];
    NSMutableArray * toolbarItems = [self.toolbar.items mutableCopy];
    [toolbarItems addObject:nextButton];
    self.toolbar.items = toolbarItems;
#endif
    
    if(self.form == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"No form selected"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    self.tapGestureRecognizer.numberOfTapsRequired = 1;
    self.tapGestureRecognizer.numberOfTouchesRequired = 1;
    self.tapGestureRecognizer.delegate = self;
    [self.scrollViewContainer addGestureRecognizer:self.tapGestureRecognizer];
    
    BOOL isTimesheet = [self.form.dailyType isEqualToString:@"Timesheet"];
    if(isTimesheet) {
        self.clientReviewButton.title = @"Review";
    }
    
    // Create form file if it does not exist
    if(self.form.localFilename == nil ||
       ![[NSFileManager defaultManager] fileExistsAtPath:[[AppData sharedData] pathToDocumentNamed:self.form.localFilename]]) {
        
        FormBuilder *formBuilder = nil;
        BOOL isTimesheet = [self.form.dailyType isEqualToString:@"Timesheet"];
        if(isTimesheet) {
            
            formBuilder = [FormBuilder timesheetFormBuilderWithDate:self.form.startDate];
        } else {
            formBuilder = [FormBuilder formBuilderWithForm:self.form];
        }

        if(formBuilder != nil) {
            [formBuilder saveNewForm:self.form];
            self.shouldOpenInfo = YES;
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"This form type is not yet implemented"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    
    FormBuilder *builder = [FormBuilder formBuilderFromFile:[[AppData sharedData] pathToDocumentNamed:self.form.localFilename]];
    [builder addMetadataFromForm:self.form];
    [self loadFormBuilder:builder];
    self.scrollView.delegate = self;
}

-(void)setNonCompliance:(BOOL)nonCompliance {
    self.formBuilder.nonCompliance = nonCompliance;
    [self loadFormBuilder:self.formBuilder];
}

-(void)setSectionRequiringKey:(NSString*)requiredKey enabled:(BOOL)enabled {
    [self.formBuilder setSectionRequiringKey:requiredKey enabled:enabled];
    [self loadFormBuilder:self.formBuilder];
}

-(void)evaluateForcedNonCompliance {
    [self.formBuilder evaluateForcedNonCompliance];
    [self loadFormBuilder:self.formBuilder];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    UIImage *pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"DidPickPhoto" object:pickedImage]];
}

- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer {
    UIView* view = gestureRecognizer.view;
    CGPoint loc = [gestureRecognizer locationInView:view];
    UIView* subview = [view hitTest:loc withEvent:nil];
    
    if(![subview isKindOfClass:[UITextView class]] && ![subview isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIButton class]] || [touch.view isKindOfClass:[UISegmentedControl class]]) {
        return NO;
    }
    return YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:sizeDidChangeObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:sectionDisabledObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:sectionEnabledObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:reevaluateNonComplianceObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:setNonComplianceObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:setComplianceObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:otherPopupObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:editChecklistsObserver];
    
    if(self.popoverController != nil && self.popoverController.popoverVisible) {
        [self.popoverController dismissPopoverAnimated:YES];
    }
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(self.shouldOpenInfo) {
        self.shouldOpenInfo = NO;
        [self openInfoWindow];
    }
    
    if(![self.form isEditable]) {
        for(UIView* subview in self.scrollView.subviews) {
            subview.userInteractionEnabled = NO;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    
    sizeDidChangeObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"FormDidChangeSize"
                                                                              object:nil
                                                                               queue:nil
                                                                          usingBlock:^(NSNotification* notification){
                                                                              [weakSelf loadFormBuilder:weakSelf.formBuilder];
                                                                              int offsetYDelta = [notification.object intValue];
                                                                              
                                                                              int offsetFromBottom = weakSelf.scrollView.contentSize.height - (weakSelf.scrollView.contentOffset.y + weakSelf.scrollView.frame.size.height);
                                                                              offsetFromBottom = offsetFromBottom < 0 ? 0 : offsetFromBottom;
                                                                              int offsetFromTop = weakSelf.scrollView.contentOffset.y;
                                                                              int finalOffsetDelta = 0;
                                                                              if(offsetYDelta < 0) {
                                                                                  finalOffsetDelta = offsetFromTop > abs(offsetFromTop) ? offsetYDelta : -1 * offsetFromTop;
                                                                              } else {
                                                                                  finalOffsetDelta = offsetFromBottom > offsetYDelta ? offsetYDelta : offsetFromBottom;
                                                                              }
                                                                          }];

    reevaluateNonComplianceObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"ReevaluateForcedNonCompliance" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [weakSelf evaluateForcedNonCompliance];
    }];
    
    
    setNonComplianceObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"DidSetNonCompliance" object:nil
                                                                                  queue:nil usingBlock:^(NSNotification *note) {
                                                                                      [weakSelf setNonCompliance:YES];
                                                                                  }];
    
    setComplianceObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"DidSetCompliance" object:nil
                                                                               queue:nil usingBlock:^(NSNotification *note) {
                                                                                   [weakSelf setNonCompliance:NO];
                                                                               }];
    
    sectionEnabledObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"DidSetSectionEnabled" object:nil
                                                                                queue:nil usingBlock:^(NSNotification *note) {
                                                                                    [weakSelf setSectionRequiringKey:note.object enabled:YES];
                                                                                }];
    sectionDisabledObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"DidSetSectionDisabled" object:nil
                                                                                 queue:nil usingBlock:^(NSNotification *note) {
                                                                                     [weakSelf setSectionRequiringKey:note.object enabled:NO];
                                                                                 }];
    
    otherPopupObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"OpenOtherInput" object:nil queue:nil usingBlock:^(NSNotification *note) {
        UISegmentedControl *segmentedControl = note.userInfo[@"segmentedControl"];
        RMASegmentedControl *control = note.userInfo[@"control"];
        int otherIndex = [note.userInfo[@"otherIndex"] intValue];
        
        CGFloat segmentWidth = segmentedControl.frame.size.width / segmentedControl.numberOfSegments;
        CGRect rect = CGRectMake(otherIndex * segmentWidth, 0, segmentWidth, control.frame.size.height);
        
        
        SmallTextInputViewController *smallTextInputViewController = [[SmallTextInputViewController alloc] init];
        
        smallTextInputViewController.complete = ^(NSString* text) {
            [control setOtherValue:text];
            [weakSelf.popoverController dismissPopoverAnimated:YES];
        };
        smallTextInputViewController.dismissed = ^() {
            [control reset];
        };
        
        weakSelf.popoverController = [[UIPopoverController alloc] initWithContentViewController:smallTextInputViewController];
        [weakSelf.popoverController presentPopoverFromRect:rect inView:control permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }];
    
    editChecklistsObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"EditChecklists" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [weakSelf editChecklists];
    }];
}

-(void)loadFormBuilder:(FormBuilder*)formBuilder {
    self.formBuilder = formBuilder;
    
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.currentY = 0;
    
    if(self.editorPages == nil) {
        self.editorPages = [NSMutableArray array];
    } else {
        [self.editorPages removeAllObjects];
    }
    
    for(int i = 0; i < formBuilder.sections.count; i++) {
        FormSection *section = [formBuilder.sections objectAtIndex:i];
        for(int j = 0; j < section.editors.count; j++) {
            FormComponentEditor *editor = [section.editors objectAtIndex:j];
            
            if(!editor.isEnabled && ![editor isKindOfClass:[AdditionalRemarksEditor class]]) {
                continue;
            }
            
            if(editor.requiredTaskCodes != nil && ![editor.requiredTaskCodes containsObject:self.form.task]) {
                [editor setFormEnabled:NO];
                continue;
            }
            
            if([editor isKindOfClass:[SectionEnablerEditor class]]) {
                if([((SectionEnablerEditor*)editor).key isEqualToString:@"checklistsQuestion"] && [self.form getAvailableChecklists].count == 0) {
                    [editor setFormEnabled:NO];
                    continue;
                }
            }
            
            if([editor isKindOfClass:[NonDSAFooterEditor class]]) {
                NonDSAFooterEditor *footerEditor = (NonDSAFooterEditor*)editor;
                NSString *employeeType = [formBuilder.metadata objectForKey:@"employeeType"];
                [footerEditor setEmployeeType:employeeType];
            }
            
            [self addEditor:editor];
        }
    }
    
    [self resetPageButtons];
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.currentY);

}


-(void)resetPageButtons {
    if(self.editorPages == nil || self.editorPages.count ==0) {
        return;
    }
    
    self.pageSegmentedControl = [[UISegmentedControl alloc] init];
    self.pageSegmentedControl.apportionsSegmentWidthsByContent = YES;
    [self.pageSegmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    for (int i = 0; i < self.editorPages.count; i++) {
        FormEditorPage *page = [self.editorPages objectAtIndex:i];
        if(self.pageSegmentedControl.numberOfSegments > i) {
            [self.pageSegmentedControl setTitle:page.shortTitle forSegmentAtIndex:i];
        } else {
            [self.pageSegmentedControl insertSegmentWithTitle:page.shortTitle atIndex:self.pageSegmentedControl.numberOfSegments animated:YES];
        }
    }
    [self.pageSegmentedControl sizeToFit];
    for(int i = 0; i < self.pageSegmentedControl.numberOfSegments; i++) {
        [self.pageSegmentedControl setWidth:0 forSegmentAtIndex:i];
    }
    [self.pageSegmentedControl sizeToFit];
    
    CGFloat maxWidth = 560;
    self.pagePopupMode = self.pageSegmentedControl.frame.size.width > maxWidth;
    if(self.pagePopupMode) {
        self.pageBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Jump to Page" style:UIBarButtonItemStylePlain target:self action:@selector(pageButtonPressed)];
    } else {
        self.pageBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.pageSegmentedControl];
        [self setPageControlToCurrentPage];
    }
    NSMutableArray *toolbarItems = [self.toolbar.items mutableCopy];
    toolbarItems[2] = self.pageBarButtonItem;
    self.toolbar.items = toolbarItems;
    [self.toolbar setNeedsDisplay];
}

-(void)pageButtonPressed {
    if(self.popoverController != nil && self.popoverController.popoverVisible) {
        [self.popoverController dismissPopoverAnimated:YES];
    }
    PageJumpTableViewController *pageJumpTableViewController = [[PageJumpTableViewController alloc] init];
    pageJumpTableViewController.delegate = self;
    pageJumpTableViewController.editorPages = self.editorPages;
   
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:pageJumpTableViewController];
    [self.popoverController presentPopoverFromBarButtonItem:self.pageBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

    [pageJumpTableViewController selectPage:[self getCurrentPage]];
}

-(void)addEditor:(FormComponentEditor*)editor {
    if(editor.view != nil) {
        if([editor isKindOfClass:[FormDivider class]]) {
            FormEditorPage *page = [[FormEditorPage alloc] init];
            FormDivider* divider = (FormDivider*)editor;
            page.shortTitle = divider.shortTitle.length > 0 ? divider.shortTitle : divider.titleLabel.text;
            page.title = divider.titleLabel.text;
            page.frame = CGRectMake(0,
                                    self.currentY,
                                    self.scrollView.contentSize.width, 0);
            [self.editorPages addObject:page];
        } else if (self.editorPages.count > 0) {
            FormEditorPage *currentPage = [self.editorPages lastObject];
            currentPage.frame = CGRectMake(currentPage.frame.origin.x,
                                           currentPage.frame.origin.y,
                                           currentPage.frame.size.width,
                                           currentPage.frame.size.height + editor.view.frame.size.height);
        }
        
        editor.view.frame = CGRectMake(0, self.currentY,
                                       editor.view.frame.size.width,
                                       editor.view.frame.size.height);
        [self.scrollView addSubview:editor.view];
        self.currentY += editor.view.frame.size.height;
     
    } else {
        NSLog(@"Editor view is nil");
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;{
    if (buttonIndex == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

-(BOOL)formValidates {
    NSMutableArray *validationErrors = [NSMutableArray array];
    for(FormSection *section in self.formBuilder.sections) {
        for(FormComponentEditor *editor in section.editors) {
            NSMutableArray *componentErrors;
            
            if(![editor validates:&componentErrors referenceView:self.scrollView]) {
                if(componentErrors != nil) {
                    [validationErrors addObjectsFromArray:componentErrors];
                }
            }
        }
    }
    
    if(validationErrors.count > 0) {
        FormValidationError *error = [validationErrors objectAtIndex:0];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [self.scrollView scrollRectToVisible:CGRectMake(error.scrollPoint.x, error.scrollPoint.y, 100, 100) animated:YES];
    }
    
    return validationErrors.count == 0;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(!self.pagePopupMode) {
        [self setPageControlToCurrentPage];
    }
}

-(int)getCurrentPage {
    int currentPage = 0;
    for(int i = 0; i < self.editorPages.count; i++) {
        FormEditorPage *page = [self.editorPages objectAtIndex:i];
        if(self.scrollView.contentOffset.y >= page.frame.origin.y) {
            currentPage = i;
        }
    }
    return currentPage;
}

-(void)setPageControlToCurrentPage {
    self.pageSegmentedControl.selectedSegmentIndex = [self getCurrentPage];
}


- (IBAction)segmentedControlValueChanged:(id)sender {
    UISegmentedControl *control = (UISegmentedControl*)sender;
    if(self.editorPages == nil) {
        return;
    }
    
    if([self.editorPages objectAtIndex:control.selectedSegmentIndex] == nil) {
        return;
    } else {
        [self jumpToPage:(int)control.selectedSegmentIndex];
    }
}

-(void)jumpToPage:(int)pageNumber {
    FormEditorPage *page = [self.editorPages objectAtIndex:pageNumber];
    [UIView animateWithDuration:0.5 animations:^(void) {
        CGFloat y = MIN(self.scrollView.contentSize.height - self.scrollView.frame.size.height, page.frame.origin.y);
        
        self.scrollView.contentOffset = CGPointMake(0, y);
        
    }];
}

-(void)preview {
    [SVProgressHUD showWithStatus:@"Loading preview.."];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        FormPreviewViewController *formPreviewController = [[FormPreviewViewController alloc] init];
        formPreviewController.formBuilder = self.formBuilder;
        formPreviewController.form = self.form;
        formPreviewController.disableApproval = [self.form.dailyType isEqualToString:@"Timesheet"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self.navigationController pushViewController:formPreviewController animated:YES];
            
            
        });
    });
}

- (IBAction)previewAndSubmit:(id)sender {
    if([self formValidates]) {
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showWithStatus:@"Saving..."];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self.formBuilder saveToFile:[[AppData sharedData] pathToDocumentNamed:self.form.localFilename]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self preview];
            });
        });        
    }
    
    
    
}

- (IBAction)openInfo:(id)sender {
    [self openInfoWindow];
}

-(void)openInfoWindow {
    if([self.popoverController isPopoverVisible]) {
        [self.popoverController dismissPopoverAnimated:YES];
        return;
    }
    
    FormInfoViewController *formInfoViewController = [[FormInfoViewController alloc] init];
    formInfoViewController.form = self.form;
    
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:formInfoViewController];
    [self.popoverController presentPopoverFromBarButtonItem:self.infoButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}

- (IBAction)save:(id)sender {
    [self save];
}

-(void)save {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:@"Saving..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.formBuilder saveToFile:[[AppData sharedData] pathToDocumentNamed:self.form.localFilename]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showSuccessWithStatus:@"Form saved"];
        });
    });
}

-(void)editChecklists {
    ChecklistSelectionViewController *checklistSelectionViewController = [[ChecklistSelectionViewController alloc] init];
    checklistSelectionViewController.formBuilder = self.formBuilder;
    checklistSelectionViewController.delegate = self;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:checklistSelectionViewController];
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:navigationController animated:YES completion:nil];
}

-(void)didUpdateChecklists {
    [self loadFormBuilder:self.formBuilder];
}

-(NSArray*)getAvailableChecklists {
    return [self.form getAvailableChecklists];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
