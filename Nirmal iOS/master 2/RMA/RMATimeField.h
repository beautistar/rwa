//
//  RMATimeField.h
//  RMA
//
//  Created by Alexander Roode on 12/18/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMATimeField : UITextField
@property (nonatomic) BOOL military;
@end
