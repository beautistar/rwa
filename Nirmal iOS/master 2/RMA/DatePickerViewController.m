//
//  DatePickerViewController.m
//  RMA
//
//  Created by Alexander Roode on 1/21/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "DatePickerViewController.h"

@interface DatePickerViewController ()
@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic) CGSize size;
@end

@implementation DatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.datePicker.datePickerMode = self.mode;
    
    self.formatter = [[NSDateFormatter alloc] init];
    if(self.mode == UIDatePickerModeTime) {
        self.formatter.dateFormat = @"h:mm a";
        self.size = CGSizeMake(250, 210);
    } else {
        self.formatter.dateFormat = @"M/d/yyyy";
        self.size = CGSizeMake(320, 210 );
    }
    
    if(self.locale != nil) {
        self.datePicker.locale = self.locale;
        self.formatter.dateFormat = @"HH:mm";
    }
}

- (CGSize)preferredContentSize {
    return self.size;
}

- (IBAction)setButtonPressed:(id)sender {
    NSString *dateString = [self.formatter stringFromDate:self.datePicker.date];
    
    [self.delegate didEnterDate:dateString];
}

-(void)setDateFromString:(NSString*)dateString {
    self.datePicker.date = [self.formatter dateFromString:dateString];
}

@end
