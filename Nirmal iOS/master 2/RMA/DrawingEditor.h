//
//  DrawingEditor.h
//  RMA
//
//  Created by Michael Beteag on 8/1/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"
#import "NICSignatureView.h"
#import "Image.h"
#import <QuartzCore/QuartzCore.h>


@interface DrawingEditor : FormComponentEditor

@property (nonatomic, retain) NICSignatureView *imageView;
@property (nonatomic, retain) UILabel *imageViewLabel;
@property (nonatomic, retain) UIButton *clearButton;
@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *name;

- (id)initWithFrame:(CGRect)frame name:(NSString*)name key:(NSString*)key;

-(NSMutableDictionary*)getData;
-(void)setData:(NSMutableDictionary*)data;

@end
