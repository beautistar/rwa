//
//  PageJumpTableViewController.h
//  RMA
//
//  Created by Alexander Roode on 1/29/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PageJumpDelegate <NSObject>
-(void)jumpToPage:(int)pageNumber;
@end

@interface PageJumpTableViewController : UITableViewController
@property (nonatomic, strong) NSArray *editorPages;
@property (nonatomic, weak) id<PageJumpDelegate> delegate;
-(void)selectPage:(int)page;
@end
