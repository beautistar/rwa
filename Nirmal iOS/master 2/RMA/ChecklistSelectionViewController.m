//
//  ChecklistSelectionViewController.m
//  RMA
//
//  Created by Alexander Roode on 1/19/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "ChecklistSelectionViewController.h"
#import "FormBuilder.h"
#import "Checklist.h"
#import "RMATableViewCell.h"

@interface ChecklistSelectionViewController ()

@end

@implementation ChecklistSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Checklists";
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    self.navigationItem.leftBarButtonItem = closeButton;
    
    UIBarButtonItem *deleteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(delete)];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add)];
    self.navigationItem.rightBarButtonItems = @[addButton, deleteButton];
    
    [self.tableView registerClass:RMATableViewCell.class forCellReuseIdentifier:@"Cell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)add {
    AddChecklistViewController *addChecklistViewController = [[AddChecklistViewController alloc] init];
    addChecklistViewController.delegate = self;
    addChecklistViewController.checklistOptions = [self.delegate getAvailableChecklists];
    [self.navigationController pushViewController:addChecklistViewController animated:YES];
}

-(void)delete {
    [self.tableView setEditing:!self.tableView.isEditing animated:YES];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.formBuilder.checklists.count;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Checklist *checklist = self.formBuilder.checklists[indexPath.row];
    cell.textLabel.text = checklist.name;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        [self.formBuilder removeChecklistAtIndex:(int)indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.delegate didUpdateChecklists];
    }
}

-(void)addChecklist:(Checklist *)checklist {
    [self.formBuilder addChecklist:checklist];
    [self.tableView reloadData];
    [self.delegate didUpdateChecklists];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
