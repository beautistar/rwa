//
//  LoginManager.m
//  RMA
//
//  Created by Michael Beteag on 4/14/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "LoginManager.h"
#import "UpdateRequest.h"
#import "SSKeychain/SSKeychain.h"
#import "Updater.h"

#define kLastLoggedInUsernameKey @"LastLoggedInUsername"

@implementation LoginManager {
    id successObserver;
    id failureObserver;
}

+(LoginManager*)loginManagerWithUsername:(NSString*)username password:(NSString*)password {
    LoginManager *manager = [[LoginManager alloc] init];
    manager.username = username;
    manager.password = password;
    return manager;
}

-(void)login {
    
    NSString *lastLoggedInUsername = [LoginManager lastLoggedInUsername];
    NSString *password = [SSKeychain passwordForService:kServiceName account:lastLoggedInUsername];
    
    if(lastLoggedInUsername.length > 0 && password.length > 0 && [lastLoggedInUsername isEqualToString:self.username]
       && [password isEqualToString:self.password]) {
        [LoginManager setLastLoggedInUsername:self.username];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginDidSucceed" object:nil];
        return;
    }

    UpdateRequest *request = [UpdateRequest updateRequestWithUsername:self.username password:self.password];
    [request begin];
    
    __weak typeof(self) weakSelf = self;
    
    successObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"UpdateRequestDidSucceed" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [LoginManager setLastLoggedInUsername:weakSelf.username];
        [SSKeychain setPassword:weakSelf.password forService:kServiceName account:weakSelf.username];
        [Updater setLastUpdatedDateForCurrentUser];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginDidSucceed" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:successObserver];
        [[NSNotificationCenter defaultCenter] removeObserver:failureObserver];
    }];
    
    failureObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"UpdateRequestDidFail" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginDidFail" object:note.object];
        [[NSNotificationCenter defaultCenter] removeObserver:successObserver];
        [[NSNotificationCenter defaultCenter] removeObserver:failureObserver];
    }];
}

+(NSString*)lastLoggedInUsername {
    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:kLastLoggedInUsernameKey];
    return username;
}

+(void)setLastLoggedInUsername:(NSString*)username {
    [[NSUserDefaults standardUserDefaults] setObject:username forKey:kLastLoggedInUsernameKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
