//
//  BreadcrumbCell.h
//  RMA
//
//  Created by Michael Beteag on 12/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BreadcrumbCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
