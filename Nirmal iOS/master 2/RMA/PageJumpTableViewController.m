//
//  PageJumpTableViewController.m
//  RMA
//
//  Created by Alexander Roode on 1/29/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "PageJumpTableViewController.h"
#import "FormEditorPage.h"
#import "RMATableViewCell.h"

@interface PageJumpTableViewController ()
@end

@implementation PageJumpTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[RMATableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.clearsSelectionOnViewWillAppear = NO;
}

- (CGSize)preferredContentSize {
    return CGSizeMake(300, 400);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.editorPages.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    FormEditorPage *page = self.editorPages[indexPath.row];
    cell.textLabel.text = page.title;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate jumpToPage:(int)indexPath.row];
}

-(void)selectPage:(int)page {
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:page inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
}
@end
