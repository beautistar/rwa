//
//  DSAFooterTechnicianEditor.m
//  RMA
//
//  Created by Michael Beteag on 4/24/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "DSAFooterTechnicianEditor.h"

@implementation DSAFooterTechnicianEditor


+(DSAFooterTechnicianEditor*)DSAFooterTechnicianEditorWithKey:(NSString *)key {
    DSAFooterTechnicianEditor *editor = [[DSAFooterTechnicianEditor alloc] initWithNibName:@"DSAFooterTechnicianEditor"];
    editor.validationType = validationTypeAll;
    editor.key = key;
    return editor;
    
}

-(BOOL)forcesNonCompliance {
    BOOL nonCompliance = NO;
    
    if(
       self.workMeetsRequirements.selectedSegmentIndex == 1 ||
       self.samplingInAccordance.selectedSegmentIndex == 1) {
        nonCompliance = YES;
    }
    return nonCompliance;
}

-(NSMutableDictionary*)getData {
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    RadioCheckboxSet *workMeetsRequirements = [RadioCheckboxSet radioCheckboxSetFromUISegmentedControl:self.workMeetsRequirements];
    RadioCheckboxSet *samlingInAccordance = [RadioCheckboxSet radioCheckboxSetFromUISegmentedControl:self.samplingInAccordance];
    
    [data setObject:workMeetsRequirements forKey:@"workMeetsRequirements"];
    [data setObject:samlingInAccordance forKey:@"samplingInAccordance"];
    
    return data;
}

-(void)setData:(NSMutableDictionary*)data {
    RadioCheckboxSet *workMeetsRequirements = [data objectForKey:@"workMeetsRequirements"];
    RadioCheckboxSet *samlingInAccordance = [data objectForKey:@"samplingInAccordance"];
    
    self.workMeetsRequirements.selectedSegmentIndex = [workMeetsRequirements.selectedIndex intValue];
    self.samplingInAccordance.selectedSegmentIndex = [samlingInAccordance.selectedIndex intValue];
}

- (IBAction)nonComplianceQuestion1Changed:(id)sender {
    [self reevaluateForcedNonComplaince];
}

- (IBAction)nonComplianceQuestion2Changed:(id)sender {
    [self reevaluateForcedNonComplaince];
}

- (IBAction)nonComplianceQuestion3Changed:(id)sender {
    [self reevaluateForcedNonComplaince];
}

-(NSDictionary*)dictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    dictionary[@"workMeetsRequirements"] = self.workMeetsRequirements.selectedSegmentIndex == 0 ? @"yes": self.workMeetsRequirements.selectedSegmentIndex == 1 ? @(NO) : @"N/A";
    dictionary[@"samplingInAccordance"] = self.samplingInAccordance.selectedSegmentIndex == 0 ? @"yes" : @"no";
    
    return dictionary;
}
@end
