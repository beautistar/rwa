//
//  ConcretePageSet.h
//  RMA
//
//  Created by Michael Beteag on 7/31/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"
#import "Paragraph.h"
#import "BreakpointsCollection.h"

@interface ConcretePageSet : NSObject <NSCoding, GRMustacheRendering>

@property (nonatomic, retain) NSString *sampleType;
@property (nonatomic, retain) NSString *setNo;
@property (nonatomic, retain) NSString *mixDesignNo;
@property (nonatomic, retain) NSString *mixDesignStrength;
@property (nonatomic, retain) NSString *admixtures;
@property (nonatomic, retain) NSString *noOfSamples;
@property (nonatomic, retain) NSString *tripTicketNo;
@property (nonatomic, retain) NSString *slumpPen;
@property (nonatomic, retain) NSString *mtlTempCF;
@property (nonatomic, retain) NSString *airTempCF;
@property (nonatomic, retain) NSString *airContent;
@property (nonatomic, retain) NSString *unitWt;
@property (nonatomic, retain) Paragraph *placedAt;
@property (nonatomic, retain) BreakpointsCollection *breaks;
@property (nonatomic, retain) NSString *designStrengthUnits;
@property (nonatomic, retain) NSString *mtlTempUnits;
@property (nonatomic, retain) NSString *airTempUnits;
@property (nonatomic, retain) NSString *timeCast;

+(ConcretePageSet*)concretePageSetFromDictionary:(NSMutableDictionary*)data;


-(NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                          context:(GRMustacheContext *)context
                         HTMLSafe:(BOOL *)HTMLSafe
                            error:(NSError **)error;

@end
