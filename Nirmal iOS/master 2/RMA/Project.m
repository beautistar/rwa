//
//  Project.m
//  RMA
//
//  Created by Michael Beteag on 9/25/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "Project.h"
#import "Client.h"
#import "Form.h"


@implementation Project

@dynamic id;
@dynamic client;
@dynamic forms;
@dynamic longName;

@end
