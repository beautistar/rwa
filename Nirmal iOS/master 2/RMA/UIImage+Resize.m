//
//  UIImage+Resize.m
//  RMA
//
//  Created by Michael Beteag on 9/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "UIImage+Resize.h"

@implementation UIImage (Resize)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToMaxSize:(CGSize)maxSize {
    CGSize size = CGSizeMake(image.size.width, image.size.height);
    float aspectRatio = size.width / size.height;
    
    if(size.width > maxSize.width) {
        size = CGSizeMake(maxSize.width, maxSize.width / aspectRatio);
    }
    if(size.height > maxSize.height) {
        float scaleFactor = maxSize.height / size.height;
        size = CGSizeMake(scaleFactor * size.width, scaleFactor * size.height);
    }
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
