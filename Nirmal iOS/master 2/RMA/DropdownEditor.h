//
//  DropdownEditor.h
//  RMA
//
//  Created by Alexander Roode on 3/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"
#import "SelectionTableViewController.h"

@interface DropdownEditor : FormComponentViewEditor <SelectionTableViewCellDelegate>
@property (nonatomic, strong) IBOutlet UIButton *button;
@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) IBOutlet UILabel *valueLabel;

-(IBAction)buttonPressed;
+(instancetype)dropdownEditorWithKey:(NSString*)key text:(NSString*)text options:(NSArray<NSString*> *)options;
@end
