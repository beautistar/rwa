//
//  InfoCell.m
//  RMA
//
//  Created by Alexander Roode on 2/17/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "InfoCell.h"
#import "NSString+Additions.h"

@implementation InfoCell

+(CGFloat)heightWithDetailText:(NSString*)detailText width:(CGFloat)width {
    CGFloat height = 16;
    CGFloat widthMinusPadding = width - 100 - 31;
    height += [detailText heightWithFont:[UIFont systemFontOfSize:17.0f] width:widthMinusPadding];
    return height;
}

-(void)prepareForReuse {
    self.button.enabled = NO;
    self.button.hidden = YES;
}

@end
