//
//  Paragraph.m
//  RMA
//
//  Created by Michael Beteag on 7/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "Paragraph.h"

@implementation Paragraph

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        [self setText:[aDecoder decodeObjectForKey:@"text"]];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.text   forKey:@"text"];
}

+(Paragraph*)paragraphWithText:(NSString*)text {
    Paragraph *paragraph = [[Paragraph alloc] init];
    paragraph.text = text;
    return paragraph;
}

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    
    //NSString *text = [tag renderContentWithContext:context HTMLSafe:HTMLSafe error:error];
    NSString *text = [self.text stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    *HTMLSafe = YES;
    return text;
}
@end
