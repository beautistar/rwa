//
//  FormApprovalViewController.h
//  RMA
//
//  Created by Michael Beteag on 10/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NICSignatureView.h"
#import "Image.h"
#import "RMATextField.h"

@protocol FormApprovalViewControllerDelegate
-(void)signWithImage:(Image*)image clientName:(NSString*)clientName;

@end

@interface FormApprovalViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet NICSignatureView *signatureView;
@property (weak, nonatomic) IBOutlet RMATextField *clientNameField;
@property (nonatomic, assign) id<FormApprovalViewControllerDelegate> delegate;

- (IBAction)sign:(id)sender;
- (IBAction)clear:(id)sender;

@end
