//
//  CheckedCheckbox.h
//  RMA
//
//  Created by Alexander Roode on 2/17/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"

@interface CheckedCheckbox : NSObject<GRMustacheRendering>

@end
