//
//  NonDSAFooterEditor.h
//  RMA
//
//  Created by Michael Beteag on 10/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"
#import "SignatureEditor.h"
#import "FormComponentViewEditor.h"


@interface NonDSAFooterEditor : FormComponentEditor

@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSArray *editors;
@property (nonatomic, retain) FormComponentViewEditor *mainEditor;

+(NonDSAFooterEditor*)nonDSAFooterEditorWithKey:(NSString*)key;
-(void)setEmployeeType:(NSString*)employeeType;

@end
