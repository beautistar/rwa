//
//  Checklist.h
//  RMA
//
//  Created by Alexander Roode on 1/19/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Checklist : NSObject
@property (nonatomic, strong) NSString *name;
@property int id;
@property (nonatomic, strong) NSArray *sectionNames;

+(instancetype)checklistWithId:(int)id name:(NSString*)name sectionNames:(NSArray*)sectionNames;
+(NSArray*)allChecklists;
+(Checklist*)checklistWithId:(int)id;

@end
