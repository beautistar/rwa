//
//  SampleCollectionEditor.h
//  RMA
//
//  Created by Michael Beteag on 7/1/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "CollectionEditor.h"

@interface SampleCollectionEditor : CollectionEditor

+(SampleCollectionEditor*)sampleCollectionEditor;
-(void)setData:(NSMutableDictionary *)data;
-(NSMutableDictionary*)getData;
-(NSMutableArray<NSNumber*>*)sampleTypeIndexes;

@end
