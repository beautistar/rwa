//
//  UpdateRequest.h
//  RMA
//
//  Created by Michael Beteag on 11/20/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface UpdateRequest : NSObject

@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSString *viewState;
@property (nonatomic, retain) NSString *eventValidation;
@property (nonatomic, retain) NSOperationQueue *queue;
@property (nonatomic, retain) AFHTTPClient *client;

+(UpdateRequest*)updateRequestWithUsername:(NSString*)username password:(NSString*)password;
-(void)begin;
    
@end
