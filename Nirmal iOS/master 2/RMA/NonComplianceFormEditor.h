//
//  NonComplianceFormEditor.h
//  RMA
//
//  Created by Michael Beteag on 9/24/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "NonComplianceFormEditor.h"
#import "FormComponentEditor.h"

@class TextAreaEditor;

@interface NonComplianceFormEditor : FormComponentEditor

@property (nonatomic, retain) TextAreaEditor *remarksEditor;
@property (nonatomic, retain) NSString *key;

+(NonComplianceFormEditor*)nonComplianceFormEditorWithKey:(NSString*)key;

@end
