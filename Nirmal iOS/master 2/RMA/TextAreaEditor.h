//
//  TextAreaEditor.h
//  RMA
//
//  Created by Michael Beteag on 7/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"
#import <QuartzCore/QuartzCore.h>
#import "Paragraph.h"

@class RMATextView;

@interface TextAreaEditor : FormComponentEditor

@property (nonatomic, retain) UILabel *textAreaLabel;
@property (nonatomic, retain) RMATextView *textArea;
@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *name;

- (id)initWithFrame:(CGRect)frame name:(NSString*)name key:(NSString*)key;

-(NSMutableDictionary*)getData;
-(void)setData:(NSMutableDictionary*)data;


@end
