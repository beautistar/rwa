//
//  ProjectDashboardPhotoCell.m
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "ProjectDashboardPhotoCell.h"

@implementation ProjectDashboardPhotoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] objectAtIndex:0];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
