//
//  FormSelectionViewController.h
//  RMA
//
//  Created by Michael Beteag on 7/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormEditorViewController.h"
#import "FormCell.h"
#import "Form.h"
#import "Project.h"
#import "Client.h"
#import "AppData.h"
#import "RescheduleViewController.h"

@interface FormSelectionViewController : UITableViewController <NSFetchedResultsControllerDelegate, FormCellDelegate, RescheduleViewControllerDelegate>
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSString *technicianId;
@property (nonatomic, retain) NSString *projectId;
@property (nonatomic, retain) NSArray *forms;
@property (nonatomic, retain) UIRefreshControl *refreshIndicator;
@property BOOL incompleteOnly;

@end
