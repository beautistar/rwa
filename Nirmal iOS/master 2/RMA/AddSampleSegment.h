#import "FormComponentViewEditor.h"

@interface AddSampleSegment : FormComponentViewEditor <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) FormComponentEditor *editor;
@property int currentSampleType;
@property int currentSampleIndex;

@property (weak, nonatomic) IBOutlet UISegmentedControl *typeControl;
- (IBAction)sampleTypeChanged:(id)sender;
+(AddSampleSegment*)standardSampleEditor;

@end
