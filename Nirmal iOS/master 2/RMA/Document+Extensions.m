//
//  Document+Extensions.m
//  RMA
//
//  Created by Alexander Roode on 1/8/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "Document+Extensions.h"

@implementation Document (Extensions)
-(NSString*)getAbsolutePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    return [documentsPath stringByAppendingPathComponent:self.path];
}
@end
