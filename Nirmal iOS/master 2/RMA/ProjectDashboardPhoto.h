//
//  ProjectDashboardPhoto.h
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProjectDashboardPhoto : NSObject

@property (nonatomic, retain) UIImage *image;
+(ProjectDashboardPhoto*)photoWithImage:(UIImage*)image;

@end
