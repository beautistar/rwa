//
//  FormBuilder.m
//  RMA
//
//  Created by Michael Beteag on 7/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormBuilder.h"
#import "FormComponentViewEditor.h"
#import "NSDate+Extensions.h"
#import "Form+Extensions.h"
#import "Checklist.h"
#import "AppData.h"
#import "SampleCollectionEditor.h"
#import "NonComplianceFormEditor.h"
#import "NonComplianceQuestionEditor.h"
#import "FormSection+TimeSheet.h"

@implementation FormBuilder

+(FormBuilder*)timesheetFormBuilderWithDate:(NSDate*)date {
    FormSection *timesheet = [FormSection timeSheetFormSectionWithStartDate:date];
    [FormSection formSectionWithName:@"Time Sheet"];
    NSMutableArray *sections = [NSMutableArray arrayWithObjects:timesheet, nil];
    FormBuilder *formBuilder = [self formBuilderWithSections:sections];
    
    return formBuilder;
}

+(FormBuilder*)formBuilderWithForm:(Form*)form {
    NSMutableArray *sections = [NSMutableArray array];
    
    if([form.dsa boolValue]) {
        if([form.dailyType isEqualToString:@"Daily Technician"]) {
            [sections addObject:[FormSection formSectionWithName:@"DSA Daily Technician Report"]];
        } else if([form.dailyType isEqualToString:@"Daily Inspection"]) {
            [sections addObject:[FormSection formSectionWithName:@"DSA Daily Inspection Report"]];
        }
    } else {
        if([form.dailyType isEqualToString:@"Daily Technician"]) {
            [sections addObject:[FormSection formSectionWithName:@"Daily Technician Report"]];
        } else if([form.dailyType isEqualToString:@"Daily Inspection"]) {
            [sections addObject:[FormSection formSectionWithName:@"Daily Inspection Report"]];
        }
    }

    [sections addObject:[FormSection formSectionWithName:@"Empty"]];

#ifndef SITESCAN
    [sections addObject:[FormSection formSectionWithName:@"Standard Samples"]];
    [sections addObject:[FormSection formSectionWithName:@"NonCompliance"]];
#endif
    
    [sections addObject:[FormSection formSectionWithName:@"Checklists"]];
    [sections addObject:[FormSection formSectionWithName:@"Photos"]];
    [sections addObject:[FormSection formSectionWithName:@"MasterForms"]];
    [sections addObject:[FormSection formSectionWithName:@"AddMoreSamples"]];
    
    FormBuilder *formBuilder = [FormBuilder formBuilderWithSections:sections];
    [formBuilder addMetadataFromForm:form];
    
    return formBuilder;
}

+(FormBuilder*)formBuilderWithSections:(NSMutableArray*)sections {
    FormBuilder *formBuilder = [[FormBuilder alloc] init];
    formBuilder.sections = sections;
    formBuilder.metadata = [NSMutableDictionary dictionary];
    formBuilder.sectionEnablerKeys = [NSMutableArray array];
    formBuilder.checklists = [NSMutableArray array];
    
    return formBuilder;
}

-(void)addMetadataFromForm:(Form *)form {
    
    if(form.displayName != nil) {
        [self.metadata setObject:form.displayName forKey:@"displayName"];
    }
    
    if(form.project.longName != nil) {
        [self.metadata setObject:form.project.longName forKey:@"project"];
    }
    
    if(form.dispatchId != nil) {
        [self.metadata setObject:form.dispatchId forKey:@"drNumber"];
    }
    
    if(form.project.id != nil) {
        [self.metadata setObject:form.project.id forKey:@"projectId"];
    }
    
    if(form.scope != nil) {
        [self.metadata setObject:form.scope forKey:@"scope"];
    }
    
    if(form.contact.phone != nil) {
        [self.metadata setObject:form.contact.phone forKey:@"phoneNumber"];
    }
    
    if(form.contact.cell != nil) {
        [self.metadata setObject:form.contact.cell forKey:@"cellNumber"];
    }
    
    if(form.contact.name != nil) {
        [self.metadata setObject:form.contact.name forKey:@"requestBy"];
    }
    
    if(form.project.client.name != nil) {
        [self.metadata setObject:form.project.client.name forKey:@"clientName"];
    }
    if(form.address.address1 != nil) {
        NSString *address = form.address.address1;
        if(form.address.address2 != nil) {
            address = [address stringByAppendingString:form.address.address2];
        }
        [self.metadata setObject:address forKey:@"address"];
    }
    if(form.address.city != nil) {
        NSString *cityState = form.address.city;
        if(form.address.address2 != nil) {
            cityState = [NSString stringWithFormat:@"%@, %@", form.address.city,
                         form.address.state];
        }
        [self.metadata setObject:cityState forKey:@"cityState"];
    }
    
    [self.metadata setObject:[form sectionIdentifier] forKey:@"requestDate"];
    [self.metadata setObject:[form shortDate] forKey:@"shortRequestDate"];
    

    if(form.startTime != nil) {
        [self.metadata setObject:[form.startTime timeString] forKey:@"requestTime"];
    }
    
    if(form.remarks != nil) {
        [self.metadata setObject:form.remarks forKey:@"remarks"];
    }
    if(form.task != nil) {
        [self.metadata setObject:form.task forKey:@"task"];
    }
    
    if(form.taskDescription != nil) {
        [self.metadata setObject:form.task forKey:@"taskDescription"];
    }

    if(form.task != nil && form.taskDescription != nil) {
        [self.metadata setObject:[NSString stringWithFormat:@"%@, %@",form.task,form.taskDescription] forKey:@"fullTask"];
    } else if (form.task != nil) {
        [self.metadata setObject:form.task forKey:@"fullTask"];
    } else if (form.taskDescription != nil) {
        [self.metadata setObject:form.taskDescription forKey:@"fullTask"];
    }
    
    [self.metadata setObject:form.technician.id forKey:@"employeeId"];
    
    if(form.technician.name != nil) {
        [self.metadata setObject:form.technician.name forKey:@"employeeName"];
    }
    
    if(form.dsaAppNumber != nil) {
        [self.metadata setObject:form.dsaAppNumber forKey:@"dsaApplNo"];
    }
    
    if(form.dsaLEANumber != nil) {
        [self.metadata setObject:form.dsaLEANumber forKey:@"dsaLEANo"];
    }
    
    if(form.dsaFileNumber != nil) {
        [self.metadata setObject:form.dsaFileNumber forKey:@"dsaFileNo"];
    }
    
    if([form.dailyType isEqualToString:@"Daily Technician"]) {
        [self.metadata setObject:@"Technician" forKey:@"employeeType"];
    }
    
    if([form.dailyType isEqualToString:@"Daily Inspection"]) {
        [self.metadata setObject:@"Inspector" forKey:@"employeeType"];
    }
    
    if(form.customerEmails != nil) {
        [self.metadata setObject:form.customerEmails forKey:@"customerEmails"];
    }
}

-(void)saveNewForm:(Form*)form {
    
    // Create file for form if does not eist
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    
    NSString *filename;
    int i = 0;
    
    do {
        filename = [NSString stringWithFormat:@"%@-%@-%@-%i.rma",
                    form.technician.id,
                    [dateFormatter stringFromDate:form.startDate],
                    form.shortDisplayName,
                    i];
        i++;
    } while([[NSFileManager defaultManager] fileExistsAtPath:[[AppData sharedData] pathToDocumentNamed:filename]]);
    
    [self saveToFile:[[AppData sharedData] pathToDocumentNamed:filename]];
    
    // Update file object in DB
    form.localFilename = filename;
    form.status = @(FormStatusIncomplete);
    
    NSError *error;
    if (![[[AppData sharedData] managedObjectContext] save:&error]) {
        NSLog(@"Error adding form: %@", [error localizedDescription]);
    }
}

-(void)saveToFile:(NSString*)path {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setObject:self.metadata forKey:@"metadata"];
    [data setObject:self.sectionEnablerKeys forKey:@"sectionEnablerKeys"];
    [data setObject:[NSNumber numberWithBool:self.nonCompliance] forKey:@"nonCompliance"];
    [data setObject:[NSNumber numberWithBool:self.forcesNonCompliance] forKey:@"forcesNonCompliance"];
    [data setObject:self.sections forKey:@"formSections"];
    
    NSMutableArray *checklistIds = [NSMutableArray array];
    for(Checklist *checklist in self.checklists) {
        [checklistIds addObject:@(checklist.id)];
    }
    [data setObject:checklistIds forKey:@"checklistIds"];
    
    for(FormSection *section in self.sections) {
        section.data = [section getDataForSaving];
    }
    
    [NSKeyedArchiver archiveRootObject:data toFile:path];
}

+(FormBuilder*)formBuilderFromFile:(NSString*)path {
    NSMutableDictionary *fileData = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    FormBuilder *formBuilder = [[FormBuilder alloc] init];
    
    formBuilder.sectionEnablerKeys = [fileData objectForKey:@"sectionEnablerKeys"];

    NSMutableDictionary *metadata = [fileData objectForKey:@"metadata"];
    formBuilder.metadata = metadata;
    
    NSMutableArray *sections = [fileData objectForKey:@"formSections"];
    [formBuilder setupSections:sections];
    
    formBuilder.nonCompliance = [[fileData objectForKey:@"nonCompliance"] boolValue];
    formBuilder.forcesNonCompliance = [[fileData objectForKey:@"forcesNonCompliance"] boolValue];
    
    for(FormSection *section in formBuilder.sections) {
        section.metadata = formBuilder.metadata;
    }
    
    NSArray *checklistIds = fileData[@"checklistIds"];
    NSMutableArray *checklists = [NSMutableArray array];
    for(NSNumber *checklistId in checklistIds) {
        Checklist *checklist = [Checklist checklistWithId:[checklistId intValue]];
        if(checklist != nil) {
            [checklists addObject:checklist];
        }
    }
    formBuilder.checklists = checklists;
    
    for(NSString *key in formBuilder.sectionEnablerKeys) {
        [formBuilder updateSectionRequiringKey:key enabled:YES];
    }
    [formBuilder disableSectionsRequiringKeysNotInArray:formBuilder.sectionEnablerKeys];
    
    return formBuilder;
}

-(void)setSection:(FormSection*)section enabled:(BOOL)enabled {
    for(FormComponentEditor *editor in section.editors) {
        [editor setFormEnabled:enabled];
    }
}

-(void)disableSectionsRequiringKeysNotInArray:(NSArray*)keys {
    for(FormSection *section in self.sections) {
        if(section.enablerKey != nil && ![keys containsObject:section.enablerKey]) {
            [self setSection:section enabled:NO];
        }
    }
}

-(void)updateSectionRequiringKey:(NSString *)requiredKey enabled:(BOOL)enabled {
    for(FormSection *section in self.sections) {
        if(section.enablerKey != nil && [section.enablerKey isEqualToString:requiredKey]) {
            [self setSection:section enabled:enabled];
        }
    }
}

-(void)setSectionRequiringKey:(NSString*)requiredKey enabled:(BOOL)enabled {
    if(enabled) {
        [self.sectionEnablerKeys addObject:requiredKey];
    } else {
        [self.sectionEnablerKeys removeObject:requiredKey];
    }
    [self updateSectionRequiringKey:requiredKey enabled:enabled];
}

-(void)evaluateForcedNonCompliance {
    BOOL forcesNonCompliance = NO;
    for(FormSection *section in self.sections) {
        for(FormComponentEditor *editor in section.editors) {
            if([editor forcesNonCompliance]) {
                forcesNonCompliance = YES;
                break;
            }
        }
    }
    self.forcesNonCompliance = forcesNonCompliance;
}

-(void)setNonCompliance:(BOOL)nonCompliance {
    _nonCompliance = nonCompliance;
    
    // Find non compliance section and enable or disable it
    for(FormSection *section in self.sections) {
        for(FormComponentEditor *editor in section.editors) {
            if([editor isKindOfClass:[NonComplianceFormEditor class]]) {
                [self setSection:section enabled:nonCompliance];
                break;
            }
        }
    }
}

-(void)setForcesNonCompliance:(BOOL)forcesNonCompliance {
    _forcesNonCompliance = forcesNonCompliance;
    self.nonCompliance = forcesNonCompliance;
    
    // Find non compliance question editor and set it to No
    for(FormSection *section in self.sections) {
        for(FormComponentEditor *editor in section.editors) {
            if([editor isKindOfClass:[NonComplianceQuestionEditor class]]) {
                [editor setFormEnabled:!forcesNonCompliance];
                
                if(forcesNonCompliance) {
                    ((NonComplianceQuestionEditor*)editor).segmentedControl.selectedSegmentIndex = 1;
                }
            }
        }
    }
}

-(void)setupSections:(NSMutableArray*)sections {
    NSMutableArray *finalSections = [NSMutableArray array];
    for(FormSection *section in sections) {
        
        FormSection *finalSection;
        if([section.sectionName isEqualToString:@"timeSheet"]) {
            finalSection = [FormSection timeSheetFormSectionWithStartDate:section.timesheetStartDate];
        } else {
            finalSection = [FormSection formSectionWithName:section.sectionName];
        }
        if(section.data != nil) {
           [finalSection setEditorData:section.data];
        }
        
        [finalSections addObject:finalSection];
    }
    
    self.sections = finalSections;
}

-(void)setEditorData:(NSMutableDictionary*)data {
    for(FormComponentEditor *editor in self.editors) {
        [editor setData:data];
    }
}

-(NSString*)getSignatureBlockHTML {
    FormSection *firstSection = [self.sections objectAtIndex:0];
    return [GRMustacheTemplate renderObject:[firstSection getDataForRendering]
                               fromResource:@"standaloneFooter"
                                     bundle:nil
                                      error:NULL];
}

-(NSMutableArray*)allSections {
    NSMutableArray *allSections = [NSMutableArray arrayWithArray:self.sections];
    int addedSections = 0;
    for(int i = 0; i < self.sections.count; i++ ) {
        FormSection *section = [self.sections objectAtIndex:i];
        for(FormComponentEditor *editor in section.editors) {
            if(editor.standalone) {
                FormSection *standaloneSecton = [FormSection formSectionWithName:@"additionalRemarks"];
                standaloneSecton.metadata = section.metadata;
                standaloneSecton.title = section.title;
                [standaloneSecton setEditorData:[section getDataForRendering]];
                [allSections insertObject:standaloneSecton atIndex:i+1+addedSections];
                addedSections++;
            }
        }
    }
    
    return allSections;
}

-(int)getSectionIndexForChecklistIndex:(int)checklistIndex {
    int sectionIndex = 5; // Right above photos
    for(int i = 0; i < checklistIndex; i++) {
        Checklist *checklist = self.checklists[i];
        sectionIndex += checklist.sectionNames.count;
    }
    return sectionIndex;
}

-(void)removeChecklistAtIndex:(int)index {
    int sectionIndex = [self getSectionIndexForChecklistIndex:index];
    
    Checklist *checklist = self.checklists[index];
    for(int i = 0; i < checklist.sectionNames.count; i++) {
        [self.sections removeObjectAtIndex:sectionIndex];
    }
    
    [self.checklists removeObjectAtIndex:index];
}

-(void)addChecklist:(Checklist*)checklist {
    [self.checklists addObject:checklist];
    int sectionIndex = [self getSectionIndexForChecklistIndex:(int)self.checklists.count - 1];
    
    for(NSString *sectionName in checklist.sectionNames) {
        FormSection *section = [FormSection formSectionWithName:sectionName];
        section.metadata = self.metadata;
        [self.sections insertObject:section atIndex:sectionIndex];
        sectionIndex++;
    }
}

-(NSArray<NSString*>*)toEmails {
#ifdef GEOSCIENCE
    return @[self.nonCompliance ? @"compliance@rmageoscience.com" : @"appdr@rmageoscience.com"];
#endif
    
    return @[self.nonCompliance ? @"compliance@rmacompanies.com" : @"appdr@rmacompanies.com"];
}

-(NSArray<NSString*>*)ccEmails {
    
    NSMutableArray<NSString*> *emails = [NSMutableArray array];
    
    NSString *customerEmailsString = self.metadata[@"customerEmails"];
    if (customerEmailsString.length > 0) {
        NSArray *customerEmails = [customerEmailsString componentsSeparatedByString:@","];
        for (NSString *customerEmail in customerEmails) {
            [emails addObject:customerEmail];
        }
    }
    
    SampleCollectionEditor *sampleCollection = [self getSampleCollectionEditor];
    if (sampleCollection == nil) {
        return emails;
    }
    
    NSMutableArray<NSNumber*> *sampleTypeIndexes = [sampleCollection sampleTypeIndexes];
    
    if (sampleTypeIndexes.count > 0) {
#ifdef GEOSCIENCE
        [emails addObject:@"pickup@rmageoscience.com"];
        return emails;
#endif
        
        [emails addObject:@"pickup@rmacompanies.com"];
    }
    
    if ([sampleTypeIndexes containsObject:@0]) {
        [emails addObject:@"aggregates@rmacompanies.com"];
    }
    
    if ([sampleTypeIndexes containsObject:@1]) {
        [emails addObject:@"soil@rmacompanies.com"];
    }
    
    if ([sampleTypeIndexes containsObject:@2]) {
        [emails addObject:@"steel@rmacompanies.com"];
    }
    
    if ([sampleTypeIndexes containsObject:@3]) {
        [emails addObject:@"asphalt@rmacompanies.com"];
    }
    
    if ([sampleTypeIndexes containsObject:@4]) {
        [emails addObject:@"concrete@rmacompanies.com"];
    }
    
    return emails;
}

-(SampleCollectionEditor*)getSampleCollectionEditor {
    for (FormSection *section in self.sections) {
        if (![section.sectionName isEqualToString:@"Standard Samples"]) {
            continue;
        }
        for (FormComponentEditor *editor in section.editors) {
            if ([editor isKindOfClass:SampleCollectionEditor.class]) {
                return (SampleCollectionEditor*)editor;
            }
        }
    }
    
    return nil;
}

-(NSDictionary*)dictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    dictionary[@"__name"] = @"form";
    
    NSMutableDictionary *groupings = [NSMutableDictionary dictionary];
    NSDictionary *metadata = self.metadata;
    dictionary[@"header"] = metadata;
    
    for (FormSection *section in self.sections) {
        if (!section.shouldOutputXml) {
            continue;
        }
        
        if (section.xmlGrouping.length > 0 && section.xmlSubGrouping.length > 0) {
            NSMutableDictionary<NSString*,  NSMutableArray*> *groupingSections = groupings[section.xmlGrouping];
            if (groupingSections == nil) {
                groupingSections = [NSMutableDictionary dictionary];
                groupings[section.xmlGrouping] = groupingSections;
            }
            
            NSMutableArray<NSDictionary*>* subGroupingItems = groupingSections[section.xmlSubGrouping];
            if (subGroupingItems == nil) {
                subGroupingItems = [NSMutableArray array];
                groupingSections[section.xmlSubGrouping] = subGroupingItems;
            }
            
            [subGroupingItems addObject:[section dictionary]];
            
            continue;
        }
        
        [dictionary addEntriesFromDictionary:[section dictionary]];
    }
    
    [dictionary addEntriesFromDictionary:groupings];
    
    return dictionary;
}

@end
