//
//  PhotoCollectionEditor.m
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "PhotoCollectionEditor.h"
#import "PhotoPageEditor.h"
#import "PhotoPage.h"
#import "Photo.h"
#import "Constants.h"

@interface PhotoCollectionEditor ()
@property (nonatomic, strong) UIButton *add2x1Button;
@property (nonatomic, strong) UIButton *add2x2Button;
@property (nonatomic, strong) NSString *itemType;
@end

@implementation PhotoCollectionEditor

+(instancetype)photoCollectionEditorWithKey:(NSString *)editorKey {
    PhotoCollectionEditor *collectionEditor;
    NSString *strItemName = @"photo";
    if ([editorKey isEqualToString:@"pdfPages"]) {
        strItemName = @"PDF";
        collectionEditor = [super collectionEditorWithInstantiationBlock:^FormComponentEditor *{
            PhotoPageEditor *editor = [PhotoPageEditor photoPageEditorWithPhotoPage:[PhotoPage photoPageWithPhotos:@[
                                                                                                                     [[Photo alloc] init]
                                                                                                                     ] type:PDFPageType1x1] itemType:@"Master Forms"];
            return editor;
        } key:editorKey itemKey:@"pdfPages" itemName:[NSString stringWithFormat:@"Master Forms"]];
    } else {
        collectionEditor = [super collectionEditorWithInstantiationBlock:^FormComponentEditor *{
            PhotoPageEditor *editor = [PhotoPageEditor photoPageEditorWithPhotoPage:[PhotoPage photoPageWithPhotos:@[
                                                                                                                     [[Photo alloc] init]
                                                                                                                     ] type:PhotoPageType1x1] itemType:strItemName];
            return editor;
        } key:editorKey itemKey:@"photoPage" itemName:[NSString stringWithFormat:@"1 %@", strItemName]];
        
        collectionEditor.add2x1Button = [UIButton buttonWithType:UIButtonTypeSystem];
        collectionEditor.add2x2Button = [UIButton buttonWithType:UIButtonTypeSystem];
        
        [collectionEditor.view addSubview:collectionEditor.add2x1Button];
        [collectionEditor layoutAddButton:collectionEditor.add2x1Button itemName:[NSString stringWithFormat:@"2 %@", strItemName]];
        [collectionEditor.add2x1Button addTarget:collectionEditor action:@selector(add2x1) forControlEvents:UIControlEventTouchUpInside];
        
        [collectionEditor.view addSubview:collectionEditor.add2x2Button];
        [collectionEditor layoutAddButton:collectionEditor.add2x2Button itemName:[NSString stringWithFormat:@"4 %@", strItemName]];
        [collectionEditor.add2x2Button addTarget:collectionEditor action:@selector(add2x2) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [collectionEditor layoutItems];
    return collectionEditor;
}

-(void)add2x1 {
    if ([_itemType isEqualToString:@"PDF"]) {
        [self addItem:[PhotoPageEditor photoPageEditorWithPhotoPage:[PhotoPage photoPageWithPhotos:@[
                                                                                                     [[Photo alloc] init],
                                                                                                     [[Photo alloc] init]
                                                                                                     ] type:PDFPageType2x1] itemType:_itemType]];
    } else {
        [self addItem:[PhotoPageEditor photoPageEditorWithPhotoPage:[PhotoPage photoPageWithPhotos:@[
                                                                                                     [[Photo alloc] init],
                                                                                                     [[Photo alloc] init]
                                                                                                     ] type:PhotoPageType2x1] itemType:_itemType]];
    }
    
}

-(void)add2x2 {
    if ([_itemType isEqualToString:@"PDF"]) {
        [self addItem:[PhotoPageEditor photoPageEditorWithPhotoPage:[PhotoPage photoPageWithPhotos:@[
                                                                                                     [[Photo alloc] init],
                                                                                                     [[Photo alloc] init],
                                                                                                     [[Photo alloc] init],
                                                                                                     [[Photo alloc] init]
                                                                                                     ] type:PDFPageType2x2] itemType:_itemType]];
    } else {
        [self addItem:[PhotoPageEditor photoPageEditorWithPhotoPage:[PhotoPage photoPageWithPhotos:@[
                                                                                                     [[Photo alloc] init],
                                                                                                     [[Photo alloc] init],
                                                                                                     [[Photo alloc] init],
                                                                                                     [[Photo alloc] init]
                                                                                                     ] type:PhotoPageType2x2] itemType:_itemType]];
    }
}

-(void)clearView {
    for(UIView *subview in self.view.subviews) {
        if(subview != self.addButton && subview != self.add2x1Button && subview != self.add2x2Button) {
            [subview removeFromSuperview];
        }
    }
}

-(void)layoutItems {
    [super layoutItems];
    self.add2x1Button.frame = CGRectMake(CGRectGetMaxX(self.addButton.frame) + kPaddingX,
                                         self.addButton.frame.origin.y,
                                         self.add2x1Button.frame.size.width,
                                         self.add2x1Button.frame.size.height);
    
    self.add2x2Button.frame = CGRectMake(CGRectGetMaxX(self.add2x1Button.frame) + kPaddingX,
                                         self.addButton.frame.origin.y,
                                         self.add2x2Button.frame.size.width,
                                         self.add2x2Button.frame.size.height);
    
    self.add2x2Button.enabled = self.addButton.enabled;
    self.add2x1Button.enabled = self.addButton.enabled;
}
@end
