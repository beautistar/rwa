//
//  TableEditorColumn.m
//  RMA
//
//  Created by Michael Beteag on 7/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "TableEditorColumn.h"

@implementation TableEditorColumn

+(TableEditorColumn*)columnWithKey:(NSString*)key displayName:(NSString*)displayName width:(int)width {
    return [TableEditorColumn columnWithKey:key displayName:displayName width:width maxLength:0];
}

+(TableEditorColumn*)columnWithKey:(NSString*)key displayName:(NSString*)displayName width:(int)width maxLength:(int)maxLenth {
    TableEditorColumn *column = [[TableEditorColumn alloc] init];
    column.displayName = displayName;
    column.key = key;
    column.width = width;
    column.editable = YES;
    column.maxLength = maxLenth;
    return column;
}

+(TableEditorColumn*)nonEditableColumnWithKey:(NSString*)key displayName:(NSString*)displayName width:(int)width {
    TableEditorColumn *column = [[TableEditorColumn alloc] init];
    column.displayName = displayName;
    column.key = key;
    column.width = width;
    return column;
}



@end
