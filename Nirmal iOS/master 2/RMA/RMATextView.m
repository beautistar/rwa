//
//  RMATextView.m
//  RMA
//
//  Created by Michael Beteag on 11/11/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "RMATextView.h"

@implementation RMATextView

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self lookLikeTextField];
    }
    return self;
}

-(void)lookLikeTextField {
    self.editable = YES;
    
    self.clipsToBounds = YES;
    self.font = [UIFont systemFontOfSize:16];
    
    [self.layer setBorderWidth:1.0];
    self.layer.cornerRadius = 7;
    [self.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.3] CGColor]];
}

-(BOOL)becomeFirstResponder {
    BOOL autocorrect = [[NSUserDefaults standardUserDefaults] boolForKey:@"autocorrect"];
    
    if(!autocorrect) {
        self.spellCheckingType = UITextSpellCheckingTypeYes;
        self.autocorrectionType = UITextAutocorrectionTypeNo;
    }
    
    return [super becomeFirstResponder];
}

-(void)awakeFromNib {
    [super awakeFromNib];
    [self lookLikeTextField];
}
@end
