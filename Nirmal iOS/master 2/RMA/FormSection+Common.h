//
//  FormSection+Common.h
//  RMA
//
//  Created by Alexander Roode on 5/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "FormSection.h"

@interface FormSection (Common)

+(FormSection*)testSection;
+(FormSection*)emptySection;
+(FormSection*)nonComplianceSection;
+(FormSection*)photosSection;
+(FormSection*)standardSamplesSection;
+(FormSection*)addMoreSamples;
+(FormSection*)additionalRemarksSection;
+(FormSection*)pdfSection;

@end
