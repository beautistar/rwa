//
//  UpdateRequest.m
//  RMA
//
//  Created by Michael Beteag on 11/20/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "UpdateRequest.h"
#import "XMLDictionary.h"
#import "AppData.h"
#import "SSKeychain/SSKeychain.h"



@implementation UpdateRequest


+(UpdateRequest*)updateRequestWithUsername:(NSString*)username password:(NSString*)password {
    UpdateRequest *request = [[UpdateRequest alloc] init];
    request.username = username;
    request.password = password;
    
    request.queue = [[NSOperationQueue alloc] init];
    request.client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:[AppData webServiceUrl]]];
    
    return request;
}

-(void)begin {
    NSOperation *viewStateAndEventValidationOperation = [self getViewStateAndEventValidation];
    [self.queue addOperation:viewStateAndEventValidationOperation];
}

-(AFHTTPRequestOperation*)getViewStateAndEventValidation {
    NSString *viewStatePattern = @"(<input[^>]+id=\"__VIEWSTATE\"[^>]+value=\")([^\"]+)(\"[^>]+>)";
    NSString *eventValidationPattern = @"(<input[^>]+id=\"__EVENTVALIDATION\"[^>]+value=\")([^\"]+)(\"[^>]+>)";
    NSError *error;
    NSRegularExpression *vsRegex = [NSRegularExpression regularExpressionWithPattern:viewStatePattern options:0 error:&error];
    NSRegularExpression *evRegex = [NSRegularExpression regularExpressionWithPattern:eventValidationPattern options:0 error:&error];
    
    NSURLRequest *request = [self.client requestWithMethod:@"GET" path:@"Login.aspx" parameters:nil];
    AFHTTPRequestOperation *operation = [self.client HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = operation.responseString;
        NSArray *vsMatches = [vsRegex matchesInString:response options:0 range:NSMakeRange(0, response.length)];
        NSArray *evMatches = [evRegex matchesInString:response options:0 range:NSMakeRange(0, response.length)];

        for(NSTextCheckingResult *match in vsMatches) {
            NSString *result = [response substringWithRange:[match rangeAtIndex:2]];
            self.viewState = result;
        }
        for(NSTextCheckingResult *match in evMatches) {
            NSString *result = [response substringWithRange:[match rangeAtIndex:2]];
            self.eventValidation = result;
        }
        [self.queue addOperation:[self getFormsOperation]];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self postFailureNotification:error.description];
    }];
    return operation;
}

-(AFHTTPRequestOperation*)getFormsOperation {
    NSDictionary *data = @{@"ToolkitScriptManager1_HiddenField":@"",
                           @"ctl00$MainContent$LoginUser$UserName":self.username,
                           @"ctl00$MainContent$LoginUser$Password":self.password,
                           @"ctl00$MainContent$LoginUser$RememberMe":@1,
                           @"__EVENTVALIDATION":self.eventValidation,
                           @"__VIEWSTATE":self.viewState,
                           @"ctl00$MainContent$LoginUser$LoginButton":@"go"};
    NSURLRequest *request = [self.client requestWithMethod:@"POST" path:@"Login.aspx?ReturnUrl=%2fDRAPP%2fDRXML.aspx" parameters:data];
    AFHTTPRequestOperation *operation = [self.client HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseData = [NSDictionary dictionaryWithXMLString:operation.responseString];
        if([responseData objectForKey:@"body"] != nil) {
            [self postFailureNotification:@"Invalid username or password"];
        } else {
            if([responseData objectForKey:@"Record"] != nil) {
                
                if([[responseData objectForKey:@"Record"] isKindOfClass:[NSDictionary class]]) {
                    [[AppData sharedData] updateForms:@[[responseData objectForKey:@"Record"]]];
                } else {
                    [[AppData sharedData] updateForms:[responseData objectForKey:@"Record"]];
                }
                
                
            }
            [self postSuccessNotification];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self postFailureNotification:error.description];
    }];
    return operation;
}

-(void)postSuccessNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateRequestDidSucceed" object:nil];
}

-(void)postFailureNotification:(NSString*)failureMessage {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateRequestDidFail" object:failureMessage];
}

@end
