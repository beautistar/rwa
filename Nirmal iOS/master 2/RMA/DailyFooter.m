//
//  DailyFooter.m
//  RMA
//
//  Created by Alexander Roode on 5/12/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "DailyFooter.h"

@implementation DailyFooter

-(NSString*)renderForMustacheTag:(GRMustacheTag *)tag context:(GRMustacheContext *)context HTMLSafe:(BOOL *)HTMLSafe error:(NSError *__autoreleasing *)error {
    
    NSString *content = @"";
    
#ifdef SITESCAN
    content = [[GRMustacheTemplate templateFromResource:@"standaloneFooter" bundle:nil error:NULL] renderContentWithContext:context HTMLSafe:HTMLSafe error:error];
#else
    NSString *sectionName = context.topMustacheObject[@"sectionName"];
    if (sectionName.length > 0) {
        NSString *templateName = @"";
        if ([sectionName isEqualToString:@"Daily Technician Report"]) {
            templateName = @"nonDSAFooterTechnician";
        } else if ([sectionName isEqualToString:@"Daily Inspection Report"]) {
            templateName = @"nonDSAFooterInspection";
        } else if ([sectionName isEqualToString:@"DSA Daily Technician Report"]) {
            templateName = @"DSAFooterTechnician";
        } else if ([sectionName isEqualToString:@"DSA Daily Inspection Report"]) {
            templateName = @"DSAFooterInspection";
        }
        
        content = [[GRMustacheTemplate templateFromResource:templateName bundle:nil error:NULL] renderContentWithContext:context HTMLSafe:HTMLSafe error:error];
    }
#endif
    
    GRMustacheTemplate *template = [GRMustacheTemplate templateFromResource:@"commonFooter" bundle:nil error:NULL];
    return [content stringByAppendingString:[template renderContentWithContext:context HTMLSafe:HTMLSafe error:error]];
}

@end
