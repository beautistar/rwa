//
//  FormEditorPage.h
//  RMA
//
//  Created by Michael Beteag on 8/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FormEditorPage : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *shortTitle;
@property CGRect frame;

@end
