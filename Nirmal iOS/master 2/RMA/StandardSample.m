//
//  StandardSample.m
//  RMA
//
//  Created by Michael Beteag on 2/19/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "StandardSample.h"

@implementation StandardSample

-(id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super init])) {
        [self setData:[aDecoder decodeObjectForKey:@"data"]];
        [self setType:[aDecoder decodeObjectForKey:@"type"]];
        [self setIndex:[aDecoder decodeObjectForKey:@"index"]];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.data forKey:@"data"];
    [aCoder encodeObject:self.type forKey:@"type"];
    [aCoder encodeObject:self.index forKey:@"index"];
}

+ (StandardSample*)standardSampleWithType:(int)type index:(int)index data:(NSMutableDictionary*)data {
    StandardSample *standardSample = [[StandardSample alloc] init];
    standardSample.data = data;
    standardSample.type = [NSNumber numberWithInt:type];
    standardSample.index = [NSNumber numberWithInt:index];
    return standardSample;
}

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    
    *HTMLSafe = YES;
    
    GRMustacheTemplate *template = nil;;
    if ([self.type intValue] == 1) {
        template = [GRMustacheTemplate templateFromResource:@"otherSample" bundle:nil error:nil];
    } else if ([self.type intValue] == 2) {
        template = [GRMustacheTemplate templateFromResource:@"steelSample" bundle:nil error:nil];
    } else if ([self.type intValue] == 3) {
        template = [GRMustacheTemplate templateFromResource:@"concreteSample" bundle:nil error:nil];
    }
    
    if (template == nil) {
        return @"";
    }
    
    GRMustacheContext *newContext = [context contextByAddingObject:self.data];
    return [template renderContentWithContext:newContext HTMLSafe:HTMLSafe error:nil];
}

@end
