//
//  FormSection+TimeSheet.m
//  RMA
//
//  Created by Alexander Roode on 5/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "FormSection+TimeSheet.h"
#import "AppData.h"
#import "TableEditor.h"
#import "TableEditorRow.h"
#import "TableEditorColumn.h"
#import "FormDivider.h"
#import "FormHeader.h"

@implementation FormSection (TimeSheet)

+(FormSection*)timeSheetFormSectionWithStartDate:(NSDate*)startDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEEE M/d";
    
    NSDateFormatter *keyDateFormatter = [[NSDateFormatter alloc] init];
    keyDateFormatter.dateFormat = @"EEEE";
    
    AppData *appData = [AppData sharedData];
    
    NSArray *dates = [[AppData sharedData] getFormDatesOfWeekForTechnicianId:appData.currentTechnicianId duringWeekOfDate:startDate];
    NSArray *uniqueDRs = [[AppData sharedData] getUniqueDRsForTechnicianId:appData.currentTechnicianId duringWeekOfDate:startDate];
    NSMutableArray *rows = [NSMutableArray array];
    for(NSArray *fixedValues in uniqueDRs) {
        [rows addObject:[TableEditorRow tableEditorRowWithFixedValues:fixedValues]];
    }
    
    NSMutableArray *editors = [NSMutableArray array];
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Time Sheet"];
    [editors addObject:divider];
    
    for(NSDate *date in dates) {
        TableEditor *editor = [TableEditor tableEditorWithTitle:[dateFormatter stringFromDate:date] columns:@[[TableEditorColumn nonEditableColumnWithKey:@"dr" displayName:@"DR #" width:1],
                                                                                                              [TableEditorColumn nonEditableColumnWithKey:@"task" displayName:@"Task" width:1],
                                                                                                              [TableEditorColumn columnWithKey:@"Reg" displayName:@"Reg. Hours" width:1 maxLength:2],
                                                                                                              [TableEditorColumn columnWithKey:@"OT" displayName:@"OT Hours" width:1 maxLength:2],
                                                                                                              [TableEditorColumn columnWithKey:@"Db" displayName:@"Db. Hours" width:1 maxLength:2]]
                                                           rows:rows key:[keyDateFormatter stringFromDate:date]];
        [editors addObject:editor];
    }
    
    FormSection *section = [FormSection formSectionWithEditors:editors templateName:@"timesheet" title:@"Time Sheet" shouldOutputXml:NO];
    section.timesheetStartDate = startDate;
    section.sectionName = @"timeSheet";
    section.headerDefault = [FormHeader headerWithType:@"blankHeader"];
    section.headerPage1 = [FormHeader headerWithType:@"blankHeader"];
    return section;
}

@end
