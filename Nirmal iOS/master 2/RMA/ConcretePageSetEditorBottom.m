//
//  ConcretePageSetEditorBottom.m
//  RMA
//
//  Created by Michael Beteag on 8/13/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "ConcretePageSetEditorBottom.h"
#import "RMATextView.h"

@implementation ConcretePageSetEditorBottom

+(ConcretePageSetEditorBottom *)concretePageSetEditorBottom {
    ConcretePageSetEditorBottom *editor = [[ConcretePageSetEditorBottom alloc] initWithNibName:@"ConcretePageSetEditorBottom"];
    
    editor.placedAtArea.delegate = editor;
    editor.curingLocationArea.delegate = editor;
    
    
    return editor;
}

-(void)setConcretePageSet:(ConcretePageSet*)set {
    self.tripTicketNoField.text = set.tripTicketNo;
    self.slumpPenField.text = set.slumpPen;
    self.mtlTempField.text = set.mtlTempCF;
    self.airTempField.text = set.airTempCF;
    self.airContentField.text = set.airContent;
    self.unitWtField.text = set.unitWt;
    self.placedAtArea.text = set.placedAt.text;
    self.timeCastField.text = set.timeCast;

    UISegmentedControl *mtlTempUnitsControl = (UISegmentedControl*)[self.view viewWithTag:2];
    [mtlTempUnitsControl selectText:set.mtlTempUnits];

    UISegmentedControl *airTempUnitsControl  = (UISegmentedControl*)[self.view viewWithTag:3];
    [airTempUnitsControl selectText:set.airTempUnits];
}

-(NSMutableDictionary*)getPartialConcreteSet {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:(self.tripTicketNoField.text.length > 0 ? self.tripTicketNoField.text : @"") forKey:@"tripTicketNo"];
    [data setObject:(self.slumpPenField.text.length > 0 ? self.slumpPenField.text : @"") forKey:@"slumpPen"];
    [data setObject:(self.mtlTempField.text.length > 0 ? self.mtlTempField.text : @"") forKey:@"mtlTempCF"];
    [data setObject:(self.airTempField.text.length > 0 ? self.airTempField.text : @"") forKey:@"airTempCF"];
    [data setObject:(self.airContentField.text.length > 0 ? self.airContentField.text : @"") forKey:@"airContent"];
    [data setObject:(self.unitWtField.text.length > 0 ? self.unitWtField.text : @"") forKey:@"unitWt"];
    [data setObject:[Paragraph paragraphWithText:self.placedAtArea.text] forKey:@"placedAt"];
    [data setObject:[Paragraph paragraphWithText:self.curingLocationArea.text] forKey:@"curingLocation"];
    [data setObject:(self.timeCastField.text.length > 0 ? self.timeCastField.text : @"") forKey:@"timeCast"];
    [data setObject:(self.truckNoField.text.length > 0 ? self.truckNoField.text : @"") forKey:@"truckNo"];
    [data setObject:[((UISegmentedControl*)[self.view viewWithTag:3]) text] forKey:@"airTempUnits"];
    [data setObject:[((UISegmentedControl*)[self.view viewWithTag:2]) text] forKey:@"mtlTempUnits"];

    return data;
}
- (IBAction)deleteButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidBeginConcretePageSetDeletion" object:[NSNumber numberWithInt:self.setId]];

}

-(NSMutableDictionary*)getData {
    return [self getPartialConcreteSet];
}
-(void)setData:(NSMutableDictionary *)data {
    self.tripTicketNoField.text = [data objectForKey:@"tripTicketNo"];
    self.slumpPenField.text = [data objectForKey:@"slumpPen"];
    self.mtlTempField.text = [data objectForKey:@"mtlTempCF"];
    self.airTempField.text = [data objectForKey:@"airTempCF"];
    self.airContentField.text = [data objectForKey:@"airContent"];
    self.unitWtField.text = [data objectForKey:@"unitWt"];
    Paragraph *paragraph = [data objectForKey:@"placedAt"];
    self.placedAtArea.text = paragraph.text;
    
    Paragraph *curingLocationParagraph = [data objectForKey:@"curingLocation"];
    self.curingLocationArea.text = curingLocationParagraph.text;
    
    self.timeCastField.text = [data objectForKey:@"timeCast"];
    self.truckNoField.text = [data objectForKey:@"truckNo"];
    
    UISegmentedControl *mtlTempUnitsControl = (UISegmentedControl*)[self.view viewWithTag:2];
    [mtlTempUnitsControl selectText:[data objectForKey:@"mtlTempUnits"]];
    
    UISegmentedControl *airTempUnitsControl  = (UISegmentedControl*)[self.view viewWithTag:3];
    [airTempUnitsControl selectText:[data objectForKey:@"airTempUnits"]];
}

-(NSDictionary*)dictionary {
    NSMutableDictionary *data = [self getData];
    
    // Rename some fields to adapt to current naming conventions
    data[@"mtlTemp"] = data[@"mtlTempCF"];
    [data removeObjectForKey:@"mtlTempCF"];
    
    data[@"airTemp"] = data[@"airTempCF"];
    [data removeObjectForKey:@"airTempCF"];
    
    data[@"truckNumber"] = data[@"truckNo"];
    [data removeObjectForKey:@"truckNo"];
    
    Paragraph *placedAt = data[@"placedAt"];
    data[@"placedAt"] = placedAt.text;
    
    Paragraph *curingLocation = data[@"curingLocation"];
    data[@"curingLocation"] = curingLocation.text;
    
    return data;
}

@end
