//
//  NSString+Additions.h
//  RMA
//
//  Created by Michael Beteag on 12/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (CGSize) sizeWithMyFont:(UIFont *)fontToUse;
-(CGFloat) heightWithFont:(UIFont*)font width:(CGFloat)width;

@end
