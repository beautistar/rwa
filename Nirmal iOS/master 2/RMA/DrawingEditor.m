//
//  DrawingEditor.m
//  RMA
//
//  Created by Michael Beteag on 8/1/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "DrawingEditor.h"
#import "Constants.h"

@implementation DrawingEditor

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.view = self;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame name:(NSString*)name key:(NSString*)key
{
    self = [self initWithFrame:frame];
    
    //self.backgroundColor = [UIColor redColor];
    self.name = name;
    self.key = key;
    
    self.imageViewLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPaddingX,
                                                                    kPaddingYWithSegmentedControl,
                                                                    self.frame.size.width - 2*kPaddingX,
                                                                    kLabelHeight)];
    self.imageViewLabel.text = self.name;
    [self.view addSubview:self.imageViewLabel];
    
    EAGLContext* context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    self.imageView = [[NICSignatureView alloc] initWithFrame:CGRectMake(kPaddingX,
                                                                 2*kPaddingY + kSegmentedControlHeight,
                                                                 self.frame.size.width - 2*kPaddingX,
                                                                 self.frame.size.height - 3*kPaddingY - kSegmentedControlHeight)
                                                     context:context];
    self.imageView.layer.cornerRadius = 5;
    self.imageView.clipsToBounds = YES;
    [self.imageView.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.imageView.layer setBorderWidth:2.0];
    [self.view addSubview:self.imageView];
    
    self.clearButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.clearButton setTitle:@"Clear" forState:UIControlStateNormal];
    [self.clearButton setFrame:CGRectMake(self.frame.size.width - 80 - kPaddingX,
                                         kPaddingY,
                                         80,
                                         kSegmentedControlHeight)];
    [self.clearButton addTarget:self action:@selector(clear) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.clearButton];
    return self;
}

-(void)clear {
    [self.imageView erase];
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    NSLog(@"%@ %@", NSStringFromCGSize(self.view.frame.size), NSStringFromCGSize(self.imageView.frame.size));
    Image *image = [Image imageWithImage:self.imageView.signatureImage size:self.imageView.frame.size];
    [data setObject:image forKey:self.key];
    return data;
    
}
-(void)setData:(NSMutableDictionary*)data {
    if([data objectForKey:self.key] != nil) {
        Image *image = ((Image*)[data objectForKey:self.key]);
        
        self.imageView.signatureImage = image.image;
    }
}

@end
