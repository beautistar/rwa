//
//  Document.h
//  RMA
//
//  Created by Michael Beteag on 12/13/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Folder;

@interface Document : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * filename;
@property (nonatomic, retain) NSString * extension;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * path;
@property (nonatomic, retain) Folder *folder;

@end
