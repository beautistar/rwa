//
//  RMASegmentedControl.m
//  RMA
//
//  Created by Michael Beteag on 6/25/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "RMASegmentedControl.h"
#import "UISegmentedControl+Text.h"
#import "RMATextField.h"

@interface RMASegmentedControl ()

@property BOOL selectedOther;
@property BOOL hasOther;
@property BOOL multiline;
@property int otherIndex;
@property (nonatomic, strong) RMATextField *otherField;
@property (nonatomic, strong) UILabel *otherLabel;
@property (nonatomic, strong) UIButton *changeButton;

@end

@implementation RMASegmentedControl

- (id)initWithItems:(NSArray*)items
{
    self = [super initWithFrame:CGRectNull];
    if (self) {
        self.segmentedControl = [[UISegmentedControl alloc] initWithItems:items];
        
        [self addSubview:self.segmentedControl];
        
        
        for(NSString *item in items) {
            if([item rangeOfString:@"\n"].location != NSNotFound) {
                self.multiline = YES;
            }
            if([item isEqualToString:@"Other"]) {
                self.hasOther = YES;
                self.otherIndex = (int)[items indexOfObject:@"Other"];
                break;
            }
        }
        
        if(self.multiline) {
            for (id segment in [self.segmentedControl subviews]) {
                for (id label in [segment subviews]) {
                    if ([label isKindOfClass:[UILabel class]]) {
                        UILabel *titleLabel = (UILabel *) label;
                        titleLabel.numberOfLines = 0;
                    }
                }
            }
            self.segmentedControl.frame = CGRectMake(self.segmentedControl.frame.origin.x,
                                                     self.segmentedControl.frame.origin.y,
                                                     self.segmentedControl.frame.size.width,
                                                     2*self.segmentedControl.frame.size.height);
            
        }
        CGRect frame = CGRectMake(self.segmentedControl.frame.origin.x,
                                  self.segmentedControl.frame.origin.y,
                                  self.segmentedControl.frame.size.width,
                                  self.segmentedControl.frame.size.height);
        self.frame = frame;
        [self.segmentedControl addTarget:self action:@selector(didChangeValue) forControlEvents:UIControlEventValueChanged];
    }
    return self;
}

-(void)didChangeValue {
    self.selectedOther = [[self.segmentedControl text] isEqualToString:@"Other"];

    if(self.selectedOther) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenOtherInput" object:nil userInfo:@{@"segmentedControl":self.segmentedControl, @"control":self, @"otherIndex":@(self.otherIndex)}];
    }
    return;
}

-(void)setOtherValue:(NSString*)otherValue {
    self.changeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.changeButton setTitle:@"Change" forState:UIControlStateNormal];
    [self.changeButton addTarget:self action:@selector(changeButtonPressed) forControlEvents:UIControlEventTouchUpInside];

    [self addSubview:self.changeButton];
    
    CGFloat buttonWidth = 80;
    self.changeButton.frame = CGRectMake(self.frame.size.width - buttonWidth, 0, buttonWidth, self.frame.size.height);
    
    self.otherLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - buttonWidth, self.frame.size.height)];
    self.otherLabel.text = otherValue;
    
    [self addSubview:self.otherLabel];
    
    [self.segmentedControl removeFromSuperview];
    
}

-(void)changeButtonPressed {
    [self addSubview:self.segmentedControl];
    [self reset];
    [self.changeButton removeFromSuperview];
    [self.otherLabel removeFromSuperview];
}

-(void)reset {
    self.segmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment;
    self.selectedOther = NO;
}

-(void)selectText:(NSString*)text {
    if(![self.segmentedControl selectText:text]) {
        if(self.hasOther && text.length > 0) {
            [self setOtherValue:text];
            self.selectedOther = YES;
        }
    }
}

-(NSString*)text {
    if(self.selectedOther) {
        return self.otherLabel.text;
    }
    return [self.segmentedControl text];
}

@end
