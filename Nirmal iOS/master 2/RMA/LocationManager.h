//
//  LocationManager.h
//  RMA
//
//  Created by Alexander Roode on 5/10/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationManagerDelegate <NSObject>
- (void)didAcquireLocation:(CLLocation *)location;
- (void)didFailToAcquireLocationWithErrorMessage:(NSString *)errorMessage;
@optional
- (void)didFindLocationName:(NSString *)locationName;
@end

@interface LocationManager : NSObject <CLLocationManagerDelegate>

@property (nonatomic, weak) id <LocationManagerDelegate>delegate;

+ (instancetype)sharedManager;
- (void)startUpdatingLocation;
- (void)findNameForLocation:(CLLocation *)location;
@end
