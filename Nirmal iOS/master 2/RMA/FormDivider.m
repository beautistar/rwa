//
//  FormDivider.m
//  RMA
//
//  Created by Michael Beteag on 8/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormDivider.h"

@implementation FormDivider

+(FormDivider*)formDividerWithTitle:(NSString*)title {
    FormDivider *divider = [[FormDivider alloc] initWithNibName:@"FormDivider"];
    divider.titleLabel.text = title;
    
    return divider;
    
}

+(FormDivider*)formDividerWithTitle:(NSString*)title shortTitle:(NSString*)shortTitle {
    FormDivider *divider = [[FormDivider alloc] initWithNibName:@"FormDivider"];
    divider.titleLabel.text = title;
    divider.shortTitle = shortTitle;
    
    return divider;
    
}



@end
