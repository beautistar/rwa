//
//  FormSection.h
//  RMA
//
//  Created by Michael Beteag on 7/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"

@class Form, FormHeader;

@interface FormSection : NSObject <NSCoding, GRMustacheRendering>

@property (nonatomic, retain) NSString *sectionName;
@property (nonatomic, retain) NSString *templateName;
@property (nonatomic, retain) NSMutableDictionary *data;
@property (nonatomic, retain) NSArray *editors;
@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) FormHeader *headerPage1;
@property (nonatomic, retain) FormHeader *headerDefault;
@property (nonatomic, retain) NSMutableDictionary *metadata;
@property (nonatomic, retain) NSMutableDictionary *extraData;
@property (nonatomic, retain) NSString *enablerKey;
@property (nonatomic, retain) NSDate *timesheetStartDate;
@property BOOL useSectionName;

// XML output settings
@property (nonatomic, copy) NSString *xmlGrouping;
@property (nonatomic, copy) NSString *xmlSubGrouping;
@property BOOL shouldOutputXml;
@property (nonatomic, copy) NSString *xmlTagName;


// Extra data added to xml output
@property (nonatomic, strong) NSMutableDictionary *extraXmlData;

- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;

-(NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error;

+(FormSection*)formSectionWithName:(NSString*)sectionName;
+(FormSection*)formSectionWithEditors:(NSArray *)editors templateName:(NSString *)templateName title:(NSString*)title xmlGrouping:(NSString*)grouping xmlSubGrouping:(NSString*)subGrouping shouldOutputXml:(BOOL)shouldOutputXml;
+(FormSection*)formSectionWithEditors:(NSArray *)editors templateName:(NSString *)templateName title:(NSString *)title;
+(FormSection*)formSectionWithEditors:(NSArray *)editors templateName:(NSString *)templateName title:(NSString*)title xmlGrouping:(NSString *)grouping xmlSubGrouping:(NSString*)subGrouping;
+(FormSection*)formSectionWithEditors:(NSArray *)editors templateName:(NSString *)templateName title:(NSString *)title shouldOutputXml:(BOOL)shouldOutputXml;

-(NSMutableDictionary*)getDataForSaving;
-(NSMutableDictionary*)getDataForRendering;
-(void)setEditorData:(NSMutableDictionary*)data;
-(NSString*)getRenderedHTML;
-(BOOL)isEnabled;
-(BOOL)isHiddenInPdf;

-(NSDictionary*)dictionary;

@end
