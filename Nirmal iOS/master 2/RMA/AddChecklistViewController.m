//
//  AddChecklistViewController.m
//  RMA
//
//  Created by Alexander Roode on 1/19/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "AddChecklistViewController.h"
#import "Checklist.h"
#import "RMATableViewCell.h"

@interface AddChecklistViewController ()

@end

@implementation AddChecklistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerClass:RMATableViewCell.class forCellReuseIdentifier:@"Cell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.checklistOptions.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Checklist *checklist = self.checklistOptions[indexPath.row];
    
    cell.textLabel.text = checklist.name;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Checklist *checklist = self.checklistOptions[indexPath.row];
    [self.delegate addChecklist:checklist];
}

@end
