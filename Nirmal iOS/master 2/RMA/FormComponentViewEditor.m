//
//  FormComponentViewEditor.m
//  RMA
//
//  Created by Michael Beteag on 7/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"

@implementation FormComponentViewEditor {
    NSString *_nibName;
}

- (instancetype)initWithNibName:(NSString*)nibName {
    self = [super initWithFrame:CGRectNull];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        
        [self addSubview:self.view];

        _nibName = nibName;
        
        
        
        for (UIView *view in self.view.subviews) {
            if ([view isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField*)view;
                textField.delegate = self;
            } else if([view isKindOfClass:[UITextView class]]) {
                UITextView *textView = (UITextView*)view;
                textView.delegate = self;
            } else if ([view isKindOfClass:[UITableView class]]) {
                UITableView *tableview = (UITableView*)view;
                tableview.delegate = self;
                tableview.dataSource = self;
            }
        }
        
        
    }
    
    return self;
}

-(NSMutableDictionary*)getSharedData {
    if([_nibName isEqualToString:@"DailyCommonEditor"] || [_nibName isEqualToString:@"DSADailyCommonEditor"]) {
        
        UITextField *referenceDocumentsField = (UITextField*)[self.view viewWithTag:1];
        UITextField *generalContractorField  = (UITextField*)[self.view viewWithTag:2];
        UITextField *subContractorField  = (UITextField*)[self.view viewWithTag:3];
        UITextField *materialSupplierField = (UITextField*)[self.view viewWithTag:4];
        
        return [NSMutableDictionary dictionaryWithDictionary:@{
                                                               @"generalContractor":generalContractorField.text,
                                                               @"referenceDocuments":referenceDocumentsField.text,
                                                               @"subContractor":subContractorField.text,
                                                               @"materialSupplier":materialSupplierField.text,
                                                               }];
    }
    return nil;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    [self addSubview:self.view];
}

-(NSDictionary*)dictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    NSDictionary *data = [self getData];
    
    if ([_nibName isEqualToString:@"DailyCommonEditor"] || [_nibName isEqualToString:@"DSADailyCommonEditor"]) {
        dictionary[@"referenceDocuments"] = data[@"t1"];
        dictionary[@"generalContractor"] = data[@"t2"];
        dictionary[@"subContractor"] = data[@"t3"];
        dictionary[@"materialSupplier"] = data[@"t4"];
    }
    
    if ([_nibName isEqualToString:@"DSADailyCommonEditor"]) {
        dictionary[@"permitNumber"] = data[@"t5"];
    
    } else if ([_nibName isEqualToString:@"NonDSAFooterEditorNarrow"]) {
        dictionary[@"regHours"] = data[@"t105"];
        dictionary[@"otHours"] = data[@"t106"];
        dictionary[@"beginTime"] = data[@"t107"];
        dictionary[@"endTime"] = data[@"t108"];
        dictionary[@"weather"] = data[@"t102"];
    
    } else if ([_nibName isEqualToString:@"EpoxyAdhesiveInformationTopEditor"]) {
        dictionary[@"codeEvaluationReport"] = data[@"t1"];
    
    } else if ([_nibName isEqualToString:@"EpoxyAdhesiveInformationEditor"]) {
        dictionary[@"epoxyNumber"] = data[@"t1"];
        dictionary[@"manufacturer"] = data[@"t2"];
        dictionary[@"expirationDate"] = data[@"t3"];
        dictionary[@"installationMethod"] = data[@"t4"];
        dictionary[@"materialTemp"] = data[@"t5"];
        dictionary[@"materialTempUnits"] = data[@"t6"];
        dictionary[@"ambientTemp"] = data[@"t7"];
        dictionary[@"ambientTempUnits"] = data[@"t8"];
        dictionary[@"gelTime"] = data[@"t9"];
        dictionary[@"initialCureTime"] = data[@"t10"];
        dictionary[@"fullCureTime"] = data[@"t11"];
    
    } else if ([_nibName isEqualToString:@"ReinforcementInformationEditor"]) {
        
        dictionary[@"itemNumber"] = data[@"t1"];
        dictionary[@"manufacturer"] = data[@"t2"];
        dictionary[@"brandType"] = data[@"t3"];
        dictionary[@"torqueTestingPerformed"] = data[@"t4"];
        dictionary[@"size"] = data[@"t5"];
        dictionary[@"intervalSpacing"] = data[@"t6"];
        dictionary[@"edgeDistance"] = data[@"t7"];
        dictionary[@"drillBitDiameter"] = data[@"t8"];
        dictionary[@"specifiedMinimumEmbedmentDepth"] = data[@"t9"];
        
    } else if ([_nibName isEqualToString:@"DrilledInAnchorInspectionTopEditor"]) {
        dictionary[@"codeEvaluationReport"] = data[@"t1"];
        dictionary[@"torqueTestingPerformed"] = data[@"t2"];
        
    } else if ([_nibName isEqualToString:@"DrilledInAnchorInspectionBottomEditor"]) {
        dictionary[@"drilledInAnchorInspectionBottomEditor"] = data[@"t3"];
        
    } else if ([_nibName isEqualToString:@"AnchorInformationEditor"]) {
        dictionary[@"manufacturer"] = data[@"t1"];
        dictionary[@"brandType"] = data[@"t2"];
        dictionary[@"torqueTestingPerformed"] = data[@"t3"];
        dictionary[@"size"] = data[@"t4"];
        dictionary[@"intervalSpacing"] = data[@"t5"];
        dictionary[@"edgeDistance"] = data[@"t6"];
        dictionary[@"drillBitDiameter"] = data[@"t7"];
        dictionary[@"specifiedMinimumEmbedmentDepth"] = data[@"t8"];
        
    } else if ([_nibName isEqualToString:@"AnchorInstallationLogEditor"]) {
        dictionary[@"itemNumber"] = data[@"t1"];
        dictionary[@"numberOfAnchors"] = data[@"t2"];
        dictionary[@"floorLocation"] = data[@"t3"];
        dictionary[@"holeDiameter"] = data[@"t4"];
        dictionary[@"holeDepth"] = data[@"t5"];
        dictionary[@"drilledHoleBlown"] = data[@"t6"];
        
    } else if ([_nibName isEqualToString:@"EpoxyAdhesiveInspectionBottomEditor"]) {
        dictionary[@"drilledInAnchorInspectionBottomEditor"] = data[@"t1"];
        
    } else if ([_nibName isEqualToString:@"EpoxyAdhesiveInspectionEditor"]) {
        dictionary[@"epoxyNumber"] = data[@"t1"];
        dictionary[@"itemNumber"] = data[@"t2"];
        dictionary[@"totalInstalled"] = data[@"t3"];
        dictionary[@"floorLocation"] = data[@"t4"];
        dictionary[@"holeDepth"] = data[@"t5"];
        dictionary[@"holeDiameter"] = data[@"t6"];
        dictionary[@"holeBlown"] = data[@"t7"];
        dictionary[@"holeBrushed"] = data[@"t8"];
        dictionary[@"holeReblown"] = data[@"t9"];
    
    } else if ([_nibName isEqualToString:@"ItemNumberDescriptionEditor"]) {
        dictionary[@"description"] = data[@"t1"];
        dictionary[@"numberOfAnchors"] = data[@"t2"];
        dictionary[@"numberTested"] = data[@"t3"];
        dictionary[@"percentageOfTotal"] = data[@"t4"];
        
    } else if ([_nibName isEqualToString:@"TorquePullLogEditor"]) {
        dictionary[@"itemNumber"] = data[@"t1"];
        dictionary[@"locationTested"] = data[@"t2"];
        dictionary[@"typeSize"] = data[@"t3"];
        dictionary[@"loadTested"] = data[@"t4"];
        dictionary[@"specifiedLoad"] = data[@"t5"];
        dictionary[@"passFailRetest"] = data[@"t6"];
        
    } else if ([_nibName isEqualToString:@"TorquePullTestingTopEditor"]) {
        dictionary[@"ram"] = @{
                               @"name": data[@"t1"],
                               @"serialNumber": data[@"t2"],
                               @"calibrationDueDate": data[@"t3"]
                               };
        dictionary[@"gage"] = @{
                               @"name": data[@"t4"],
                               @"serialNumber": data[@"t5"],
                               @"calibrationDueDate": data[@"t6"]
                               };
        dictionary[@"torqueWrench"] = @{
                               @"name": data[@"t7"],
                               @"serialNumber": data[@"t8"],
                               @"calibrationDueDate": data[@"t9"]
                               };
        dictionary[@"material"] = data[@"t10"];
        dictionary[@"method"] = data[@"t11"];
    
    } else if ([_nibName isEqualToString:@"MagneticParticleExaminationTopEditor"]) {
        dictionary[@"materialType"] = data[@"t1"];
        dictionary[@"surfaceCondition"] = data[@"t2"];
        dictionary[@"materialTemp"] = data[@"t3"];
        dictionary[@"examinationStandard"] = data[@"t4"];
        dictionary[@"acceptanceStandard"] = data[@"t5"];
        dictionary[@"ndtProcedureNumber"] = data[@"t6"];
        
        dictionary[@"method"] = @{
                                  @"yoke" : [self xmlValueForCheckboxValue:data[@"t8"]],
                                  @"coil" : [self xmlValueForCheckboxValue:data[@"t9"]],
                                  @"prods" : [self xmlValueForCheckboxValue:data[@"t10"]],
                                  
                                  @"dry" : [self xmlValueForCheckboxValue:data[@"t11"]],
                                  @"wet" : [self xmlValueForCheckboxValue:data[@"t12"]],
                                  @"visible" : [self xmlValueForCheckboxValue:data[@"t13"]],
                                  @"fluorescent" : [self xmlValueForCheckboxValue:data[@"t14"]],
                                  
                                  @"AC" : [self xmlValueForCheckboxValue:data[@"t15"]],
                                  @"DC" : [self xmlValueForCheckboxValue:data[@"t16"]],
                                  @"Rectified" : [self xmlValueForCheckboxValue:data[@"t17"]]
                                  };
        
        dictionary[@"amperage"] = data[@"t18"];
        dictionary[@"prodSpacing"] = data[@"t19"];
        dictionary[@"particleColors"] = data[@"t20"];
        
        dictionary[@"equipment"] = @{
                                     @"SN" : data[@"t21"],
                                     @"modelNumber" : data[@"t23"],
                                     @"mfg" : data[@"t25"]
                                     };
        
        dictionary[@"blackLight"] = @{
                                      @"SN" : data[@"t22"],
                                      @"modelNumber" : data[@"t24"],
                                      @"mfg" : data[@"t26"]
                                      };
        
        dictionary[@"particle"] = @{
                                     @"SN" : data[@"t27"],
                                     @"modelNumber" : data[@"t29"],
                                     @"mfg" : data[@"t31"]
                                     };
        
        dictionary[@"lightMeter"] = @{
                                    @"SN" : data[@"t28"],
                                    @"modelNumber" : data[@"t30"],
                                    @"mfg" : data[@"t32"]
                                    };
    } else if ([_nibName isEqualToString:@"MagneticParticleExaminationItemEditor"]) {
        dictionary[@"pcNumberSnNumber"] = data[@"t1"];
        dictionary[@"acc"] = data[@"t2"];
        dictionary[@"rej"] = data[@"t3"];
        dictionary[@"defect"] = data[@"t4"];
        dictionary[@"remarks"] = data[@"t5"];
        
    } else if ([_nibName isEqualToString:@"LiquidPenetrantExaminationTopEditor"]) {
        dictionary[@"materialType"] = data[@"t1"];
        dictionary[@"surfaceCondition"] = data[@"t2"];
        dictionary[@"materialTemp"] = data[@"t3"];
        dictionary[@"examinationStandard"] = data[@"t4"];
        dictionary[@"acceptanceStandard"] = data[@"t5"];
        dictionary[@"ndtProcedureNumber"] = data[@"t6"];
        
        dictionary[@"method"] = @{
                                 @"visible": [self xmlValueForCheckboxValue:data[@"t8"]],
                                 @"fluorescent": [self xmlValueForCheckboxValue:data[@"t9"]],
                                 
                                 @"emulsified": [self xmlValueForCheckboxValue:data[@"t10"]],
                                 @"postEmulsified": [self xmlValueForCheckboxValue:data[@"t11"]],
                                 
                                 @"waterWashable": [self xmlValueForCheckboxValue:data[@"t12"]],
                                 @"solventRemovable": [self xmlValueForCheckboxValue:data[@"t13"]]
                                  };
        dictionary[@"penetrant"] = @{
                                     @"brand" : data[@"t15"],
                                     @"batchNumber" : data[@"t15"],
                                     @"dwellTime" : data[@"t16"]
                                     };
        
        dictionary[@"cleaner"] = @{
                                   @"brand" : data[@"t17"],
                                   @"batchNumber" : data[@"t18"]
                                 };
        
        dictionary[@"emulsifier"] = @{
                                      @"brand" : data[@"t19"],
                                      @"batchNumber" : data[@"t20"],
                                      @"emulsificationTime" : data[@"t21"]
                                    };
        
        dictionary[@"developer"] = @{
                                     @"brand" : data[@"t22"],
                                     @"batchNumber" : data[@"t23"],
                                     @"devTime" : data[@"t24"],
                                     @"dry": [self xmlValueForCheckboxValue:data[@"t25"]],
                                     @"dry": [self xmlValueForCheckboxValue:data[@"t26"]],
                                     @"dry": [self xmlValueForCheckboxValue:data[@"t27"]],
                                     };
        
    } else if ([_nibName isEqualToString:@"LiquidPenetrantExaminationItemEditor"] || [_nibName isEqualToString:@"UltrasonicExaminationMiddleItemEditor"]) {
        dictionary[@"lineNumber"] = data[@"t1"];
        dictionary[@"floor"] = data[@"t2"];
        dictionary[@"line"] = data[@"t3"];
        dictionary[@"row"] = data[@"t4"];
        dictionary[@"dir"] = data[@"t5"];
        dictionary[@"fig"] = data[@"t6"];
        dictionary[@"acc"] = data[@"t7"];
        dictionary[@"rej"] = data[@"t8"];
        dictionary[@"welder"] = data[@"t9"];
        dictionary[@"detail"] = data[@"t10"];
    
    } else if ([_nibName isEqualToString:@"UltrasonicExaminationBottomItemEditor"]) {
        dictionary[@"linenumber"] = data[@"t1"];
        dictionary[@"thickn"] = data[@"t2"];
        dictionary[@"ang"] = data[@"t3"];
        dictionary[@"face"] = data[@"t4"];
        dictionary[@"decibels"] = @{
                                    @"A": data[@"t5"],
                                    @"B": data[@"t6"],
                                    @"C": data[@"t7"],
                                    @"D": data[@"t8"]
                                    };
        dictionary[@"decibels"] = @{
                                    @"length": data[@"t9"],
                                    @"SP": data[@"t10"],
                                    @"distX": data[@"t11"],
                                    @"distY": data[@"t12"]
                                    };
        
        dictionary[@"depth"] = data[@"t13"];
        dictionary[@"rej"] = data[@"t14"];
        dictionary[@"remarks"] = data[@"t15"];
        
    } else if ([_nibName isEqualToString:@"UltrasonicExaminationTopItemEditor"]) {
        dictionary[@"transMfr"] = data[@"t1"];
        dictionary[@"mod"] = data[@"t2"];
        dictionary[@"SN"] = data[@"t3"];
        dictionary[@"angle"] = data[@"t4"];
        dictionary[@"freq"] = data[@"t5"];
        dictionary[@"dia"] = data[@"t6"];
        
    } else if ([_nibName isEqualToString:@"UltrasonicExaminationMiddleEditor"]) {
        dictionary[@"couplantType"] = data[@"t12"];
        dictionary[@"iiwBlock"] = data[@"t13"];
        dictionary[@"iiwBlockSN"] = data[@"t14"];
        dictionary[@"typeDsc"] = data[@"t15"];
        dictionary[@"typeDscSn"] = data[@"t16"];
        dictionary[@"typeRcBlock"] = data[@"t17"];
        dictionary[@"typeRcBlockSn"] = data[@"t18"];
        dictionary[@"otherType"] = data[@"t19"];
        dictionary[@"otherTypeSn"] = data[@"t20"];
        
        NSMutableArray *screenSizes = [NSMutableArray array];
        if ([[self xmlValueForCheckboxValue:data[@"t21"]] isEqualToString:@"yes"]) {
            [screenSizes addObject:@"2.5"];
        }
        if ([[self xmlValueForCheckboxValue:data[@"t22"]] isEqualToString:@"yes"]) {
            [screenSizes addObject:@"5"];
        }
        if ([[self xmlValueForCheckboxValue:data[@"t23"]] isEqualToString:@"yes"]) {
            [screenSizes addObject:@"10"];
        }
        if ([[self xmlValueForCheckboxValue:data[@"t24"]] isEqualToString:@"yes"]) {
            [screenSizes addObject:@"20"];
        }
        if (((NSString*)data[@"t25"]).length > 0) {
            [screenSizes addObject:data[@"t25"]];
        }
        
        dictionary[@"screenSizesUsed"] = @{@"screenSize": screenSizes};
        
    } else if ([_nibName isEqualToString:@"UltrasonicExaminationTopEditor"]) {
        dictionary[@"materialType"] = data[@"t1"];
        dictionary[@"surfaceCondition"] = data[@"t2"];
        dictionary[@"materialTemp"] = data[@"t3"];
        dictionary[@"examinationStandard"] = data[@"t4"];
        dictionary[@"acceptanceStandard"] = data[@"t5"];
        dictionary[@"ndtProcedureNumber"] = data[@"t6"];
        dictionary[@"ultrasonicUnit"] = @{
                                          @"mfr": data[@"t7"],
                                          @"model": data[@"t8"],
                                          @"serialNumber": data[@"t9"]
                                          };
        
        dictionary[@"annualCalDueDate"] = data[@"t10"];
        dictionary[@"dbGainCalDueDate"] = data[@"t11"];
        
    } else if ([_nibName isEqualToString:@"SteelSampleEditor"]) {
        dictionary[@"steelSampleType"] = data[@"t2"];
        dictionary[@"materialDescription"] = data[@"t3"];
        dictionary[@"shapeSize"] = data[@"t4"];
        dictionary[@"lengt"] = data[@"t5"];
        dictionary[@"quantity"] = data[@"t6"];
        
    } else if ([_nibName isEqualToString:@"OtherSampleEditor"]) {
        dictionary[@"containerIdNumber"] = data[@"t2"];
        dictionary[@"numberOfContainers"] = data[@"t3"];
        dictionary[@"containerType"] = data[@"t4"];
        dictionary[@"sampleLocation"] = data[@"t5"];
        dictionary[@"materialDescription"] = data[@"t6"];
        
    }
    
    return dictionary;
}

-(NSString*)xmlValueForCheckboxValue:(NSString*)checkBoxValue {
    if ([checkBoxValue isEqualToString:@"1"]) {
        return @"yes";
    }
    
    return @"no";
}

//-(void) LoadLabSamples
//{
//    NSString *BaseURLString = [NSString stringWithFormat:@"http://174.127.39.132/LIMSApp_Service/ServiceDataServe.svc/Get_All_Master_Sample_Type_Lab"];
//    NSURL *url = [NSURL URLWithString:BaseURLString];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//
//    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//
//    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
//     {
//         NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//         self.LabSamples = [[NSMutableDictionary alloc]init];
//         self.LabSamples = (NSMutableDictionary *) dictionary;
//         NSLog(@"%@",dictionary);
//         NSLog(@"Self.LabSamples.InsideSVCall ===>>> %@",self.LabSamples);
//         [self.LabSamplesTableView reloadData];
//     }];
//}

//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    NSLog(@"self.LabSamples.count === %lu",(unsigned long)self.LabSamples.count);
//    return self.LabSamples.count;
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    NSLog(@"Self.LabSamples.CellForRowAtIndexPath === %@",self.LabSamples);
//    cell.textLabel.text = [self.LabSamples objectForKey:@"SampleType"];
//    return cell;
//}

@end
