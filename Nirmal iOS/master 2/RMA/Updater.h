//
//  Updater.h
//  RMA
//
//  Created by Michael Beteag on 4/14/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Updater : NSObject

+(BOOL)updateNeededForCurrentUser;
+(void)setLastUpdatedDateForCurrentUser;
-(void)updateForms;

@end
