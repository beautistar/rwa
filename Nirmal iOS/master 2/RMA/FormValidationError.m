//
//  FormValidationError.m
//  RMA
//
//  Created by Michael Beteag on 8/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormValidationError.h"

@implementation FormValidationError

+(FormValidationError*)FormValidationErrorWithMessage:(NSString*)message scrollPoint:(CGPoint)point {
    FormValidationError *validationError = [[FormValidationError alloc] init];
    validationError.message = message;
    validationError.scrollPoint = point;
    return validationError;
}

@end
