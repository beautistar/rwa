//
//  TimesheetSelectionViewController.m
//  RMA
//
//  Created by Michael Beteag on 12/18/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "TimesheetSelectionViewController.h"
#import "AppData.h"
#import "FormEditorViewController.h"
#import "Form+Extensions.h"

@interface TimesheetSelectionViewController ()

@end

@implementation TimesheetSelectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    __weak typeof(self) weakSelf = self;
    observer = [[NSNotificationCenter defaultCenter] addObserverForName:@"DidUpdateTimesheets"
                                                                 object:nil
                                                                  queue:nil
                                                             usingBlock:^(NSNotification* notification){
                                                                 [weakSelf.tableView reloadData];
                                                             }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:observer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Time sheets";
    NSError *error = nil;
    
    [NSFetchedResultsController deleteCacheWithName:@"Root"];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:[[AppData sharedData] managedObjectContext]];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];

    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND dailyType ==[c] %@", self.technicianId,@"Timesheet"];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[AppData sharedData] managedObjectContext] sectionNameKeyPath:@"sectionIdentifier" cacheName:@"Root"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"M/d";
    
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    [self.tableView reloadData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController.sections objectAtIndex:section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Form *form = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"Week of %@", [self.dateFormatter stringFromDate:form.startDate]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Form *form = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    FormEditorViewController *formEditorViewController = [[FormEditorViewController alloc] init];
    formEditorViewController.form = form;
    formEditorViewController.title = [NSString stringWithFormat:@"Time sheet - Week of %@", [self.dateFormatter stringFromDate:form.startDate]];
    [self.navigationController pushViewController:formEditorViewController animated:YES];
}


@end
