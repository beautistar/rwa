//
//  BreakpointsCollection.h
//  RMA
//
//  Created by Michael Beteag on 7/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"
#import "NSData+Base64.h"

@interface BreakpointsCollection : NSObject <NSCoding, GRMustacheRendering>

@property (nonatomic, retain) NSMutableArray *breakpoints;
@property CGSize size;

- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;

-(NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error;
+(BreakpointsCollection*)breakpointsCollectionWithValues:(NSMutableArray*)values;

@end
