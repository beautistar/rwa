#import "FormComponentViewEditor.h"

@interface LabSamplesEditor : FormComponentViewEditor <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) FormComponentEditor *editor;
@property int currentSampleType;
@property int currentSampleIndex;
@property NSMutableDictionary *LabSamples;
@property (weak, nonatomic) IBOutlet UITableView *LabSamplesTable;
@property (weak, nonatomic) IBOutlet UISegmentedControl *typeControl;
- (IBAction)sampleTypeChanged:(id)sender;
+(LabSamplesEditor*)standardSampleEditor;  

@end
