//
//  SmallTextInputViewController.m
//  RMA
//
//  Created by Michael Beteag on 6/25/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "SmallTextInputViewController.h"
#import "RMATextField.h"

@interface SmallTextInputViewController ()
@property BOOL isComplete;
@end

@implementation SmallTextInputViewController

- (CGSize) preferredContentSize {
    return CGSizeMake(253, 30);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.textField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    if(!self.isComplete) {
        self.dismissed();
    }
    [super viewWillDisappear:animated];
}

- (IBAction)okButtonPressed:(id)sender {
    [self.textField resignFirstResponder];
    self.isComplete = YES;
    self.complete(self.textField.text);
}


@end
