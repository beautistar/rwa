//
//  SelectionTableViewController.h
//  RMA
//
//  Created by Alexander Roode on 3/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectionTableViewCellDelegate <NSObject>
-(void)didSelectItem:(NSString*)item;
@end

@interface SelectionTableViewController : UITableViewController
@property (nonatomic, strong) NSArray<NSString*> *items;
@property (nonatomic, assign) id<SelectionTableViewCellDelegate> delegate;
@end
