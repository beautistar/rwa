//
//  FormBuilder.h
//  RMA
//
//  Created by Michael Beteag on 7/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Form.h"
#import "Project.h"
#import "Client.h"
#import "Address.h"
#import "GRMustache.h"
#import "TableEditor.h"
#import "TableEditorColumn.h"
#import "TextAreaEditor.h"
#import "SVProgressHUD.h"
#import "DrawingEditor.h"
#import "Employee.h"
#import "DSAFooterEditor.h"
#import "FormDivider.h"
#import "SignatureEditor.h"
#import "FormSection.h"
@class Checklist;

@interface FormBuilder : NSObject

@property (nonatomic, retain) NSString *templateName;
@property (nonatomic, retain) NSString *displayName;
@property (nonatomic, retain) NSMutableDictionary *metadata;
@property (nonatomic, retain) NSMutableArray *editors;
@property (nonatomic, retain) NSMutableArray *sections;
@property (nonatomic, retain) NSMutableArray *checklists;
@property (nonatomic) BOOL nonCompliance;
@property (nonatomic) BOOL forcesNonCompliance;
@property (nonatomic, retain) NSMutableArray *sectionEnablerKeys;

+(FormBuilder*)formBuilderWithForm:(Form*)form;
-(void)saveNewForm:(Form*)form;
-(void)saveToFile:(NSString*)path;
+(FormBuilder*)formBuilderFromFile:(NSString*)path;
//-(NSString*)getRenderedHTML;
-(NSString*)getSignatureBlockHTML;
+(FormBuilder*)timesheetFormBuilderWithDate:(NSDate*)date;

-(NSMutableArray*)allSections;
-(void)removeChecklistAtIndex:(int)index;
-(void)addChecklist:(Checklist*)checklist;

-(void)setSectionRequiringKey:(NSString*)requiredKey enabled:(BOOL)enabled;
-(void)evaluateForcedNonCompliance;
-(void)addMetadataFromForm:(Form *)form;

-(NSArray<NSString*>*)toEmails;
-(NSArray<NSString*>*)ccEmails;

-(NSDictionary*)dictionary;

@end
