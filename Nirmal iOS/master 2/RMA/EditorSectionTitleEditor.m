//
//  EditorSectionTitleEditor.m
//  RMA
//
//  Created by Alexander Roode on 1/21/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "EditorSectionTitleEditor.h"

@implementation EditorSectionTitleEditor

+(instancetype)editorSectionTitleWithText:(NSString*)title {
    EditorSectionTitleEditor *editor = [[EditorSectionTitleEditor alloc] initWithNibName:@"EditorSectionTitleEditor"];
    editor.titleLabel.text = [title uppercaseString];
    
    return editor;
}
@end
