//
//  FullScreenDrawingEditor.m
//  RMA
//
//  Created by Michael Beteag on 9/23/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FullScreenDrawingEditor.h"

@interface FullScreenDrawingEditor ()

@end

@implementation FullScreenDrawingEditor

- (void)viewDidUnload {
    [self setImageEditorView:nil];
    [super viewDidUnload];
}

- (IBAction)doneButtonPressed:(id)sender {
    if(!self.imageEditorView.hasSignature) {
        [self.delegate cancel];
        return;
    }
    if(self.delegate != nil) {
        [self.delegate setImage:self.imageEditorView.signatureImage];
    }
}

- (IBAction)clearButtonPressed:(id)sender {
    [self.imageEditorView erase];
    [self.delegate clear];
}

- (CGSize)preferredContentSize {
    return CGSizeMake(768, 254);
}

@end
