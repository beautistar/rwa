//
//  RMATextField.m
//  RMA
//
//  Created by Michael Beteag on 11/11/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "RMATextField.h"

@implementation RMATextField

-(void)awakeFromNib {
    [super awakeFromNib];
    if (self.numeric) {
        self.keyboardType = UIKeyboardTypePhonePad;
    }
}

-(BOOL)becomeFirstResponder {
    BOOL autocorrect = [[NSUserDefaults standardUserDefaults] boolForKey:@"autocorrect"];
    
    if(!autocorrect) {
        self.spellCheckingType = UITextSpellCheckingTypeYes;
        self.autocorrectionType = UITextAutocorrectionTypeNo;
    }
    
    return [super becomeFirstResponder];
}

@end
