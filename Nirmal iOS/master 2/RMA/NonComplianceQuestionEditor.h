//
//  NonComplianceQuestionEditor.h
//  RMA
//
//  Created by Michael Beteag on 10/28/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"

@interface NonComplianceQuestionEditor : FormComponentViewEditor
@property (weak, nonatomic) IBOutlet UISegmentedControl*segmentedControl;
@property BOOL nonCompliance;
@property (nonatomic, retain) NSString *key;

+(NonComplianceQuestionEditor*)nonComplianceQuestionEditorWithKey:(NSString*)key technician:(BOOL)technician;

@end
