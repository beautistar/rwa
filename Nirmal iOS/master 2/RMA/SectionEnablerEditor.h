//
//  SectionEnablerEditor.h
//  RMA
//
//  Created by Michael Beteag on 11/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"

@interface SectionEnablerEditor : FormComponentViewEditor

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *enablerLabel;
@property (nonatomic, retain) NSString *key;
@property BOOL sectionEnabled;

+(SectionEnablerEditor*)sectionEnablerEditorWithText:(NSString*)text key:(NSString*)key;

@end
