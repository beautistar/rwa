//
//  Timesheet.m
//  RMA
//
//  Created by Michael Beteag on 12/18/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "Timesheet.h"

@implementation Timesheet

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        [self setDRs:[aDecoder decodeObjectForKey:@"DRs"]];
        //[self setSelectedIndex:[aDecoder decodeObjectForKey:@"selectedIndex"]];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.DRs forKey:@"DRs"];
    //[aCoder encodeObject:self.selectedIndex forKey:@"selectedIndex"];
}

+(Timesheet*)timesheetFromDictionary:(NSDictionary*)dictionary {
    Timesheet *timesheet = [[Timesheet alloc] init];
    
    timesheet.DRs = [NSMutableDictionary dictionary];
    timesheet.totals = [NSMutableDictionary dictionary];
    
    NSArray *days = @[@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",@"Sunday"];
    NSArray *hourTypeKeys = @[@"Reg",@"OT",@"Db"];
    
    for(NSString *day in days) {
        if([timesheet.totals objectForKey:day] == nil) {
            NSMutableDictionary *totalsDict = [@{@"Reg":[NSNumber numberWithInt:0],
                                                 @"OT":[NSNumber numberWithInt:0],
                                                 @"Db":[NSNumber numberWithInt:0],
                                                 @"total":[NSNumber numberWithInt:0]} mutableCopy];
            [timesheet.totals setValue:totalsDict forKey:day];
        }
        NSDictionary *dayTotalsDict = [timesheet.totals valueForKey:day];
        
        NSArray *DRArray = [dictionary objectForKey:day];
        if(DRArray == nil) {
            continue;
        }
        for(NSDictionary *dayDict in DRArray) {
            NSString *DR = [dayDict objectForKey:@"dr"];
            if([timesheet.DRs objectForKey:DR] == nil) {
                [timesheet.DRs setValue:[NSMutableDictionary dictionary] forKey:DR];
            }
            NSMutableDictionary *DRDict = [timesheet.DRs objectForKey:DR];
            if([DRDict objectForKey:day] == nil) {
                [DRDict setValue:[NSMutableDictionary dictionary] forKey:day];
            }
            NSMutableDictionary *DRDayDict = [DRDict objectForKey:day];
            
            for(NSString *hourType in hourTypeKeys) {
                NSString *hours = [dayDict objectForKey:hourType];
                if(hours != nil) {
                    int hoursValue = [hours intValue];
                    NSNumber *currentHours = [dayTotalsDict objectForKey:hourType];
                    [dayTotalsDict setValue:[NSNumber numberWithInt:hoursValue +[currentHours intValue]] forKey:hourType];
                    
                    NSNumber *currentTotal = [dayTotalsDict objectForKey:@"total"];
                    [dayTotalsDict setValue:[NSNumber numberWithInt:hoursValue +[currentTotal intValue]] forKey:@"total"];
                    
                    
                    [DRDayDict setValue:hours forKey:hourType];
                }
            }
            
            NSString *task = @"";
            if([dayDict objectForKey:@"task"] != nil) {
                task = [dayDict objectForKey:@"task"];
            }
            [DRDict setValue:task forKey:@"task"];

        }
        
    }
    
    return timesheet;
}

-(NSString*)totalsForDay:(NSString*)day {
    return [self totalsForDay:day forHourType:@"total"];
}

-(NSString*)totalsForDay:(NSString*)day forHourType:(NSString*)hourType {
    NSDictionary *dayDict = [self.totals objectForKey:day];
    if([dayDict objectForKey:hourType] != nil) {
        return [dayDict objectForKey:hourType];
    }
    return @"";
}

-(NSString*)hoursOfType:(NSString*)hourType ForDay:(NSString*)day forDR:(NSString*)DR {
    NSString *hours = @"0";
    NSDictionary *DRdict = [self.DRs objectForKey:DR];
    if(DRdict != nil) {
        NSDictionary *dayDict = [DRdict objectForKey:day];
        if(dayDict != nil) {
            NSString *hourTypeHours = [dayDict objectForKey:hourType];
            if(hourTypeHours.length > 0) {
                hours = hourTypeHours;
            }
        }
    }
    return hours;
}



-(NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                          context:(GRMustacheContext *)context
                         HTMLSafe:(BOOL *)HTMLSafe
                            error:(NSError **)error {
    *HTMLSafe = YES;
    
    NSMutableString *HTML = [@"<table class=\"timeSheet\">"
                            @"<tr><th>Daily Report</th>"
                             @"<th class=\"alt\" colspan=\"3\">Monday</th>"
                             @"<th colspan=\"3\">Tuesday</th>"
                             @"<th class=\"alt\" colspan=\"3\">Wednesday</th>"
                             @"<th colspan=\"3\">Thursday</th>"
                             @"<th class=\"alt\" colspan=\"3\">Friday</th>"
                             @"<th colspan=\"3\">Saturday</th>"
                             @"<th class=\"alt\" colspan=\"3\">Sunday</th></tr>"
                             @"<tr><th>Time Type</th>"
                             @"<th class=\"alt\">Reg</th><th class=\"alt\">OT</th><th class=\"alt\" >Db</th>"
                             @"<th>Reg</th><th>OT</th><th>Db</th>"
                             @"<th class=\"alt\">Reg</th><th class=\"alt\">OT</th><th class=\"alt\" >Db</th>"
                             @"<th>Reg</th><th>OT</th><th>Db</th>"
                             @"<th class=\"alt\">Reg</th><th class=\"alt\">OT</th><th class=\"alt\" >Db</th>"
                             @"<th>Reg</th><th>OT</th><th>Db</th>"
                             @"<th class=\"alt\">Reg</th><th class=\"alt\">OT</th><th class=\"alt\" >Db</th></tr>"
                             mutableCopy];
    NSArray *days = @[@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",@"Sunday"];
    NSArray *hourTypes = @[@"Reg",@"OT",@"Db"];
    for(NSString* DR in [self.DRs allKeys]) {
        NSDictionary *DRDict = [self.DRs objectForKey:DR];
        NSString *task = [DRDict objectForKey:@"task"];
        if(task.length == 0) {
            task = @"";
        }
        [HTML appendFormat:@"<tr><td><b>DR#</b> %@<br/><b>Task Code</b> %@</td>",DR,task];
        for(int i = 0; i < days.count; i++) {
            NSString *class = @"";
            if(i%2==0) {
                class = @" class=\"alt\"";
            }
            NSString *day = [days objectAtIndex:i];
            for(NSString* hourType in hourTypes) {
                [HTML appendFormat:@"<td%@>%@</td>",class,[self hoursOfType:hourType ForDay:day forDR:DR]];
            }
        }
        [HTML appendFormat:@"</tr>"];
    }
    [HTML appendFormat:@"<tr><th>Totals</th>"];
    for(int i = 0; i < days.count; i++) {
        NSString *class = @"";
        if(i%2==0) {
            class = @" class=\"alt\"";
        }
        NSString *day = [days objectAtIndex:i];
        for(NSString* hourType in hourTypes) {
            [HTML appendFormat:@"<td%@>%@</td>",class,[self totalsForDay:day forHourType:hourType]];
        }
    }
    [HTML appendFormat:@"</tr><tr><th>Day Total</th>"];
    for(int i = 0; i < days.count; i++) {
        NSString *class = @"";
        if(i%2==0) {
            class = @" class=\"alt\"";
        }
        NSString *day = [days objectAtIndex:i];
        [HTML appendFormat:@"<td%@ colspan=\"3\">%@</td>",class,[self totalsForDay:day]];
    }
    
    [HTML appendString:@"</tr></table>"];
    return HTML;
}

@end
