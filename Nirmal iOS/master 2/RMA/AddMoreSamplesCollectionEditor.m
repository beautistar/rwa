#import "AddMoreSamplesCollectionEditor.h"
#import "StandardSampleEditor.h"
#import "StandardSample.h"

@implementation AddMoreSamplesCollectionEditor

+(AddMoreSamplesCollectionEditor*)addMoreSampleCollectionEditor {
    AddMoreSamplesCollectionEditor *editor = [AddMoreSamplesCollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        StandardSampleEditor *editor = [StandardSampleEditor standardSampleEditor];
        return editor;
    } key:@"addmoresamples" itemKey:@"addmoresamples" itemName:@"AddMoreSample"];
    
    return editor;
}

-(void)setData:(NSMutableDictionary *)data {
    NSMutableDictionary *modifiedData = [NSMutableDictionary dictionary];
    NSMutableArray *samples = [NSMutableArray array];
    
    NSMutableArray *concreteSamples = data[@"concreteSamples"];
    NSMutableArray *otherSamples = data[@"samples"];
    [samples addObjectsFromArray:concreteSamples];
    [samples addObjectsFromArray:otherSamples];
    
    modifiedData[@"samples"] = samples;
    
    [super setData:modifiedData];
}

// Separate concrete samples from other samples
-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [super getData];
    
    NSArray *samples = [data objectForKey:@"samples"];
    
    NSMutableDictionary *modifiedData = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                        @"concreteSamples":[NSMutableArray array],
                                                                                        @"samples":[NSMutableArray array]
                                                                                        }];
    
    for(NSMutableDictionary *sampleDict in samples) {
        StandardSample *sample = sampleDict[@"sample"];
        
        if([sample.type intValue] == 3) {
            [modifiedData[@"concreteSamples"] addObject:sampleDict];
        } else {
            [modifiedData[@"samples"] addObject:sampleDict];
        }
    }
    
    return modifiedData;
}

-(NSMutableArray<NSNumber*>*)addMoreSampleTypeIndexes {
    NSMutableArray<NSNumber*> *sampleTypes = [NSMutableArray array];
    NSMutableDictionary *data = [super getData];
    
    NSArray *samples = [data objectForKey:@"samples"];
    
    for (NSMutableDictionary *sampleDict in samples) {
        StandardSample *sample = sampleDict[@"sample"];
        
        if (sample != nil) {
            [sampleTypes addObject:sample.index];
        }
    }
    
    return sampleTypes;
}

-(NSDictionary*)dictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    for (int i = 0; i < self.items.count; i++) {
        StandardSampleEditor *item = self.items[i];
        NSString *xmlKey = [self xmlKeyForSampleIndex:item.currentSampleIndex];
        
        // Array containing all samples of a certain index (type)
        NSMutableArray *sampleIndexArray = dictionary[xmlKey];
        if (sampleIndexArray == nil) {
            sampleIndexArray = [NSMutableArray array];
            dictionary[xmlKey] = sampleIndexArray;
        }
        
        NSDictionary *itemDictionary = [item dictionary];
        
        [sampleIndexArray addObject:itemDictionary];
    }
    
    return dictionary;
}

-(NSString*)xmlKeyForSampleIndex:(int)sampleIndex {
    switch (sampleIndex) {
        case 0:
            return @"aggregatesSample";
        case 1:
            return @"soilSample";
        case 2:
            return @"steelSample";
        case 3:
            return @"asphaltSample";
        case 4:
            return @"concreteSample";
        default:
            return @"";
    }
}

@end

