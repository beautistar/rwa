//
//  FormPreviewViewController.h
//  RMA
//
//  Created by Michael Beteag on 7/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormBuilder.h"
#import "AppData.h"
#import "SVProgressHUD.h"
#import <MessageUI/MessageUI.h>
#import "FormApprovalViewController.h"
#import "PDFGenerator.h"
#import "SubmissionManager.h"

@interface FormPreviewViewController : UIViewController <UIWebViewDelegate, MFMailComposeViewControllerDelegate, FormApprovalViewControllerDelegate, PDFGeneratorDelegate, SubmissionManagerDelegate>

@property (nonatomic, retain) Form *form;
@property (nonatomic, retain) FormBuilder *formBuilder;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, retain) NSString *PDFPath;
@property (nonatomic, retain) NSString *PDFFilename;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *signToCompleteButton;
@property (nonatomic, retain) UIPopoverController *approvalPopoverController;
@property BOOL submitting;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property BOOL disableApproval;

- (IBAction)email:(id)sender;
- (IBAction)signToComplete:(id)sender;

-(void)signWithImage:(UIImage *)image clientName:(NSString *)clientName;

@end
