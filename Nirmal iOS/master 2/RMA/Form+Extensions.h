//
//  Form+Extensions.h
//  RMA
//
//  Created by Alexander Roode on 12/18/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "Form.h"

typedef NS_ENUM(NSInteger, FormStatus) {
    FormStatusNotStarted,
    FormStatusIncomplete,
    FormStatusComplete,
    FormStatusResubmit,
    FormStatusCompletedAfterResubmit,
    FormStatusCanceled
};

@interface Form (Extensions)

-(NSString*)sectionIdentifier;
-(NSString*)shortDate;
-(NSArray*)getAvailableChecklists;
-(void)updateNames;
-(BOOL)isPastDue;
-(BOOL)isEditable;

@end
