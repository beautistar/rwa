//
//  Contact.m
//  RMA
//
//  Created by Michael Beteag on 9/25/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "Contact.h"
#import "Form.h"


@implementation Contact

@dynamic name;
@dynamic phone;
@dynamic id;
@dynamic forms;
@dynamic cell;

@end
