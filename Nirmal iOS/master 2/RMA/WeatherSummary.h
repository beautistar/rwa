//
//  WeatherSummary.h
//  RMA
//
//  Created by Alexander Roode on 5/10/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherSummary : NSObject

@property (nonatomic, copy) NSString *locationName;
@property (nonatomic, copy) NSString *summary;
@property (nonatomic, copy) NSString *dateString;
@property (nonatomic) float minTemp;
@property (nonatomic) float maxTemp;
@property (nonatomic) float currentTemp;
@property (nonatomic) float precipChance;

@end
