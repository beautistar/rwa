//
//  AppData.h
//  RMA
//
//  Created by Michael Beteag on 7/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Employee.h"
#import "Form.h"
#import "Address.h"
#import "Client.h"
#import "Project.h"
#import "Contact.h"
#import "Form+Extensions.h"

@class Folder;

@interface AppData : NSObject

@property (nonatomic, retain) NSString *documentsDirectory;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) AFHTTPClient *client;
@property (nonatomic, retain) NSString *currentTechnicianId;

+(id)sharedData;
+(NSString*)webServiceUrl;

-(NSString*)pathToDocumentNamed:(NSString*)filename;
-(NSArray*)getFormsForTechnicianId:(NSString*)technicianId startDate:(NSDate*)startDate endDate:(NSDate*)endDate;
-(int)getIncompleteFormCountForTechnician:(NSString*)technicianId projectId:(NSString*)projectId;
-(void)saveImage:(UIImage*)image ToFilename:(NSString*)filename;
-(NSString*)getGuidFilenameWithExtension:(NSString*)extension;
-(void)setFormStatus:(FormStatus)formStatus forFormId:(NSString*)formId;

-(void)updateForms:(NSArray*)formData;

-(void)syncDocumentsWithArray:(NSArray*)documentArray;
-(Folder*)folderWithPath:(NSString*)path;
-(Folder*)getRootFolder;

-(NSArray*)getDefaultProjects;
-(void)createTimeSheetsForTechnician:(NSString*)technicianId;

-(NSArray*)getUniqueDRsForTechnicianId:(NSString*)technicianId duringWeekOfDate:(NSDate*)date;
-(NSArray*)getFormDatesOfWeekForTechnicianId:(NSString*)technicianId duringWeekOfDate:(NSDate*)date;
-(void)addDebugForms;
-(void)clearLog;
-(void)logHTML:(NSString*)html;

@end
