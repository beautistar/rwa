//
//  SharepointUtility.h
//  RMA
//
//  Created by Michael Beteag on 12/13/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AFHTTPClient;

@interface SharepointUtility : NSObject

@property (nonatomic, retain) NSString *documentsDirectory;
@property (nonatomic, retain) AFHTTPClient *client;
@property (nonatomic, retain) NSURL *rootURL;

+(id)sharedUtilty;
-(NSString*)URLfromPath:(NSString*)path;
-(void)updateDocuments;
+(NSString*)getValueFromSharepointDict:(NSDictionary*)dict withKey:(NSString*)key;
-(void)downloadFileAtURL:(NSString*)urlString toPath:(NSString*)pathString;

@end
