//
//  Client.m
//  RMA
//
//  Created by Alexander Roode on 2/25/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "Client.h"
#import "Project.h"


@implementation Client

@dynamic id;
@dynamic name;
@dynamic projects;

@end
