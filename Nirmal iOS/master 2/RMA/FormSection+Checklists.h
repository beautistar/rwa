//
//  FormSection+Checklists.h
//  RMA
//
//  Created by Alexander Roode on 5/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "FormSection.h"

@interface FormSection (Checklists)

+(FormSection*)checklistsSection;
+(FormSection*)drilledInAnchorInspectionSection;
+(FormSection*)epoxyAdhesiveInformationSection;
+(FormSection*)epoxyAdhesiveInspectionSection;
+(FormSection*)torquePullTestingSection;
+(FormSection*)magneticParticleExaminationSection;
+(FormSection*)liquidPenetrantExaminationSection;
+(FormSection*)ultrasonicExaminationSection;
+(FormSection*)hmaLaydownSection;

@end
