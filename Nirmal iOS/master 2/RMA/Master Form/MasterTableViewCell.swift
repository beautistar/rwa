//
//  MasterTableViewCell.swift
//  RMA
//
//  Created by Shahil Shah on 7/17/18.
//  Copyright © 2018 Forebrain. All rights reserved.
//

import UIKit

class MasterTableViewCell: UITableViewCell {

    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgViewCheckmark: UIImageView!
    
    var parentDelegate:MasterFormViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonDownloadClicked(_ sender: Any) {
        parentDelegate?.buttonDownloadPDFClicked(self.tag)
    }
}
