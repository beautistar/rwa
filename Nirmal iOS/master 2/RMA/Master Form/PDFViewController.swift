//
//  PDFViewController.swift
//  RMA
//
//  Created by Shahil Shah on 6/26/18.
//  Copyright © 2018 Forebrain. All rights reserved.
//

import UIKit
import PDFKit

class PDFViewController: UIViewController {

    @IBOutlet weak var pdfView: PDFView!
    var path:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let pdfDocument = PDFDocument(url: URL(fileURLWithPath: path!)) {
                pdfView.displayMode = .singlePageContinuous
                pdfView.autoScales = true
                pdfView.displayDirection = .vertical
                pdfView.document = pdfDocument
            }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        let fileData = pdfView.document?.dataRepresentation()
        let docUrl = URL(fileURLWithPath: path!)
        do {
            try fileData?.write(to: docUrl, options: .atomic)
        } catch {}
    }
}
