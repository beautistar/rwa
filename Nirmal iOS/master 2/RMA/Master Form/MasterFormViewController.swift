//
//  MasterFormViewController.swift
//  RMA
//
//  Created by Shahil Shah on 6/23/18.
//  Copyright © 2018 Forebrain. All rights reserved.
//

import UIKit
import AFNetworking
import SwiftyJSON
import SVProgressHUD

extension Data
{
    func toString() -> String
    {
        return String(data: self, encoding: .utf8)!
    }
}


class MasterFormViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var data:JSON = []
    var parentDelegate:PhotoEditor?
    var formId:String?
    var dispatchId:String = ""
    var arrPDFForms:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Master Forms List"
        
        if parentDelegate != nil {
            loadForms()
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(rightBarButtonClicked))
        } else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(rightBarButtonClicked))
        }
        
        self.tableView.register(UINib.init(nibName: "MasterTableViewCell", bundle: nil), forCellReuseIdentifier: "MasterTableViewCell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if parentDelegate == nil {
            loadForms()
        }
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        var pdfs = ""
        var pdfIds = ""
        var counter = 0
        for var i in 0..<self.data.count {
            if data[i]["Is_Selected"].stringValue == "Yes" {
                pdfs = pdfs + data[i]["Is_Selected"].stringValue
                pdfIds = pdfIds + "," + data[i]["Form_Id"].stringValue
                counter = counter + 1
            }
        }
        print("#### " + pdfIds)
        print("##### " + pdfs)
        UserDefaults.standard.set(pdfIds, forKey: dispatchId)
        UserDefaults.standard.synchronize()
        self.parentDelegate?.pdfDownloaded(nil, pdfName: String(format: "%d forms", counter))
    }
    
    func fileExists(_ index:Int) -> Bool {
        var docUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        docUrl = docUrl?.appendingPathComponent(String(format: "%@-%d.pdf", dispatchId, self.data[index]["Form_Id"].int!))
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: (docUrl?.path)!) {
            return true
        }
        
        return false
    }
    
    func downloadPDF(_ index:Int) {
        if fileExists(index) {
            print("FILE AVAILABLE")
        } else {
            print("FILE NOT AVAILABLE")
            
            SVProgressHUD.show(withStatus: "Downloading the PDF")
            let filename:String = (data[index]["Doc_Path"].string)!
            let filenameComponents = filename.split(separator: ".")
            let filenameForAPI:String = filenameComponents[0].addingPercentEncoding(withAllowedCharacters: .alphanumerics)!
            let client = AFHTTPClient(baseURL: URL(string: "http://174.127.39.132/LIMSApp_Service/"))
            
            let request = NSMutableURLRequest(url: URL(string: String(format: "http://174.127.39.132/LIMSApp_Service/ServiceFileServe.svc/DownloadFile/MASTER_FORM/%@/pdf", filenameForAPI))!)
            request.httpMethod = "GET"
            
            let operation = AFHTTPRequestOperation(request: request as URLRequest!)
            operation?.setCompletionBlockWithSuccess({ (operation, response) in
                SVProgressHUD.dismiss()
                let fileData = response as! Data
                var docUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
                docUrl = docUrl?.appendingPathComponent(String(format: "%@-%d.pdf", self.dispatchId, self.data[index]["Form_Id"].int!))
                do {
                    try fileData.write(to: docUrl!, options: .atomic)
                    print("#### " + (docUrl?.absoluteURL.relativePath)!)
                    self.showPDF((docUrl?.absoluteURL.relativePath)!)
                } catch {}
            }, failure: { (operation, error) in
                SVProgressHUD.dismiss()
                print(error)
            })
            
            client?.operationQueue.addOperation(operation!)
        }
    }
    
    func loadForms() {
        var url = "http://174.127.39.132/LIMSApp_Service/ServiceDataServe.svc/Get_All_Master_Form"
        if parentDelegate != nil {
            url = "http://174.127.39.132/LIMSApp_Service/ServiceDataServe.svc/Get_All_Master_Form_Active"
        }
        SVProgressHUD.show(withStatus: "Loading master forms")
        let client = AFHTTPClient(baseURL: URL(string: "http://174.127.39.132/LIMSApp_Service/"))
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.setValue("text/json", forHTTPHeaderField: "Content-Type")
        
        let operation = AFHTTPRequestOperation(request: request as URLRequest!)
        operation?.setCompletionBlockWithSuccess({ (operation, response) in
            SVProgressHUD.dismiss()
            do {
                self.data = try JSON(data: response as! Data)
                
                if self.dispatchId != nil {
                    if (UserDefaults.standard.value(forKey: self.dispatchId) != nil) {
                        let strPDFIds:String = UserDefaults.standard.value(forKey: self.dispatchId) as! String
                        let arrPDFIds = strPDFIds.split(separator: ",")
                        for var i in 0..<self.data.count {
                            for var j in 0..<arrPDFIds.count {
                                if self.data[i]["Form_Id"].stringValue == arrPDFIds[j] {
                                   self.data[i]["Is_Selected"] = "Yes"
                                }
                            }
                        }
                    }
                }
            } catch _ {}
            self.tableView.reloadData()
        }, failure: { (operation, error) in
            SVProgressHUD.dismiss()
            print(error)
        })
        
        client?.operationQueue.addOperation(operation!)
    }
    
    func showPDF(_ path:String) {
        let storyboard = UIStoryboard(name: "MasterForm", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PDFViewController") as! PDFViewController
        controller.path = path
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func rightBarButtonClicked() {
        if parentDelegate != nil {
            self.dismiss(animated: true, completion: nil)
        } else {
            let storyboard = UIStoryboard(name: "MasterForm", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AddMasterFormViewController") as! AddMasterFormViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func buttonDownloadPDFClicked(_ index:Int) {
        if fileExists(index) {
            var docUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
            docUrl = docUrl?.appendingPathComponent(String(format: "%@-%d.pdf", dispatchId, self.data[index]["Form_Id"].int!))
            showPDF((docUrl?.path)!)
        } else {
            downloadPDF(index)
        }
    }
}

extension MasterFormViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "MasterTableViewCell", for: indexPath) as! MasterTableViewCell
        cell.tag = indexPath.row
        cell.lblTitle.text = data[indexPath.row]["Form_Name"].string
        cell.lblDesc.text = data[indexPath.row]["Doc_Path"].string
        cell.parentDelegate = self
        
        if fileExists(indexPath.row) {
            cell.btnDownload.setTitle("View", for: .normal)
        } else{
            cell.btnDownload.setTitle("Download", for: .normal)
        }
        
        if self.data[indexPath.row]["Is_Selected"] == "Yes" {
            cell.imgViewCheckmark.isHidden = false
        } else {
            cell.imgViewCheckmark.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if parentDelegate != nil {
            if self.data[indexPath.row]["Is_Selected"] == "Yes" {
                self.data[indexPath.row]["Is_Selected"] = "No"
            } else {
                self.data[indexPath.row]["Is_Selected"] = "Yes"
                downloadPDF(indexPath.row)
            }
            tableView.reloadData()
        } else {
            let storyboard = UIStoryboard(name: "MasterForm", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AddMasterFormViewController") as! AddMasterFormViewController
            controller.json = data[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
