//
//  AddMasterFormViewController.swift
//  RMA
//
//  Created by Shahil Shah on 6/22/18.
//  Copyright © 2018 Forebrain. All rights reserved.
//

import UIKit
import MobileCoreServices
import AFNetworking
import SwiftyJSON
import SVProgressHUD

extension Date {
    var ticks: Double {
        return NSDate().timeIntervalSince1970 //UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

class AddMasterFormViewController: UITableViewController {
    var data:Data?
    var pathType:String = "MASTER_FORM"
    var fileName: String = ""
    var formId:String = ""
    var json:JSON?
    
    @IBOutlet weak var textFieldFormName: UITextField!
    @IBOutlet weak var switchIsActive: UISwitch!
    @IBOutlet weak var lblForm: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Master Form"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(rightBarButtonClicked))
        if json != nil {
            textFieldFormName.text = json!["Form_Name"].string
            lblForm.text = "Select Form > " + json!["Doc_Path"].string!
            pathType = "MASTER_FORM_UPDATED"
            formId = json!["Form_Id"].stringValue
        }
    }

    func showErrorMessage(_ title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
        })
        self.present(alert, animated: true)
    }
    
    func rightBarButtonClicked() {
        if textFieldFormName.text == "" {
            showErrorMessage("Error", message: "Please enter the form name")
            return
        }
        if data == nil  && json == nil {
            showErrorMessage("Error", message: "Please select a file to upload")
            return
        }

        uploadForm()
    }

    func generateRandomNumber(min: Int, max: Int) -> Int {
        let randomNum = Int(arc4random_uniform(UInt32(max) - UInt32(min)) + UInt32(min))
        return randomNum
    }
    
    func uploadForm() {
        let fileName = String(format: "%d", generateRandomNumber(min: 100, max: 5000))
        if formId == "" {
            formId = fileName
        }
        
        SVProgressHUD.show(withStatus: "Uploading the PDF form")
        let client = AFHTTPClient(baseURL: URL(string: "http://174.127.39.132/LIMSApp_Service/"))
        let request = NSMutableURLRequest(url: URL(string: String(format: "http://174.127.39.132/LIMSApp_Service/ServiceFileServe.svc/UploadFile?pathType=%@&fileName=%@.pdf", pathType, fileName))!)
        request.httpMethod = "POST"
        request.setValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
        request.httpBody = data
        
        let operation = AFHTTPRequestOperation(request: request as URLRequest!)
        operation?.setCompletionBlockWithSuccess({ (operation, response) in
            var data = response as! Data
            SVProgressHUD.dismiss()
            SVProgressHUD.show(withStatus: "Uploading the form")
            let submissionManager:SubmissionManager? = SubmissionManager(delegate: self as SubmissionManagerDelegate)
            submissionManager?.submitMasterForm(self.textFieldFormName.text, active: self.switchIsActive.isOn, formId: "0" /*self.formId*/, fileName: fileName + ".pdf")
        }, failure: { (operation, error) in
            print(error)
        })
        
        client?.operationQueue.addOperation(operation!)
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
//            let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text"], in: .import)
            let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.data"], in: .import)
            documentPicker.delegate = self as UIDocumentPickerDelegate
            present(documentPicker, animated: true, completion: nil)
        }
    }
}

extension AddMasterFormViewController: SubmissionManagerDelegate {
    func getFormsDidSucceed() {
        
    }
    
    func hoursSubmissionDidSucceed() {
        print("")
    }
    
    func xmlSubmissionDidSucceed() {
        print("")
    }
    
    func submissionDidFail(_ error: Error!) {
        SVProgressHUD.dismiss()
        print("save failed")
    }
    
    func formUploadDidSucceed() {
        SVProgressHUD.dismiss()
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddMasterFormViewController: UIDocumentPickerDelegate {
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print(url.absoluteString)
        let arrFileComponents = url.absoluteString.split(separator: "/")
        lblForm.text = "Select Form > " + String(arrFileComponents[(arrFileComponents.count - 1)])
        do {
            data = try NSData(contentsOf: url, options: NSData.ReadingOptions()) as Data
        } catch {
            print(error)
        }
    }
}
