//
//  PhotoEditor.m
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "PhotoEditor.h"
#import "RMATextField.h"
#import "Photo.h"
#import "UIImage+Resize.h"
#import "NSData+Base64.h"
#import "Constants.h"
#import "RMA-Swift.h"
#import "AppDelegate.h"

@interface PhotoEditor ()
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) RMATextField *captionView;
@property (nonatomic, strong) UIButton *chooseImageButton;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) UIPopoverController *popoverController;
@end

@implementation PhotoEditor

-(void)setPhoto:(Photo *)photo {
    _photo = photo;
    [self updateLayout];
}

-(void)updateLayout {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (self.photo.image == nil) {
        self.chooseImageButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [self addSubview:self.chooseImageButton];
        if (self.pageType == PDFPageType1x1 || self.pageType == PDFPageType2x1 || self.pageType == PDFPageType2x2) {
            [self.chooseImageButton setTitle:@"Choose Master Forms" forState:UIControlStateNormal];
        } else {
            [self.chooseImageButton setTitle:@"Choose photo" forState:UIControlStateNormal];
        }
        
        [self.chooseImageButton addTarget:self action:@selector(chooseImageButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.chooseImageButton sizeToFit];
        
        [self setNeedsLayout];
        return;
    }
    
    self.captionView = [[RMATextField alloc] initWithFrame:CGRectMake(0, 0, 0, 30)];
    self.captionView.maxLength = 50;
    self.captionView.borderStyle = UITextBorderStyleRoundedRect;
    self.captionView.text = self.photo.caption;
    self.captionView.delegate = self;
    [self addSubview:self.captionView];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.backgroundColor = [UIColor colorWithRed:0.95f green:0.95f blue:0.95f alpha:1.0f];
    [self addSubview:self.imageView];
    self.imageView.image = self.photo.image;
    
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.deleteButton setImage:[UIImage imageNamed:@"icon-delete"] forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(deleteButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.deleteButton];
    [self setNeedsLayout];
}

- (void)updateButtonTitle { //TODO
    if (self.pageType == PDFPageType1x1 || self.pageType == PDFPageType2x1 || self.pageType == PDFPageType2x2) {
        [self.chooseImageButton setTitle:@"Choose Master Forms" forState:UIControlStateNormal];
    } else {
        [self.chooseImageButton setTitle:@"Choose photo" forState:UIControlStateNormal];
    }
}

-(void)layoutSubviews {
    [super layoutSubviews];
    int padding = 2;
    //////------
    self.chooseImageButton.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    self.chooseImageButton.frame = CGRectMake(150, 100, 200, 50);
    
    self.captionView.frame = CGRectMake(self.bounds.origin.x + padding,
                                        self.frame.size.height - self.captionView.frame.size.height - padding,
                                        self.bounds.size.width - 2 * padding,
                                        self.captionView.frame.size.height);
    
    self.imageView.frame = CGRectMake(padding, padding, self.bounds.size.width - 2 * padding,
               self.captionView.frame.origin.y - 2 * padding);
    
    self.deleteButton.frame = CGRectMake(kPaddingX,
                                         kPaddingY + 10,
                                         kDeleteButtonWidth,
                                         kDeleteButtonHeight);
}

- (void)pdfDownloaded:(NSURL *)url pdfName:(NSString *)pdfName {
    _url = url;
    [_chooseImageButton setTitle:pdfName forState:UIControlStateNormal];
    [self.popoverController dismissPopoverAnimated:YES];
}

-(void)chooseImageButtonPressed {
    if (self.pageType == PDFPageType1x1 || self.pageType == PDFPageType2x1 || self.pageType == PDFPageType2x2) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MasterForm" bundle:nil];
        MasterFormViewController *masterFormViewController = [storyboard instantiateViewControllerWithIdentifier:@"MasterFormViewController"];
        masterFormViewController.parentDelegate = self;
        masterFormViewController.dispatchId = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).dispatchId;
        UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:masterFormViewController];
        [self.window.rootViewController presentViewController:navC animated:YES completion:nil];
    } else {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        self.popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
        
    }
    
    [self.popoverController presentPopoverFromRect:self.chooseImageButton.frame inView:self.chooseImageButton.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self.popoverController dismissPopoverAnimated:YES];
    UIImage *pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    CGSize resizedSize = CGSizeMake(700, 700);
    switch (self.pageType) {
        case PhotoPageType2x1:
            resizedSize = CGSizeMake(700, 350);
            break;
        case PhotoPageType2x2:
            resizedSize = CGSizeMake(350, 350);
        default:
            break;
    }
    
    UIImage *resizedImage = [UIImage imageWithImage:pickedImage scaledToMaxSize:resizedSize];

    self.photo.image = resizedImage;
    [self updateLayout];
}

-(void)deleteButtonPressed {
    self.photo.image = nil;
    self.photo.caption = nil;
    [self updateLayout];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    self.photo.caption = textField.text;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [self.delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

@end
