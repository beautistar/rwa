//
//  TableEditor.m
//  RMA
//
//  Created by Michael Beteag on 7/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "TableEditor.h"
#import "TableEditorRow.h"
#import "RMATextField.h"

@implementation TableEditor

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.view = self;
    }
    return self;
}

+(TableEditor*)tableEditorWithTitle:(NSString*)title columns:(NSArray*)columns rows:(NSArray*)rows key:(NSString*)key {
    TableEditor *editor = [[TableEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 0) title:title columns:columns rows:rows key:key editable:NO];
    return editor;
}

- (id)initWithFrame:(CGRect)frame columns:(NSArray*)columns key:(NSString*)key {
    return [self initWithFrame:frame title:nil columns:columns rows:nil key:key editable:YES];
}

- (id)initWithFrame:(CGRect)frame title:(NSString*)title columns:(NSArray*)columns rows:(NSArray*)rows key:(NSString*)key editable:(BOOL)editable
{
    self = [self initWithFrame:CGRectMake(frame.origin.x,
                                          frame.origin.y,
                                          frame.size.width,
                                          3*kRowHeight + 2*kVerticalPadding)];
    self.view.autoresizesSubviews = NO;
    self.columns = columns;
    self.key = key;
    //self.view.backgroundColor = [UIColor redColor];
    
    self.titleHeight = 0;
    if(title != nil) {
        self.titleHeight = kTitleHeight;
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kHorizontalPadding,
                                                                        kVerticalPadding,
                                                                        self.frame.size.width - 2*kHorizontalPadding,
                                                                        kTitleHeight - 2*kVerticalPadding)];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [self.view addSubview:titleLabel];
    }

    int availableWidth = self.frame.size.width - kHorizontalPadding*2 - kAddButtonWidth;
    int totalWidthUnits = 0;
    // Get width of 1 "width unit"
    for (TableEditorColumn *column in self.columns) {
        totalWidthUnits += column.width;
    }
    self.unitWidth = availableWidth/totalWidthUnits;
    int currentWidthUnit = 0;
    // Setup labels from columns & first row of text fields
    for(int i = 0; i < self.columns.count; i ++) {
        TableEditorColumn *column = [self.columns objectAtIndex:i];
        int width = column.width * self.unitWidth - 2*kHorizontalPadding;
        int x = 2*kHorizontalPadding + kAddButtonWidth + currentWidthUnit * self.unitWidth;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x,
                                                                   kVerticalPadding + self.titleHeight,
                                                                   width,
                                                                   kRowHeight)];
        label.text = column.displayName;
        label.numberOfLines = 0;
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:label];
        
        
        currentWidthUnit += column.width;

    }
    self.rowCount = 0;
    if(self.editable) {
        self.addButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.addButton.frame = CGRectMake(kHorizontalPadding,
                                    kVerticalPadding + kRowHeight + self.titleHeight,
                                    kAddButtonWidth,
                                     kRowHeight);
        
        [self.addButton setTitle:@"+" forState:UIControlStateNormal];
        [self.addButton addTarget:self action:@selector(addRow) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.addButton];
        [self addRow];
    }
    
    if(rows != nil) {
        self.rows = rows;
        for(int i = 0; i < self.rows.count; i++) {
            TableEditorRow *row = [self.rows objectAtIndex:i];
            
            [self addRow];
            for(int j = 0; j < row.fixedValues.count; j++) {
                NSString *rowLabel = [row.fixedValues objectAtIndex:j];
                int tag = i * (int)self.columns.count + 1 + j;
                
                UILabel *label = (UILabel*)[self.view viewWithTag:tag];
                label.text = rowLabel;
            }
        }
    }
    
    
    
    
    return self;
}

-(void)addRow {
    self.view.frame = CGRectMake(self.view.frame.origin.x,
                                 self.view.frame.origin.y,
                                 self.view.frame.size.width,
                                 self.view.frame.size.height + kRowHeight);
    if(self.editable) {
        UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        deleteButton.frame = CGRectMake(kHorizontalPadding,
                                         kVerticalPadding + (self.rowCount + 1)*kRowHeight + self.titleHeight,
                                         kAddButtonWidth,
                                         kRowHeight - 2 * kVerticalPadding);
        deleteButton.tag = -1*(self.rowCount + 1);
        [deleteButton addTarget:self action:@selector(deleteRow:) forControlEvents:UIControlEventTouchUpInside];
        [deleteButton setTitle:@"-" forState:UIControlStateNormal];
        [self.view addSubview:deleteButton];
    }
    
    int currentWidthUnit = 0;
    
    for(int i = 0; i < self.columns.count; i ++) {
         TableEditorColumn *column = [self.columns objectAtIndex:i];
        int width = column.width * self.unitWidth - 2*kHorizontalPadding;
        int x = 2*kHorizontalPadding + kAddButtonWidth + currentWidthUnit * self.unitWidth;
        CGRect frame = CGRectMake(x,
                                  (self.rowCount+1) * kRowHeight + kVerticalPadding + self.titleHeight,
                                  width,
                                  kRowHeight - 2 * kVerticalPadding);
        int tag = (int)self.rowCount * (int)self.columns.count + i + 1;
        if(column.editable) {
            RMATextField *textField = [[RMATextField alloc] initWithFrame:frame];
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.tag = tag;
            textField.delegate = self;
            
            if(column.maxLength > 0) {
                textField.maxLength = column.maxLength;
            }
            
            [self.view addSubview:textField];
        } else {
            UILabel *label = [[UILabel alloc] initWithFrame:frame];
            label.tag = tag;
            label.textAlignment = NSTextAlignmentCenter;
            [self.view addSubview:label];
        }
        
        currentWidthUnit += column.width;
    }
    
    if(self.editable) {
        self.addButton.frame = CGRectMake(self.addButton.frame.origin.x,
                                          self.addButton.frame.origin.y + kRowHeight,
                                          self.addButton.frame.size.width,
                                          self.addButton.frame.size.height);
    }
    self.rowCount++;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FormDidChangeSize" object:[NSNumber numberWithInt:kRowHeight]];
}

-(void)deleteRow:(UIButton*)sender {
    int rowToDelete = -1 * (int)sender.tag;
    
    if(self.rowCount == 1) {
        for(UIView *view in self.view.subviews) {
            if(view.tag > 0) {
                ((UITextField*)view).text = @"";
            }
        }
        return;
    }
    
    for(UIView *view in self.view.subviews) {
        // Check if view is in deleted row
        if(view.tag == -1 * rowToDelete) {
            [view removeFromSuperview];
            continue;
        } else if(view.tag > self.columns.count * (rowToDelete - 1) &&
                  view.tag <= self.columns.count * rowToDelete) {
            NSLog(@"delete tag %li", (long)view.tag);
            [view removeFromSuperview];
            continue;
        }
    
        
        // Check if view should be moved up 1 row height & renumbered
        if(view.tag < (-1 * rowToDelete) || view.tag > ((NSInteger)self.columns.count * rowToDelete)) {
            if(view.tag < 0) {
                view.tag += 1;
            } else {
                view.tag -= self.columns.count;
            }
            
            view.frame = CGRectMake(view.frame.origin.x,
                                    view.frame.origin.y - kRowHeight,
                                    view.frame.size.width,
                                    view.frame.size.height);
        }
    }
    self.addButton.frame = CGRectMake(self.addButton.frame.origin.x,
                                      self.addButton.frame.origin.y - kRowHeight,
                                      self.addButton.frame.size.width,
                                      self.addButton.frame.size.height);
    self.rowCount--;
    self.view.frame = CGRectMake(self.view.frame.origin.x,
                                 self.view.frame.origin.y,
                                 self.view.frame.size.width,
                                 self.view.frame.size.height - kRowHeight);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FormDidChangeSize" object:[NSNumber numberWithInt:-1*kRowHeight]];
}

// Returns dictionary containing 1 key value pair - value is an array of dictionaries
// containing the data for each row
-(NSMutableDictionary*)getData {
    
    NSMutableArray *tableData = [[NSMutableArray alloc] init];
    for(int i = 0; i < self.rowCount; i++) {
        [tableData addObject:[[NSMutableDictionary alloc] init]];
    }
    for(UIView *view in self.view.subviews) {
        if(view.tag < 1) {
            continue;
        }
        int row = (int)(view.tag - 1) / (int)self.columns.count;
        int colIndex = (view.tag - 1) % (int)self.columns.count;
        TableEditorColumn *column = [self.columns objectAtIndex:colIndex];
        NSString *key = column.key;
        NSString *text = @"";
        if(column.editable) {
            text = ((UITextField*)view).text;
        } else {
            UILabel *label = (UILabel*)view;
            if(label.text.length > 0 ) {
                text = label.text;
            }
        }
        NSMutableDictionary *rowData = [tableData objectAtIndex:row];
        [rowData setObject:text forKey:key];
    }
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setObject:tableData forKey:self.key];
    return data;
    
}

// Sets up UITextViews with the data in the same format in getData
-(void)setData:(NSMutableDictionary*)data {
    NSMutableDictionary *keyMappings = [[NSMutableDictionary alloc] init];
    for(int i = 0; i < self.columns.count; i++) {
        TableEditorColumn *column = [self.columns objectAtIndex:i];
        [keyMappings setObject:[NSNumber numberWithInt:i] forKey:column.key];
    }
    
    NSMutableArray *tableData = [data objectForKey:self.key];
    while(self.rowCount < tableData.count) {
        [self addRow];
    }
    
    for(int i = 0; i < tableData.count; i++) {
        NSMutableDictionary *rowData = [tableData objectAtIndex:i];
        
        for (NSString *key in [rowData allKeys]) {
            
            int tag = 0;
            if([keyMappings objectForKey:key] != nil) {
                tag = i * (int)self.columns.count + ([[keyMappings objectForKey:key] intValue] + 1);
            }
            if(tag > 0) {
                if([self.view viewWithTag:tag] != nil) {
                    int colIndex = (tag - 1) % self.columns.count;
                    TableEditorColumn *column = [self.columns objectAtIndex:colIndex];
                    
                    if(column.editable) {
                        ((UITextField*)[self.view viewWithTag:tag]).text = [rowData objectForKey:key];
                    }
                }
            }
        }
    }
}


@end
