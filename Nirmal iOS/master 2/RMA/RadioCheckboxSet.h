//
//  RadioCheckboxSet.h
//  RMA
//
//  Created by Michael Beteag on 8/27/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"
#import "NSData+Base64.h"

@interface RadioCheckboxSet : NSObject <NSCoding, GRMustacheRendering>

@property (nonatomic, retain) NSMutableArray *values;
@property (nonatomic, retain) NSNumber *selectedIndex;

- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;

+(RadioCheckboxSet*)radioCheckboxSetFromUISegmentedControl:(UISegmentedControl*)control;

-(NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error;

@end
