//
//  RescheduleViewController.h
//  RMA
//
//  Created by Alexander Roode on 5/10/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class Form;

@protocol RescheduleViewControllerDelegate <NSObject>
-(void)didRescheduleForm:(Form*)form;
@end

@interface RescheduleViewController : UIViewController <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) Form *form;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) id<RescheduleViewControllerDelegate> delegate;

@end
