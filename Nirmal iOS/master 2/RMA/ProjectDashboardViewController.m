//
//  ProjectDashboardViewController.m
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "ProjectDashboardViewController.h"
#import "ProjectDashboardPhotoCell.h"
#import "ProjectDashboardPhoto.h"
#import "RMACollectionViewCell.h"
#import "Project.h"
#import "Client.h"
#import "BreadcrumbCell.h"
#import "Folder.h"
#import "AppData.h"
#import "Document.h"
#import "PDFViewer.h"
#import "NSString+Additions.h"
#import "FullScreenWebViewViewController.h"
#import "FormSelectionViewController.h"
#import "TimesheetSelectionViewController.h"
#import "SharepointUtility.h"
#import "Document+Extensions.h"
#import "NSMutableArray+Shuffling.h"
#import "Forecastr.h"
#import "WeatherSummary.h"
#import "WeatherSummaryCell.h"
#import "RMA-Swift.h"
#import "AppDelegate.h"

@interface ProjectDashboardViewController ()
@property (nonnull, strong) NSTimer *imageRotatorTimer;
@property (nonnull, strong) NSArray *rotatingImages;
@property (nonatomic) int rotatingIndex;
@property (nonatomic, strong) LocationManager *locationManager;
@property (nonatomic, strong) WeatherSummary *weatherSummary;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) CLLocation* location;
@end

@implementation ProjectDashboardViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    __weak typeof(self) weakSelf = self;
    observer = [[NSNotificationCenter defaultCenter] addObserverForName:@"DidUpdateDocuments"
                                                                 object:nil
                                                                  queue:nil
                                                             usingBlock:^(NSNotification* notification){
                                                                 if(weakSelf.currentFolder != nil) {
                                                                     weakSelf.currentFolder = [[AppData sharedData] folderWithPath:self.currentFolder.path];
                                                                     [weakSelf reloadFieldFiles];
                                                                     ProjectDashboardBreadcrumb *breadcrumb = [self.breadcrumbs lastObject];
                                                                     breadcrumb.folder = self.currentFolder;
                                                                     [weakSelf.breadcrumbCollectionView reloadData];
                                                                     [weakSelf.refreshControl endRefreshing];
                                                                 }
                                                                 [weakSelf.refreshControl endRefreshing];
                                                             }];
    
    UIImage *largeImage = [self largeImageForProjectId:[self.project objectForKey:@"Project"]];
#ifdef SITESCAN
    largeImage = [UIImage imageNamed:@"sitescan-header"];
#endif
    
    if(largeImage != nil) {
        self.largeImageView.image = largeImage;
    } else {
        [self setupImageRotator];
    }
    
    [[LocationManager sharedManager] startUpdatingLocation];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:observer];
    self.navigationController.toolbarHidden = YES;
    
    [self.imageRotatorTimer invalidate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.weatherSummary = [[WeatherSummary alloc] init];
    self.edgesForExtendedLayout = UIRectEdgeNone;
//    self.dateFormatter = [[NSDateFormatter alloc] init];
//    self.dateFormatter.dateFormat = @"MMM d, yyyy";
//
//    [LocationManager sharedManager].delegate = self;
//    [[Forecastr sharedManager] setApiKey:@"3ad8e7c5fcb864cf41d4ade1ed957c61"];
//    [[Forecastr sharedManager] setCacheExpirationInMinutes:60];
    
    if (self.project != nil) {
        self.projectNameLabel.text = [self.project objectForKey:@"Descr"];
        self.projectIdLabel.text = [self.project objectForKey:@"Project"];
        self.clientNameLabel.text = [self.project objectForKey:@"ClientName"];
    } else {
        self.projectNameLabel.text = @"RMA Mobile App";

#ifdef GEOSCIENCE
        self.projectNameLabel.text = @"RMA GeoScience";
#endif
#ifdef CVT
        self.projectNameLabel.text = @"Central Valley Testing";
#endif
#ifdef SITESCAN
        self.projectNameLabel.text = @"SiteScan";
#endif
        
        self.clientNameLabel.text = @"All Clients";
        self.projectIdLabel.text = @"RMA";
    }

    [self.breadcrumbCollectionView registerClass:[BreadcrumbCell class] forCellWithReuseIdentifier:@"BreadcrumbCell"];
    [self.photoCollectionView registerClass:[ProjectDashboardPhotoCell class] forCellWithReuseIdentifier:@"ProjectDashboardPhotoCell"];
	[self.mainCollectionView registerClass:[RMACollectionViewCell class] forCellWithReuseIdentifier:@"RMACollectionViewCell"];
    [self.mainCollectionView registerClass:[WeatherSummaryCell class] forCellWithReuseIdentifier:@"WeatherCell"];
	
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshDocs:) forControlEvents:UIControlEventValueChanged];
    //[self.mainCollectionView addSubview:refreshControl];
    self.mainCollectionView.alwaysBounceVertical = YES;
    self.refreshControl = refreshControl;
    
    //self.photos = [self photosForProjectId:[self.project objectForKey:@"Project"]];
    //[self.photoCollectionView reloadData];
    
    self.currentSection = DashboardSectionTypeMain;
        
    self.breadcrumbs = [NSMutableArray array];
    [self addBreadcrumbWithName:@"Project Dashboard" forSection:DashboardSectionTypeMain];
    //[self addBreadcrumbWithName:@"Admin Settings" forSection:DashboardSectionTypeMain1];
}

-(void)refreshDocs:(UIRefreshControl*) sender {
    [sender beginRefreshing];
    [[SharepointUtility sharedUtilty] updateDocuments];
    
}

-(UIImage*)largeImageForProjectId:(NSString*)projectId {
    if([projectId isEqualToString:@"13-942-0"]) {
        return [UIImage imageNamed:@"13-942-0-large.png"];
    } else if([projectId isEqualToString:@"12-090-0"]) {
        return [UIImage imageNamed:@"12-090-0-large.png"];
    }
    
    return nil;
}

-(void)setupImageRotator {
    NSMutableArray *images = [NSMutableArray arrayWithArray:@[@"rma-dashboard1.jpg",
                                                              @"rma-dashboard2.jpg",
                                                              @"rma-dashboard3.jpg",
                                                              @"rma-dashboard4.jpg",
                                                              @"rma-dashboard5.jpg"]];
    [images shuffle];
                              
                              
    
    self.rotatingImages = images;
    self.rotatingIndex = 0;
    [self updateLargeImage];
    
    self.imageRotatorTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(rotateLargeImage) userInfo:nil repeats:YES];
}

-(void)updateLargeImage {
    UIImage *image = [UIImage imageNamed:self.rotatingImages[self.rotatingIndex]];
    self.largeImageView.image = image;
}

-(void)rotateLargeImage {
    self.rotatingIndex++;
    if (self.rotatingIndex == self.rotatingImages.count) {
        self.rotatingIndex = 0;
    }
    [self updateLargeImage];
}

-(NSArray*)photosForProjectId:(NSString*)projectId {
    if([projectId isEqualToString:@"13-942-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"photo-cupertino.png"]] ,
                 [ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"photo-apple2.png"]],
                 [ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"photo-apple3.png"]],
                 [ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"photo-apple4.png"]],
                 [ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"photo-apple5.png"]]];
    } else if([projectId isEqualToString:@"09-098-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"09-098-0-img01.png"]]];
    } else if([projectId isEqualToString:@"11-038-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-038-0-img01.png"]],
                 [ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-038-0-img02.png"]],
                 [ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-038-0-img03.png"]]];
    } else if([projectId isEqualToString:@"11-068-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-038-0-img01.png"]],
                 [ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-038-0-img02.png"]]];
    } else if([projectId isEqualToString:@"11-038-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-038-0-img01.png"]],
                 [ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-038-0-img02.png"]],
                 [ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-038-0-img03.png"]]];
    } else if([projectId isEqualToString:@"11-582-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-582-0-img01.png"]]];
    } else if([projectId isEqualToString:@"11-583-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-583-0-img01.png"]]];
    } else if([projectId isEqualToString:@"11-615-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"11-615-0-img01.png"]]];
    } else if([projectId isEqualToString:@"12-88-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"12-88-0-img01.png"]]];
    } else if([projectId isEqualToString:@"12-090-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"12-090-0-img01.png"]]];
    } else if([projectId isEqualToString:@"12-710-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"12-710-0-img01.png"]]];
    } else if([projectId isEqualToString:@"12-823-0"]) {
        return @[[ProjectDashboardPhoto photoWithImage:[UIImage imageNamed:@"12-823-0-img01.png"]]];
    }
    
    return @[];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(collectionView == self.photoCollectionView) {
        return self.photos.count;
    }
    if(collectionView == self.breadcrumbCollectionView) {
        return self.breadcrumbs.count;
    }
    
    if(collectionView == self.mainCollectionView) {
        switch (self.currentSection) {
            case DashboardSectionTypeMain:
                return 1;
            case DashboardSectionTypeMain1:
                return 1;
            case DashboardSectionTypeFieldFile:
                return self.subfolders.count + self.documents.count;
            case DashboardSectionTypeReports:
                return 1;
        }
    }
    return 0;
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == self.breadcrumbCollectionView) {
        ProjectDashboardBreadcrumb *breadcrumb = [self.breadcrumbs objectAtIndex:indexPath.item];
        CGSize size = [breadcrumb.name sizeWithMyFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f]];
        return CGSizeMake(size.width + 40, 50);
    }
    if(collectionView == self.photoCollectionView) {
        ProjectDashboardPhoto *photo = [self.photos objectAtIndex:indexPath.item];
        UIImage *image = photo.image;
        CGFloat width = image.size.width;
        CGFloat height = image.size.height;
        if (height > 160) {
            width = width * (height / 160);
            height = 160;
        }
        return CGSizeMake(width, 210);
    }
    if(collectionView == self.mainCollectionView) {
        return CGSizeMake(200, 200);
    }
    return CGSizeMake(0, 0);
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.breadcrumbCollectionView) {
        static NSString *CellIdentifier = @"BreadcrumbCell";
        BreadcrumbCell *cell = (BreadcrumbCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        ProjectDashboardBreadcrumb *breadcrumb = [self.breadcrumbs objectAtIndex:indexPath.item];
        cell.label.text = breadcrumb.name;
        
        if(indexPath.item == self.currentSection) {
            cell.backgroundColor = [UIColor whiteColor];
        } else {
            cell.backgroundColor = self.breadcrumbCollectionView.backgroundColor;
        }
        
        return cell;
    }
    if (collectionView == self.photoCollectionView) {
        static NSString *CellIdentifier = @"ProjectDashboardPhotoCell";
        ProjectDashboardPhotoCell *cell = (ProjectDashboardPhotoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        ProjectDashboardPhoto *photo = [self.photos objectAtIndex:indexPath.item];
        cell.imageView.image = photo.image;

        return cell;
    } else if(collectionView == self.mainCollectionView) {
        static NSString *CellIdentifier = @"RMACollectionViewCell";
        if (self.currentSection == DashboardSectionTypeMain && indexPath.item == 1) {
            WeatherSummaryCell *cell = (WeatherSummaryCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"WeatherCell" forIndexPath:indexPath];
            cell.weatherSummary = self.weatherSummary;
            return cell;
        }
        
        RMACollectionViewCell *cell = (RMACollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        
        switch (self.currentSection) {
            case DashboardSectionTypeMain: {
                switch (indexPath.item) {
                    case 0:
                        cell.imageView.image = [UIImage imageNamed:@"icon-inspectionitems.png"];
                        cell.label.text = @"Open\nInspection Items";
                        break;
                    case 2:
                        cell.imageView.image = [UIImage imageNamed:@"icon-fieldfile.png"];
                        cell.label.text = @"Field File";
                        break;
                    case 3:
                        cell.imageView.image = [UIImage imageNamed:@"icon-timesheets.png"];
                        cell.label.text = @"Time Sheets";
                        break;
                    case 4:
                        cell.imageView.image = [UIImage imageNamed:@"icon-reports"];
                        cell.label.text = @"Reports";
                        break;
                }
                break;
            }
            case DashboardSectionTypeMain1: {
                switch (indexPath.item) {
                    case 0:
                        cell.imageView.image = [UIImage imageNamed:@"icon-inspectionitems.png"];
                        cell.label.text = @"Master Form";
                        break;
                    case 2:
                        cell.imageView.image = [UIImage imageNamed:@"icon-fieldfile.png"];
                        cell.label.text = @"Field File";
                        break;
                    case 3:
                        cell.imageView.image = [UIImage imageNamed:@"icon-timesheets.png"];
                        cell.label.text = @"Time Sheets";
                        break;
                    case 4:
                        cell.imageView.image = [UIImage imageNamed:@"icon-reports"];
                        cell.label.text = @"Reports";
                        break;
                }
                break;
            }
            case DashboardSectionTypeFieldFile: {
                if(indexPath.item < self.currentFolder.subfolders.count) {
                    cell.imageView.image = [UIImage imageNamed:@"icon-folder.png"];
                    Folder *folder = [self.subfolders objectAtIndex:indexPath.item];
                    cell.label.text = folder.name;
                }
                else {
                    int folderCount = (int)self.subfolders.count;
                    Document *document = [self.documents objectAtIndex:indexPath.item - folderCount];
                    cell.imageView.image = nil;
                    cell.label.text = document.name;
                }
                break;
            }
            case DashboardSectionTypeReports: {
                switch (indexPath.item) {
                    case 0:
                        cell.imageView.image = [UIImage imageNamed:@"icon-folder.png"];
                        cell.label.text = @"DR Lookup";
                        break;
                    case 1:
                        cell.imageView.image = [UIImage imageNamed:@"icon-folder.png"];
                        cell.label.text = @"Correspondence";
                        break;
                }
                break;
            }
        }
        
        return cell;
    }
    return nil;
    
}

-(void)reloadFieldFiles {
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    self.documents = [self.currentFolder.documents sortedArrayUsingDescriptors:sortDescriptors];
    self.subfolders = [self.currentFolder.subfolders sortedArrayUsingDescriptors:sortDescriptors];
    [self.mainCollectionView reloadData];
}

-(void)addBreadcrumbWithName:(NSString*)name forSection:(DashboardSectionType)section {
    ProjectDashboardBreadcrumb *breadcrumb = [[ProjectDashboardBreadcrumb alloc] init];
    breadcrumb.sectionType = section;
    breadcrumb.name = [name uppercaseString];
    [self.breadcrumbs addObject:breadcrumb];
    [self.breadcrumbCollectionView reloadData];
}

-(void)addBreadcrumbForFolder:(Folder*)folder name:(NSString*)name {
    ProjectDashboardBreadcrumb *breadcrumb = [[ProjectDashboardBreadcrumb alloc] init];
    breadcrumb.folder = folder;
    breadcrumb.name = [name uppercaseString];
    [self.breadcrumbs addObject:breadcrumb];
    [self.breadcrumbCollectionView reloadData];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.photoCollectionView) {
        return;
    }
    if (collectionView == self.breadcrumbCollectionView) {
        ProjectDashboardBreadcrumb *breadcrumb = [self.breadcrumbs objectAtIndex:indexPath.item];
        
//        NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:(NSRange){indexPath.item + 1, self.breadcrumbs.count - indexPath.item - 1}];
//        [self.breadcrumbs removeObjectsAtIndexes:set];
//        [self.breadcrumbCollectionView reloadData];
        
        if (breadcrumb.folder != nil) {
            self.currentSection = DashboardSectionTypeFieldFile;
            self.currentFolder = breadcrumb.folder;
            [self reloadFieldFiles];
        } else {
            self.currentSection = breadcrumb.sectionType;
            [self.breadcrumbCollectionView reloadData];
            [self.mainCollectionView reloadData];
        }
        return;
    }
    switch (self.currentSection) {
        case DashboardSectionTypeMain: {
            switch (indexPath.item) {
                case 0: {
                    FormSelectionViewController *formSelectionViewController = [[FormSelectionViewController alloc] init];
                    formSelectionViewController.technicianId = self.technicianId;
                    formSelectionViewController.projectId = [self.project objectForKey:@"Project"];
                    [self.navigationController pushViewController:formSelectionViewController animated:YES];
                    break;
                }
                case 1: {
                    [self openForecastForLocation:self.location];
                    break;
                }
                case 2: {
                    Folder *rootFolder = nil;
                    if([self.project objectForKey:@"Project"] !=nil) {
                        rootFolder = [[AppData sharedData] folderWithPath:[NSString stringWithFormat:@"rma/Documents/%@",[self.project objectForKey:@"Project"]]];
                    } else {
                        rootFolder = [[AppData sharedData] getRootFolder];
                    }
                    if(rootFolder != nil) {
                        [self addBreadcrumbForFolder:rootFolder name:@"Field File"];
                        self.currentFolder = rootFolder;
                        [self reloadFieldFiles];
                    } else {
                        [self addBreadcrumbWithName:@"Field File" forSection:DashboardSectionTypeFieldFile];
                    }
                    
                    self.currentSection = DashboardSectionTypeFieldFile;
                    [self.mainCollectionView reloadData];
                    break;
                }
                case 3: {
                    [[AppData sharedData] createTimeSheetsForTechnician:self.technicianId];
                    TimesheetSelectionViewController *timesheetSelectionViewController = [[TimesheetSelectionViewController alloc] init];
                    timesheetSelectionViewController.technicianId = self.technicianId;
                    [self.navigationController pushViewController:timesheetSelectionViewController animated:YES];
                    break;
                }
                case 4:
                    [self addBreadcrumbWithName:@"Reports" forSection:DashboardSectionTypeReports];
                    self.currentSection = DashboardSectionTypeReports;
                    [self.mainCollectionView reloadData];
                    break;
            }
            break;
        }
        case DashboardSectionTypeMain1: {
            switch (indexPath.item) {
                case 0: {
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    appDelegate.dispatchId = @"";
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MasterForm" bundle:nil];
                    MasterFormViewController *masterFormViewController = [storyboard instantiateViewControllerWithIdentifier:@"MasterFormViewController"];
                    [self.navigationController pushViewController:masterFormViewController animated:YES];
                    break;
                }
                case 1: {
                    [self openForecastForLocation:self.location];
                    break;
                }
                case 2: {
                    Folder *rootFolder = nil;
                    if([self.project objectForKey:@"Project"] !=nil) {
                        rootFolder = [[AppData sharedData] folderWithPath:[NSString stringWithFormat:@"rma/Documents/%@",[self.project objectForKey:@"Project"]]];
                    } else {
                        rootFolder = [[AppData sharedData] getRootFolder];
                    }
                    if(rootFolder != nil) {
                        [self addBreadcrumbForFolder:rootFolder name:@"Field File"];
                        self.currentFolder = rootFolder;
                        [self reloadFieldFiles];
                    } else {
                        [self addBreadcrumbWithName:@"Field File" forSection:DashboardSectionTypeFieldFile];
                    }
                    
                    self.currentSection = DashboardSectionTypeFieldFile;
                    [self.mainCollectionView reloadData];
                    break;
                }
                case 3: {
                    [[AppData sharedData] createTimeSheetsForTechnician:self.technicianId];
                    TimesheetSelectionViewController *timesheetSelectionViewController = [[TimesheetSelectionViewController alloc] init];
                    timesheetSelectionViewController.technicianId = self.technicianId;
                    [self.navigationController pushViewController:timesheetSelectionViewController animated:YES];
                    break;
                }
                case 4:
                    [self addBreadcrumbWithName:@"Reports" forSection:DashboardSectionTypeReports];
                    self.currentSection = DashboardSectionTypeReports;
                    [self.mainCollectionView reloadData];
                    break;
            }
            break;
        }
        case DashboardSectionTypeFieldFile: {
            if (indexPath.item < self.currentFolder.subfolders.count) {
                Folder *folder = [self.subfolders objectAtIndex:indexPath.item];
                self.currentFolder = folder;
                [self addBreadcrumbForFolder:folder name:folder.name];
                [self reloadFieldFiles];
            }
            else {
                int folderCount = (int)self.subfolders.count;
                Document *document = [self.documents objectAtIndex:indexPath.item - folderCount];
                if ([[NSFileManager defaultManager] fileExistsAtPath:[document getAbsolutePath]]) {
                    [[PDFViewer sharedViewer] viewPDF:[document getAbsolutePath] withTitle:document.name];
                } else {
                    [SVProgressHUD showErrorWithStatus:@"Document not yet downloaded - plase wait"];
                }
                
                
            }
            break;
        }
        case DashboardSectionTypeReports: {
            switch (indexPath.item) {
                case 0: {
                    FullScreenWebViewViewController *webViewController = [[FullScreenWebViewViewController alloc] init];
                    webViewController.url = [NSURL URLWithString:@"http://174.127.39.132/DRLookUp/Default.aspx"];
                    webViewController.username = @"12-793-0";
                    webViewController.password = @"password";
                    webViewController.title = @"DR Lookup";
                    [self.navigationController pushViewController:webViewController animated:YES];
                    break;
                }
                case 1: {
                    FullScreenWebViewViewController *webViewController = [[FullScreenWebViewViewController alloc] init];
                    webViewController.url = [NSURL URLWithString:@"https://www.dropbox.com/sh/kwby2m2xov339qr/bG0FvBAK9n"];
                    webViewController.title = @"Correspondence";
                    [self.navigationController pushViewController:webViewController animated:YES];
                    break;
                }
            }
            break;
        }
    }
}

-(void)openForecastForLocation:(CLLocation*)location {
    if (location == nil) {
        return;
    }
    
    NSString *URLString = [NSString stringWithFormat:@"http://forecast.io/#/f/%f,%f", location.coordinate.latitude, location.coordinate.longitude];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLString]];
}

#pragma mark LocationManagerDelegate

- (void)didAcquireLocation:(CLLocation *)location {
    self.location = location;
    self.weatherSummary = [[WeatherSummary alloc] init];
    [[Forecastr sharedManager] getForecastForLatitude:location.coordinate.latitude longitude:location.coordinate.longitude time:nil exclusions:nil extend:nil success:^(id JSON) {
        [self updateWeather:JSON];
    } failure:^(NSError *error, id response) {
        NSLog(@"Error while retrieving forecast: %@", [[Forecastr sharedManager] messageForError:error withResponse:response]);
    }];

    // Set the location name
    [[LocationManager sharedManager] findNameForLocation:location];
}

- (void)didFailToAcquireLocationWithErrorMessage:(NSString *)errorMessage {
    NSLog(@"Failed to find location: %@", errorMessage);
}

- (void)didFindLocationName:(NSString *)locationName {
    self.weatherSummary.locationName = locationName;
    [self refreshLocationCell];
}

- (void)updateWeather:(NSDictionary*)forecast {
    NSDictionary *currently = forecast[@"currently"];
    NSDictionary *daily = forecast[@"daily"];
    
    if (currently[@"time"] != nil) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[currently[@"time"] intValue]];
        self.weatherSummary.dateString = [self.dateFormatter stringFromDate:date];
    } else {
        self.weatherSummary.dateString = @"";
    }
    
    self.weatherSummary.currentTemp = [currently[@"temperature"] floatValue];
    self.weatherSummary.summary = currently[@"summary"];
    
    if (daily != nil) {
        NSArray *dailyData = daily[@"data"];
        NSDictionary *today = [dailyData firstObject];
        
        self.weatherSummary.precipChance = [today[@"precipProbability"] floatValue];
        self.weatherSummary.maxTemp = [today[@"temperatureMax"] floatValue];
        self.weatherSummary.minTemp = [today[@"temperatureMin"] floatValue];
    } else {
        self.weatherSummary.minTemp = 0;
        self.weatherSummary.maxTemp = 0;
        self.weatherSummary.precipChance = 0;
    }
    
    [self refreshLocationCell];
}

- (void)refreshLocationCell {
    [self.mainCollectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:1 inSection:0]]];
}

@end
