//
//  PhotoPageEditor.m
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "PhotoPageEditor.h"
#import "PhotoPage.h"
#import "Photo.h"
#import "PhotoEditor.h"
#import "Constants.h"

@interface PhotoPageEditor ()
@property (nonatomic, strong) PhotoPage *photoPage;
@property (nonatomic, strong) NSMutableArray<PhotoEditor*> *photoEditorViews;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) NSString *strItemType;
@end

@implementation PhotoPageEditor

+(instancetype)photoPageEditorWithPhotoPage:(PhotoPage*)photoPage itemType:(NSString *)itemType {
    Float32 newHeight;
    if ([itemType isEqualToString:@"Master Forms"])
    {
        newHeight = kPhotoPageRowHeight;
    }
    else
    {
        newHeight = kPhotoPageHeaderHeight + 2 * kPhotoPageRowHeight;
    }
    PhotoPageEditor *editor = [[PhotoPageEditor alloc] initWithFrame:CGRectMake(0, 0, kEditorWidth, newHeight)];
    editor.photoPage = photoPage;
    [editor layoutPhotoEditors:itemType];
    return editor;
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:self.photoPage forKey:@"photoPage"];
    return data;
}

-(void)setData:(NSMutableDictionary *)data {
    PhotoPage *page = data[@"photoPage"];
    self.photoPage = page;
    [self layoutPhotoEditors:@""];
}

-(BOOL)validates:(NSMutableArray**)validationErrors referenceView:(UIView*)referenceView {
    
    BOOL validationSuccess = YES;
    for(UIView *cell in self.photoEditorViews) {
        BOOL cellSuccess = [self requireAllFields:cell.subviews validationErrors:validationErrors referenceView:referenceView];
        validationSuccess = validationSuccess && cellSuccess;
    }
    
    return validationSuccess;
}

-(void)layoutPhotoEditors:(NSString *)itemType {
    _strItemType = itemType;
    [self.photoEditorViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.photoEditorViews = [NSMutableArray array];
    
    if (self.label == nil) {
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kPhotoPageHeaderHeight)];
        self.label.textAlignment = NSTextAlignmentCenter;

        self.label.font = [UIFont boldSystemFontOfSize:17.0f];
        [self addSubview:self.label];
    }
    
    NSString *pageTypeTitle;
    switch (self.photoPage.pageType) {
        case PhotoPageType1x1:
            pageTypeTitle = [NSString stringWithFormat:@"1 %@", _strItemType];
            break;
        case PhotoPageType2x1:
            pageTypeTitle = [NSString stringWithFormat:@"2 %@", _strItemType];
            break;
        case PhotoPageType2x2:
            pageTypeTitle = [NSString stringWithFormat:@"4 %@", _strItemType];
            break;
        case PDFPageType1x1:
            pageTypeTitle = [NSString stringWithFormat:@"1 %@", _strItemType];
            break;
        case PDFPageType2x1:
            pageTypeTitle = [NSString stringWithFormat:@"2 %@", _strItemType];
            break;
        case PDFPageType2x2:
            pageTypeTitle = [NSString stringWithFormat:@"4 %@", _strItemType];
            break;
    }
    self.label.text = [NSString stringWithFormat:@"Photo Page - %@", pageTypeTitle];
    
    for (int i = 0; i < self.photoPage.photos.count; i++) {
        Photo *photo = self.photoPage.photos[i];
        PhotoEditor *photoEditorView = [[PhotoEditor alloc] initWithFrame:CGRectZero];
        photoEditorView.delegate = self;
        photoEditorView.photo = photo;
        photoEditorView.pageType = self.photoPage.pageType;
        [photoEditorView updateButtonTitle];
        [self.view addSubview:photoEditorView];
        [self.photoEditorViews addObject:photoEditorView];
        
        switch (self.photoPage.pageType) {
            case PhotoPageType1x1:
                photoEditorView.frame = CGRectMake(0, kPhotoPageHeaderHeight, kEditorWidth, 2*kPhotoPageRowHeight);
                break;
            case PhotoPageType2x1:
                photoEditorView.frame = CGRectMake(0, kPhotoPageHeaderHeight + (i / 1) * kPhotoPageRowHeight, kEditorWidth, kPhotoPageRowHeight);
                break;
            case PhotoPageType2x2:
                photoEditorView.frame = CGRectMake((i % 2) * kEditorWidth / 2, kPhotoPageHeaderHeight + (i / 2) * kPhotoPageRowHeight, kEditorWidth / 2, kPhotoPageRowHeight);
                break;
            case PDFPageType1x1:
                photoEditorView.frame = CGRectMake(0, kPhotoPageHeaderHeight, kEditorWidth, 2*kPhotoPageRowHeight);
                break;
            case PDFPageType2x1:
                photoEditorView.frame = CGRectMake(0, kPhotoPageHeaderHeight + (i / 1) * kPhotoPageRowHeight, kEditorWidth, kPhotoPageRowHeight);
                break;
            case PDFPageType2x2:
                photoEditorView.frame = CGRectMake((i % 2) * kEditorWidth / 2, kPhotoPageHeaderHeight + (i / 2) * kPhotoPageRowHeight, kEditorWidth / 2, kPhotoPageRowHeight);
                break;
        }
    }
}


@end
