//
//  FormInfoViewController.m
//  RMA
//
//  Created by Michael Beteag on 7/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormInfoViewController.h"
#import "NSDate+Extensions.h"
#import "Form+Extensions.h"
#import "InfoCell.h"

@interface FormInfoViewController ()

@end

@implementation FormInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"InfoCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
}

- (CGSize)preferredContentSize {
    return CGSizeMake(400, 517);
}

-(NSString*)titleTextForIndexPath:(NSIndexPath*)indexPath {
    switch (indexPath.row) {
        case 0:
            return @"DR #";
        case 1:
            return @"Type";
        case 2:
            return @"Date";
        case 3:
            return @"Project";
        case 4:
            return @"Task Code";
        case 5:
            return @"Scope";
        case 6:
            return @"Client";
        case 7:
            return @"Address";
        case 8:
            return @"Special Instructions";
        default:
            break;
    }
    return @"";
}

-(NSString*)detailTextForIndexPath:(NSIndexPath*)indexPath {
    switch (indexPath.row) {
        case 0:
            return self.form.dispatchId;
        case 1:
            return self.form.displayName;
        case 2:
            return [NSString stringWithFormat:@"%@\n%@", self.form.sectionIdentifier, [self.form.startTime timeString]];
        case 3:
            return self.form.project.id;
        case 4:
            return [NSString stringWithFormat:@"%@ - %@", self.form.task, self.form.taskDescription];
        case 5:
            return self.form.scope;
        case 6:
            return self.form.project.client.name;
        case 7:
            return [self getAddress];
        case 8:
            return [self getContact];
        default:
            break;
    }
    return @"";
}


-(NSString*)getContact {
    if(self.form.contact != nil) {
        return [NSString stringWithFormat:@"%@\n%@", self.form.contact.name,
                                  self.form.contact.phone];
    }
    return @"";
}

-(NSString*)getAddress {
    if(self.form.address != nil) {
        NSMutableArray *address = [NSMutableArray array];
        if(self.form.address.address1.length > 0) {
            [address addObject:self.form.address.address1];
        }
        if(self.form.address.address2.length > 0) {
            [address addObject:self.form.address.address2];
        }
        [address addObject:[NSString stringWithFormat:@"%@, %@", self.form.address.city,
                            self.form.address.state]];
        return [address componentsJoinedByString:@"\n"];
    }
    return @"";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *detailText = [self detailTextForIndexPath:indexPath];
    return ceilf([InfoCell heightWithDetailText:detailText width:tableView.frame.size.width]);
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.titleLabel.text = [self titleTextForIndexPath:indexPath];
    cell.detailLabel.text = [self detailTextForIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row == 7) {
        cell.button.enabled = YES;
        cell.button.hidden = NO;
        [cell.button setTitle:@"Map" forState:UIControlStateNormal];
        [cell.button addTarget:self action:@selector(openMap) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

-(void)openMap {
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:[self getAddress]
                     completionHandler:^(NSArray *placemarks, NSError *error) {
                         if(error != nil ) {
                             [SVProgressHUD showErrorWithStatus:@"Error locating address"];
                         }
                         
                         // Convert the CLPlacemark to an MKPlacemark
                         // Note: There's no error checking for a failed geocode
                         CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc]
                                                   initWithCoordinate:geocodedPlacemark.location.coordinate
                                                   addressDictionary:geocodedPlacemark.addressDictionary];
                         
                         // Create a map item for the geocoded address to pass to Maps app
                         MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                         [mapItem setName:geocodedPlacemark.name];
                         
                         // Pass the current location and destination map items to the Maps app
                         // Set the direction mode in the launchOptions dictionary
                         [mapItem openInMapsWithLaunchOptions:nil];
                         
                     }];
    }
}

@end
