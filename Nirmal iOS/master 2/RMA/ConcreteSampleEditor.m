//
//  ConcreteSampleEditor.m
//  RMA
//
//  Created by Michael Beteag on 4/3/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "ConcreteSampleEditor.h"
#import "ConcretePageSetEditorTop.h"
#import "ConcretePageSetBreakPointsEditor.h"
#import "ConcretePageSetEditorBottom.h"

@implementation ConcreteSampleEditor


+(ConcreteSampleEditor*)concreteSampleEditor {
    
    ConcreteSampleEditor *editor = [[ConcreteSampleEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 20)];
    
    FormComponentEditor *topEditor = [ConcretePageSetEditorTop concretePageSetEditorTop];
    FormComponentEditor *breakPointsEditor = [ConcretePageSetBreakPointsEditor concretePageSetBreakPointsEditor];
    FormComponentEditor *bottomEditor = [ConcretePageSetEditorBottom concretePageSetEditorBottom];
    
    [editor addSubview:topEditor];
    [editor addSubview:breakPointsEditor];
    [editor addSubview:bottomEditor];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:editor selector:@selector(layout) name:@"DidChangeSampleCount" object:nil];

    editor.editors = @[topEditor, breakPointsEditor, bottomEditor];
    
    [editor layout];
    
    return  editor;
}

-(void)layout {
    float y = 0;
    for (FormComponentEditor *editor in self.editors) {
        editor.frame = CGRectMake(0, y, editor.view.frame.size.width, editor.view.frame.size.height);
        y += editor.view.frame.size.height;
    }
    
    self.frame = CGRectMake(0, self.frame.origin.y, 768, y);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StandardSampleDidChangeSize" object:nil];
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];

    for (FormComponentEditor *editor in self.editors) {
        [data addEntriesFromDictionary:[editor getData]];
    }
    return data;
}

-(void)setData:(NSMutableDictionary *)data {
    for (FormComponentEditor *editor in self.editors) {
        [editor setData:data];
    }
    
    [self layout];
}

-(BOOL)validates:(NSMutableArray *__autoreleasing *)validationErrors referenceView:(UIView *)referenceView {
    BOOL validationSuccess = YES;
    for (FormComponentEditor *editor in self.editors) {
        editor.validationType = self.validationType;
        
        validationSuccess = [editor validates:validationErrors referenceView:referenceView] && validationSuccess;
    }
    
    return validationSuccess;
}

-(NSDictionary*)dictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    for (FormComponentEditor *editor in self.editors) {
        [dictionary addEntriesFromDictionary:[editor dictionary]];
    }
    
    return dictionary;
}

@end
