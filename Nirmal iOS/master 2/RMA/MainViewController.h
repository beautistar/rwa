//
//  MainViewController.h
//  RMA
//
//  Created by Michael Beteag on 7/12/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppData.h"
#import "FormSelectionViewController.h"
#import "NICSignatureView.h"
#import "ProjectSelectionViewController.h"

@interface MainViewController : UIViewController <ProjectSelectionDelegate> {
    id loginSuccessObserver;
    id loginFailureObserver;
}
@property (weak, nonatomic) IBOutlet UIImageView *logoView;
@property (weak, nonatomic) IBOutlet UITextField *empId;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UILabel *selectedProjectLabel;
@property (nonatomic, retain) UIPopoverController *projectPopoverController;
@property NSDictionary *selectedProject;
@property (weak, nonatomic) IBOutlet UIButton *projectSelectButton;

- (IBAction)selectProjectButtonPressed:(id)sender;

- (IBAction)downloadForms:(id)sender;
@end
