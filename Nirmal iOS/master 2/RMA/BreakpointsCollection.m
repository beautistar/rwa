//
//  BreakpointsCollection.m
//  RMA
//
//  Created by Michael Beteag on 7/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "BreakpointsCollection.h"

@implementation BreakpointsCollection

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        [self setBreakpoints:[aDecoder decodeObjectForKey:@"breakpoints"]];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.breakpoints forKey:@"breakpoints"];
}
+(BreakpointsCollection*)breakpointsCollectionWithValues:(NSMutableArray*)values {
    BreakpointsCollection *collection = [[BreakpointsCollection alloc] init];
    collection.breakpoints = values;
    return collection;
}

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    return [self.breakpoints componentsJoinedByString:@" "];
}
@end
