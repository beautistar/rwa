//
//  SignatureEditor.h
//  RMA
//
//  Created by Michael Beteag on 8/1/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#define kSignButtonWidth 100

#import "FormComponentEditor.h"
#import "NICSignatureView.h"
#import "Image.h"
#import "FullScreenDrawingEditor.h"
#import <QuartzCore/QuartzCore.h>


@interface SignatureEditor : FormComponentEditor <FullScreenDrawingEditorDelegate>

@property (nonatomic, retain) UIPopoverController *popoverController;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UILabel *imageViewLabel;
@property (nonatomic, retain) UIButton *clearButton;
@property (nonatomic, retain) UIButton *signButton;
@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) UIImage *currentImage;
@property BOOL formSigned;

- (id)initWithFrame:(CGRect)frame name:(NSString*)name key:(NSString*)key;

-(NSMutableDictionary*)getData;
-(void)setData:(NSMutableDictionary*)data;
-(void)setImage:(UIImage *)image;
-(void)setLabel:(NSString*)text;

@end
