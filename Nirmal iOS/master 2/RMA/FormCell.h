//
//  FormCell.h
//  RMA
//
//  Created by Michael Beteag on 7/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Form;

@protocol FormCellDelegate <NSObject>
-(void)rescheduleButtonPressedForForm:(Form*)form;
@end

@interface FormCell : UITableViewCell
@property (nonatomic, strong) Form *form;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectLabel;
@property (weak, nonatomic) IBOutlet UILabel *clientLabel;
@property (weak, nonatomic) IBOutlet UILabel *drNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *rescheduleButton;
@property (nonatomic, weak) id<FormCellDelegate> delegate;

+(NSString *) reuseIdentifier;
-(NSString *) reuseIdentifier;
-(IBAction)rescheduleButtonPressed:(id)sender;

@end
