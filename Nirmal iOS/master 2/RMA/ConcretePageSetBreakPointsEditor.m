//
//  ConcretePageSetBreakPointsEditor.m
//  RMA
//
//  Created by Michael Beteag on 11/12/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "ConcretePageSetBreakPointsEditor.h"
#import "ConcretePageSet.h"
#import "BreakpointsCollection.h"
#import "RMATextField.h"
#import "RMASegmentedControl.h"
#import "Constants.h"

@implementation ConcretePageSetBreakPointsEditor

+(ConcretePageSetBreakPointsEditor*)concretePageSetBreakPointsEditor {
    UILabel *numberOfSamplesLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 206, 21)];
    numberOfSamplesLabel.text = @"Number of Samples";
    numberOfSamplesLabel.textAlignment = NSTextAlignmentRight;
    
    ConcretePageSetBreakPointsEditor *editor = [[ConcretePageSetBreakPointsEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 30 + 2 *kPaddingY)];
    editor.numberOfSamplesField = [[RMATextField alloc] initWithFrame:CGRectMake(245, 1, 285, 30)];
    editor.numberOfSamplesField.maxLength = 2;
    editor.numberOfSamplesField.borderStyle = UITextBorderStyleRoundedRect;
    editor.numberOfSamplesField.font = [UIFont systemFontOfSize:14];
    editor.numberOfSamplesField.delegate = editor;
    [editor.view addSubview:numberOfSamplesLabel];
    [editor.view addSubview:editor.numberOfSamplesField];
    
    editor.breakPointSegmentedControls = [NSMutableArray array];
    
    return editor;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *text = textField.text;
    int count = [text intValue];
    if (count > 9) {
        count = 9;
    }
    int lastCount = (int)self.breakPointSegmentedControls.count;
    if (lastCount == count) {
        return;
    }
    int lastHeight = self.view.frame.size.height;
    if (lastCount > count) {
        // Delete controls from count to lastcount
        int controlsToDelete = lastCount - count;
        [self removeControls:controlsToDelete];
        
        NSIndexSet *indices = [[NSIndexSet alloc] initWithIndexesInRange:(NSRange){count,controlsToDelete}];
        [self.breakPointSegmentedControls removeObjectsAtIndexes:indices];
        
    } else {
        for (int i = lastCount; i < count; i++) {
            //Create segmented control
            NSArray *items = @[@"3",@"7",@"14",@"28",@"Hold",@"Other"];
            RMASegmentedControl *control = [[RMASegmentedControl alloc] initWithItems:items];
            control.tag = 1 + 2*i;
            [self.breakPointSegmentedControls addObject:control];
        }
        [self addSegmentedControls:count - lastCount];
    }
    int finalHeight = self.view.frame.size.height;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidChangeSampleCount" object:[NSNumber numberWithInt:finalHeight-lastHeight]];
}

//Delete last countToRemove segmented controls
-(void)removeControls:(int)countToRemove {
    for (int i = (int)self.breakPointSegmentedControls.count; i > self.breakPointSegmentedControls.count - countToRemove; i--) {
        UIView *label = [self.view viewWithTag:2*i];
        [label removeFromSuperview];
        
        RMASegmentedControl *control = (RMASegmentedControl*)[self.view viewWithTag:2*i-1];
        
        if (control != nil) {
            self.view.frame = CGRectMake(self.view.frame.origin.x,
                                         self.view.frame.origin.y,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - control.frame.size.height - kPaddingY);
            [control removeFromSuperview];
        }
    }
    
}
//Add controlsToAdd segmentedControls
-(void)addSegmentedControls:(int)controlsToAdd {
    int currentY = self.view.frame.size.height;
    for (int i = (int)self.breakPointSegmentedControls.count - controlsToAdd; i < self.breakPointSegmentedControls.count; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, currentY+5, 206, 21)];
        label.text = @"Break";
        label.textAlignment = NSTextAlignmentRight;
        label.tag = 2*i + 2;
        [self.view addSubview:label];
        
        RMASegmentedControl *control = [self.breakPointSegmentedControls objectAtIndex:i];
        control.frame = CGRectMake(245,
                                   currentY,
                                   control.frame.size.width,
                                   control.frame.size.height);
        
        [self.view addSubview:control];
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height + control.frame.size.height + kPaddingY);
        currentY += control.frame.size.height + kPaddingY;
    }
}


-(void)setConcretePageSet:(ConcretePageSet*)set {
    self.numberOfSamplesField.text = set.noOfSamples;
    
    [self textFieldDidEndEditing:self.numberOfSamplesField];
    
    BreakpointsCollection *breaks = set.breaks;
    for (int i = 0; i < breaks.breakpoints.count; i++) {
        NSString *breakValue = [breaks.breakpoints objectAtIndex:i];
        RMASegmentedControl *control = (RMASegmentedControl*)[self.view viewWithTag:2*i+1];
        if (control != nil) {
            [control selectText:breakValue];
        }
    }
    
}
-(NSMutableDictionary*)getPartialConcreteSet {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:(self.numberOfSamplesField.text.length > 0 ? self.numberOfSamplesField.text : @"") forKey:@"noOfSamples"];
    
    NSMutableArray *breakValues = [NSMutableArray array];
    for (int i = 0; i < self.breakPointSegmentedControls.count; i++) {
        RMASegmentedControl *control = (RMASegmentedControl*)[self.view viewWithTag:2*i+1];
        if (control != nil) {
            [breakValues addObject:[control text]];
        }
    }
    [data setObject:[BreakpointsCollection breakpointsCollectionWithValues:breakValues] forKey:@"breaks"];
    
    return data;
}

-(NSMutableDictionary*)getData {
    return [self getPartialConcreteSet];
}

-(void)setData:(NSMutableDictionary *)data {
    self.numberOfSamplesField.text = [data objectForKey:@"noOfSamples"];
    
    [self textFieldDidEndEditing:self.numberOfSamplesField];
    
    BreakpointsCollection *breaks = [data objectForKey:@"breaks"];
    for (int i = 0; i < breaks.breakpoints.count; i++) {
        NSString *breakValue = [breaks.breakpoints objectAtIndex:i];
        RMASegmentedControl *control = (RMASegmentedControl*)[self.view viewWithTag:2*i+1];
        if (control != nil) {
            [control selectText:breakValue];
        }
    }
}

-(NSDictionary*)dictionary {
    NSDictionary *data = [self getPartialConcreteSet];
    BreakpointsCollection *collection = data[@"breaks"];
    
    return @{ @"breaks": [collection.breakpoints componentsJoinedByString:@" "] };
}

@end
