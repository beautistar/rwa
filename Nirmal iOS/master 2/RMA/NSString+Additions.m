//
//  NSString+Additions.m
//  RMA
//
//  Created by Michael Beteag on 12/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (StringSizeWithFont)

- (CGSize) sizeWithMyFont:(UIFont *)fontToUse {
    return [self sizeWithAttributes: @{NSFontAttributeName:fontToUse}];
}

-(CGFloat) heightWithFont:(UIFont*)font width:(CGFloat)width {
    
    NSDictionary *attributes = @{NSFontAttributeName: font};
    CGRect rect = [self boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:attributes
                                     context:nil];
    return rect.size.height;
}

@end