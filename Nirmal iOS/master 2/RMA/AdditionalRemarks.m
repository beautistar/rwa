//
//  AdditionalRemarks.m
//  RMA
//
//  Created by Michael Beteag on 10/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "AdditionalRemarks.h"

@implementation AdditionalRemarks

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        [self setRemarksParagraph:[aDecoder decodeObjectForKey:@"remarksParagraph"]];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.remarksParagraph forKey:@"remarksParagraph"];
}

+ (AdditionalRemarks*)additionalRemarksWithParagraph:(Paragraph*)paragraph {
    AdditionalRemarks *additionalRemarks = [[AdditionalRemarks alloc] init];
    additionalRemarks.remarksParagraph = paragraph;
    return additionalRemarks;
}

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    *HTMLSafe = YES;
    NSDictionary *data = @{@"additionalRemarks":self.remarksParagraph};
    
    return [GRMustacheTemplate renderObject:data fromResource:@"additionalRemarks" bundle:nil error:NULL];
}
@end
