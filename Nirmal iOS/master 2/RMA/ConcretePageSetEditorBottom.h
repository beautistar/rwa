//
//  ConcretePageSetEditorBottom.h
//  RMA
//
//  Created by Michael Beteag on 8/13/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"
#import "ConcretePageSet.h"
#import "UISegmentedControl+Text.h"
#import "Paragraph.h"

@interface ConcretePageSetEditorBottom : FormComponentViewEditor

@property (nonatomic, retain) NSString *key;
@property (weak, nonatomic) IBOutlet UITextField *setNoField;
@property (weak, nonatomic) IBOutlet UITextField *mixDesignNoField;
@property (weak, nonatomic) IBOutlet UITextField *mixDesignStrengthNo;
@property (weak, nonatomic) IBOutlet UITextField *admixturesField;
@property (weak, nonatomic) IBOutlet UITextField *noOfSamplesField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *breaksControl;
@property (weak, nonatomic) IBOutlet UITextField *tripTicketNoField;
@property (weak, nonatomic) IBOutlet UITextField *slumpPenField;
@property (weak, nonatomic) IBOutlet UITextField *mtlTempField;
@property (weak, nonatomic) IBOutlet UITextField *airTempField;
@property (weak, nonatomic) IBOutlet UITextField *airContentField;
@property (weak, nonatomic) IBOutlet UITextField *unitWtField;
@property (weak, nonatomic) IBOutlet UITextView *placedAtArea;
@property (weak, nonatomic) IBOutlet UITextField *timeCastField;
@property (nonatomic, retain) UISegmentedControl *sampleTypeControl;
@property (weak, nonatomic) IBOutlet UITextField *truckNoField;
@property (weak, nonatomic) IBOutlet UITextView *curingLocationArea;
@property int setId;

-(void)setConcretePageSet:(ConcretePageSet*)set;
-(NSMutableDictionary*)getPartialConcreteSet;
+(ConcretePageSetEditorBottom*)concretePageSetEditorBottom;

@end
