//
//  NSDate+Extensions.m
//  RMA
//
//  Created by Michael Beteag on 9/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "NSDate+Extensions.h"

@implementation NSDate (Extensions)

+(NSDate*)dateFromISO8601String:(NSString*)string {
    NSString *stringWithoutZ = [string substringToIndex:19];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate *date = [dateFormatter dateFromString:stringWithoutZ];
    return date;
}

-(NSString*)timeString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"h:mm a"];
    
    return [dateFormatter stringFromDate:self];
}


@end
