//
//  Employee.h
//  RMA
//
//  Created by Michael Beteag on 7/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Employee : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *forms;
@end

@interface Employee (CoreDataGeneratedAccessors)

- (void)addFormsObject:(NSManagedObject *)value;
- (void)removeFormsObject:(NSManagedObject *)value;
- (void)addForms:(NSSet *)values;
- (void)removeForms:(NSSet *)values;

@end
