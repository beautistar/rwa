//
//  RMACheckbox.m
//  RMA
//
//  Created by Alexander Roode on 1/22/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "RMACheckbox.h"

@implementation RMACheckbox

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
    }
    return self;
}

-(void)setup {
    [self setImage];
    [self addTarget:self action:@selector(pressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)pressed {
    self.checked = !self.checked;
}

-(void)setChecked:(BOOL)checked {
    _checked = checked;
    [self setImage];
}

-(void)setImage {
    if(self.checked) {
        [self setImage:[UIImage imageNamed:@"icon-checkbox-checked"] forState:UIControlStateNormal];
    } else {
        [self setImage:[UIImage imageNamed:@"icon-checkbox"] forState:UIControlStateNormal];
    }
}

@end
