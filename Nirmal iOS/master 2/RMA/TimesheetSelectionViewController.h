//
//  TimesheetSelectionViewController.h
//  RMA
//
//  Created by Michael Beteag on 12/18/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimesheetSelectionViewController : UITableViewController <NSFetchedResultsControllerDelegate> {
    id observer;
}
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSString *technicianId;
@property (nonatomic, retain) NSDateFormatter *dateFormatter; 
@end
