//
//  TableEditorRow.m
//  RMA
//
//  Created by Michael Beteag on 12/18/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "TableEditorRow.h"

@implementation TableEditorRow
+(TableEditorRow*)tableEditorRowWithFixedValues:(NSArray*)fixedValues {
    TableEditorRow *row = [[TableEditorRow alloc] init];
    row.fixedValues = fixedValues;
    return row;
}
@end
