//
//  DropdownEditor.m
//  RMA
//
//  Created by Alexander Roode on 3/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "DropdownEditor.h"

@interface DropdownEditor ()
@property (nonatomic, strong) UIPopoverController *popoverController;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSArray<NSString*> *options;
@end

@implementation DropdownEditor

+(instancetype)dropdownEditorWithKey:(NSString*)key text:(NSString*)text options:(NSArray<NSString*> *)options {
    DropdownEditor *editor = [[DropdownEditor alloc] initWithNibName:@"DropdownEditor"];
    editor.key = key;
    editor.value = options[0];
    editor.label.text = text;
    editor.options = options;
    [editor updateView];
    return editor;
}

-(void)buttonPressed {
    SelectionTableViewController *selectionTableViewController = [[SelectionTableViewController alloc] init];
    selectionTableViewController.delegate = self;
    selectionTableViewController.items = self.options;
    
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:selectionTableViewController];
    selectionTableViewController.delegate = self;
    [self.popoverController presentPopoverFromRect:self.button.frame inView:self.button.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)updateView {
    self.valueLabel.text = self.value;
}

-(void)didSelectItem:(NSString *)item {
    self.value = item;
    [self updateView];
    [self.popoverController dismissPopoverAnimated:YES];
}

-(void)setData:(NSMutableDictionary *)data {
    self.value = data[self.key];
    [self updateView];
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    if (self.isEnabled && self.value != nil) {
        [data setObject:self.value forKey:self.key];
    }
    
    return data;
}

-(NSDictionary*)dictionary {
    return [self getData];
}

@end
