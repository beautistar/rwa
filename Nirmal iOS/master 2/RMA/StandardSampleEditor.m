//
//  StandardSampleEditor.m
//  RMA
//
//  Created by Michael Beteag on 2/18/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "StandardSampleEditor.h"
#import "StandardSample.h"
#import "ConcreteSampleEditor.h"
#import "LabSamplesEditor.h"

@implementation StandardSampleEditor
- (IBAction)sampleTypeChanged:(id)sender {
    int index = (int)((UISegmentedControl*)sender).selectedSegmentIndex;
    
    [self changeSampleTypeToIndex:index];
}

-(void)changeSampleTypeToIndex:(int)index {
    FormComponentEditor *samplesEditor = nil;
    self.currentSampleIndex = index;
    
//    // Steel
//    if (index == 2) {
//        if (self.currentSampleType != 2) {
//            samplesEditor = [[FormComponentViewEditor alloc] initWithNibName:@"SteelSampleEditor"];
//            self.currentSampleType = 2;
//        }
//    }
    
    // LabSamples
    if (index == 2) {
        if (self.currentSampleType != 2) {
            samplesEditor = [[FormComponentViewEditor alloc] initWithNibName:@"LabSamplesEditor"];
            self.currentSampleType = 2;
        }
    }
    
    // Concrete
    if (index == 4) {
        if (self.currentSampleType != 3) {
            samplesEditor = [ConcreteSampleEditor concreteSampleEditor];
            self.currentSampleType = 3;
        }
    }
    
    // All others
    if (index != 2 && index != 4) {
        if (self.currentSampleType != 1) {
            samplesEditor = [[FormComponentViewEditor alloc] initWithNibName:@"OtherSampleEditor"];
            self.currentSampleType = 1;
        }
    }
    
    if (samplesEditor != nil) {
        
        if (self.editor != nil) {
            [self.editor.view removeFromSuperview];
        }
        
        self.editor = samplesEditor;
        self.editor.validationType = self.validationType;
        
        self.editor.view.frame = CGRectMake(0,
                                            52,
                                            self.editor.view.frame.size.width,
                                            self.editor.view.frame.size.height);
        
        [self.view addSubview:self.editor.view];
        
        [self updateSize];
    }
}

-(void)updateSize {
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width,
                                 52 + self.editor.view.frame.size.height);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CollectionItemDidChangeSize" object:nil];
}

+(StandardSampleEditor*)standardSampleEditor {
    StandardSampleEditor *editor = [[StandardSampleEditor alloc] initWithNibName:@"StandardSampleEditor"];
    editor.currentSampleType = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:editor selector:@selector(updateSize) name:@"StandardSampleDidChangeSize" object:nil];
    return editor;
}


-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    if (self.editor != nil) {
        NSMutableDictionary *editorData = [self.editor getData];
        [editorData addEntriesFromDictionary:[super getData]];
        StandardSample *sample = [StandardSample standardSampleWithType:self.currentSampleType index:(int)self.typeControl.selectedSegmentIndex data:editorData];
        
        [data setObject:sample forKey:@"sample"];
    }
    
    return data;
}

-(void)setData:(NSMutableDictionary *)data {
    if ([data objectForKey:@"sample"] != nil) {
        StandardSample *sample = [data objectForKey:@"sample"];
        self.typeControl.selectedSegmentIndex = [sample.index intValue];
        [self changeSampleTypeToIndex:[sample.index intValue]];
        
        [self.editor setData:sample.data];
    }
    
}

-(BOOL)validates:(NSMutableArray *__autoreleasing *)validationErrors referenceView:(UIView *)referenceView {
    BOOL validationSuccess = YES;
    validationSuccess = [self requireAllFields:self.view.subviews validationErrors:validationErrors referenceView:referenceView] && validationSuccess;
    if (self.editor != nil) {
        validationSuccess = [self.editor validates:validationErrors referenceView:referenceView] && validationSuccess;
    }
    
    return validationSuccess;
}

-(NSDictionary*)dictionary {
    return [self.editor dictionary];
}

@end
