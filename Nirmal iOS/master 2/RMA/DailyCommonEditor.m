//
//  DailyCommonEditor.m
//  RMA
//
//  Created by Alexander Roode on 3/27/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "DailyCommonEditor.h"

@implementation DailyCommonEditor

-(NSMutableDictionary*)getSharedData {
    NSMutableDictionary *sharedData = [NSMutableDictionary dictionary];
    
    UITextField *generalContractorField  = (UITextField*)[self.view viewWithTag:2];
    UITextField *subContractorField  = (UITextField*)[self.view viewWithTag:3];
    UITextField *materialSupplierField  = (UITextField*)[self.view viewWithTag:4];
    
    [sharedData setObject:generalContractorField.text forKey:@"DRGeneralContractor"];
    [sharedData setObject:subContractorField.text forKey:@"DRSubContractor"];
    [sharedData setObject:materialSupplierField.text forKey:@"DRMaterialSupplier"];
    
    return sharedData;
}

@end
