//
//  FormInfoViewController.h
//  RMA
//
//  Created by Michael Beteag on 7/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Form.h"
#import "Client.h"
#import "Project.h"
#import "Address.h"
#import "Contact.h"
#import <MapKit/MapKit.h>
#import "SVProgressHUD.h"

@interface FormInfoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) Form *form;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
