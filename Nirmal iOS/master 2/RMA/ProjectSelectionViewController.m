//
//  ProjectSelectionViewController.m
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "ProjectSelectionViewController.h"
#import "Project.h"
#import "AppData.h"

@interface ProjectSelectionViewController ()

@end

@implementation ProjectSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.projects = [[AppData sharedData] getDefaultProjects];
    [self.tableView reloadData];
}

- (CGSize)preferredContentSize {
    return CGSizeMake(400, 600);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.projects.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if(indexPath.row == self.projects.count) {
        cell.textLabel.text = @"All";
    } else {
        NSDictionary *projectDict = [self.projects objectAtIndex:indexPath.row];
        cell.textLabel.text = [projectDict objectForKey:@"Descr"];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == self.projects.count) {
        [self.delegate didSelectProject:nil];
    } else {
        NSDictionary *projectDict = [self.projects objectAtIndex:indexPath.row];
        [self.delegate didSelectProject:projectDict];
    }
}

@end
