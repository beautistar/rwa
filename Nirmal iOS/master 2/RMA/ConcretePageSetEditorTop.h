//
//  ConcretePageSetEditorTop.h
//  RMA
//
//  Created by Michael Beteag on 8/13/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"
#import "ConcretePageSet.h"
#import "UISegmentedControl+Text.h"
#import "Paragraph.h"
#import "RMASegmentedControl.h"

@interface ConcretePageSetEditorTop : FormComponentViewEditor

@property (nonatomic, retain) NSString *key;
@property (weak, nonatomic) IBOutlet UITextField *setNoField;
@property (weak, nonatomic) IBOutlet UITextField *mixDesignNoField;
@property (weak, nonatomic) IBOutlet UITextField *mixDesignStrengthNo;
@property (weak, nonatomic) IBOutlet UITextField *admixturesField;
@property (nonatomic, retain) RMASegmentedControl *sampleTypeControl;

-(void)setConcretePageSet:(ConcretePageSet*)set;
+(ConcretePageSetEditorTop*)concretePageSetEditorTop;
-(NSMutableDictionary*)getPartialConcreteSet;

@end
