//
//  Employee.m
//  RMA
//
//  Created by Michael Beteag on 7/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "Employee.h"


@implementation Employee

@dynamic id;
@dynamic name;
@dynamic forms;

@end
