//
//  Form+Extensions.m
//  RMA
//
//  Created by Alexander Roode on 12/18/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "Form+Extensions.h"
#import "Checklist.h"

@implementation Form (Extensions)

-(NSString*)sectionIdentifier {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"EEEE, d MMMM yyyy";
    return [formatter stringFromDate:self.startDate];
}

-(NSString*)shortDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/d/yyyy";
    
    return [formatter stringFromDate:self.startDate];
}

-(NSArray*)getAvailableChecklists {
    NSArray *checklistIds = [self.checklists componentsSeparatedByString:@","];
    NSMutableArray *checklists = [NSMutableArray array];
    for (NSString *checklistIdString in checklistIds) {
        int id = [checklistIdString intValue];
        Checklist *checklist = [Checklist checklistWithId:id];
        if(checklist != nil) {
            [checklists addObject:checklist];
        }
    }
    
    //[checklists addObject:[Checklist checklistWithId:170]];
    
    return checklists;
}

-(void)updateNames {
    if (self.name.length > 0) {
        self.displayName = [NSString stringWithFormat:@"%@Daily Field Report | %@",[self.dsa boolValue] ? @"DSA ":@"", self.name];
    } else if (self.dailyType.length > 0){
        self.displayName = [NSString stringWithFormat:@"%@Daily Field Report", [self.dsa boolValue] ? @"DSA ":@""];
    } else {
        self.displayName = @"";
    }
    
    self.shortDisplayName = [self shortNameFromName:self.displayName];
}

-(NSString*)shortNameFromName:(NSString*)name {
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    return [[name componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
}

-(BOOL)isPastDue {
    if (![self isEditable]) {
        return NO;
    }
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = 1;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *dayAfter = [calendar dateByAddingComponents:components toDate:self.startDate options:0];
    
    if ([[NSDate date] compare:dayAfter] == NSOrderedDescending) {
        return YES;
    }
    
    return  NO;
}

-(BOOL)isEditable {
    return !([self.status isEqualToNumber:@(FormStatusComplete)] || [self.status isEqualToNumber:@(FormStatusCompletedAfterResubmit)] || [self.status isEqualToNumber:@(FormStatusCanceled)]);
}

@end
