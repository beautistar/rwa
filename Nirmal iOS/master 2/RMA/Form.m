//
//  Form.m
//  RMA
//
//  Created by Alexander Roode on 2/25/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "Form.h"
#import "Address.h"
#import "Contact.h"
#import "Employee.h"
#import "Project.h"


@implementation Form

@dynamic checklists;
@dynamic dailyType;
@dynamic dispatchId;
@dynamic displayName;
@dynamic dsa;
@dynamic dsaAppNumber;
@dynamic dsaFileNumber;
@dynamic dsaLEANumber;
@dynamic hours;
@dynamic localFilename;
@dynamic name;
@dynamic remarks;
@dynamic scope;
@dynamic shortDisplayName;
@dynamic specialInstructions;
@dynamic startDate;
@dynamic startTime;
@dynamic status;
@dynamic task;
@dynamic taskDescription;
@dynamic contact;
@dynamic project;
@dynamic technician;
@dynamic address;
@dynamic customerEmails;

@end
