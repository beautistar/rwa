//
//  RMATableViewCell.h
//  RMA
//
//  Created by Alexander Roode on 1/29/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMATableViewCell : UITableViewCell

@end
