//
//  BlankCell.h
//  RMA
//
//  Created by Yin on 2018/8/22.
//  Copyright © 2018 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlankCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *tfTestNo;
@property (weak, nonatomic) IBOutlet UITextField *tfItem;
@property (weak, nonatomic) IBOutlet UITextField *tfLocation;
@property (weak, nonatomic) IBOutlet UITextField *tfFG;
@property (weak, nonatomic) IBOutlet UITextField *tfTotal;

@end
