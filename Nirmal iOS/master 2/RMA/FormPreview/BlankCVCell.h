//
//  BlankCVCell.h
//  RMA
//
//  Created by Yin on 2018/8/22.
//  Copyright © 2018 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlankCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UITextField *tfInput;

@end
