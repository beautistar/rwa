//
//  NuclearMethodD2950ViewController.m
//  RMA
//
//  Created by Yin on 2018/8/22.
//  Copyright © 2018 Forebrain. All rights reserved.
//

#import "NuclearMethodD2950ViewController.h"
#import "BlankCVCell.h"
#import "BlankCell.h"
#import "HeaderCell.h"

@interface NuclearMethodD2950ViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@end

@implementation NuclearMethodD2950ViewController {

    CGFloat screenWidth;
    CGFloat screenHeight;
    CGFloat cellHeight;
    CGFloat headerCellHeight;
    NSArray * dataArray;
    int topTableTotalRow;
    int bottomTableTotalRow;
    CGFloat heightConsume; // Top part view height, Bottom part view height + margin
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataArray = @[@"Gauge No.        Test No.",@"Gauge Depth", @"Density Count (DC)", @"Density Std. Count (DS)", @"Wet Density (lb/ft^3)", @"AC Type", @"Mix Design or Mix ID", @"Maximum Density (lb/ft^3)", @"% relative Compaction", @"% Relative Compaction Req'd", @"" ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initValues];
}

#pragma mark - Initialize

- (void) initValues {
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
    headerCellHeight = 80;
    topTableTotalRow = 10;
    bottomTableTotalRow = 5;
    heightConsume = 340.0;
    CGFloat totalCellCount = (CGFloat)(topTableTotalRow + bottomTableTotalRow);
    cellHeight = (screenHeight - heightConsume - headerCellHeight) / totalCellCount;
    _collectionViewHeightConstraint.constant = cellHeight * topTableTotalRow;
    _tableViewHeightConstraint.constant = cellHeight * bottomTableTotalRow + headerCellHeight;
    
}

#pragma mark - CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return topTableTotalRow*6+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BlankCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BlankCVCell" forIndexPath:indexPath];
    
    if (indexPath.row % 6 == 0) {
        cell.lblValue.text =  [NSString stringWithFormat: @"%@", dataArray[indexPath.row/6]];
        cell.tfInput.userInteractionEnabled = false;
    }
    
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 6 == 0) {
        return CGSizeMake((screenWidth-45)/7.0 * 2, cellHeight);
    } else {
        return CGSizeMake((screenWidth-45)/7.0, cellHeight);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return bottomTableTotalRow;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return headerCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return cellHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    HeaderCell * headerCell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    
    return headerCell;
    
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    BlankCell * cell = [tableView dequeueReusableCellWithIdentifier:@"BlankCell"];
    return cell;
}
@end
