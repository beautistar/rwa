//
//  HeaderCell.h
//  RMA
//
//  Created by Yin on 2018/8/22.
//  Copyright © 2018 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imvCheck1;
@property (weak, nonatomic) IBOutlet UIImageView *imvCheck2;
@property (weak, nonatomic) IBOutlet UIImageView *imvCheck3;
@property (weak, nonatomic) IBOutlet UIImageView *imvCheck4;

@property (weak, nonatomic) IBOutlet UIButton *btnCheck1;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck2;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck3;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck4;



@end
