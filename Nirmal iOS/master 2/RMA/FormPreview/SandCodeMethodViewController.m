//
//  SandCodeMethodViewController.m
//  RMA
//
//  Created by Yin on 2018/8/22.
//  Copyright © 2018 Forebrain. All rights reserved.
//

#import "SandCodeMethodViewController.h"
#import "HeaderCell.h"
#import "BlankCell.h"
#import "BlankCVCell.h"

@interface SandCodeMethodViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@end

@implementation SandCodeMethodViewController {

    CGFloat screenWidth;
    CGFloat screenHeight;
    CGFloat cellHeight;
    CGFloat headerCellHeight;
    NSArray * dataArray;
    int topTableTotalRow;
    int bottomTableTotalRow;
    CGFloat heightConsume; // Top part view height, Bottom part view height + margin
    
    NSMutableArray *checkStatus;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataArray = @[@"Test No.",@"Initial Wt. of Sand (lb)", @"Final Wt. of Sand (lb)", @"Difference (lb)", @"Tare Wt. (Cone) (lb)", @"Net Wt. of Sand Used (lb)", @"Density of Sand(lb/ft^3)", @"Volumn of Hole (ft^3)", @"Gross2 Wt. of Sample (lb)", @"Tare (lb)", @"Net Wt. of Sample (lb)", @"Wet Density (lb/ft^3)", @"Gross Wet Weight (g)", @"Gross Dry Weight (g)", @"Tare Weight (g)", @"Net Dry Weight (g)", @"% Moisture", @"Dry Density (lb/ft^3)", @"Sieve Size", @"% Rock Retained", @"Corrected Dry Density", @"Material Type No.", @"Max. Density (lb/ft^3)", @"% Optimum Moisture", @"% Relative Compaction", @"% Relative Compaction Req'd", @"" ];
    
    checkStatus = [[NSMutableArray alloc] init];
    for (int i = 0; i <4; i++) {
        [checkStatus addObject:[NSNumber numberWithBool:NO]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initValues];
}

#pragma mark - Initialize

- (void) initValues {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
    headerCellHeight = 80;
    topTableTotalRow = 26;
    bottomTableTotalRow = 5;
    heightConsume = 320.0;
    CGFloat totalCellCount = (CGFloat)(topTableTotalRow + bottomTableTotalRow);
    cellHeight = (screenHeight - heightConsume - headerCellHeight) / totalCellCount;
    _collectionViewHeightConstraint.constant = cellHeight * topTableTotalRow;
    _tableViewHeightConstraint.constant = cellHeight * bottomTableTotalRow + headerCellHeight;
}

#pragma mark - CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return topTableTotalRow*6+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BlankCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BlankCVCell" forIndexPath:indexPath];
    
    if (indexPath.row % 6 == 0) {
        cell.lblValue.text =  [NSString stringWithFormat: @"%@", dataArray[indexPath.row/6]];
        cell.tfInput.userInteractionEnabled = false;
    }
    
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 6 == 0) {
        return CGSizeMake((screenWidth-45)/7.0 * 2, cellHeight);
    } else {
        return CGSizeMake((screenWidth-45)/7.0, cellHeight);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return bottomTableTotalRow;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return headerCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return cellHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    HeaderCell * headerCell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    if ([[checkStatus objectAtIndex:0] boolValue]) {
        [headerCell.imvCheck1 setImage: [UIImage imageNamed:@"checkmark"]];
    } else {
        [headerCell.imvCheck1 setImage: nil];
    }
    if ([[checkStatus objectAtIndex:1] boolValue]) {
        [headerCell.imvCheck2 setImage: [UIImage imageNamed:@"checkmark"]];
    } else {
        [headerCell.imvCheck2 setImage: nil];
    }
    if ([[checkStatus objectAtIndex:2] boolValue]) {
        [headerCell.imvCheck3 setImage: [UIImage imageNamed:@"checkmark"]];
    } else {
        [headerCell.imvCheck3 setImage: nil];
    }
    if ([[checkStatus objectAtIndex:3] boolValue]) {
        [headerCell.imvCheck4 setImage: [UIImage imageNamed:@"checkmark"]];
    } else {
        [headerCell.imvCheck4 setImage: nil];
    }
    
    [headerCell.btnCheck1 addTarget:self action:@selector(setChecked:) forControlEvents:UIControlEventTouchUpInside];
    [headerCell.btnCheck2 addTarget:self action:@selector(setChecked:) forControlEvents:UIControlEventTouchUpInside];
    [headerCell.btnCheck3 addTarget:self action:@selector(setChecked:) forControlEvents:UIControlEventTouchUpInside];
    [headerCell.btnCheck4 addTarget:self action:@selector(setChecked:) forControlEvents:UIControlEventTouchUpInside];

    return headerCell;
    
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    BlankCell * cell = [tableView dequeueReusableCellWithIdentifier:@"BlankCell"];
    return cell;
}

#pragma mark - check action
- (void) setChecked:(UIButton *) sender {
    
    int index = (int)sender.tag;
    
    BOOL isChecked = [[checkStatus objectAtIndex:index-1] boolValue];
    
    isChecked = !isChecked;
    
    [checkStatus replaceObjectAtIndex:index-1 withObject:[NSNumber numberWithBool:isChecked]];
    
    [_tableView reloadData];
}

- (IBAction)saveTapped:(id)sender {
    
    
}

@end
