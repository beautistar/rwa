//
//  Document.m
//  RMA
//
//  Created by Michael Beteag on 12/13/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "Document.h"
#import "Folder.h"


@implementation Document

@dynamic id;
@dynamic filename;
@dynamic extension;
@dynamic url;
@dynamic modified;
@dynamic name;
@dynamic path;
@dynamic folder;

@end
