//
//  FormSection+Daily.m
//  RMA
//
//  Created by Alexander Roode on 5/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "FormSection+Daily.h"
#import "DailyCommonEditor.h"
#import "DropdownEditor.h"
#import "SectionEnablerEditor.h"
#import "DSAFooterEditor.h"
#import "DSAFooterTechnicianEditor.h"
#import "FormDivider.h"
#import "TextAreaEditor.h"
#import "AdditionalRemarksEditor.h"
#import "NonComplianceQuestionEditor.h"
#import "NonDSAFooterEditor.h"
#import "FormHeader.h"

@implementation FormSection (Daily)

+(FormSection*)dailyInspectionSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Daily Inspection Report" shortTitle:@"Report"];
    
    FormComponentViewEditor *editor = [[DailyCommonEditor alloc] initWithNibName:@"DailyCommonEditor"];
    editor.validationType = validationTypeAll;
    
    TextAreaEditor *remarks = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 300) name:@"Work Inspected" key:@"remarksParagraph"];
    remarks.textArea.maxHeight = 540;
    remarks.validationType = validationTypeAll;
    
    AdditionalRemarksEditor *additionalRemarksEditor = [AdditionalRemarksEditor additionalRemarksEditorWithKey:@"additionalRemarks"];
    
#ifndef SITESCAN
    NonComplianceQuestionEditor *noncomplianceQuestion = [NonComplianceQuestionEditor nonComplianceQuestionEditorWithKey:@"nonComplianceQuestion" technician:NO];
    noncomplianceQuestion.validationType = validationTypeAll;
#endif
    
    DropdownEditor *densitySheetsTakenEditor = [DropdownEditor dropdownEditorWithKey:@"densitySheets" text:@"Density Sheets Taken" options:@[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"]];
    
    densitySheetsTakenEditor.requiredTaskCodes = @[@"H2001", @"H2002", @"H2005", @"H2006", @"H2101", @"H2102", @"H2103", @"H2107"];
    
    SectionEnablerEditor *sectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add photos?" key:@"photoQuestion"];
    sectionEnablerEditor.validationType = validationTypeAll;
    
    SectionEnablerEditor *sectionMasterFormsEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add Master Forms?" key:@"pdfQuestion"];
    sectionMasterFormsEnablerEditor.validationType = validationTypeAll;
    sectionMasterFormsEnablerEditor.requiredTaskCodes = @[@"-1"];
    
#ifndef SITESCAN
    SectionEnablerEditor *samplesSectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Did you obtain samples?" key:@"samplesQuestion"];
    samplesSectionEnablerEditor.validationType = validationTypeAll;
#endif
    
    SectionEnablerEditor *addMoreSamplesEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add density form?" key:@"AddMoreSamples"];
    addMoreSamplesEnablerEditor.validationType = validationTypeAll;
    addMoreSamplesEnablerEditor.requiredTaskCodes = @[@"H5201", @"H2002", @"H2005", @"H2006", @"H2101", @"H2102", @"H2103", @"H2107"];
    
    SectionEnablerEditor *checklistsSectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add checklists?" key:@"checklistsQuestion"];
    checklistsSectionEnablerEditor.validationType = validationTypeAll;
    
    NonDSAFooterEditor *footerEditor = [NonDSAFooterEditor nonDSAFooterEditorWithKey:@"footer"];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    editor,
                                                    remarks,
                                                    additionalRemarksEditor,
#ifndef SITESCAN
                                                    noncomplianceQuestion,
#endif
                                                    densitySheetsTakenEditor,
                                                    checklistsSectionEnablerEditor,
                                                    sectionEnablerEditor,
                                                    sectionMasterFormsEnablerEditor,
#ifndef SITESCAN
                                                    samplesSectionEnablerEditor,
#endif
                                                    addMoreSamplesEnablerEditor,
                                                    footerEditor]
                                     templateName:@"dailyInspectionReport"
                                            title:@"Daily Inspection Report"];
    section.headerPage1 = [FormHeader headerWithType:@"nonDSAPage1Header"];
    
    return section;
}

+(FormSection*)dailyTechnicianSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Daily Technician Report" shortTitle:@"Report"];
    
    FormComponentViewEditor *editor = [[DailyCommonEditor alloc] initWithNibName:@"DailyCommonEditor"];
    editor.validationType = validationTypeAll;
    
    TextAreaEditor *equipmentOnSite = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 95) name:@"Equipment On-Site" key:@"equipmentOnSite"];
    equipmentOnSite.textArea.maxHeight = 40;
    equipmentOnSite.validationType = validationTypeAll;
    
    TextAreaEditor *summaryOfObservations = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 150) name:@"Summary of Observations" key:@"observationsSummary"];
    summaryOfObservations.textArea.maxHeight = 206;
    summaryOfObservations.validationType = validationTypeAll;
    
    TextAreaEditor *areasApprovedLimitsEditor = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 110) name:@"Limits of Areas Approved" key:@"areasApprovedLimits"];
    areasApprovedLimitsEditor.textArea.maxHeight = 60;
    areasApprovedLimitsEditor.validationType = validationTypeAll;
    
    TextAreaEditor *areasNotApprovedLimitsEditor = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 110) name:@"Limits of Areas Not Approved" key:@"areasNotApprovedLimits"];
    areasNotApprovedLimitsEditor.textArea.maxHeight = 60;
    areasNotApprovedLimitsEditor.validationType = validationTypeAll;
    
    AdditionalRemarksEditor *additionalRemarksEditor = [AdditionalRemarksEditor additionalRemarksEditorWithKey:@"additionalRemarks"];
    
#ifndef SITESCAN
    NonComplianceQuestionEditor *noncomplianceQuestion = [NonComplianceQuestionEditor nonComplianceQuestionEditorWithKey:@"nonComplianceQuestion" technician:YES];
    noncomplianceQuestion.validationType = validationTypeAll;
#endif
    
    DropdownEditor *densitySheetsTakenEditor = [DropdownEditor dropdownEditorWithKey:@"densitySheets" text:@"Density Sheets Taken" options:@[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"]];
    
    densitySheetsTakenEditor.requiredTaskCodes = @[@"H2001", @"H2002", @"H2005", @"H2006", @"H2101", @"H2102", @"H2103", @"H2107"];
    
    SectionEnablerEditor *sectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add photos?" key:@"photoQuestion"];
    sectionEnablerEditor.validationType = validationTypeAll;
    
    SectionEnablerEditor *sectionMasterFormsEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add Master Forms?" key:@"pdfQuestion"];
    sectionMasterFormsEnablerEditor.validationType = validationTypeAll;
    sectionMasterFormsEnablerEditor.requiredTaskCodes = @[@"-1"];
    
#ifndef SITESCAN
    SectionEnablerEditor *samplesSectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Did you obtain samples?" key:@"samplesQuestion"];
    samplesSectionEnablerEditor.validationType = validationTypeAll;
#endif
    
    SectionEnablerEditor *addMoreSamplesEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add density form?" key:@"AddMoreSamples"];
    addMoreSamplesEnablerEditor.validationType = validationTypeAll;
    addMoreSamplesEnablerEditor.requiredTaskCodes = @[@"H5201", @"H2002", @"H2005", @"H2006", @"H2101", @"H2102", @"H2103", @"H2107"];
    
    SectionEnablerEditor *checklistsSectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add checklists?" key:@"checklistsQuestion"];
    checklistsSectionEnablerEditor.validationType = validationTypeAll;
    
    NonDSAFooterEditor *footerEditor = [NonDSAFooterEditor nonDSAFooterEditorWithKey:@"footer"];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    editor,
                                                    equipmentOnSite,
                                                    summaryOfObservations,
                                                    areasApprovedLimitsEditor,
                                                    areasNotApprovedLimitsEditor,
                                                    additionalRemarksEditor,
#ifndef SITESCAN
                                                    noncomplianceQuestion,
#endif
                                                    densitySheetsTakenEditor,
                                                    checklistsSectionEnablerEditor,
                                                    sectionEnablerEditor,
                                                    sectionMasterFormsEnablerEditor,
#ifndef SITESCAN
                                                    samplesSectionEnablerEditor,
#endif
                                                    addMoreSamplesEnablerEditor,
                                                    footerEditor]
                                     templateName:@"dailyTechnicianReport"
                                            title:@"Daily Technician Report"];
    section.headerPage1 = [FormHeader headerWithType:@"nonDSAPage1Header"];
    //section.headerPage1 = [FormHeader headerWithType:@"DSAPage1Header"];
    
    return section;
}

+(FormSection*)DSAdailyInspectionSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"DSA Daily Inspection Report" shortTitle:@"Report"];
    FormComponentViewEditor *editor = [[DailyCommonEditor alloc] initWithNibName:@"DSADailyCommonEditor"];
    editor.validationType = validationTypeAll;
    
    TextAreaEditor *remarks = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 300) name:@"Work Inspected" key:@"remarksParagraph"];
    remarks.textArea.maxHeight = 400;
    remarks.validationType = validationTypeAll;
    
    AdditionalRemarksEditor *additionalRemarksEditor = [AdditionalRemarksEditor additionalRemarksEditorWithKey:@"additionalRemarks"];
    
    SectionEnablerEditor *sectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add photos?" key:@"photoQuestion"];
    sectionEnablerEditor.validationType = validationTypeAll;
    
    SectionEnablerEditor *sectionMasterFormsEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add Master Forms?" key:@"pdfQuestion"];
    sectionMasterFormsEnablerEditor.validationType = validationTypeAll;
    sectionMasterFormsEnablerEditor.requiredTaskCodes = @[@"-1"];
    
#ifndef SITESCAN//first visit marker
    SectionEnablerEditor *samplesSectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Did you obtain samples?" key:@"samplesQuestion"];
    samplesSectionEnablerEditor.validationType = validationTypeAll;
#endif
    
    SectionEnablerEditor *addMoreSamplesEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add density form?" key:@"AddMoreSamples"];
    addMoreSamplesEnablerEditor.validationType = validationTypeAll;
    addMoreSamplesEnablerEditor.requiredTaskCodes = @[@"H5201", @"H2002", @"H2005", @"H2006", @"H2101", @"H2102", @"H2103", @"H2107"];
    
    SectionEnablerEditor *checklistsSectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add checklists?" key:@"checklistsQuestion"];
    checklistsSectionEnablerEditor.validationType = validationTypeAll;
    
#ifndef SITESCAN
    DSAFooterEditor *footerEditor = [DSAFooterEditor DSAFooterEditorWithKey:@"footer1"];
#endif
    NonDSAFooterEditor *footerEditor2 = [NonDSAFooterEditor nonDSAFooterEditorWithKey:@"footer2"];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    editor,
                                                    remarks,
                                                    additionalRemarksEditor,
                                                    //noncomplianceQuestion,
                                                    checklistsSectionEnablerEditor,
                                                    sectionEnablerEditor,
                                                    sectionMasterFormsEnablerEditor,
#ifndef SITESCAN
                                                    samplesSectionEnablerEditor,
                                                    footerEditor,
#endif
                                                    addMoreSamplesEnablerEditor,
                                                    footerEditor2]
                                     templateName:@"DSADailyInspectionReport"
                                            title:@"DSA Daily Inspection Report"];
    section.headerPage1 = [FormHeader headerWithType:@"DSAPage1Header"];
    
    return section;
}

+(FormSection*)DSAdailyTechnicianSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"DSA Daily Technician Report" shortTitle:@"Report"];
    
    FormComponentViewEditor *editor = [[DailyCommonEditor alloc] initWithNibName:@"DSADailyCommonEditor"];
    editor.validationType = validationTypeAll;
    
    TextAreaEditor *equipmentOnSite = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 95) name:@"Equipment On-Site" key:@"equipmentOnSite"];
    equipmentOnSite.textArea.maxHeight = 40;
    equipmentOnSite.validationType = validationTypeAll;
    
    TextAreaEditor *summaryOfObservations = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 150) name:@"Summary of Observations" key:@"observationsSummary"];
    summaryOfObservations.textArea.maxHeight = 80;
    summaryOfObservations.validationType = validationTypeAll;
    
    TextAreaEditor *areasApprovedLimitsEditor = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 110) name:@"Limits of Areas Approved" key:@"areasApprovedLimits"];
    areasApprovedLimitsEditor.textArea.maxHeight = 60;
    areasApprovedLimitsEditor.validationType = validationTypeAll;
    
    TextAreaEditor *areasNotApprovedLimitsEditor = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 110) name:@"Limits of Areas Not Approved" key:@"areasNotApprovedLimits"];
    areasNotApprovedLimitsEditor.textArea.maxHeight = 60;
    areasNotApprovedLimitsEditor.validationType = validationTypeAll;
    
    AdditionalRemarksEditor *additionalRemarksEditor = [AdditionalRemarksEditor additionalRemarksEditorWithKey:@"additionalRemarks"];
    
    SectionEnablerEditor *sectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add photos?" key:@"photoQuestion"];
    sectionEnablerEditor.validationType = validationTypeAll;
    
    SectionEnablerEditor *sectionMasterFormsEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add Master Forms?" key:@"pdfQuestion"];
    sectionMasterFormsEnablerEditor.validationType = validationTypeAll;
    sectionMasterFormsEnablerEditor.requiredTaskCodes = @[@"-1"];
    
#ifndef SITESCAN
    SectionEnablerEditor *samplesSectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Did you obtain samples?" key:@"samplesQuestion"];
    samplesSectionEnablerEditor.validationType = validationTypeAll;
#endif
    
    SectionEnablerEditor *addMoreSamplesEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add density form?" key:@"AddMoreSamples"];
    addMoreSamplesEnablerEditor.validationType = validationTypeAll;
    addMoreSamplesEnablerEditor.requiredTaskCodes = @[@"H5201", @"H2002", @"H2005", @"H2006", @"H2101", @"H2102", @"H2103", @"H2107"];
    
    SectionEnablerEditor *checklistsSectionEnablerEditor = [SectionEnablerEditor sectionEnablerEditorWithText:@"Do you want to add checklists?" key:@"checklistsQuestion"];
    checklistsSectionEnablerEditor.validationType = validationTypeAll;

#ifndef SITESCAN
    DSAFooterTechnicianEditor *footerEditor = [DSAFooterTechnicianEditor DSAFooterTechnicianEditorWithKey:@"footer1"];
#endif
    
    NonDSAFooterEditor *footerEditor2 = [NonDSAFooterEditor nonDSAFooterEditorWithKey:@"footer2"];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    editor,
                                                    equipmentOnSite,
                                                    summaryOfObservations,
                                                    areasApprovedLimitsEditor,
                                                    areasNotApprovedLimitsEditor,
                                                    additionalRemarksEditor,
                                                    //noncomplianceQuestion,
                                                    checklistsSectionEnablerEditor,
                                                    sectionEnablerEditor,
                                                    sectionMasterFormsEnablerEditor,
#ifndef SITESCAN
                                                    samplesSectionEnablerEditor,
                                                    footerEditor,
#endif
                                                    addMoreSamplesEnablerEditor,
                                                    footerEditor2]
                                     templateName:@"DSADailyTechnicianReport"
                                            title:@"DSA Daily Technician Report"];
    section.headerPage1 = [FormHeader headerWithType:@"DSAPage1Header"];
    
    return section;
}

@end
