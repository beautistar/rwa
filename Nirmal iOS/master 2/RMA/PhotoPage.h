//
//  PhotoPage.h
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"
#import "NSData+Base64.h"
#import "PhotoPageType.h"

@class Photo;

@interface PhotoPage : NSObject <NSCoding, GRMustacheRendering>
+(instancetype)photoPageWithPhotos:(NSArray*)photos type:(PhotoPageType)pageType;
@property (nonatomic) PhotoPageType pageType;
@property (nonatomic, strong) NSArray<Photo*> *photos;
@end
