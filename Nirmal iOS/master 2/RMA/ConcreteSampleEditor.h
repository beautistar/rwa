//
//  ConcreteSampleEditor.h
//  RMA
//
//  Created by Michael Beteag on 4/3/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"

@interface ConcreteSampleEditor : FormComponentEditor

@property (nonatomic, strong) NSArray *editors;

+(ConcreteSampleEditor*)concreteSampleEditor;

@end
