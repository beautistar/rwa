//
//  WeatherSummaryCell.m
//  RMA
//
//  Created by Alexander Roode on 5/10/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "WeatherSummaryCell.h"
#import "WeatherSummary.h"

@implementation WeatherSummaryCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] objectAtIndex:0];
    }
    return self;
}

- (void)setWeatherSummary:(WeatherSummary *)weatherSummary {
    _weatherSummary = weatherSummary;
    
    self.locationNameLabel.text = weatherSummary.locationName;
    self.summaryLabel.text = weatherSummary.summary;
    self.currentTemperatureLabel.text = [self temperatureString:weatherSummary.currentTemp];
    self.maxTemperatureLabel.text = [self temperatureString:weatherSummary.maxTemp];
    self.minTemperatureLabel.text = [self temperatureString:weatherSummary.minTemp];
    
    if (weatherSummary.currentTemp > 0) {
        self.precipitationChanceLabel.text = [self precipChanceString:weatherSummary.precipChance];
    } else {
        self.precipitationChanceLabel.text = @"";
    }
    
    self.dateLabel.text = weatherSummary.dateString;
}

- (NSString*)temperatureString:(float)temperature {
    if (temperature <= 0) {
        return @"";
    }
    
    return [NSString stringWithFormat:@"%.0f°", temperature];
}

- (NSString*)precipChanceString:(float)chance {
    return [NSString stringWithFormat:@"Precip.: %.0f%%", 100 * chance];
}

@end
