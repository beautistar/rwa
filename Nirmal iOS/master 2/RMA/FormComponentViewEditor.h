//
//  FormComponentViewEditor.h
//  RMA
//
//  Created by Michael Beteag on 7/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormComponentEditor.h"

@interface FormComponentViewEditor : FormComponentEditor <UITableViewDelegate, UITableViewDataSource>

//@property (retain, nonatomic) NSMutableDictionary *LabSamples;
//@property (retain, nonatomic) IBOutlet UITableView *LabSamplesTableView;
- (instancetype)initWithNibName:(NSString*)nibName;

-(void) LoadLabSamples;

@end
