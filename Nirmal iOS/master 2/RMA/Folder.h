//
//  Folder.h
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Document, Folder;

@interface Folder : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSString * path;
@property (nonatomic, retain) NSSet *documents;
@property (nonatomic, retain) NSSet *subfolders;
@property (nonatomic, retain) Folder *parent;
@end

@interface Folder (CoreDataGeneratedAccessors)

- (void)addDocumentsObject:(Document *)value;
- (void)removeDocumentsObject:(Document *)value;
- (void)addDocuments:(NSSet *)values;
- (void)removeDocuments:(NSSet *)values;

- (void)addSubfoldersObject:(Folder *)value;
- (void)removeSubfoldersObject:(Folder *)value;
- (void)addSubfolders:(NSSet *)values;
- (void)removeSubfolders:(NSSet *)values;

@end
