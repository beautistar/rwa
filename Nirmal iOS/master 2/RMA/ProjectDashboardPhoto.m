//
//  ProjectDashboardPhoto.m
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "ProjectDashboardPhoto.h"

@implementation ProjectDashboardPhoto

+(ProjectDashboardPhoto*)photoWithImage:(UIImage*)image {
    ProjectDashboardPhoto *photo = [[ProjectDashboardPhoto alloc] init];
    photo.image = image;
    return photo;
}
@end
