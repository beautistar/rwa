//
//  StandardSample.h
//  RMA
//
//  Created by Michael Beteag on 2/19/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"

@interface StandardSample : NSObject <NSCoding,GRMustacheRendering>

@property (nonatomic, retain) NSMutableDictionary *data;
@property (nonatomic, retain) NSNumber *type;
@property (nonatomic, retain) NSNumber *index;
+ (StandardSample*)standardSampleWithType:(int)type index:(int)index data:(NSMutableDictionary*)data;
@end
