//
//  Photo.m
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "Photo.h"

@implementation Photo

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        self.caption = [aDecoder decodeObjectForKey:@"caption"];
        self.image = [aDecoder decodeObjectForKey:@"image"];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.caption forKey:@"caption"];
    [aCoder encodeObject:self.image forKey:@"image"];
}

@end
