//
//  FormCell.m
//  RMA
//
//  Created by Michael Beteag on 7/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormCell.h"
#import "Form.h"
#import "Project.h"
#import "Client.h"
#import "NSDate+Extensions.h"
#import "Form+Extensions.h"

@implementation FormCell

-(void)layoutSubviews {
    [super layoutSubviews];
    [self.rescheduleButton sizeToFit];
}

-(void)setForm:(Form *)form {
    _form = form;
    
    self.typeLabel.text = form.displayName;
    self.projectLabel.text = form.project.id;
    self.clientLabel.text = form.project.client.name;
    self.startTimeLabel.text = [form.startTime timeString];
    self.drNumberLabel.text = form.dispatchId;
    self.taskCodeLabel.text = [NSString stringWithFormat:@"%@ - %@", form.task, form.taskDescription];
    
    switch([form.status intValue]) {
        case FormStatusNotStarted:
            self.statusLabel.text = @"Not started";
            self.statusLabel.textColor = [UIColor redColor];
            break;
        case FormStatusIncomplete:
            self.statusLabel.text = @"Incomplete";
            self.statusLabel.textColor = [UIColor colorWithRed:249/255.0 green:191/255.0 blue:59/255.0 alpha:1.0];
            break;
        case FormStatusComplete:
        case FormStatusCompletedAfterResubmit:
            self.statusLabel.text = @"Complete";
            self.statusLabel.textColor = [UIColor greenColor];
            break;
        case FormStatusResubmit:
            self.statusLabel.text = @"Re-submit";
            self.statusLabel.textColor = [UIColor orangeColor];
            break;
        case FormStatusCanceled:
            self.statusLabel.text = @"Canceled";
            self.statusLabel.textColor = [UIColor colorWithRed:249/255.0 green:191/255.0 blue:59/255.0 alpha:1.0];
    }
    
    self.rescheduleButton.enabled = [form isEditable];
    self.rescheduleButton.hidden = ![form isEditable];
}

+(NSString *) reuseIdentifier {
    return NSStringFromClass(self);
}

-(NSString *) reuseIdentifier {
    return [[self class] reuseIdentifier];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        self.contentView.backgroundColor = [UIColor colorWithRed:17/255.0 green:150/255.0 blue:237/255.0 alpha:1.0];
        [self.rescheduleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.rescheduleButton setTitleColor:[UIColor colorWithRed:17/255.0 green:150/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
    }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) {
        self.contentView.backgroundColor = [UIColor colorWithRed:17/255.0 green:150/255.0 blue:237/255.0 alpha:1.0];
        [self.rescheduleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.rescheduleButton setTitleColor:[UIColor colorWithRed:17/255.0 green:150/255.0 blue:237/255.0 alpha:1.0] forState:UIControlStateNormal];
    }
}

-(IBAction)rescheduleButtonPressed:(id)sender {
    [self.delegate rescheduleButtonPressedForForm:self.form];
}

@end
