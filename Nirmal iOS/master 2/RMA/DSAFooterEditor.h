//
//  DSAFooterEditor.h
//  RMA
//
//  Created by Michael Beteag on 7/3/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"
#import "RadioCheckboxSet.h"
#import "FormComponentViewEditor.h"

@interface DSAFooterEditor : FormComponentViewEditor

@property (weak, nonatomic) IBOutlet UISegmentedControl *inspectedInAccordance;
@property (weak, nonatomic) IBOutlet UISegmentedControl *workMeetsRequirements;
@property (weak, nonatomic) IBOutlet UISegmentedControl *samplingInAccordance;

//@property (weak, nonatomic) IBOutlet UITextField *nameTitle;
//@property (weak, nonatomic) IBOutlet UITextField *certificationNumber;

@property (nonatomic, retain) FormComponentViewEditor *mainEditor;

@property (nonatomic, retain) IBOutlet UIView *view;

@property (nonatomic, strong) NSString *key;
@property (nonatomic, retain) NSArray *editors;


- (IBAction)nonComplianceQuestion1Changed:(id)sender;
- (IBAction)nonComplianceQuestion2Changed:(id)sender;
- (IBAction)nonComplianceQuestion3Changed:(id)sender;

+(DSAFooterEditor*)DSAFooterEditorWithKey:(NSString *)key;

@end
