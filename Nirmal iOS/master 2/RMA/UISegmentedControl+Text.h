//
//  UISegmentedControl+Text.h
//  RMA
//
//  Created by Michael Beteag on 7/25/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UISegmentedControl (UISegmentedControl_Text)

-(NSString*)text;
-(BOOL)selectText:(NSString*)text;

@end
