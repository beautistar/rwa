//
//  AdditionalRemarksEditor.m
//  RMA
//
//  Created by Michael Beteag on 10/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "AdditionalRemarksEditor.h"
#import "Constants.h"

@implementation AdditionalRemarksEditor

+(AdditionalRemarksEditor*)additionalRemarksEditorWithKey:(NSString*)key {
    AdditionalRemarksEditor *additionalRemarksEditor = [[AdditionalRemarksEditor alloc] initWithFrame:CGRectMake(0, 0, 768, kConcretePageSetButtonHeight + 2*kPaddingY)];
    additionalRemarksEditor.key = key;
    additionalRemarksEditor.additionalRemarksOn = NO;
    additionalRemarksEditor.standalone = YES;
    additionalRemarksEditor.standaloneSectionType = @"additionalRemarks";
    
    additionalRemarksEditor.toggleButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    additionalRemarksEditor.toggleButton.frame = CGRectMake(kConcretePageSetHorizontalPadding,
                                      kConcretePageSetVerticalPadding,
                                      kAdditionalRemarksButtonWidth, kConcretePageSetButtonHeight);
    [additionalRemarksEditor.toggleButton setTitle:@"Add additional remarks" forState:UIControlStateNormal];
    [additionalRemarksEditor.toggleButton addTarget:additionalRemarksEditor action:@selector(toggleAdditionalRemarks) forControlEvents:UIControlEventTouchUpInside];
    [additionalRemarksEditor.view addSubview:additionalRemarksEditor.toggleButton];
    
    return additionalRemarksEditor;
}

-(void)toggleAdditionalRemarks {
    if(self.additionalRemarksOn) {
        [self.toggleButton setTitle:@"Add additional remarks" forState:UIControlStateNormal];
        if(self.additionalRemarksEditor != nil) {
            [self.additionalRemarksEditor removeFromSuperview];
            self.view.frame = CGRectMake(0, 0, 768, kConcretePageSetButtonHeight + 2*kPaddingY);
            
            [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"FormDidChangeSize" object:[NSNumber numberWithInt:-300]]];
            
        }
        
    } else {
        [self.toggleButton setTitle:@"Delete additional remarks" forState:UIControlStateNormal];
        self.additionalRemarksEditor = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 2*kPaddingY + kConcretePageSetButtonHeight, 768, 300)
                                                                        name:@"Additional Remarks" key:@"additionalRemarks"];
        self.additionalRemarksEditor.textArea.maxHeight = 750;
        [self.view addSubview:self.additionalRemarksEditor];
        
        self.view.frame = CGRectMake(0, 0, 768, self.view.frame.size.height + 300);
        
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"FormDidChangeSize" object:[NSNumber numberWithInt:300]]];
    }
    self.additionalRemarksOn = !self.additionalRemarksOn;
    
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    if(self.additionalRemarksOn) {
        NSMutableDictionary *remarksEditorData = [self.additionalRemarksEditor getData];
        Paragraph *remarksParagraph = [remarksEditorData objectForKey:@"additionalRemarks"];
        
        //AdditionalRemarks *additionalRemarks = [AdditionalRemarks additionalRemarksWithParagraph:remarksParagraph];
        [data setObject:remarksParagraph forKey:self.key];
    }
    self.isEnabled = self.additionalRemarksOn;
    return data;
    
}

-(void)setData:(NSMutableDictionary *)data {
    if([data objectForKey:self.key] != nil) {
        [self toggleAdditionalRemarks];
        //AdditionalRemarks *additionalRemarks = [data objectForKey:self.key];
        Paragraph *remarksParagraph = [data objectForKey:self.key];
        
        NSMutableDictionary *remarksEditorData = [NSMutableDictionary dictionary];
        [remarksEditorData setObject:remarksParagraph forKey:@"additionalRemarks"];
        
        [self.additionalRemarksEditor setData:remarksEditorData];
    } else {
        self.isEnabled = NO;
    }
}

-(NSDictionary*)dictionary {
    
    if (self.additionalRemarksOn) {
        NSMutableDictionary *remarksEditorData = [self.additionalRemarksEditor getData];
        Paragraph *remarksParagraph = [remarksEditorData objectForKey:@"additionalRemarks"];
        
        return @{@"additionalRemarks": remarksParagraph.text};
    }
    
    return @{};
}

@end
