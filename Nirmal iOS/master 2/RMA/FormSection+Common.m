//
//  FormSection+Common.m
//  RMA
//
//  Created by Alexander Roode on 5/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "FormSection+Common.h"
#import "PhotoCollectionEditor.h"
#import "SampleCollectionEditor.h"
#import "FormDivider.h"
#import "NonComplianceFormEditor.h"
#import "AdditionalRemarksEditor.h"
#import "StandardSampleEditor.h"
#import "AddMoreSamplesCollectionEditor.h"
#import "AddSampleSegment.h"

@implementation FormSection (Common)

+(FormSection*)testSection {
    int rand = arc4random_uniform(10);
    NSString *title = [NSString stringWithFormat:@"Test %i", rand];
    NSArray *editors = @[[FormDivider formDividerWithTitle:title],
                         [[FormComponentViewEditor alloc] initWithNibName:@"TestEditor"]];
    FormSection *section = [FormSection formSectionWithEditors:editors templateName:@"Empty" title:@"Test Section" shouldOutputXml:NO];
    section.useSectionName = YES;
    
    return section;
}

+(FormSection*)emptySection {
    return [FormSection formSectionWithEditors:nil templateName:@"Empty" title:@"" shouldOutputXml:NO];
}

+(FormSection*)nonComplianceSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Non-Compliance"];
    NonComplianceFormEditor *editor = [NonComplianceFormEditor nonComplianceFormEditorWithKey:@"nonCompliance"];
    FormSection *section = [FormSection formSectionWithEditors:@[divider, editor]
                                     templateName:@"nonCompliance"
                                            title:@"Non-Compliance"];
    section.headerPage1 = nil;
    
    return section;
}

+(FormSection*)photosSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Photos" shortTitle:@"Photos"];
    PhotoCollectionEditor *editor = [PhotoCollectionEditor photoCollectionEditorWithKey:@"photoPages"];
    editor.maxCount = 3;
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider, editor] templateName:@"photoPage" title:@"Photos" shouldOutputXml:NO];
    section.enablerKey = @"photoQuestion";
    
    return section;
}

+(FormSection*)pdfSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"MasterForms" shortTitle:@"MasterForms"];
    PhotoCollectionEditor *editor = [PhotoCollectionEditor photoCollectionEditorWithKey:@"pdfPages"];
    editor.maxCount = 3;
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider, editor] templateName:@"photoPage" title:@"MasterForms" shouldOutputXml:NO];
    section.enablerKey = @"pdfQuestion";
    
    return section;
}

+(FormSection*)standardSamplesSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Samples" shortTitle:@"Samples"];
    
    SampleCollectionEditor *editor = [SampleCollectionEditor sampleCollectionEditor];
    editor.validationType = validationTypeAll;
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider, editor] templateName:@"standardSamples" title:@"Samples"];
    section.enablerKey = @"samplesQuestion";
    section.xmlTagName = @"samples";
    
    return section;
}

+(FormSection*)addMoreSamples {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Density Forms" shortTitle:@"Density Forms"];
    
    AddMoreSamplesCollectionEditor *editor = [AddMoreSamplesCollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        AddSampleSegment *editor = [AddSampleSegment standardSampleEditor];
        return editor;
    } key:@"moresamples" itemKey:@"moresamples" itemName:@"MoreSample"];
    editor.validationType = validationTypeAll;
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider, editor] templateName:@"addMoreSamples" title:@"MoreSamples"];
    section.enablerKey = @"AddMoreSamples";
    section.xmlTagName = @"addmoresamples";
    
    return section;
}

+(FormSection*)additionalRemarksSection {
    AdditionalRemarksEditor *editor = [AdditionalRemarksEditor additionalRemarksEditorWithKey:@"additionalRemarks"];
    return [FormSection formSectionWithEditors:@[editor] templateName:@"additionalRemarks" title:nil shouldOutputXml:NO];
}

@end
