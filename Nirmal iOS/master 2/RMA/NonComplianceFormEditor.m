//
//  NonComplianceFormEditor.m
//  RMA
//
//  Created by Michael Beteag on 9/24/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "NonComplianceFormEditor.h"
#import "TextAreaEditor.h"
#import "Constants.h"

@implementation NonComplianceFormEditor

+(NonComplianceFormEditor*)nonComplianceFormEditorWithKey:(NSString*)key {
    NonComplianceFormEditor *editor = [[NonComplianceFormEditor alloc] initWithFrame:CGRectMake(0,
                                                    kPaddingY,
                                                    768 - 2 * kPaddingX,
                                                    0)];
    
    editor.key = key;
    
    editor.remarksEditor = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 300) name:@"Describe the non-complying work in detail:" key:@"nonComplianceRemarks"];
    editor.remarksEditor.textArea.maxHeight = 790;
    [editor addComponent:editor.remarksEditor];

    //[editor addEnablerWithText:@"The work inspected met the requirements of the approved plans and specifications" reversed:YES];
    
    
    
    return editor;
}

-(void)setFormEnabled:(BOOL)enabled {
    [self setControls:self.remarksEditor.subviews enabled:enabled];
    self.isEnabled = enabled;
}

-(BOOL)validates:(NSMutableArray**)validationErrors referenceView:(UIView*)referenceView {
    
    BOOL validationSuccess = YES;

    //validationSuccess = [self requireAllFields:self.view.subviews validationErrors:validationErrors referenceView:referenceView] && validationSuccess;
    validationSuccess = [self requireAllFields:self.remarksEditor.subviews validationErrors:validationErrors referenceView:referenceView] && validationSuccess;

    return validationSuccess;
    
}


-(void)addComponent:(UIView*)view {
    float y = self.view.frame.size.height;
    
    
    
    view.frame = CGRectMake(view.frame.origin.x,
                            y,
                            view.frame.size.width,
                            view.frame.size.height);
    [self.view addSubview:view];
    
    self.view.frame = CGRectMake(self.view.frame.origin.x,
                                 self.view.frame.origin.y,
                                 self.view.frame.size.width,
                                 self.view.frame.size.height + view.frame.size.height);
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    if(self.isEnabled) {
        [data addEntriesFromDictionary:[self.remarksEditor getData]];
    }
    
    [data setObject:[NSNumber numberWithInt:(int)self.enabler.selectedSegmentIndex] forKey:self.key];
    
    return data;
    
}
-(void)setData:(NSMutableDictionary*)data {
    int enablerIndex = [[data objectForKey:self.key] intValue];
    self.enabler.selectedSegmentIndex = enablerIndex;

    [self.remarksEditor setData:data];
    
    if(enablerIndex == 0) {
        [self setFormEnabled:YES];
    } else {
        [self setFormEnabled:NO];
    }
    
}

-(NSDictionary*)dictionary {
    if (self.isEnabled) {
        return [self.remarksEditor dictionary];
    }
    return @{};
}

@end
