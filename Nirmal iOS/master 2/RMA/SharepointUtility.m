//
//  SharepointUtility.m
//  RMA
//
//  Created by Michael Beteag on 12/13/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "SharepointUtility.h"
#import "AFNetworking.h"
#import "XMLDictionary.h"
#import "AppData.h"

@implementation SharepointUtility


+ (id)sharedUtilty {
    static SharepointUtility *sharedUtility = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUtility = [[self alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        sharedUtility.documentsDirectory = basePath;
        sharedUtility.rootURL = [NSURL URLWithString:@"http://204.144.120.241"];
        sharedUtility.client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"rma" relativeToURL:sharedUtility.rootURL]];
        [sharedUtility.client registerHTTPOperationClass:[AFXMLRequestOperation class]];
        [sharedUtility.client setDefaultCredential:[NSURLCredential credentialWithUser:@"fore.admin"
                                                                            password:@"f0R3&@dM!n"
                                                                          persistence:NSURLCredentialPersistenceNone]];
        sharedUtility.client.operationQueue.maxConcurrentOperationCount = 1;
        
    });
    return sharedUtility;
}

-(NSString*)URLfromPath:(NSString*)path {
    NSURL *url = [NSURL URLWithString:[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] relativeToURL:self.rootURL];
    return [url absoluteString];
    //return [[self.rootURL absoluteString] stringByAppendingFormat:@"/%@",path];
}

-(void)downloadFileAtURL:(NSString*)urlString toPath:(NSString*)pathString {
    
    NSString *folder = [pathString stringByDeletingLastPathComponent];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folder]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:folder
      withIntermediateDirectories:YES
                       attributes:nil
                            error:nil];
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPRequestOperation *operation = [self.client HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Successfully downloaded file to %@",pathString);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@",error);
        if([[NSFileManager defaultManager] fileExistsAtPath:pathString]) {
            [[NSFileManager defaultManager] removeItemAtPath:pathString error:nil];
        }
    }];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:pathString append:NO];
     
    [self.client.operationQueue addOperation:operation];
}

-(void)updateDocuments {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"_vti_bin/Lists.asmx" relativeToURL:self.client.baseURL]];
    request.HTTPMethod = @"POST";
    [request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"http://schemas.microsoft.com/sharepoint/soap/GetListItems" forHTTPHeaderField:@"SOAPAction"];
	
    NSString *listName = @"Documents";
    NSString *postBody = [NSString stringWithFormat:@"<s:Envelope xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'>"
                          @"<s:Body>"
                          @"<GetListItems xmlns='http://schemas.microsoft.com/sharepoint/soap/' xmlns:i='http://www.w3.org/2001/XMLSchema-instance'>"
                          @"<listName>%@</listName>"
                          @"<queryOptions><QueryOptions>"
                          @"<IncludeMandatoryColumns>TRUE</IncludeMandatoryColumns>"
                          @"<ViewAttributes Scope=\"RecursiveAll\"/>"
                          @"<DateInUtc>TRUE</DateInUtc>"
                          @"</QueryOptions></queryOptions>"
                          @"<rowLimit>%d</rowLimit>"
                          @"</GetListItems>"
                          @"</s:Body>"
                          @"</s:Envelope>",listName,INT32_MAX];
    NSData *postData = [postBody dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    [request setHTTPBody:postData];

    AFHTTPRequestOperation *operation = [self.client HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = [NSDictionary dictionaryWithXMLData:responseObject];
        
        NSArray *documents = [[[[[[responseDictionary objectForKey:@"soap:Body"] objectForKey:@"GetListItemsResponse"] objectForKey:@"GetListItemsResult"] objectForKey:@"listitems"] objectForKey:@"rs:data"] objectForKey:@"z:row"];
        
        [[AppData sharedData] syncDocumentsWithArray:documents];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",[error description]);
    }];
    
    
    [operation start];
}


+(NSString*)getValueFromSharepointDict:(NSDictionary*)dict withKey:(NSString*)key {
    NSString *value = [dict valueForKey:[NSString stringWithFormat:@"_ows_%@",key]];
    NSArray *array = [value componentsSeparatedByString:@";#"];
    return [array lastObject];
}

@end
