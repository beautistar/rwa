//
//  Image.m
//  RMA
//
//  Created by Michael Beteag on 7/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "Image.h"

@implementation Image

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        [self setImage:[aDecoder decodeObjectForKey:@"image"]];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.image forKey:@"image"];
}

+(Image*)imageWithImage:(UIImage *)image size:(CGSize)size {
    Image *newImage = [[Image alloc] init];
    newImage.image = image;
    newImage.size = size;
    return newImage;
}

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    
    *HTMLSafe = YES;
    
    if(self.image != nil) {
        NSData *imageData = UIImageJPEGRepresentation(self.image, 1.0);
        NSString *encodedString = [imageData base64EncodingWithLineLength:0];

        NSString *text = [NSString stringWithFormat:@"<img alt=\"\" class=\"signature\" src=\"data:image/jpg;base64,%@\" />", encodedString];
        return text;
    } else {
        
        //return @"";
        return [NSString stringWithFormat:@"<div></div>"];
    }  
}
@end
