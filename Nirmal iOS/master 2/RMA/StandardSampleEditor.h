//
//  StandardSampleEditor.h
//  RMA
//
//  Created by Michael Beteag on 2/18/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"

@interface StandardSampleEditor : FormComponentViewEditor

@property (nonatomic, strong) FormComponentEditor *editor;
@property int currentSampleType;
@property int currentSampleIndex;

@property (weak, nonatomic) IBOutlet UISegmentedControl *typeControl;
- (IBAction)sampleTypeChanged:(id)sender;
+(StandardSampleEditor*)standardSampleEditor;

@end
