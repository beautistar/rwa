//
//  FormSelectionViewController.m
//  RMA
//
//  Created by Michael Beteag on 7/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormSelectionViewController.h"
#import "NSDate+Extensions.h"
#import "Updater.h"
#import "RMARefreshControl.h"
#import "Form+Extensions.h"
#import "AppDelegate.h"

@interface FormSelectionViewController ()

@end

@implementation FormSelectionViewController {
    id updateSuccessObserver;
    id updateFailureObserver;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateToolbar];
    
    __weak typeof (self) weakSelf = self;
    
    updateSuccessObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"UpdateDidSucceed"
                                                                              object:nil
                                                                               queue:nil
                                                                          usingBlock:^(NSNotification *note) {
                                                                              [weakSelf.refreshIndicator endRefreshing];
                                                                              [weakSelf updateTable];
                                                                              [weakSelf notifyPastDue];
                                                                          }];
    updateFailureObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"UpdateDidFail"
                                                                              object:nil
                                                                               queue:nil
                                                                          usingBlock:^(NSNotification *note) {
                                                                              [weakSelf.refreshIndicator endRefreshing];
                                                                          }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:updateSuccessObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:updateFailureObserver];
    self.navigationController.toolbarHidden = YES;
}

-(void)updateForms {
    if(![self.refreshIndicator isRefreshing]) {
        [self.refreshIndicator beginRefreshing];
    }
    
    Updater *updater = [[Updater alloc] init];
    [updater updateForms];
}
-(void)updateTable {
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);        
    }
    
    [self updateToolbar];
    [self.tableView reloadData];
    
}

-(void)addDebugForms {
    [[AppData sharedData] addDebugForms];
}

-(void)updateToolbar {
    if(self.incompleteOnly) {
        self.navigationController.toolbarHidden = YES;
        return;
    }
    int incompleteFormCount = [[AppData sharedData] getIncompleteFormCountForTechnician:self.technicianId projectId:self.projectId];
    if(incompleteFormCount > 0) {

        UIBarButtonItem *incompleteFormButton = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Incomplete forms (%i)",incompleteFormCount] style:UIBarButtonItemStylePlain target:self action:@selector(openIncompleteForms)];
        
        NSMutableArray *toolbarItems = [NSMutableArray array];
        [toolbarItems addObject:incompleteFormButton];
#ifdef DEBUG
        UIBarButtonItem *addDebug = [[UIBarButtonItem alloc] initWithTitle:@"Add Debug Fornms" style:UIBarButtonItemStylePlain target:self action:@selector(addDebugForms)];
        [toolbarItems addObject:addDebug];
#endif
        
        [self.navigationController.toolbar setItems:toolbarItems];
        self.navigationController.toolbarHidden = NO;
    } else {
        self.navigationController.toolbarHidden = YES;
    }
}
                                                 
-(void)openIncompleteForms {
    FormSelectionViewController *formSelectionViewController = [[FormSelectionViewController alloc] init];
    [self.navigationController pushViewController:formSelectionViewController animated:YES];
    formSelectionViewController.technicianId = self.technicianId;
    formSelectionViewController.projectId = self.projectId;
    
    formSelectionViewController.incompleteOnly = YES;
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateTable];
    
    if ([Updater updateNeededForCurrentUser]) {
        [self updateForms];
    } else {
        [self notifyPastDue];
    }
    
    [self updateToolbar];
}

-(void)notifyPastDue {
    NSMutableArray<NSString*> *pastDueDRNumbers = [NSMutableArray array];
    for (Form *form in self.fetchedResultsController.fetchedObjects) {
        if ([form isPastDue]) {
            [pastDueDRNumbers addObject:form.dispatchId];
        }
        if (pastDueDRNumbers.count == 10) {
            break;
        }
    }
    
    if (pastDueDRNumbers.count == 0) {
        return;
    }
    
    NSMutableString *message = [[NSMutableString alloc] initWithString:@"The following DRs are past due:\r\n\r\n"];
    for (NSString *drNumber in pastDueDRNumbers) {
        [message appendString:drNumber];
        [message appendString:@"\r\n"];
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Past Due DRs" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

-(NSFetchedResultsController*)formFetchedResultsController {
    [NSFetchedResultsController deleteCacheWithName:@"Root"];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:[[AppData sharedData] managedObjectContext]];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    if(self.projectId.length > 0) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND project.id ==[c] %@ AND type != %@", self.technicianId,self.projectId,@"Timesheet"];
    } else {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND dailyType != %@", self.technicianId,@"Timesheet"];
    }
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[AppData sharedData] managedObjectContext] sectionNameKeyPath:@"sectionIdentifier" cacheName:nil];
    return aFetchedResultsController;
}

-(NSFetchedResultsController*)incompleteFormFetchedResultsController {
    NSDate *date = [NSDate date];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.hour = -24;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    date = [gregorian dateByAddingComponents:components toDate:date options:0];
    
    [NSFetchedResultsController deleteCacheWithName:@"Root"];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:[[AppData sharedData] managedObjectContext]];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    if(self.projectId.length > 0) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND startDate <= %@ AND (status < 2 || status == 3 || status == nil) AND project.id ==[c] %@ AND type != %@", self.technicianId, date, self.projectId,@"Timesheet"];
    } else {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND startDate <= %@ AND (status < 2 || status == 3 || status == nil) AND dailyType != %@", self.technicianId, date, @"Timesheet"];
    }
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[AppData sharedData] managedObjectContext] sectionNameKeyPath:@"sectionIdentifier" cacheName:nil];
    return aFetchedResultsController;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([self.tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)]) {
        self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FormCell" bundle:nil] forCellReuseIdentifier:[FormCell reuseIdentifier]];
    
    self.title = self.incompleteOnly ? @"Incomplete Forms" : @"Forms";
    self.tableView.alwaysBounceVertical = YES;
    
    self.fetchedResultsController = self.incompleteOnly ? [self incompleteFormFetchedResultsController] : [self formFetchedResultsController];
    self.fetchedResultsController.delegate = self;
    
    [self updateTable];
    
    RMARefreshControl *refreshControl = [[RMARefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(updateForms) forControlEvents:UIControlEventValueChanged];
    self.refreshIndicator = refreshControl;
    
    [self.tableView addSubview:self.refreshIndicator];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> theSection = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [theSection name];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FormCell *cell = [tableView dequeueReusableCellWithIdentifier:[FormCell reuseIdentifier] forIndexPath:indexPath];
    
    Form *form = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.form = form;
    cell.delegate = self;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 96;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id <NSFetchedResultsSectionInfo> section = [[self.fetchedResultsController sections] objectAtIndex:indexPath.section];
    NSString *dateString = [section name];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    Form *form = [self.fetchedResultsController objectAtIndexPath:indexPath];
    appDelegate.dispatchId = form.dispatchId;
    
    if ([form.status isEqual:@(FormStatusCanceled)]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"This DR has been canceled or rescheduled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    if (![form isEditable]) {
        FormPreviewViewController *formPreviewViewController = [[FormPreviewViewController alloc] init];
        formPreviewViewController.form = form;
        formPreviewViewController.formBuilder = [FormBuilder formBuilderFromFile:[[AppData sharedData] pathToDocumentNamed:form.localFilename]];
        [self.navigationController pushViewController:formPreviewViewController animated:YES];
        return;
    }
    
    FormEditorViewController *formEditorViewController = [[FormEditorViewController alloc] init];
    formEditorViewController.form = form;
    formEditorViewController.title = [NSString stringWithFormat:@"%@ - %@",form.displayName, dateString];
    [self.navigationController pushViewController:formEditorViewController animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

#pragma mark - FormCellDelegate

-(void)rescheduleButtonPressedForForm:(Form *)form {
    RescheduleViewController *rescheduleViewController = [[RescheduleViewController alloc] init];
    rescheduleViewController.delegate = self;
    rescheduleViewController.form = form;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:rescheduleViewController];
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark RescheduleViewControllerDelegate 

-(void)didRescheduleForm:(Form *)form {
    [[AppData sharedData] setFormStatus:FormStatusCanceled forFormId:form.dispatchId];
    [self updateTable];
}

@end
