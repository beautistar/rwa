//
//  SubmissionManager.h
//  RMA
//
//  Created by Alexander Roode on 6/27/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
@class FormBuilder;

@protocol SubmissionManagerDelegate <NSObject>
-(void)hoursSubmissionDidSucceed;
-(void)xmlSubmissionDidSucceed;
-(void)submissionDidFail:(NSError*)error;
-(void)formUploadDidSucceed;
-(void)getFormsDidSucceed;
@end

@interface SubmissionManager : NSObject

@property (nonatomic) id<SubmissionManagerDelegate> delegate;

-(instancetype)initWithDelegate:(id<SubmissionManagerDelegate>)delegate;

-(void)submitHours:(FormBuilder*)formBuilder;
-(void)submitXml:(FormBuilder*)formBuilder;
-(void)submitMasterForm:(NSString *)formName active:(BOOL)isActive formId:(NSString *)formId fileName:(NSString *)fileName;
-(void)getMasterForms;
-(void)submitDRForm:(FormBuilder *)formBuilder;

@end
