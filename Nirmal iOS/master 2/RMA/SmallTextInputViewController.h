//
//  SmallTextInputViewController.h
//  RMA
//
//  Created by Michael Beteag on 6/25/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RMATextField;
@interface SmallTextInputViewController : UIViewController

@property (nonatomic, copy) void (^complete)(NSString*);
@property (nonatomic, copy) void (^dismissed)(void);
@property (weak, nonatomic) IBOutlet RMATextField *textField;
- (IBAction)okButtonPressed:(id)sender;
@end
