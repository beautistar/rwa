//
//  FullScreenWebViewViewController.h
//  RMA
//
//  Created by Michael Beteag on 12/18/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullScreenWebViewViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, retain) NSURL *url;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *password;

@end
