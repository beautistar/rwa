//
//  FormValidationError.h
//  RMA
//
//  Created by Michael Beteag on 8/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FormValidationError : NSObject

@property (nonatomic, retain) NSString *message;
@property CGPoint scrollPoint;

+(FormValidationError*)FormValidationErrorWithMessage:(NSString*)message scrollPoint:(CGPoint)point;


@end
