//
//  CollectionEditor.m
//  RMA
//
//  Created by Michael Beteag on 2/5/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "CollectionEditor.h"
#import "FormComponentViewEditor.h"
#import "HiddenLine.h"
#import "Constants.h"

@implementation CollectionEditor

+(instancetype)collectionEditorWithInstantiationBlock:(FormComponentEditor*(^)())instantiationBlock key:(NSString*)editorKey itemKey:(NSString*)itemKey itemName:(NSString*)itemName {
    return [self collectionEditorWithInstantiationBlock:instantiationBlock key:editorKey itemKey:itemKey itemName:itemName maxCount:0];
}

+(instancetype)collectionEditorWithInstantiationBlock:(FormComponentEditor *(^)())instantiationBlock key:(NSString *)editorKey itemKey:(NSString *)itemKey itemName:(NSString *)itemName maxCount:(int)maxCount {
    CollectionEditor *editor = [[self alloc] initWithFrame:CGRectMake(0, 0,
                                                                      768,
                                                                      kBottomBarHeight)];
    
    editor.instantiationBlock = instantiationBlock;
    editor.editorKey = editorKey;
    editor.itemKey = itemKey;
    editor.itemName = itemName;
    editor.items = [NSMutableArray array];
    editor.deleteButtons = [NSMutableArray array];
    editor.maxCount = maxCount;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:editor selector:@selector(layoutItems) name:@"CollectionItemDidChangeSize" object:nil];
    
    editor.addButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [editor layoutAddButton:editor.addButton itemName:itemName];
    
    editor.addButton.frame = CGRectMake(kPaddingX/2,
                                        kPaddingY,
                                        editor.addButton.frame.size.width,
                                        editor.addButton.frame.size.height);
    
    [editor.addButton addTarget:editor action:@selector(addItem) forControlEvents:UIControlEventTouchUpInside];
    [editor.view addSubview:editor.addButton];
    [editor layoutItems];
    
    return editor;
}

-(void)layoutAddButton:(UIButton*)button itemName:(NSString*)itemName {
    [button setImage:[UIImage imageNamed:@"icon-add"] forState:UIControlStateNormal];
    [button setTitle:[NSString stringWithFormat:@"Add %@", itemName] forState:UIControlStateNormal];
    
    [button sizeToFit];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0.5*(button.frame.size.height - 25),
                                                          0,
                                                          0.5*(button.frame.size.height - 25),
                                                          button.frame.size.width - 25)];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(button.titleEdgeInsets.top,
                                                          button.titleEdgeInsets.left - 25,
                                                          button.titleEdgeInsets.bottom,
                                                          button.titleEdgeInsets.right)];
}

-(void)clearView {
    for(UIView *subview in self.view.subviews) {
        if(subview != self.addButton) {
            [subview removeFromSuperview];
        }
    }
}

-(void)layoutItems {
    int currentHeight = self.view.frame.size.height;
    
    [self clearView];
    
    self.view.frame = CGRectMake(0, self.view.frame.origin.y, 768, 0);
    int currentY = 0;
    
    for(int i = 0; i < self.items.count; i++) {
        FormComponentEditor *editor = self.items[i];
        UIView *view = nil;
        if([editor isKindOfClass:[FormComponentViewEditor class]]) {
            view = editor.view;
        } else {
            view = editor;
        }
    
        if(i % 2 == 0) {
            view.backgroundColor = [UIColor colorWithRed:251/255.0 green:251/255.0 blue:251/255.0 alpha:1.0];
        }
        
        view.frame = CGRectMake(view.frame.origin.x,
                                currentY,
                                view.frame.size.width,
                                view.frame.size.height);
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height + view.frame.size.height);
        [self.view addSubview:view];
        currentY += view.frame.size.height;
    }

    self.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height + kBottomBarHeight);
    self.addButton.frame = CGRectMake(kPaddingX,
                                      self.view.frame.size.height - kPaddingY - self.addButton.frame.size.height,
                                      self.addButton.frame.size.width,
                                      self.addButton.frame.size.height);
    
    int finalHeight = self.view.frame.size.height;
    
    if(self.items.count == self.maxCount && self.maxCount != 0) {
        self.addButton.enabled = NO;
    } else {
        self.addButton.enabled = YES;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FormDidChangeSize" object:[NSNumber numberWithInt:finalHeight-currentHeight]];
}

-(void)addDeleteButtonToView:(UIView*)view {
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [deleteButton setImage:[UIImage imageNamed:@"icon-delete"] forState:UIControlStateNormal];
    deleteButton.frame = CGRectMake(kPaddingX,
                                   kPaddingY + 10,
                                   25,
                                   25);
    
    [deleteButton addTarget:self action:@selector(deleteItem:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.deleteButtons addObject:deleteButton];
    
    [view addSubview:deleteButton];
}

-(void)addItem {
    FormComponentEditor *editor =  self.instantiationBlock();
    [self addItem:editor];
}

-(void)addItem:(FormComponentEditor*)item {
    item.validationType = self.validationType;
    [self.items addObject:item];
    
    [self addDeleteButtonToView:item.view];
    [self layoutItems];
}

-(void)deleteItem:(UIButton*)sender {
    int index = (int)[self.deleteButtons indexOfObject:sender];
    [self.items removeObjectAtIndex:index];
    [self.deleteButtons removeObjectAtIndex:index];
    [self layoutItems];
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    NSMutableArray *items = [NSMutableArray array];
    for (int i = 0; i < self.items.count; i++) {
        FormComponentEditor *item = [self.items objectAtIndex:i];
        NSDictionary *itemData = [item getData];
        [items addObject:itemData];
    }
    
    [data setObject:items forKey:self.editorKey];
    return data;
}

-(NSMutableDictionary*)getDataForRendering {
    if(self.maxCount == 0) {
        return [self getData];
    }

    NSMutableDictionary *data = [self getData];
    NSMutableArray *items = data[self.editorKey];
    int difference = self.maxCount - (int)items.count;
    for(int i = 0; i < difference; i++) {
        [items addObject:[self getEmptyItem]];
    }
    
    return data;
}

-(NSDictionary*)getEmptyItem {
    return @{@"t1":[[HiddenLine alloc] init],
             @"t2":@"",
             @"t3":@"",
             @"t4":@"",
             @"t5":@"",
             @"t6":@"",
             @"t7":@"",
             @"t8":@"",
             @"t9":@"",
             @"t10":@"",
             @"t11":@"",
             @"t12":@"",
             @"t13":@"",
             @"t14":@"",
             @"t15":@""};
}

-(void)setData:(NSMutableDictionary*)data {
    NSMutableArray *items = [data objectForKey:self.editorKey];
    for(int i = 0; i < items.count; i++) {
        [self addItem];

        FormComponentEditor *editor = self.items[i];
        [editor setData:items[i]];
    }
}

-(BOOL)validates:(NSMutableArray *__autoreleasing *)validationErrors referenceView:(UIView *)referenceView {
    BOOL validationSuccess = YES;
    for(FormComponentEditor *item in self.items) {
        validationSuccess = [item validates:validationErrors referenceView:referenceView] && validationSuccess;
    }
    
    return validationSuccess;
}

-(NSDictionary*)dictionary {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    NSMutableArray *items = [NSMutableArray array];
    for (int i = 0; i < self.items.count; i++) {
        FormComponentEditor *item = self.items[i];
        NSDictionary *itemDictionary = [item dictionary];
        [items addObject:itemDictionary];
    }
    
    dictionary[self.editorKey] = items;
    return dictionary;
}

@end
