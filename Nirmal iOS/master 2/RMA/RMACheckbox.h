//
//  RMACheckbox.h
//  RMA
//
//  Created by Alexander Roode on 1/22/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMACheckbox : UIButton
@property (nonatomic) BOOL checked;
@end
