//
//  PDFViewer.h
//  rushmore
//
//  Created by Michael Beteag on 7/19/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuickLook/QuickLook.h>
@class PreviewItem;
@interface PDFViewer : NSObject <QLPreviewControllerDataSource, QLPreviewControllerDelegate>

@property (nonatomic, retain) UINavigationController *navigationController;
@property (nonatomic, retain) PreviewItem *previewItem;

+ (id)sharedViewer;
-(void)viewPDF:(NSString*)path withTitle:(NSString*)title;
@end
