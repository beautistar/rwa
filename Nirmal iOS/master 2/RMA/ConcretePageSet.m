//
//  ConcretePageSet.m
//  RMA
//
//  Created by Michael Beteag on 7/31/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "ConcretePageSet.h"

@implementation ConcretePageSet

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        [self setSampleType:[aDecoder decodeObjectForKey:@"sampleType"]];
        [self setSetNo:[aDecoder decodeObjectForKey:@"setNo"]];
        [self setMixDesignNo:[aDecoder decodeObjectForKey:@"mixDesignNo"]];
        [self setMixDesignStrength:[aDecoder decodeObjectForKey:@"mixDesignStrength"]];
        [self setAdmixtures:[aDecoder decodeObjectForKey:@"admixtures"]];
        [self setNoOfSamples:[aDecoder decodeObjectForKey:@"noOfSamples"]];
        [self setBreaks:[aDecoder decodeObjectForKey:@"breaks"]];
        [self setTripTicketNo:[aDecoder decodeObjectForKey:@"tripTicketNo"]];
        [self setSlumpPen:[aDecoder decodeObjectForKey:@"slumpPen"]];
        [self setMtlTempCF:[aDecoder decodeObjectForKey:@"mtlTempCF"]];
        [self setAirTempCF:[aDecoder decodeObjectForKey:@"airTempCF"]];
        [self setAirContent:[aDecoder decodeObjectForKey:@"airContent"]];
        [self setUnitWt:[aDecoder decodeObjectForKey:@"unitWt"]];
        [self setPlacedAt:[aDecoder decodeObjectForKey:@"placedAt"]];
        [self setAirTempUnits:[aDecoder decodeObjectForKey:@"airTempUnits"]];
        [self setMtlTempUnits:[aDecoder decodeObjectForKey:@"mtlTempUnits"]];
        [self setDesignStrengthUnits:[aDecoder decodeObjectForKey:@"designStrengthUnits"]];
        [self setTimeCast:[aDecoder decodeObjectForKey:@"timeCast"]];

    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.sampleType forKey:@"sampleType"];
    [aCoder encodeObject:self.setNo forKey:@"setNo"];
    [aCoder encodeObject:self.mixDesignNo forKey:@"mixDesignNo"];
    [aCoder encodeObject:self.mixDesignStrength forKey:@"mixDesignStrength"];
    [aCoder encodeObject:self.admixtures forKey:@"admixtures"];
    [aCoder encodeObject:self.noOfSamples forKey:@"noOfSamples"];
    [aCoder encodeObject:self.breaks forKey:@"breaks"];
    [aCoder encodeObject:self.tripTicketNo forKey:@"tripTicketNo"];
    [aCoder encodeObject:self.slumpPen forKey:@"slumpPen"];
    [aCoder encodeObject:self.mtlTempCF forKey:@"mtlTempCF"];
    [aCoder encodeObject:self.airTempCF forKey:@"airTempCF"];
    [aCoder encodeObject:self.airContent forKey:@"airContent"];
    [aCoder encodeObject:self.unitWt forKey:@"unitWt"];
    [aCoder encodeObject:self.placedAt forKey:@"placedAt"];
    [aCoder encodeObject:self.designStrengthUnits forKey:@"designStrengthUnits"];
    [aCoder encodeObject:self.airTempUnits forKey:@"airTempUnits"];
    [aCoder encodeObject:self.mtlTempUnits forKey:@"mtlTempUnits"];
    [aCoder encodeObject:self.timeCast forKey:@"timeCast"];
    
}

+(ConcretePageSet*)concretePageSetFromDictionary:(NSMutableDictionary*)data {
    ConcretePageSet *set = [[ConcretePageSet alloc] init];
    set.sampleType = [data objectForKey:@"sampleType"];
    set.setNo = [data objectForKey:@"setNo"] ;
    set.mixDesignNo = [data objectForKey:@"mixDesignNo"];
    set.mixDesignStrength = [data objectForKey:@"mixDesignStrength"];
    set.admixtures = [data objectForKey:@"admixtures"];
    set.noOfSamples = [data objectForKey:@"noOfSamples"];
    set.breaks = [data objectForKey:@"breaks"];
    set.tripTicketNo = [data objectForKey:@"tripTicketNo"];
    set.slumpPen = [data objectForKey:@"slumpPen"];
    set.mtlTempCF = [data objectForKey:@"mtlTempCF"];
    set.airTempCF = [data objectForKey:@"airTempCF"];
    set.airContent = [data objectForKey:@"airContent"];
    set.unitWt = [data objectForKey:@"unitWt"];
    set.placedAt = [data objectForKey:@"placedAt"];
    set.timeCast = [data objectForKey:@"timeCast"];
    set.designStrengthUnits =  [data objectForKey:@"designStrengthUnits"];
    set.airTempUnits = [data objectForKey:@"airTempUnits"];
    set.mtlTempUnits = [data objectForKey:@"mtlTempUnits"];
    
    return set;
}

-(NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                          context:(GRMustacheContext *)context
                         HTMLSafe:(BOOL *)HTMLSafe
                            error:(NSError **)error {
    GRMustacheTemplate *partial = [tag.templateRepository templateNamed:@"concretePageSet" error:NULL];
    context = [context contextByAddingObject:self];
    return [partial renderContentWithContext:context HTMLSafe:HTMLSafe error:error];
    
}

@end
