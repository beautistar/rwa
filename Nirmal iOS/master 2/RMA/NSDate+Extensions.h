//
//  NSDate+Extensions.h
//  RMA
//
//  Created by Michael Beteag on 9/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extensions)

+(NSDate*)dateFromISO8601String:(NSString*)string;
-(NSString*)timeString;

@end