//
//  ImageCollection.h
//  RMA
//
//  Created by Michael Beteag on 8/21/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"
#import "NSData+Base64.h"

@interface ImageCollection : NSObject <NSCoding, GRMustacheRendering>

@property (nonatomic, retain) NSMutableArray *images;
@property (nonatomic, retain) NSMutableArray *descriptions;

-(NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                          context:(GRMustacheContext *)context
                         HTMLSafe:(BOOL *)HTMLSafe
                            error:(NSError **)error;
+(ImageCollection*)imageCollectionWithImages:(NSMutableArray*)images descriptions:(NSMutableArray*)descriptions;

@end
