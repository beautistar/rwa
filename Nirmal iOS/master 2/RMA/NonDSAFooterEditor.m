//
//  NonDSAFooterEditor.m
//  RMA
//
//  Created by Michael Beteag on 10/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "NonDSAFooterEditor.h"
#import "Constants.h"

@implementation NonDSAFooterEditor

+(NonDSAFooterEditor*)nonDSAFooterEditorWithKey:(NSString *)key {
    NonDSAFooterEditor *editor = [[NonDSAFooterEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 227)];
    editor.key = key;
    editor.validationType = validationTypeAll;
    
    editor.mainEditor = [[FormComponentViewEditor alloc] initWithNibName:@"NonDSAFooterEditorNarrow"];
    SignatureEditor *signatureEditor1 = [[SignatureEditor alloc] initWithFrame:CGRectMake(0, 0, kSignatureEditorWidth, kSignatureEditorHeight) name:@"" key:kSignatureEditorKey];
    //SignatureEditor *signatureEditor2 = [[SignatureEditor alloc] initWithFrame:CGRectMake(0, kSignatureEditorHeight, kSignatureEditorWidth, kSignatureEditorHeight) name:kClientSignatureEditorName key:kClientSignatureEditorKey];
    
    editor.editors = @[editor.mainEditor, signatureEditor1];
    
    [editor.view addSubview:signatureEditor1];
    //[editor.view addSubview:signatureEditor2];
    [editor.view addSubview:editor.mainEditor.view];
    editor.mainEditor.view.frame = CGRectMake(kSignatureEditorWidth, 0,384,editor.mainEditor.view.frame.size.height);
    
    
    return editor;
}
-(void)setEmployeeType:(NSString*)employeeType {
    SignatureEditor * editor = [self.editors objectAtIndex:1];
    [editor setLabel:[NSString stringWithFormat:@"%@ Signature",employeeType]];
}

-(BOOL)validates:(NSMutableArray**)validationErrors referenceView:(UIView*)referenceView {
    [self requireAllFields:self.view.subviews validationErrors:validationErrors referenceView:referenceView];
    [self requireAllFields:self.mainEditor.view.subviews validationErrors:validationErrors referenceView:referenceView];
    
    return *validationErrors == nil;
    
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    for(FormComponentEditor *editor in self.editors) {
        [data addEntriesFromDictionary:[editor getData]];
    }
    return data;
}

-(void)setData:(NSMutableDictionary *)data {
    for(FormComponentEditor *editor in self.editors) {
        [editor setData:data];
    }
}

-(NSDictionary*)dictionary {
    return [self.mainEditor dictionary];
}

@end
