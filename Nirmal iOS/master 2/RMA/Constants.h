//
//  Constants.h
//  RMA
//
//  Created by Alexander Roode on 3/15/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#define kPaddingX 10
#define kPaddingY 5
#define kLabelHeight 20
#define kSegmentedControlHeight 44
#define kSegmentedControlBarHeight 30
#define kPaddingYWithSegmentedControl 25
#define kBottomBarHeight 55
#define kConcretePageSetHorizontalPadding 20
#define kConcretePageSetVerticalPadding 4
#define kConcretePageSetButtonWidth 100
#define kConcretePageSetButtonHeight 49
#define kConcretePageSetEditorHeight 654
#define kAdditionalRemarksButtonWidth 180
#define kSignatureEditorName @"Inspector signature"
#define kSignatureEditorKey @"technicianSignature"
#define kSignatureEditorWidth 384
#define kSignatureEditorHeight 154
#define kClientSignatureEditorKey @"clientSignature"
#define kClientSignatureEditorName @"Client authorization"
#define kRequiredLabelPadding 3
#define kDeleteButtonWidth 25
#define kDeleteButtonHeight 25
#define kViewPDFButtonWidth 100
#define kViewPDFButtonHeight 25
#define kPhotoPageHeaderHeight 60
#define kPhotoPageRowHeight 300
#define kEditorWidth 768
#define kRMAWebServiceRootUrl @"http://rmaht.forebrain.net/api"
#define kHourTrackingUrl @"http://rmaht.forebrain.net/api"
