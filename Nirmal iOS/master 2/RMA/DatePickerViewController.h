//
//  DatePickerViewController.h
//  RMA
//
//  Created by Alexander Roode on 1/21/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerDelegate
-(void)didEnterDate:(NSString*)date;
@end

@interface DatePickerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, weak) id<DatePickerDelegate> delegate;
@property UIDatePickerMode mode;
@property (nonatomic, strong) NSLocale *locale;
- (IBAction)setButtonPressed:(id)sender;
-(void)setDateFromString:(NSString*)dateString;
@end
