//
//  AppData.m
//  RMA
//
//  Created by Michael Beteag on 7/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "AppData.h"
#import "NSDate+Extensions.h"
#import "Document.h"
#import "Folder.h"
#import "SharepointUtility.h"
#import "Document+Extensions.h"
@implementation AppData


+ (id)sharedData {
    static AppData *sharedData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedData = [[self alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        sharedData.documentsDirectory = basePath;
        sharedData.client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:[AppData webServiceUrl]]];
        
    });
    return sharedData;
}

+(NSString*)webServiceUrl {
    NSString *webServiceUrl = @"http://174.127.39.132/DRAPP/";
#ifdef GEOSCIENCE
    webServiceUrl = @"http://24.199.54.210/DRAPP/";
#endif
    
    return webServiceUrl;
}

-(NSArray*)getDefaultProjects {
    NSArray *seed = @[@{@"Project":@"12-090-0",
                        @"Descr":@"San Jose Convention Center Expansion",
                        @"ClientName":@"City of San Jose"},
                      @{@"Project":@"11-038-0",
                        @"Descr":@"Warm Springs Extension LTSS – BART",
                        @"ClientName":@"Warm Springs Constructors"},
                      @{@"Project":@"13-942-0",
                        @"Descr":@"Apple Campus 2",
                        @"ClientName":@"City of Cupertino"},
                      @{@"Project":@"09-098-0",
                        @"Descr":@"I-405 Sepulveda Pass Widening Project",
                        @"ClientName":@"Kiewit Pacific"},
                       @{@"Project":@"11-068-0",
                         @"Descr":@"Metro Gold Line: Phase 2A Foothill Ext",
                         @"ClientName":@"Foothill Transit Constructors"},
                       @{@"Project":@"12-710-0",
                         @"Descr":@"11-2T2004, I-805 HOV/BRT Design Build",
                         @"ClientName":@"Skanska USA Civil West California District, Inc."},
                       @{@"Project":@"11-615-0",
                         @"Descr":@"SCCRA Train Control & Operations Support",
                         @"ClientName":@"USS Cal Builders"},
                      @{@"Project":@"12-823-0",
                        @"Descr":@"Kaiser Antelope Valley Specialty MOB",
                        @"ClientName":@"Kaiser Foundation Health Plan, Inc"},
                      @{@"Project":@"11-583-0",
                        @"Descr":@"Light Rail Transit Phase 2 - Expo Metro",
                        @"ClientName":@"Skanska-Rados Expo 2 Joint Venture"},
                      @{@"Project":@"12-880-0",
                        @"Descr":@"Orangethorpe Avenue Grade Separation",
                        @"ClientName":@"Flatiron West Inc"},
                      @{@"Project":@"12-880-0",
                        @"Descr":@"Kaiser Antelope Valley Specialty MOB",
                        @"ClientName":@"Kaiser Foundation Health Plan, Inc"},
                      @{@"Project":@"12-685-0",
                        @"Descr":@"Carlsbad Desalination Plant",
                        @"ClientName":@"Kiewit Shea Desalination"}
                      ];
    
    return seed;
}
-(NSDate*)getMondayOfWeekContainingDate:(NSDate*)date {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYearForWeekOfYear | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday fromDate:date];
    [components setWeekday:2];
    return [calendar dateFromComponents:components];
}

-(NSArray*)getFormDatesOfWeekForTechnicianId:(NSString*)technicianId duringWeekOfDate:(NSDate*)date {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *oneWeekComponents = [[NSDateComponents alloc] init];
    oneWeekComponents.weekOfYear = 1;
    NSDate *endDate = [calendar dateByAddingComponents:oneWeekComponents toDate:date options:0];
    
    NSMutableArray *uniqueDates = [NSMutableArray array];
    
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:self.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND dailyType !=[c] %@ AND startDate >= %@ AND startDate < %@", technicianId, @"Timesheet",date,endDate];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Form *form in fetchedObjects) {
        NSDate *date = form.startDate;
        
        if([uniqueDates indexOfObject:date] == NSNotFound) {
            [uniqueDates addObject:date];
        }
    }
    return uniqueDates;
}

-(NSArray*)getUniqueDRsForTechnicianId:(NSString*)technicianId duringWeekOfDate:(NSDate*)date {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *oneWeekComponents = [[NSDateComponents alloc] init];
    oneWeekComponents.weekOfYear = 1;
    NSDate *endDate = [calendar dateByAddingComponents:oneWeekComponents toDate:date options:0];

    NSMutableArray *uniqueDRs = [NSMutableArray array];
    
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:self.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND dailyType !=[c] %@ AND startDate >= %@ AND startDate < %@", technicianId, @"Timesheet",date,endDate];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Form *form in fetchedObjects) {
        NSString *DRNumber = form.dispatchId;
        NSString *taskCode = form.task;
        NSArray *DR = @[DRNumber,taskCode];
        if([uniqueDRs indexOfObject:DR] == NSNotFound) {
            [uniqueDRs addObject:DR];
        }
    }
    return uniqueDRs;
}

-(void)createTimeSheetsForTechnician:(NSString*)technicianId {
    NSError *error;
    
    //NSMutableDictionary *timesheetsByWeek = [NSMutableDictionary dictionary];
    
    NSMutableArray *weeks = [NSMutableArray array];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:self.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND dailyType !=[c] %@", technicianId, @"Timesheet"];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Form *form in fetchedObjects) {
        NSLog(@"Found form type %@", form.dailyType);
        
        NSDate *mondayDate = [self getMondayOfWeekContainingDate:form.startDate];
        
        if([weeks indexOfObject:mondayDate] == NSNotFound) {
            [weeks addObject:mondayDate];
        }
        /*if([timesheetsByWeek objectForKey:mondayDate] == nil) {
            timesheetsByWeek setObject:[NSMutableArray array] forKey:
        }*/
    }
    
    //NSCalendar *calendar = [NSCalendar currentCalendar];
    
    for(NSDate *startDate in weeks) {
        
        Form *form = [self timeSheetWithStartDate:startDate forTechnicianId:technicianId];
        if(form != nil) {
            NSLog(@"Timesheet already found for date %@", startDate);
            continue;
        }
        
        form = [NSEntityDescription insertNewObjectForEntityForName:@"Form" inManagedObjectContext:self.managedObjectContext];

        form.technician = [self technicianWithId:technicianId];
        form.startDate = startDate;
        form.dailyType = @"Timesheet";

        /*NSDateComponents *oneWeekComponents = [[NSDateComponents alloc] init];
        oneWeekComponents.week = 1;
        NSDate *endDate = [calendar dateByAddingComponents:oneWeekComponents toDate:startDate options:nil];
        */
    }
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error syncing documents: %@", [error localizedDescription]);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidUpdateTimesheets" object:nil];

}

-(Form*)timeSheetWithStartDate:(NSDate*)startDate forTechnicianId:(NSString*)id {
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:self.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"technician.id ==[c] %@ AND dailyType ==[c] %@ AND startDate == %@", id, @"Timesheet", startDate];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Form *timesheet in fetchedObjects) {
        return timesheet;
    }
    return nil;
}

-(Employee*)technicianWithId:(NSString*)id {
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Employee" inManagedObjectContext:self.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"id ==[c] %@", id];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Employee *technician in fetchedObjects) {
        return technician;
    }
    return nil;
}


-(NSString*)pathToDocumentNamed:(NSString*)filename {
    return [self.documentsDirectory stringByAppendingPathComponent:filename];
}

-(NSString*)getGuidFilenameWithExtension:(NSString*)extension {
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    NSString *uuidStr = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    
    return [NSString stringWithFormat:@"%@.%@", uuidStr,extension];
}

-(Folder*)getRootFolder {
    return [self folderWithPath:@"rma/Documents"];
}

-(Folder*)folderWithPath:(NSString*)path {
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Folder" inManagedObjectContext:self.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"path ==[c] %@", path];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Folder *folder in fetchedObjects) {
        return folder;
    }
    return nil;
    
    /*
    Folder *folder = [NSEntityDescription insertNewObjectForEntityForName:@"Folder" inManagedObjectContext:self.managedObjectContext];
    folder.path = path;
    NSMutableArray *pathComponents = [NSMutableArray arrayWithArray:[path componentsSeparatedByString:@"/"]];
    if(pathComponents.count > 0) {
        [pathComponents removeObject:[pathComponents lastObject]];
        NSString *parentPath = [pathComponents componentsJoinedByString:@"/"];
        Folder *parentFolder = [self folderWithPath:parentPath];
        
        [parentFolder addSubfoldersObject:folder];
    }
    return folder;*/
}



-(Folder*)folderWithId:(int)id {
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Folder" inManagedObjectContext:self.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"id == %i", id];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Folder *folder in fetchedObjects) {
        return folder;
    }
    return nil;
}
-(Document*)documentWithId:(int)id {
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Document" inManagedObjectContext:self.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"id == %i", id];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Document *document in fetchedObjects) {
        return document;
    }
    return nil;
}

-(void)syncDocumentsWithArray:(NSArray*)documentArray {
    NSError *error;
    Folder *rootFolder = [self folderWithPath:@"rma/Documents"];
    if(rootFolder == nil) {
        rootFolder = [NSEntityDescription insertNewObjectForEntityForName:@"Folder" inManagedObjectContext:self.managedObjectContext];
        rootFolder.modified = [NSDate date];
        rootFolder.path = @"rma/Documents";
        rootFolder.name = @"Field File";
        rootFolder.id = [NSNumber numberWithInt:0];
    }
    
    NSMutableArray *folderIDs = [NSMutableArray array];
    [folderIDs addObject:[NSNumber numberWithInt:0]];
    NSMutableArray *documentIDs = [NSMutableArray array];
    
    for(NSDictionary *objectDictionary in documentArray) {
        int type = [[SharepointUtility getValueFromSharepointDict:objectDictionary withKey:@"FSObjType"] intValue];
        
        int id = [[SharepointUtility getValueFromSharepointDict:objectDictionary withKey:@"ID"] intValue];
        NSString *dateString = [SharepointUtility getValueFromSharepointDict:objectDictionary withKey:@"Modified"];
        NSDate *date = [NSDate dateFromISO8601String:dateString];
        NSString *filename = [SharepointUtility getValueFromSharepointDict:objectDictionary withKey:@"LinkFilename"];
        
        NSString *name = [SharepointUtility getValueFromSharepointDict:objectDictionary withKey:@"Title"];
        if(name.length == 0) {
            name = filename;
        }
        NSString *path = [SharepointUtility getValueFromSharepointDict:objectDictionary withKey:@"FileRef"];

        if(type ==1) {
            [folderIDs addObject:[NSNumber numberWithInt:id]];
            Folder *folder = [self folderWithId:id];
            if(folder == nil) {
                folder = [NSEntityDescription insertNewObjectForEntityForName:@"Folder" inManagedObjectContext:self.managedObjectContext];
                NSString *parentPath = [path stringByDeletingLastPathComponent];
                Folder *parentFolder = [self folderWithPath:parentPath];
                if(parentFolder != nil) {
                    [parentFolder addSubfoldersObject:folder];
                } else {
                    
                }
            }
            if(folder.modified == nil || [date compare:folder.modified] == NSOrderedDescending) {
                folder.path = path;
                folder.modified = [NSDate date];
                folder.id = [NSNumber numberWithInt:id];
                folder.name = name;
            }
        } else {
            [documentIDs addObject:[NSNumber numberWithInt:id]];
            Document *document = [self documentWithId:id];
            
            if(document == nil) {
                document = [NSEntityDescription insertNewObjectForEntityForName:@"Document" inManagedObjectContext:self.managedObjectContext];
                NSString *parentPath = [path stringByDeletingLastPathComponent];
                Folder *parentFolder = [self folderWithPath:parentPath];
                if(parentFolder != nil) {
                    [parentFolder addDocumentsObject:document];
                } else {
                    
                }
            }
            
            if(document.modified == nil || [date compare:document.modified] == NSOrderedDescending) {
                document.id = [NSNumber numberWithInt:id];
                document.url = [[SharepointUtility sharedUtilty] URLfromPath:path];
                document.path = path;
                document.filename = filename;
                document.extension = [[filename componentsSeparatedByString:@"."] lastObject];
                document.modified = date;
                if(name.length >0) {
                    document.name = name;
                } else {
                    document.name = filename;
                }
                
                if([[NSFileManager defaultManager] fileExistsAtPath:[document getAbsolutePath]]) {
                    [[NSFileManager defaultManager] removeItemAtPath:[document getAbsolutePath] error:&error];
                }
                
            }
            if(![[NSFileManager defaultManager] fileExistsAtPath:[document getAbsolutePath]]) {
                [[SharepointUtility sharedUtilty] downloadFileAtURL:document.url toPath:[document getAbsolutePath]];
            }
        }
    }
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error syncing documents: %@", [error localizedDescription]);
    }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Document" inManagedObjectContext:self.managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (id in %@)", documentIDs];
    request.predicate = predicate;
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Document *document in fetchedObjects) {
        if([[NSFileManager defaultManager] fileExistsAtPath:[document getAbsolutePath]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[document getAbsolutePath] error:NULL];
        }
        [self.managedObjectContext deleteObject:document];
    };
    
    request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Folder" inManagedObjectContext:self.managedObjectContext]];
    predicate = [NSPredicate predicateWithFormat:@"NOT (id in %@)", folderIDs];
    request.predicate = predicate;
    
    fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (Folder *folder in fetchedObjects) {
        [self.managedObjectContext deleteObject:folder];
    };
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error deleting old docs and folders %@", [error localizedDescription]);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidUpdateDocuments" object:nil];
    
    
    
}

-(void)updateForms:(NSArray *)formData {
    NSError *error;
    
    NSMutableArray *idsToKeep = [NSMutableArray array];
    
    for(id formDict in formData) {

#ifdef GEOSCIENCE
        NSString *cvtFlag = formDict[@"CVT"];
#ifdef CVT
        if ([cvtFlag isEqualToString:@"N"]) {
            continue;
        }
#else
        if ([cvtFlag isEqualToString:@"Y"]) {
            continue;
        }
#endif
#else
        NSString *siteScanFlag = formDict[@"SiteScan"];
#ifdef SITESCAN
        if ([siteScanFlag isEqualToString:@"N"]) {
            continue;
        }
#else
        if ([siteScanFlag isEqualToString:@"Y"]) {
            continue;
        }
#endif
#endif
        
        //Check if there exists a form with the form ID
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSString *dispatchID = [formDict valueForKey:@"DispID"];
        [idsToKeep addObject:dispatchID];
        [request setEntity:[NSEntityDescription entityForName:@"Form" inManagedObjectContext:self.managedObjectContext]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dispatchId == %@", dispatchID];
        request.predicate = predicate;

        NSArray *forms = [self.managedObjectContext executeFetchRequest:request error:&error];
        if(forms.count == 0) {
            Form *form = [NSEntityDescription insertNewObjectForEntityForName:@"Form" inManagedObjectContext:self.managedObjectContext];
            
            form.dispatchId = dispatchID;
            
            form.checklists = [formDict objectForKey:@"CheckLists"];
            form.project = [self addOrGetProject:formDict];
            form.technician = [self addOrGetTechnician:formDict];
            form.contact = [self addOrGetContact:formDict];
            form.startDate = [NSDate dateFromISO8601String:[formDict valueForKey:@"StartDate"]];
            form.startTime = [NSDate dateFromISO8601String:[formDict valueForKey:@"StartTime"]];
            form.hours = [formDict valueForKey:@"Hours"];
            form.scope = [formDict valueForKey:@"Scope"];
            form.dailyType = [formDict valueForKey:@"DailyForm"];

            Address* address = [NSEntityDescription insertNewObjectForEntityForName:@"Address" inManagedObjectContext:self.managedObjectContext];
            address.address1 = [formDict valueForKey:@"Address1"];
            address.address2 = [formDict valueForKey:@"Address2"];
            address.city = [formDict valueForKey:@"City"];
            address.state = [formDict valueForKey:@"State"];
            form.address = address;
            
            if([formDict valueForKey:@"DRRemarks"] != nil) {
                form.remarks = [formDict valueForKey:@"DRRemarks"];
            }
            if([formDict valueForKey:@"CallNotes"] != nil) {
                 form.specialInstructions = [formDict valueForKey:@"CallNotes"];
            }
            if([formDict valueForKey:@"DSAFileNo"] != nil) {
                form.dsaFileNumber = [formDict valueForKey:@"DSAFileNo"];
            }
            if([formDict valueForKey:@"DSAAppNo"] != nil) {
                form.dsaAppNumber = [formDict valueForKey:@"DSAAppNo"];
            }
            if([formDict valueForKey:@"DSALEANo"] != nil) {
                form.dsaLEANumber = [formDict valueForKey:@"DSALEANo"];
            }
        
            form.status = @(FormStatusNotStarted);
            form.task = [formDict valueForKey:@"TaskCode"];
            
            form.taskDescription = [formDict objectForKey:@"TaskCodeDescr"];
            
            form.dsa = @(form.dsaFileNumber.length > 0);
            
            form.dailyType = [formDict objectForKey:@"Template"];
            
            form.name = [formDict objectForKey:@"FormName"];
            
            form.customerEmails = [self getCustomerEmails:formDict];
            [form updateNames];
        } else {
            // Update logic
            Form *form = [forms firstObject];
            if(formDict[@"Resubmit"] != nil && [formDict[@"Resubmit"] isEqualToString:@"Y"] && [form.status isEqual: @(FormStatusComplete)]) {
                form.status = @(FormStatusResubmit);
            }
            
            if(form.address == nil) {
                Address* address = [NSEntityDescription insertNewObjectForEntityForName:@"Address" inManagedObjectContext:self.managedObjectContext];
                address.address1 = [formDict valueForKey:@"Address1"];
                address.address2 = [formDict valueForKey:@"Address2"];
                address.city = [formDict valueForKey:@"City"];
                address.state = [formDict valueForKey:@"State"];
                form.address = address;
            }
            
            form.customerEmails = [self getCustomerEmails:formDict];
            [form updateNames];
            form.checklists = [formDict objectForKey:@"CheckLists"];
        }
    }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Form" inManagedObjectContext:self.managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"!(dispatchId in %@) AND dailyType !=[c] %@", idsToKeep, @"Timesheet"];
    request.predicate = predicate;
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    for (id object in fetchedObjects) {
        [self.managedObjectContext deleteObject:object];
    };
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error adding form: %@", [error localizedDescription]);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidAddForms" object:[NSNumber numberWithBool:YES]];

}

-(NSString*)getCustomerEmails:(NSDictionary*)formDictionary {
    NSMutableArray<NSString*> *customerEmails = [NSMutableArray array];
    for (int i = 1; i < 11; i++) {
        NSString *key = [NSString stringWithFormat:@"CustEmail%i", i];
        NSString *email = [formDictionary objectForKey:key];
        
        if (email != nil) {
            [customerEmails addObject:email];
        }
    }
    return [customerEmails componentsJoinedByString:@","];
}

-(Client*)addOrGetClient:(id)formJSON {
    // Check if client exists
    NSError *error;
    NSFetchRequest *clientRequest = [[NSFetchRequest alloc] init];
    clientRequest.entity = [NSEntityDescription entityForName:@"Client" inManagedObjectContext:self.managedObjectContext];
    clientRequest.predicate = [NSPredicate predicateWithFormat:@"id ==[c] %@", [formJSON valueForKey:@"ClientID"]];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:clientRequest error:&error];
    for (Client *client in fetchedObjects) {
        return client;
    }

    Client *client = [NSEntityDescription insertNewObjectForEntityForName:@"Client" inManagedObjectContext:self.managedObjectContext];
    client.name = [formJSON valueForKey:@"ClientName"];
    client.id = [formJSON valueForKey:@"ClientID"];
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error adding client: %@", [error localizedDescription]);
    }
    
    return client;
}


-(Contact*)addOrGetContact:(id)formJSON {
    // Check if client exists
    
    if([formJSON valueForKey:@"ContactName"] == nil || [formJSON valueForKey:@"ContactId"] == nil) {
        return nil;
    }
    
    NSError *error;
    NSFetchRequest *clientRequest = [[NSFetchRequest alloc] init];
    clientRequest.entity = [NSEntityDescription entityForName:@"Contact" inManagedObjectContext:self.managedObjectContext];
    clientRequest.predicate = [NSPredicate predicateWithFormat:@"id ==[c] %@", [formJSON valueForKey:@"ContactId"]];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:clientRequest error:&error];
    for (Contact *contact in fetchedObjects) {
        return contact;
    }
    
    Contact *contact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:self.managedObjectContext];
    contact.name = [formJSON valueForKey:@"ContactName"];
    contact.phone = [formJSON valueForKey:@"ContactPhone"];
    contact.cell = [formJSON valueForKey:@"Cell"];
    contact.id = [formJSON valueForKey:@"ContactId"];

    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error adding client: %@", [error localizedDescription]);
    }
    
    return contact;
}

-(Employee*)addOrGetTechnician:(id)formJSON {
    // Check if technician exists
    NSError *error;
    NSFetchRequest *employeeRequest = [[NSFetchRequest alloc] init];
    employeeRequest.entity = [NSEntityDescription entityForName:@"Employee" inManagedObjectContext:self.managedObjectContext];
    employeeRequest.predicate = [NSPredicate predicateWithFormat:@"id ==[c] %@", [formJSON valueForKey:@"TechnicianId"]];
    NSArray *fetchedEmployees = [self.managedObjectContext executeFetchRequest:employeeRequest error:&error];
    for (Employee *employee in fetchedEmployees) {
        return employee;
    }

    Employee *employee = [NSEntityDescription insertNewObjectForEntityForName:@"Employee" inManagedObjectContext:self.managedObjectContext];
    employee.id = [formJSON valueForKey:@"TechnicianId"];
    employee.name = [formJSON valueForKey:@"TechnicianName"];

    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error adding employee: %@", [error localizedDescription]);
    }
    
    return employee;
}

-(Project*)addOrGetProject:(id)formJSON {
    // Check if project exists
    NSError *error;
    NSFetchRequest *projectRequest = [[NSFetchRequest alloc] init];
    projectRequest.entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:self.managedObjectContext];
    projectRequest.predicate = [NSPredicate predicateWithFormat:@"id ==[c] %@", [formJSON valueForKey:@"Project"]];
    NSArray *fetchedProjects = [self.managedObjectContext executeFetchRequest:projectRequest error:&error];
    for (Project *project in fetchedProjects) {
        return project;
    }
    
    Project *project = [NSEntityDescription insertNewObjectForEntityForName:@"Project" inManagedObjectContext:self.managedObjectContext];
    project.id = [formJSON valueForKey:@"Project"];
    project.longName = [formJSON valueForKey:@"Descr"];
    project.client = [self addOrGetClient:formJSON];
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error adding project: %@", [error localizedDescription]);
    }
    
    return project;
}

-(NSArray*)getFormsForTechnicianId:(NSString*)technicianId startDate:(NSDate*)startDate endDate:(NSDate*)endDate {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:[[AppData sharedData] managedObjectContext]];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ && date >= %@ && date <= %@", technicianId, startDate, endDate];
    
    NSError *error;
    return  [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
}

-(void)setFormStatus:(FormStatus)formStatus forFormId:(NSString*)formId {
    NSError *error;
    NSFetchRequest *formRequest = [[NSFetchRequest alloc] init];
    formRequest.entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:self.managedObjectContext];
    formRequest.predicate = [NSPredicate predicateWithFormat:@"dispatchId ==[c] %@", formId];
    NSArray *fetchedForms = [self.managedObjectContext executeFetchRequest:formRequest error:&error];
    for (Form *form in fetchedForms) {
        form.status = @(formStatus);
    }
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error setting form status: %@", [error localizedDescription]);
    }
}

-(void)clearLog {
    NSString *path = [self pathToDocumentNamed:@"log.html"];
    if([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:NULL];
    }
}

-(void)logHTML:(NSString*)html {
    NSString *path = [self pathToDocumentNamed:@"log.html"];
    
    NSString *htmlWithBreaks = [NSString stringWithFormat:@"%@\n\n\n",html];
    if([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:path];
        [handle seekToEndOfFile];
        [handle writeData:[htmlWithBreaks dataUsingEncoding:NSUTF8StringEncoding]];
    } else {
        [htmlWithBreaks writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:NULL];
    }
    
}

-(void)addDebugForms {
    
    NSError *error = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Form" inManagedObjectContext:self.managedObjectContext];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    Form *modelForm = [fetchedObjects firstObject];
    
    for(int i = 1; i < 5; i++) {
        Form *form = [NSEntityDescription insertNewObjectForEntityForName:@"Form" inManagedObjectContext:self.managedObjectContext];
        
        form.dispatchId = [NSString stringWithFormat:@"%@%i",modelForm.dispatchId,i];
        form.project = modelForm.project;
        form.technician = modelForm.technician;
        form.contact = modelForm.contact;
        form.startDate = [NSDate date];
        form.startTime = modelForm.startTime;
        form.hours = modelForm.hours;
        form.scope = modelForm.scope;
        
        NSString *dailyType = @"Daily Technician";
        if(i<3) {
            dailyType = @"Daily Inspection";
        }
        
        form.dailyType = dailyType;
        
        form.status = @(FormStatusNotStarted);
        form.task = modelForm.task;
        form.taskDescription = modelForm.task;
        
        
        form.dsa = @(i%2 == 0);
        
        form.remarks = @"Test remarks";
        form.specialInstructions = @"Test special instructions";
        
        if([form.dsa boolValue]) {
            form.dsaFileNumber = @"1234";
            form.dsaAppNumber = @"1234";
            form.dsaLEANumber = @"1234";
        }
     
        [form updateNames];
    }

    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error adding form: %@", [error localizedDescription]);
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidAddForms" object:[NSNumber numberWithBool:YES]];

}

-(int)getIncompleteFormCountForTechnician:(NSString*)technicianId projectId:(NSString*)projectId {
    // Get datetime of 24 hours ago
    NSDate *date = [NSDate date];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.hour = -24;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    date = [gregorian dateByAddingComponents:components toDate:date options:0];
    
    // Count incomplete (non-submitted) forms older than 24 hours
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Form" inManagedObjectContext:self.managedObjectContext]];
    if(projectId.length > 0) {
        request.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND startDate <= %@ AND (status < 2 || status == 3 || status == nil) AND project.id ==[c] %@ AND type != %@", technicianId, date, projectId,@"Timesheet"];
    } else {
        request.predicate = [NSPredicate predicateWithFormat:@"ANY technician.id ==[c] %@ AND startDate <= %@ AND (status < 2 || status == 3 || status == nil) AND dailyType != %@", technicianId, date, @"Timesheet"];
    }
    NSError *error;
    NSUInteger count = [self.managedObjectContext countForFetchRequest:request error:&error];
    
    return (int)count;
}

-(void)saveImage:(UIImage*)image ToFilename:(NSString*)filename {
    NSData *pngData = UIImagePNGRepresentation(image);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    [pngData writeToFile:filePath atomically:YES];
}


@end
