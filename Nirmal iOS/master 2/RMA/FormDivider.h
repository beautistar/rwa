//
//  FormDivider.h
//  RMA
//
//  Created by Michael Beteag on 8/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"

@interface FormDivider : FormComponentViewEditor
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) NSString *shortTitle;

+(FormDivider*)formDividerWithTitle:(NSString*)title;
+(FormDivider*)formDividerWithTitle:(NSString*)title shortTitle:(NSString*)shortTitle;

@end
