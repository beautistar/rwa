//
//  EditChecklistsButtonEditor.h
//  RMA
//
//  Created by Alexander Roode on 3/12/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"

@interface EditChecklistsButtonEditor : FormComponentViewEditor
- (IBAction)editChecklistsButtonPressed:(id)sender;
@end
