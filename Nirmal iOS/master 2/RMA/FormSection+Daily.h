//
//  FormSection+Daily.h
//  RMA
//
//  Created by Alexander Roode on 5/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "FormSection.h"

@interface FormSection (Daily)

+(FormSection*)dailyInspectionSection;
+(FormSection*)dailyTechnicianSection;
+(FormSection*)DSAdailyInspectionSection;
+(FormSection*)DSAdailyTechnicianSection;

@end
