#import "FormComponentViewEditor.h"

@interface ConcreteSamplesEditor : FormComponentViewEditor

@property (nonatomic, strong) FormComponentEditor *editor;
@property int currentSampleType;
@property int currentSampleIndex;

@property (weak, nonatomic) IBOutlet UISegmentedControl *typeControl;
- (IBAction)sampleTypeChanged:(id)sender;
+(ConcreteSamplesEditor*)standardSampleEditor;

@end

