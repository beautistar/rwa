//
//  FormSection+Checklists.m
//  RMA
//
//  Created by Alexander Roode on 5/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "FormSection+Checklists.h"
#import "EditorSectionTitleEditor.h"
#import "CollectionEditor.h"
#import "HMALaydownTopEditor.h"
#import "EditChecklistsButtonEditor.h"
#import "FormDivider.h"
#import "TextAreaEditor.h"
#import "Checklist.h"

@implementation FormSection (Checklists)

+(FormSection*)checklistsSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Checklists"];
    
    EditChecklistsButtonEditor *editChecklistsButtonEditor = [[EditChecklistsButtonEditor alloc] initWithNibName:@"EditChecklistsButtonEditor"];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider, editChecklistsButtonEditor] templateName:nil title:@"Checklists" shouldOutputXml:NO];
    section.enablerKey = @"checklistsQuestion";
    return section;
}

+(FormSection*)drilledInAnchorInspectionSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Drilled in Anchor Inspection" shortTitle:@"Anchors"];
    FormComponentViewEditor *topEditor = [[FormComponentViewEditor alloc] initWithNibName:@"DrilledInAnchorInspectionTopEditor"];
    EditorSectionTitleEditor *anchorInformationTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Anchor Information"];
    CollectionEditor *anchorInformationCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *anchorInformationEditor = [[FormComponentViewEditor alloc] initWithNibName:@"AnchorInformationEditor"];
        return anchorInformationEditor;
    } key:@"anchorInfo" itemKey:@"anchorInfo" itemName:@"Anchor Information" maxCount:5];
    
    EditorSectionTitleEditor *anchorInstallationLogTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Anchor Installation Log"];
    CollectionEditor *anchorInstallationLogCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *anchorInstallationLog = [[FormComponentViewEditor alloc] initWithNibName:@"AnchorInstallationLogEditor"];
        return anchorInstallationLog;
    } key:@"anchorInstallationLog" itemKey:@"anchorInstallationLog" itemName:@"Anchor Installation" maxCount:9];
    
    FormComponentViewEditor *bottomEditor = [[FormComponentViewEditor alloc] initWithNibName:@"DrilledInAnchorInspectionBottomEditor"];
    
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    topEditor,
                                                    anchorInformationTitle,
                                                    anchorInformationCollection,
                                                    anchorInstallationLogTitle,
                                                    anchorInstallationLogCollection,
                                                                 bottomEditor] templateName:@"drilledInAnchorInspection" title:@"Drilled in Anchor Inspection" xmlGrouping:@"checklists" xmlSubGrouping:@"drilledInAnchorInspection"];
    section.useSectionName = YES;

    return section;
}

+(FormSection*)epoxyAdhesiveInformationSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Epoxy Adhesive Information" shortTitle:@"Epoxy"];
    FormComponentViewEditor *topEditor = [[FormComponentViewEditor alloc] initWithNibName:@"EpoxyAdhesiveInformationTopEditor"];
    EditorSectionTitleEditor *epoxyAdhesiveInformationTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Epoxy Adjesive Information"];
    CollectionEditor *epoxyAdhesiveInformationCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *epoxyAdhesiveInformation = [[FormComponentViewEditor alloc] initWithNibName:@"EpoxyAdhesiveInformationEditor"];
        return epoxyAdhesiveInformation;
    } key:@"epoxyAdhesiveInformation" itemKey:@"epoxyAdhesiveInformation" itemName:@"Epoxy Adhesive Information" maxCount:5];
    
    EditorSectionTitleEditor *reinforcementInformationTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Reinforcement Information"];
    CollectionEditor *reinforcementInformationCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *reinforcementInformation = [[FormComponentViewEditor alloc] initWithNibName:@"ReinforcementInformationEditor"];
        return reinforcementInformation;
    } key:@"reinforcementInformation" itemKey:@"reinforcementInformation" itemName:@"Reinforcement Information" maxCount:9];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    topEditor,
                                                    epoxyAdhesiveInformationTitle,
                                                    epoxyAdhesiveInformationCollection,
                                                    reinforcementInformationTitle,
                                                    reinforcementInformationCollection] templateName:@"epoxyAdhesiveInformation" title:@"Epoxy Adhesive Information" xmlGrouping:@"checklists" xmlSubGrouping:@"epoxyAdhesiveInformation"];
    section.useSectionName = YES;

    return section;
}

+(FormSection*)epoxyAdhesiveInspectionSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Epoxy Adhesive Inspection" shortTitle:@"Epoxy"];
    EditorSectionTitleEditor *title = [EditorSectionTitleEditor editorSectionTitleWithText:@"Installation Log"];
    CollectionEditor *epoxyAdhesiveInspectionCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *epoxyAdhesiveInspection = [[FormComponentViewEditor alloc] initWithNibName:@"EpoxyAdhesiveInspectionEditor"];
        return epoxyAdhesiveInspection;
    } key:@"epoxyAdhesiveInspection" itemKey:@"epoxyAdhesiveInspection" itemName:@"Installation" maxCount:25];
    FormComponentViewEditor *bottomEditor = [[FormComponentViewEditor alloc] initWithNibName:@"EpoxyAdhesiveInspectionBottomEditor"];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    title,
                                                    epoxyAdhesiveInspectionCollection,
                                                    bottomEditor] templateName:@"epoxyAdhesiveInspection" title:@"Epoxy Adhesive Inspection" xmlGrouping:@"checklists" xmlSubGrouping:@"epoxyAdhesiveInspection"];
    section.useSectionName = YES;
    
    return section;
}

+(FormSection*)torquePullTestingSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Torque/Pull Testing" shortTitle:@"Torque/Pull"];
    FormComponentViewEditor *topEditor = [[FormComponentViewEditor alloc] initWithNibName:@"TorquePullTestingTopEditor"];
    
    EditorSectionTitleEditor *itemNumberDescriptionTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Item Number Description"];
    CollectionEditor *itemNumberDescriptionCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *itemNumberDescription = [[FormComponentViewEditor alloc] initWithNibName:@"ItemNumberDescriptionEditor"];
        return itemNumberDescription;
    } key:@"itemNumberDescription" itemKey:@"itemNumberDescription" itemName:@"Item Number Description" maxCount:3];
    
    EditorSectionTitleEditor *torquePullLogTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Torque/Pull Log"];
    CollectionEditor *torquePullLogCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *torquePullLog = [[FormComponentViewEditor alloc] initWithNibName:@"TorquePullLogEditor"];
        return torquePullLog;
    } key:@"torquePullLog" itemKey:@"torquePullLog" itemName:@"Torque/Pull" maxCount:13];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    topEditor,
                                                    itemNumberDescriptionTitle,
                                                    itemNumberDescriptionCollection,
                                                    torquePullLogTitle,
                                                    torquePullLogCollection] templateName:@"torquePullTesting" title:@"Torque/Pull Testing" xmlGrouping:@"checklists" xmlSubGrouping:@"torquePullTesting"];
    section.useSectionName = YES;
    
    return section;
}

+(FormSection*)magneticParticleExaminationSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Magnetic Particle Examination" shortTitle:@"Magnetic Particle"];
    FormComponentViewEditor *topEditor = [[FormComponentViewEditor alloc] initWithNibName:@"MagneticParticleExaminationTopEditor"];
    
    EditorSectionTitleEditor *title = [EditorSectionTitleEditor editorSectionTitleWithText:@"Section 1"];
    CollectionEditor *magneticParticleItemCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *magneticParticleItem = [[FormComponentViewEditor alloc] initWithNibName:@"MagneticParticleExaminationItemEditor"];
        return magneticParticleItem;
    } key:@"magneticParticleItem" itemKey:@"magneticParticleItem" itemName:@"Item" maxCount:26];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    topEditor,
                                                    title,
                                                    magneticParticleItemCollection] templateName:@"magneticParticleExamination" title:@"Magnetic Particle Examination" xmlGrouping:@"checklists" xmlSubGrouping:@"magneticParticleExamination"];
    section.useSectionName = YES;
    
    return section;
}

+(FormSection*)liquidPenetrantExaminationSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Liquid Penetrant Examination" shortTitle:@"Liquid Penetrant"];
    FormComponentViewEditor *topEditor = [[FormComponentViewEditor alloc] initWithNibName:@"LiquidPenetrantExaminationTopEditor"];
    
    EditorSectionTitleEditor *title = [EditorSectionTitleEditor editorSectionTitleWithText:@"Section 1"];
    CollectionEditor *liquidPenetrantItemCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *liquidPenetrantItem = [[FormComponentViewEditor alloc] initWithNibName:@"LiquidPenetrantExaminationItemEditor"];
        return liquidPenetrantItem;
    } key:@"liquidPenetrantItem" itemKey:@"liquidPenetrantItem" itemName:@"Item" maxCount:26];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    topEditor,
                                                    title,
                                                    liquidPenetrantItemCollection] templateName:@"liquidPenetrantExamination" title:@"Liquid Penetrant Examination" xmlGrouping:@"checklists" xmlSubGrouping:@"liquidPenetrantExamination"];
    section.useSectionName = YES;
    
    return section;
}

+(FormSection*)ultrasonicExaminationSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"Ultrasonic Examination" shortTitle:@"Ultrasonic"];
    FormComponentViewEditor *topEditor = [[FormComponentViewEditor alloc] initWithNibName:@"UltrasonicExaminationTopEditor"];
    EditorSectionTitleEditor *topTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Section 1"];
    CollectionEditor *topItemCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *topItem = [[FormComponentViewEditor alloc] initWithNibName:@"UltrasonicExaminationTopItemEditor"];
        return topItem;
    } key:@"topItem" itemKey:@"topItem" itemName:@"Item" maxCount:3];
    
    FormComponentViewEditor *middleEditor = [[FormComponentViewEditor alloc] initWithNibName:@"UltrasonicExaminationMiddleEditor"];
    EditorSectionTitleEditor *middleTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Section 2"];
    CollectionEditor *middleItemCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *middleItem = [[FormComponentViewEditor alloc] initWithNibName:@"UltrasonicExaminationMiddleItemEditor"];
        return middleItem;
    } key:@"middleItem" itemKey:@"middleItem" itemName:@"Item" maxCount:7];
    
    EditorSectionTitleEditor *bottomTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Section 3"];
    CollectionEditor *bottomItemCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *bottomItem = [[FormComponentViewEditor alloc] initWithNibName:@"UltrasonicExaminationBottomItemEditor"];
        return bottomItem;
    } key:@"bottomItem" itemKey:@"bottomItem" itemName:@"Item" maxCount:7];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    topEditor,
                                                    topTitle,
                                                    topItemCollection,
                                                    middleEditor,
                                                    middleTitle,
                                                    middleItemCollection,
                                                    bottomTitle,
                                                    bottomItemCollection] templateName:@"ultrasonicExamination" title:@"Ultrasonic Examination" xmlGrouping:@"checklists" xmlSubGrouping:@"ultrasonicExamination"];
    section.useSectionName = YES;
   
    return section;
}

+(FormSection*)hmaLaydownSection {
    FormDivider *divider = [FormDivider formDividerWithTitle:@"HMA Laydown" shortTitle:@"HMA Laydown"];
    HMALaydownTopEditor *topEditor = [[HMALaydownTopEditor alloc] initWithNibName:@"HMALaydownTopEditor"];
    TextAreaEditor *remarksEditor = [[TextAreaEditor alloc] initWithFrame:CGRectMake(0, 0, 768, 400) name:@"Remarks" key:@"remarksParagraph"];
    remarksEditor.textArea.maxHeight = 300;
    
    EditorSectionTitleEditor *rollingPatternsTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Rolling Patterns"];
    FormComponentViewEditor *rollingPatternsTopEditor = [[FormComponentViewEditor alloc] initWithNibName:@"RollingPatternsTopEditor"];
    
    CollectionEditor *rollingPatternsCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *item = [[FormComponentViewEditor alloc] initWithNibName:@"RollingPatternsEditor"];
        return item;
    } key:@"topItem" itemKey:@"topItem" itemName:@"Item" maxCount:14];
    
    EditorSectionTitleEditor *compactionEqupmentTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Compaction Equipment"];
    FormComponentViewEditor *compactionEquipmentTopEditor = [[FormComponentViewEditor alloc] initWithNibName:@"CompactionEquipmentTopEditor"];
    
    EditorSectionTitleEditor *compactionCoresTitle = [EditorSectionTitleEditor editorSectionTitleWithText:@"Compaction - Cores"];
    CollectionEditor *compactionCoresCollection = [CollectionEditor collectionEditorWithInstantiationBlock:^FormComponentEditor *{
        FormComponentViewEditor *item = [[FormComponentViewEditor alloc] initWithNibName:@"CompactionCoresEditor"];
        return item;
    } key:@"core" itemKey:@"core" itemName:@"Core" maxCount:30];
    
    FormSection *section = [FormSection formSectionWithEditors:@[divider,
                                                    topEditor,
                                                    remarksEditor,
                                                    rollingPatternsTitle,
                                                    rollingPatternsTopEditor,
                                                    rollingPatternsCollection,
                                                    compactionEqupmentTitle,
                                                    compactionEquipmentTopEditor,
                                                    compactionCoresTitle,
                                                    compactionCoresCollection] templateName:@"hmaLaydown" title:@"HMA Laydown" xmlGrouping:@"checklists" xmlSubGrouping:@"hmaLaydown"];
    section.useSectionName = YES;
    
    return section;
}

@end
