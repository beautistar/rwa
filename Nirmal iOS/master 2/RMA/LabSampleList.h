#import <UIKit/UIKit.h>

@interface LabSampleList : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *LabSampleListTableView;

@end
