//
//  RescheduleViewController.m
//  RMA
//
//  Created by Alexander Roode on 5/10/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "RescheduleViewController.h"
#import "SVProgressHud.h"
#import "Form.h"
#import "Employee.h"

@interface RescheduleViewController ()
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@end

@implementation RescheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.title = [NSString stringWithFormat:@"Reschedule/Cancel %@", self.form.dispatchId];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Continue" style:UIBarButtonItemStylePlain target:self action:@selector(continueButtonPressed)];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"MMM d, yyyy";
    
    self.textView.textContainerInset = UIEdgeInsetsMake(8, 8, 8, 8);
}

- (void)cancelButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)continueButtonPressed {
    if (self.textView.text.length == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a reason for your cancelation" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    if (![MFMailComposeViewController canSendMail]) {
        [SVProgressHUD showErrorWithStatus:@"Email not available. Please configure an email account on your device"];
        return;
    }
    
    MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
    mailViewController.mailComposeDelegate = self;
    
    [mailViewController setToRecipients:@[@"dispatch@rmacompanies.com"]];
    [mailViewController setSubject:[NSString stringWithFormat:@"Cancelation/Rescheduling request for %@", self.form.dispatchId]];
    
    NSMutableString *body = [[NSMutableString alloc] init];
    [body appendFormat:@"%@\r\n", [self.dateFormatter stringFromDate:[NSDate date]]];
    [body appendFormat:@"Employee ID: %@\r\n", self.form.technician.id];
    [body appendFormat:@"%@\r\n\r\n", self.form.dispatchId];
    [body appendFormat:@"Comments: \r\n\r\n%@", self.textView.text];
    
    [mailViewController setMessageBody:body isHTML:NO];
    
    [self presentViewController:mailViewController animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    if (result == MFMailComposeResultFailed) {
        [SVProgressHUD showErrorWithStatus:@"Message not sent - check your internet connection"];
    }
    else if (result == MFMailComposeResultSent) {
        [SVProgressHUD showSuccessWithStatus:@"Message sent"];
        [self.delegate didRescheduleForm:self.form];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
