//
//  SignatureEditor.m
//  RMA
//
//  Created by Michael Beteag on 8/1/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "SignatureEditor.h"
#import "Constants.h"

@implementation SignatureEditor

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.view = self;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame name:(NSString*)name key:(NSString*)key
{
    self = [self initWithFrame:frame];
    self.name = name;
    self.key = key;
    
    self.signButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.signButton setTitle:@"Sign" forState:UIControlStateNormal];
    self.signButton.frame = CGRectMake(self.frame.size.width - kSignButtonWidth - 2*kPaddingX,
                                  kPaddingY,
                                  kSignButtonWidth,
                                  kSegmentedControlHeight);
    [self.signButton addTarget:self action:@selector(signButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.signButton];
    
    
    self.imageViewLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPaddingX,
                                                                    kPaddingYWithSegmentedControl,
                                                                    self.frame.size.width - 3*kPaddingX - kSignButtonWidth,
                                                                    kLabelHeight)];
    self.imageViewLabel.text = self.name;
    [self.view addSubview:self.imageViewLabel];
    

    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kPaddingX,
                                                                 2*kPaddingY + kSegmentedControlHeight,
                                                                 self.frame.size.width - 2*kPaddingX,
                                                                 self.frame.size.height - 3*kPaddingY - kSegmentedControlHeight)];
    self.imageView.layer.cornerRadius = 5;
    self.imageView.clipsToBounds = YES;
    [self.imageView.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.imageView.layer setBorderWidth:2.0];
    [self.view addSubview:self.imageView];
    return self;
}

-(void)signButtonPressed {
    FullScreenDrawingEditor *editor = [[FullScreenDrawingEditor alloc] init];
    editor.delegate = self;
    
    [self.superview endEditing:YES];

    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:editor];
    [self.popoverController presentPopoverFromRect:self.signButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)setLabel:(NSString*)text {
    self.imageViewLabel.text = text;
}

-(void)setImage:(UIImage *)image {
    self.imageView.image = image;
    [self.popoverController dismissPopoverAnimated:YES];
    self.formSigned = YES;
}

-(void)clear {
    self.imageView.image = nil;
    self.formSigned = NO;
}

-(void)cancel {
    [self clear];
    [self.popoverController dismissPopoverAnimated:YES];
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    if(self.formSigned) {
        NSLog(@"Image size %@ %@", NSStringFromCGSize(self.imageView.image.size), NSStringFromCGSize(self.imageView.frame.size));
        
        Image *image = [Image imageWithImage:self.imageView.image size:self.imageView.frame.size];
        
        [data setObject:image forKey:self.key];
    }
    
    
    return data;
    
}
-(void)setData:(NSMutableDictionary*)data {
    if([data objectForKey:self.key] != nil) {
        Image *image = ((Image*)[data objectForKey:self.key]);
        self.imageView.image = image.image;
        self.formSigned = YES;
    }
}

@end
