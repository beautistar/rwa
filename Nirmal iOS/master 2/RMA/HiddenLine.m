//
//  HiddenLine.m
//  RMA
//
//  Created by Alexander Roode on 1/26/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "HiddenLine.h"

@implementation HiddenLine

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    *HTMLSafe = YES;
    
    return @"&nbsp;";
}

@end
