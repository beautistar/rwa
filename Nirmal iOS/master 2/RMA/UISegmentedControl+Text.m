//
//  UISegmentedControl+Text.m
//  RMA
//
//  Created by Michael Beteag on 7/25/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "UISegmentedControl+Text.h"

@implementation UISegmentedControl (UISegmentedControl_Text)

-(NSString*)text {
    if(self.selectedSegmentIndex == UISegmentedControlNoSegment) {
        return  @"";
    }
    return [[self titleForSegmentAtIndex:self.selectedSegmentIndex] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
}
-(BOOL)selectText:(NSString*)text {
    BOOL textFound = NO;
    int attemptNumber = 0;
    while (!textFound) {
        for(int i = 0; i < [self numberOfSegments]; i++) {
            if([[self titleForSegmentAtIndex:i] isEqualToString:text]) {
                [self setSelectedSegmentIndex:i];
                textFound = YES;
                return YES;
                break;
                
            }
        }
        if(attemptNumber == 1) {
            return NO;
            break;
        }
        if(!textFound) {
            text = [text stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
            attemptNumber += 1;
        }
    }
    return NO;
    
}

@end
