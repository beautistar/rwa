//
//  NonComplianceQuestionEditor.m
//  RMA
//
//  Created by Michael Beteag on 10/28/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "NonComplianceQuestionEditor.h"
#import "RadioCheckboxSet.h"

@implementation NonComplianceQuestionEditor

+(NonComplianceQuestionEditor*)nonComplianceQuestionEditorWithKey:(NSString *)key technician:(BOOL)technician {
    NSString *nibName = technician ? @"NonComplianceQuestionTechnicianEditor" : @"NonComplianceQuestionEditor";
    
    NonComplianceQuestionEditor *editor = [[NonComplianceQuestionEditor alloc] initWithNibName:nibName];
    editor.key = key;
    
    return editor;
}

- (IBAction)complianceControlChanged:(id)sender {
    UISegmentedControl *control = (UISegmentedControl*)sender;
    
    if (control.selectedSegmentIndex == 0) {
        self.nonCompliance = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DidSetCompliance" object:nil];
        
    } else {
        self.nonCompliance = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DidSetNonCompliance" object:nil];
        
    }
}

-(void)setFormEnabled:(BOOL)enabled {
    self.isEnabled = enabled;
    [self setControls:self.view.subviews enabled:enabled];
}

-(void)setData:(NSMutableDictionary *)data {
    NSNumber *selectedValue = [data objectForKey:self.key];
    self.segmentedControl.selectedSegmentIndex = [selectedValue intValue];
    
    self.nonCompliance = self.segmentedControl.selectedSegmentIndex == 1;
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];

    [data setObject:[NSNumber numberWithInteger:self.segmentedControl.selectedSegmentIndex] forKey:self.key];
    
    RadioCheckboxSet *set = [RadioCheckboxSet radioCheckboxSetFromUISegmentedControl:self.segmentedControl];
    set.values = [NSMutableArray arrayWithArray:@[@"met",@"did not meet"]];
    
    [data setObject:set forKey:@"workMeetsRequirements"];
    
    return data;
}

-(NSDictionary*)dictionary {
    return @{@"selectedNonCompliance": self.nonCompliance? @"yes": @"no"};
}

@end
