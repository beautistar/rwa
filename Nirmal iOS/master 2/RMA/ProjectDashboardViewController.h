//
//  ProjectDashboardViewController.h
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectDashboardBreadcrumb.h"
#import "LocationManager.h"

@class Project;

@interface ProjectDashboardViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, LocationManagerDelegate> {
    id observer;
}
@property (weak, nonatomic) IBOutlet UILabel *projectNameLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *photoCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *mainCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *breadcrumbCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *clientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectIdLabel;
@property (weak, nonatomic) IBOutlet UIImageView *largeImageView;


@property (nonatomic, retain) NSArray *photos;
@property (nonatomic, retain) NSDictionary *project;
@property (nonatomic, retain) NSString *technicianId;
@property DashboardSectionType currentSection;
@property (nonatomic, retain) NSMutableArray *breadcrumbs;
@property (nonatomic, retain) UIRefreshControl *refreshControl;


@property (nonatomic, retain) Folder *currentFolder;
@property (nonatomic, retain) NSArray *subfolders;
@property (nonatomic, retain) NSArray *documents;

@end
