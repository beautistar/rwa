//
//  NSMutableArray+Shuffling.h
//  RMA
//
//  Created by Alexander Roode on 3/23/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Shuffling)
- (void)shuffle;
@end
