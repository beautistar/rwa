//
//  ProjectDashboardBreadcrumb.h
//  RMA
//
//  Created by Michael Beteag on 12/17/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DashboardSectionType) {
    DashboardSectionTypeMain,
    DashboardSectionTypeMain1,
    DashboardSectionTypeFieldFile,
    DashboardSectionTypeReports
};

@class Folder;
@interface ProjectDashboardBreadcrumb : NSObject

@property (nonatomic, retain) NSString *name;
@property DashboardSectionType sectionType;
@property Folder *folder;

@end
