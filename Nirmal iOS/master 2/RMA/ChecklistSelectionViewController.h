//
//  ChecklistSelectionViewController.h
//  RMA
//
//  Created by Alexander Roode on 1/19/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddChecklistViewController.h"

@class FormBuilder;
@protocol ChecklistSelectionViewControllerDelegate
-(void)didUpdateChecklists;
-(NSArray*)getAvailableChecklists;
@end

@interface ChecklistSelectionViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, AddChecklistViewControllerDelegate>

@property (nonatomic, strong) FormBuilder *formBuilder;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id<ChecklistSelectionViewControllerDelegate> delegate;

@end
