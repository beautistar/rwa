//
//  ProjectSelectionViewController.h
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Project;

@protocol ProjectSelectionDelegate <NSObject>
-(void)didSelectProject:(NSDictionary*)projectDict;
@end

@interface ProjectSelectionViewController : UITableViewController

@property (nonatomic, retain) NSArray *projects;
@property (nonatomic, assign) id<ProjectSelectionDelegate> delegate;

@end
