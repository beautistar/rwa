//
//  RMARefreshControl.h
//  RMA
//
//  Created by Michael Beteag on 4/14/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMARefreshControl : UIRefreshControl

@end
