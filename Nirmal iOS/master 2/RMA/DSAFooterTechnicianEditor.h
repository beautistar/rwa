//
//  DSAFooterTechnicianEditor.h
//  RMA
//
//  Created by Michael Beteag on 4/24/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"
#import "RadioCheckboxSet.h"
#import "FormComponentViewEditor.h"

@interface DSAFooterTechnicianEditor : FormComponentViewEditor

@property (weak, nonatomic) IBOutlet UISegmentedControl *workMeetsRequirements;
@property (weak, nonatomic) IBOutlet UISegmentedControl *samplingInAccordance;

@property (nonatomic, retain) FormComponentViewEditor *mainEditor;

@property (nonatomic, retain) IBOutlet UIView *view;

@property (nonatomic, strong) NSString *key;
@property (nonatomic, retain) NSArray *editors;


- (IBAction)nonComplianceQuestion1Changed:(id)sender;
- (IBAction)nonComplianceQuestion2Changed:(id)sender;
- (IBAction)nonComplianceQuestion3Changed:(id)sender;

+(DSAFooterTechnicianEditor*)DSAFooterTechnicianEditorWithKey:(NSString *)key;

@end
