//
//  PDFGenerator.h
//  RMA
//
//  Created by Michael Beteag on 10/5/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RenderSection.h"

@class FormBuilder;

@protocol PDFGeneratorDelegate
-(void)PDFGenerationDidFail:(NSString*)error;
-(void)PDFGenerationDidSucceed:(NSString*)path;
@end

@interface PDFGenerator : NSObject <RenderSectionDelegate>
@property (nonatomic, weak) id<PDFGeneratorDelegate> delegate;
-(void)generatePDFFromFormBuilder:(FormBuilder*)formBuilder outputPath:(NSString*)path;
@end
