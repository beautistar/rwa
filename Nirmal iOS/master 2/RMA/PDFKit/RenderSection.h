//
//  RenderSection.h
//  RMA
//
//  Created by Michael Beteag on 10/5/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FormSection.h"
#import "PDFRenderOperation.h"
#import "BNHtmlPdfKit.h"
#import "AppData.h"

@class RenderSection;
@protocol RenderSectionDelegate <NSObject>

-(void)sectionRenderDidFail;
-(void)sectionRenderDidSucceed:(NSString*)path currentPageNumber:(int)currentPageNumber;

@end

@interface RenderSection : NSObject <PDFRenderOperationDelegate, BNHtmlPdfKitDelegate>

@property (nonatomic, assign) id<RenderSectionDelegate> delegate;
@property (nonatomic, retain) BNHtmlPdfKit *pdfKit;
@property (nonatomic, retain) FormSection *section;
@property (nonatomic, retain) NSMutableDictionary *metadata;
@property int startingPageNumber;
@property (nonatomic, retain) UIPrintFormatter *defaultFormatter;
@property int height;
@property (nonatomic, retain) UIPrintFormatter *page1Formatter;
@property int page1height;
@property (nonatomic, retain) UIPrintFormatter *page1FooterFormatter;
@property int page1FooterHeight;
@property int defaultFooterHeight;


+(RenderSection*)renderSectionWithFormSection:(FormSection*)formSection;
-(void)prepareForRender;

@end
