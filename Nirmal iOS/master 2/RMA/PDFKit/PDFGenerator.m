//
//  PDFGenerator.m
//  RMA
//
//  Created by Michael Beteag on 10/5/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "PDFGenerator.h"
#import "AppData.h"
#import "FormBuilder.h"

@interface PDFGenerator ()
@property (nonatomic, strong) NSArray *formBuilderSections;
@property (nonatomic, strong) NSMutableArray *renderSections;
@property (nonatomic, strong) RenderSection *currentRenderSection;
@property int currentRenderSectionIndex;
@property (nonatomic, strong) UIPrintFormatter *footerFormatter;
@property (nonatomic, strong) NSMutableArray *renderedPDFPaths;
@property (nonatomic, strong) NSMutableArray *combinedPDFPaths;
@property (nonatomic, strong) NSMutableDictionary *sharedData;
@property (nonatomic, strong) NSOperationQueue *renderOperationQueue;
@property (nonatomic, strong) NSMutableDictionary *metaData;
@property (nonatomic, strong) NSString *outputPath;
@property int currentPageNumber;
@end

@implementation PDFGenerator

-(void)generatePDFFromFormBuilder:(FormBuilder*)formBuilder outputPath:(NSString*)path {
#ifdef DEBUG
    [[AppData sharedData] clearLog];
#endif
    
    self.currentPageNumber = 1;
    self.renderedPDFPaths = [NSMutableArray array];
    self.combinedPDFPaths = [NSMutableArray array];
    self.renderSections = [NSMutableArray array];
    self.currentRenderSectionIndex = -1;
    self.outputPath = path;
    
    self.formBuilderSections = [formBuilder allSections];
    
    self.sharedData = [NSMutableDictionary dictionary];
    for(FormSection *section in self.formBuilderSections) {
        if(section.isEnabled) {
            for(FormComponentEditor *editor in section.editors) {
                NSDictionary *sharedData = [editor getSharedData];
                
                if(sharedData != nil) {
                    [self.sharedData addEntriesFromDictionary:sharedData];
                }
            }
        }
    }
    
    [self nextRenderSection];
}


-(void)nextRenderSection {
    self.currentRenderSectionIndex++;

    if(self.formBuilderSections.count == self.currentRenderSectionIndex) {
        [self joinPDFs];
        return;
    }
    
    FormSection *section = [self.formBuilderSections objectAtIndex:self.currentRenderSectionIndex];
    
    while(![section isEnabled] || [section isHiddenInPdf] || section.templateName == nil) {
        self.currentRenderSectionIndex++;
        if(self.currentRenderSectionIndex == self.formBuilderSections.count) {
            [self joinPDFs];
            return;
        }
        section = [self.formBuilderSections objectAtIndex:self.currentRenderSectionIndex];
    }
    
    if(section.extraData == nil) {
        section.extraData = [NSMutableDictionary dictionary];
    }
    [section.extraData addEntriesFromDictionary:self.sharedData];
    
    
    RenderSection *renderSection = [RenderSection renderSectionWithFormSection:section];
    [self.renderSections addObject:renderSection];
    self.currentRenderSection = renderSection;
    renderSection.startingPageNumber = self.currentPageNumber;
    renderSection.delegate = self;

    renderSection.defaultFooterHeight = 30;
    NSMutableDictionary *metadata = [NSMutableDictionary dictionaryWithDictionary:self.metaData];
    [metadata setObject:renderSection.section.title forKey:@"sectionTitle"];
    renderSection.metadata = metadata;
    [renderSection.metadata addEntriesFromDictionary:[section getDataForRendering]];
    [renderSection prepareForRender];
}

-(void)sectionRenderDidSucceed:(NSString *)path currentPageNumber:(int)currentPageNumber {
    self.currentPageNumber = currentPageNumber;
    [self.renderedPDFPaths addObject:path];
    if(self.currentRenderSectionIndex < self.formBuilderSections.count) {
        [self nextRenderSection];
    } else {
        [self joinPDFs];
    }
}

-(void)sectionRenderDidFail {
    [self.delegate PDFGenerationDidFail:@"An error occurred"];
}

-(void)joinPDFs {
    if(self.renderedPDFPaths.count == 0) {
        [self.delegate PDFGenerationDidSucceed:self.outputPath];
        return;
    }
    
    NSString *currentPath = [self.renderedPDFPaths objectAtIndex:0];
    
    if(self.renderedPDFPaths.count == 1) {
        if([[NSFileManager defaultManager] fileExistsAtPath:self.outputPath]) {
            [[NSFileManager defaultManager] removeItemAtPath:self.outputPath error:NULL];
        }
        [[NSFileManager defaultManager] moveItemAtPath:currentPath toPath:self.outputPath error:NULL];
    } else {
        for(int i = 1; i < self.renderedPDFPaths.count; i++) {
            NSString *currentPath2 = [self.renderedPDFPaths objectAtIndex:i];
            NSString *combinedPath = [[AppData sharedData] pathToDocumentNamed:[[AppData sharedData] getGuidFilenameWithExtension:@"pdf"]];
            if(i == self.renderedPDFPaths.count - 1) {
                combinedPath = self.outputPath;
            } else {
                [self.combinedPDFPaths addObject:combinedPath];
            }
            
            [self joinPDF:currentPath withPDF:currentPath2 toPath:combinedPath];
            currentPath = combinedPath;
        }
        
        NSMutableArray *itemsToDelete = [NSMutableArray arrayWithArray:self.combinedPDFPaths];
        [itemsToDelete addObjectsFromArray:self.renderedPDFPaths];
        
        for(int i = 0; i < itemsToDelete.count; i++) {
            NSString *path = [itemsToDelete objectAtIndex:i];
            if([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                [[NSFileManager defaultManager] removeItemAtPath:path error:NULL];
            }
        }
    }
    
    [self.delegate PDFGenerationDidSucceed:self.outputPath];
}

-(void)joinPDF:(NSString*)path1 withPDF:(NSString*)path2 toPath:(NSString*)outPath {
    // File URLs
    CFURLRef pdfURL1 = (__bridge CFURLRef)[NSURL fileURLWithPath:path1];
    CFURLRef pdfURL2 = (__bridge CFURLRef)[NSURL fileURLWithPath:path2];
    CFURLRef pdfURLOutput = (__bridge CFURLRef)[NSURL fileURLWithPath:outPath];
    
    // File references
    CGPDFDocumentRef pdfRef1 = CGPDFDocumentCreateWithURL((CFURLRef) pdfURL1);
    CGPDFDocumentRef pdfRef2 = CGPDFDocumentCreateWithURL((CFURLRef) pdfURL2);
    
    // Number of pages
    NSInteger numberOfPages1 = CGPDFDocumentGetNumberOfPages(pdfRef1);
    NSInteger numberOfPages2 = CGPDFDocumentGetNumberOfPages(pdfRef2);
    
    // Create the output context
    CGContextRef writeContext = CGPDFContextCreateWithURL(pdfURLOutput, NULL, NULL);
    
    // Loop variables
    CGPDFPageRef page;
    CGRect mediaBox;
    
    // Read the first PDF and generate the output pages
    for (int i=1; i<=numberOfPages1; i++) {
        page = CGPDFDocumentGetPage(pdfRef1, i);
        mediaBox = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
        CGContextBeginPage(writeContext, &mediaBox);
        CGContextDrawPDFPage(writeContext, page);
        CGContextEndPage(writeContext);
    }
    
    // Read the second PDF and generate the output pages
    for (int i=1; i<=numberOfPages2; i++) {
        page = CGPDFDocumentGetPage(pdfRef2, i);
        mediaBox = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
        CGContextBeginPage(writeContext, &mediaBox);
        CGContextDrawPDFPage(writeContext, page);
        CGContextEndPage(writeContext);
    }
    // Finalize the output file
    CGPDFContextClose(writeContext);
    
    // Release from memory
    CGPDFDocumentRelease(pdfRef1);
    CGPDFDocumentRelease(pdfRef2);
    CGContextRelease(writeContext);
}

@end
