//
//  RenderSection.m
//  RMA
//
//  Created by Michael Beteag on 10/5/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "RenderSection.h"
#import "FormHeader.h"

@implementation RenderSection

+(RenderSection*)renderSectionWithFormSection:(FormSection*)formSection {
    RenderSection *renderSection = [[RenderSection alloc] init];
    renderSection.section = formSection;
    return  renderSection;
}

-(void)prepareForRender {
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSInvocationOperation *completionOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(finishedPreparation) object:nil];
    
    if(self.section.headerDefault != nil) {
        FormHeader *header = self.section.headerDefault;
        self.height = header.height;
        PDFRenderOperation *operation = [[PDFRenderOperation alloc] initWithHtmlContent:[header HTMLwithData:self.metadata] sectionId:0 sectionType:0];
        operation.delegate = self;
        [queue addOperation:operation];
        [completionOperation addDependency:operation];
    }
    if(self.section.headerPage1 != nil) {
        FormHeader *header = self.section.headerPage1;
        self.page1height = header.height;
        
        PDFRenderOperation *operation = [[PDFRenderOperation alloc] initWithHtmlContent:[header HTMLwithData:self.metadata] sectionId:1 sectionType:0];
        operation.delegate = self;
        [queue addOperation:operation];
        [completionOperation addDependency:operation];
    }
    [queue addOperation:completionOperation];
}

-(void)finishedPreparation {
    //[self.delegate renderSection:self];
    [self performSelectorOnMainThread:@selector(render) withObject:nil waitUntilDone:NO];
    
}


-(void)render {
    self.pdfKit = [[BNHtmlPdfKit alloc] init];
    self.pdfKit.delegate = self;
    self.pdfKit.footerText =  @"Rancho Cucamonga 909.989.1751 | Sacramento 916.631.7194 | Carson 310.684.4854";
    self.pdfKit.footerText2 = @"San Diego 858.609.7138 |  San Jose 408.362.4920 |  Van Nuys 818.392.8778";
    
#ifdef GEOSCIENCE
    self.pdfKit.footerText = @"Sun Valley 818.806.8942 | Fresno 559.708.8865 | Visalia 559.732.3039";
    self.pdfKit.footerText2 = @"";
#endif
    
    NSString *path = [[AppData sharedData] pathToDocumentNamed:[[AppData sharedData] getGuidFilenameWithExtension:@"pdf"]];

    [self.pdfKit saveHtmlAsPdf:[self.section getRenderedHTML] toFile:path];
}

-(void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfFile:(NSString *)file {
    CFURLRef pdfURL1 = (__bridge CFURLRef)[NSURL fileURLWithPath:file];
    CGPDFDocumentRef pdfRef1 = CGPDFDocumentCreateWithURL((CFURLRef) pdfURL1);
    size_t pageCount = CGPDFDocumentGetNumberOfPages(pdfRef1);
    
    [self.delegate sectionRenderDidSucceed:file currentPageNumber:(self.startingPageNumber + (int)pageCount)];
}

- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error {
    [self.delegate sectionRenderDidFail];
}

-(void)didFinishLoadingSection:(int)sectionId type:(int)sectionType withPrintFormatter:(UIPrintFormatter *)formatter height:(NSInteger)height {
    if(sectionType == 0) {
        if(sectionId == 0) {
            self.defaultFormatter = formatter;
            //self.height = height;
        }
        if(sectionId == 1) {
            self.page1Formatter = formatter;
            //self.page1height = height;
        }
    }
}


-(UIPrintFormatter*)footerPage1Formatter {
    return self.page1FooterFormatter;
}

-(UIPrintFormatter*)headerDefaultFormatter {
    return self.defaultFormatter;
}

-(UIPrintFormatter*)headerPage1Formatter {
    return self.page1Formatter;
}

-(int)headerDefaultHeight {
    return self.height;
}

-(int)headerPage1Height {
    return self.page1height;
}

-(int)footerDefaultHeight {
    return self.defaultFooterHeight;
}

-(int)footerPage1Height {
    return self.page1FooterHeight;
}

-(int)initialPageNumer {
    return self.startingPageNumber;
}
@end
