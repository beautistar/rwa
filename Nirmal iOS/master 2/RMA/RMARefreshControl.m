//
//  RMARefreshControl.m
//  RMA
//
//  Created by Michael Beteag on 4/14/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "RMARefreshControl.h"

@implementation RMARefreshControl

- (void)beginRefreshing
{
    [super beginRefreshing];
    
    if ([self.superview isKindOfClass:[UIScrollView class]]) {
        UIScrollView *view = (UIScrollView *)self.superview;
        [view setContentOffset:CGPointMake(0, view.contentOffset.y - self.frame.size.height) animated:YES];
    }
}

@end
