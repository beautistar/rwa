#import "CollectionEditor.h"

@interface AddMoreSamplesCollectionEditor : CollectionEditor

+(AddMoreSamplesCollectionEditor*)addMoreSampleCollectionEditor;
-(void)setData:(NSMutableDictionary *)data;
-(NSMutableDictionary*)getData;
-(NSMutableArray<NSNumber*>*)addMoreSampleTypeIndexes;

@end

