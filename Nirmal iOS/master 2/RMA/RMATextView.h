//
//  RMATextView.h
//  RMA
//
//  Created by Michael Beteag on 11/11/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMATextView : UITextView
@property NSInteger maxHeight;
-(void)awakeFromNib;
@end
