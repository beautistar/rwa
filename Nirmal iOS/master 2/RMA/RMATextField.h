//
//  RMATextField.h
//  RMA
//
//  Created by Michael Beteag on 11/11/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMATextField : UITextField

@property NSInteger maxLength;
@property NSInteger maxWidth;
@property BOOL numeric;

@end
