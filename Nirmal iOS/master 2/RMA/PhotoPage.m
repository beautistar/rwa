//
//  PhotoPage.m
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "PhotoPage.h"
#import "Photo.h"
#import "NSData+Base64.h"

@interface PhotoPage ()

@end

@implementation PhotoPage

+(instancetype)photoPageWithPhotos:(NSArray*)photos type:(PhotoPageType)pageType {
    PhotoPage *page = [[PhotoPage alloc] init];
    page.photos = photos;
    page.pageType = pageType;
    
    return page;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        self.photos = [aDecoder decodeObjectForKey:@"photos"];
        self.pageType = (PhotoPageType)[[aDecoder decodeObjectForKey:@"pageType"] integerValue];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.photos forKey:@"photos"];
    [aCoder encodeObject:@(self.pageType) forKey:@"pageType"];
}

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    NSMutableString *html = [NSMutableString stringWithString:@"<div class=\"page-break\">&nbsp;</div><table class=\"photo-table\">"];
    *HTMLSafe = YES;
    
    NSString *cellClass;
    
    switch (self.pageType) {
        case PhotoPageType1x1: {
            cellClass = @"photo-cell photo-cell-1x1";
            
            [html appendFormat:@"<tr><td class=\"%@\">", cellClass];
            Photo *photo = [self.photos firstObject];
            if (photo != nil) {
                [html appendString:[self htmlForPhoto:photo]];
            }
            [html appendString:@"</td></tr>"];
            break;
        }
        case PhotoPageType2x1: {
            cellClass = @"photo-cell photo-cell-2x1";
            
            [html appendFormat:@"<tr><td class=\"%@\">", cellClass];
            Photo *photo = self.photos[0];
            if (photo != nil) {
                [html appendString:[self htmlForPhoto:photo]];
            }
            [html appendFormat:@"</td></tr><tr><td class=\"%@\">", cellClass];
            Photo *photo2 = self.photos[1];
            if (photo2 != nil) {
                [html appendString:[self htmlForPhoto:photo2]];
            }
            [html appendString:@"</td></tr>"];
            break;
        }
        case PhotoPageType2x2: {
            cellClass = @"photo-cell photo-cell-2x2";
            
            [html appendFormat:@"<tr><td class=\"%@\">", cellClass];
            Photo *photo1 = self.photos[0];
            if (photo1 != nil) {
                [html appendString:[self htmlForPhoto:photo1]];
            }
            
            [html appendFormat:@"</td><td class=\"%@\">", cellClass];
            Photo *photo2 = self.photos[1];
            if (photo2 != nil) {
                [html appendString:[self htmlForPhoto:photo2]];
            }
            
            [html appendFormat:@"</td></tr><tr><td class=\"%@\">", cellClass];
            Photo *photo3 = self.photos[2];
            if (photo3 != nil) {
                [html appendString:[self htmlForPhoto:photo3]];
            }
            
            [html appendFormat:@"</td><td class=\"%@\">", cellClass];
            Photo *photo4 = self.photos[3];
            if (photo4 != nil) {
                [html appendString:[self htmlForPhoto:photo4]];
            }
            
            [html appendString:@"</td></tr>"];
            break;
        }
    }
    
    [html appendString:@"</table>"];
    
    return html;
}

-(NSString*)htmlForPhoto:(Photo*)photo {
    NSMutableString *html = [[NSMutableString alloc] init];
    [html appendString:@"<div class=\"photo-image\">"];
    if (photo.image != nil) {
        NSData *imageData = UIImageJPEGRepresentation(photo.image, 1.0);
        NSString *base64imageString = [imageData base64EncodingWithLineLength:0];
        
        [html appendFormat:@"<img alt=\"\" src=\"data:image/jpg;base64,%@\" />", base64imageString];
    }
    [html appendString:@"</div><div class=\"photo-caption\">"];
    if (photo.caption.length > 0) {
        [html appendFormat:@"<p class=\"image-description\">%@</p>", photo.caption];
    }
    
    [html appendString:@"</div>"];
    return html;
}


@end
