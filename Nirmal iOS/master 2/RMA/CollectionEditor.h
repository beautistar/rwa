//
//  CollectionEditor.h
//  RMA
//
//  Created by Michael Beteag on 2/5/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"

@interface CollectionEditor : FormComponentEditor

@property (nonatomic, copy) FormComponentEditor *(^instantiationBlock)(void);
@property (nonatomic, strong) NSString *editorKey;
@property (nonatomic, strong) NSString *itemKey;
@property (nonatomic, strong) NSString *itemName;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *deleteButtons;
@property (nonatomic, strong) UIButton *addButton;
@property (nonatomic) int maxCount;

+(instancetype)collectionEditorWithInstantiationBlock:(FormComponentEditor*(^)())instantiationBlock key:(NSString*)editorKey itemKey:(NSString*)itemKey itemName:(NSString*)itemName;
+(instancetype)collectionEditorWithInstantiationBlock:(FormComponentEditor*(^)())instantiationBlock key:(NSString*)editorKey itemKey:(NSString*)itemKey itemName:(NSString*)itemName maxCount:(int)maxCount;
-(void)layoutAddButton:(UIButton*)button itemName:(NSString*)itemName;
-(void)addItem:(FormComponentEditor*)item;
-(void)layoutItems;
-(void)clearView;
@end
