//
//  ConcretePageSetEditorTop.m
//  RMA
//
//  Created by Michael Beteag on 8/13/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "ConcretePageSetEditorTop.h"

@implementation ConcretePageSetEditorTop

+(ConcretePageSetEditorTop*)concretePageSetEditorTop {
    ConcretePageSetEditorTop *editor = [[ConcretePageSetEditorTop alloc] initWithNibName:@"ConcretePageSetEditorTop"];
    
    NSArray *items = @[@"Concrete",@"Mortar",@"Grout",@"Caltrans\nCylinders",@"Caltrans\nBeams",@"LCB",@"Other"];
    editor.sampleTypeControl = [[RMASegmentedControl alloc] initWithItems:items];
    editor.sampleTypeControl.frame = CGRectMake(245, 17, editor.sampleTypeControl.frame.size.width,
                                                editor.sampleTypeControl.frame.size.height);
    [editor.view addSubview:editor.sampleTypeControl];
    
    return editor;
}

-(void)setConcretePageSet:(ConcretePageSet*)set {
    [self.sampleTypeControl selectText:set.sampleType];
    self.setNoField.text = set.setNo;
    self.mixDesignNoField.text = set.mixDesignNo;
    self.mixDesignStrengthNo.text = set.mixDesignStrength;
    self.admixturesField.text = set.admixtures;
    
    UISegmentedControl *designStrengthUnitsControl = (UISegmentedControl*)[self.view viewWithTag:1];
    [designStrengthUnitsControl selectText:set.designStrengthUnits];
    
}
-(NSMutableDictionary*)getPartialConcreteSet {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:[self.sampleTypeControl text] forKey:@"sampleType"];
    [data setObject:(self.setNoField.text.length > 0 ? self.setNoField.text : @"") forKey:@"setNo"];
    [data setObject:(self.mixDesignNoField.text.length > 0 ? self.mixDesignNoField.text : @"") forKey:@"mixDesignNo"];
    [data setObject:(self.mixDesignStrengthNo.text.length > 0 ? self.mixDesignStrengthNo.text : @"") forKey:@"mixDesignStrength"];
    [data setObject:(self.admixturesField.text.length > 0 ? self.admixturesField.text : @"") forKey:@"admixtures"];
    [data setObject:[((UISegmentedControl*)[self.view viewWithTag:1]) text] forKey:@"designStrengthUnits"];
    return data;
}

-(NSMutableDictionary*)getData {
    return [self getPartialConcreteSet];
}

-(void)setData:(NSMutableDictionary *)data {
    [self.sampleTypeControl selectText:[data objectForKey:@"sampleType"]];
    self.setNoField.text = [data objectForKey:@"setNo"];
    self.mixDesignNoField.text = [data objectForKey:@"mixDesignNo"];
    self.mixDesignStrengthNo.text = [data objectForKey:@"mixDesignStrength"];
    self.admixturesField.text = [data objectForKey:@"admixtures"];
    
    UISegmentedControl *designStrengthUnitsControl = (UISegmentedControl*)[self.view viewWithTag:1];
    [designStrengthUnitsControl selectText:[data objectForKey:@"designStrengthUnits"]];
}

-(NSDictionary*)dictionary {
    NSMutableDictionary *data = [self getData];

    // Rename some fields to adapt to current naming conventions
    data[@"mixDesignNumber"] = data[@"mixDesignNo"];
    [data removeObjectForKey:@"mixDesignNo"];
    
    data[@"setNumber"] = data[@"setNo"];
    [data removeObjectForKey:@"setNo"];
    
    return data;
}

@end
