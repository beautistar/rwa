//
//  SelectionTableViewController.m
//  RMA
//
//  Created by Alexander Roode on 3/11/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "SelectionTableViewController.h"
#import "RMATableViewCell.h"

@interface SelectionTableViewController ()

@end

@implementation SelectionTableViewController

#pragma mark - Table view data source

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:RMATableViewCell.class forCellReuseIdentifier:@"Cell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = self.items[indexPath.row];
    
    return cell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *item = self.items[indexPath.row];
    [self.delegate didSelectItem:item];
}


@end
