//
//  Checklist.m
//  RMA
//
//  Created by Alexander Roode on 1/19/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "Checklist.h"

@implementation Checklist

+(instancetype)checklistWithId:(int)id name:(NSString*)name sectionNames:(NSArray*)sectionNames {
    Checklist *checklist = [[Checklist alloc] init];
    checklist.id = id;
    checklist.name = name;
    checklist.sectionNames = sectionNames;
    
    return checklist;
}

+(NSArray*)allChecklists {
    return @[
             [Checklist checklistWithId:155 name:@"Drilled in Anchor Inspection" sectionNames:@[@"Drilled in Anchor Inspection"]],
             [Checklist checklistWithId:156 name:@"Epoxy Adhesive Information" sectionNames:@[@"Epoxy Adhesive Information"]],
             [Checklist checklistWithId:157 name:@"Epoxy Adhesive Inspection" sectionNames:@[@"Epoxy Adhesive Inspection"]],
             [Checklist checklistWithId:158 name:@"Torque/Pull Testing" sectionNames:@[@"Torque/Pull Testing"]],
             //[Checklist checklistWithId:170 name:@"HMA Laydown" sectionNames:@[@"HMA Laydown"]],
             [Checklist checklistWithId:181 name:@"Ultrasonic Examination" sectionNames:@[@"Ultrasonic Examination"]],
             [Checklist checklistWithId:182 name:@"Magnetic Particle Examination" sectionNames:@[@"Magnetic Particle Examination"]],
             [Checklist checklistWithId:183 name:@"Liquid Penetrant Examination" sectionNames:@[@"Liquid Penetrant Examination"]]
             ];
}

+(Checklist*)checklistWithId:(int)id {
    NSArray *checklists = [Checklist allChecklists];
    for(Checklist *checklist in checklists) {
        if(checklist.id == id) {
            return checklist;
        }
    }
    return nil;
}
@end
