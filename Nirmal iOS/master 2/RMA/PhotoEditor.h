//
//  PhotoEditor.h
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoPageType.h"

@class Photo;

@interface PhotoEditor : UIView <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) Photo *photo;
@property (nonatomic) PhotoPageType pageType;
@property (nonatomic, assign) id<UITextFieldDelegate> delegate;

- (void)pdfDownloaded:(NSURL *)url pdfName:(NSString *)pdfName;
- (void)updateButtonTitle;
@end
