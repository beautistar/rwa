//
//  SectionEnablerEditor.m
//  RMA
//
//  Created by Michael Beteag on 11/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "SectionEnablerEditor.h"

@implementation SectionEnablerEditor

+(SectionEnablerEditor*)sectionEnablerEditorWithText:(NSString*)text key:(NSString*)key {
    SectionEnablerEditor *sectionEnablerEditor = [[SectionEnablerEditor alloc] initWithNibName:@"SectionEnablerEditor"];
    sectionEnablerEditor.enablerLabel.text = text;
    sectionEnablerEditor.key = key;
    
    return sectionEnablerEditor;
}


- (IBAction)segmentedControlValueChanged:(id)sender {
    UISegmentedControl *control = (UISegmentedControl*)sender;
    
    if (control.selectedSegmentIndex == 0) {
        self.sectionEnabled = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DidSetSectionEnabled" object:self.key];
        
    } else {
        self.sectionEnabled = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DidSetSectionDisabled" object:self.key];
        
    }
}

-(void)setFormEnabled:(BOOL)enabled {
    self.isEnabled = enabled;
    [self setControls:self.view.subviews enabled:enabled];
}

-(void)setData:(NSMutableDictionary *)data {
    NSNumber *selectedValue = [data objectForKey:self.key];
    self.segmentedControl.selectedSegmentIndex = [selectedValue intValue];
    
    self.sectionEnabled = self.segmentedControl.selectedSegmentIndex == 0;
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:[NSNumber numberWithInteger:self.segmentedControl.selectedSegmentIndex] forKey:self.key];
    return data;
}

-(NSDictionary*)dictionary {
    return @{};
}

@end
