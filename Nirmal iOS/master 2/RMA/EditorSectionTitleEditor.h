//
//  EditorSectionTitleEditor.h
//  RMA
//
//  Created by Alexander Roode on 1/21/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "FormComponentViewEditor.h"

@interface EditorSectionTitleEditor : FormComponentViewEditor
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
+(instancetype)editorSectionTitleWithText:(NSString*)title;
@end
