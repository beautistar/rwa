//
//  PhotoPageType.h
//  RMA
//
//  Created by Alexander Roode on 4/1/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

typedef NS_ENUM(NSInteger) {
    PhotoPageType1x1,
    PhotoPageType2x1,
    PhotoPageType2x2,
    PDFPageType1x1,
    PDFPageType2x1,
    PDFPageType2x2
} PhotoPageType;
