//
//  PDFViewer.m
//  rushmore
//
//  Created by Michael Beteag on 7/19/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "PDFViewer.h"

@interface PreviewItem : NSObject <QLPreviewItem>
@property (readonly) NSString *previewItemTitle;
@property (readonly) NSURL *previewItemURL;
-(void)setURL:(NSURL*)url title:(NSString*)title;
@end

@implementation PreviewItem;
-(void)setURL:(NSURL*)url title:(NSString*)title {
    _previewItemTitle = title;
    _previewItemURL = url;
    
}
@end

@implementation PDFViewer

+ (id)sharedViewer {
    static PDFViewer *viewer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        viewer = [[self alloc] init];
    });
    return viewer;
}

-(void)viewPDF:(NSString*)path withTitle:(NSString*)title {
    QLPreviewController *previewController = [[QLPreviewController alloc] init];
    previewController.delegate = self;
    previewController.dataSource = self;
    
    PreviewItem *item = [[PreviewItem alloc] init];
    [item setURL:[NSURL fileURLWithPath:path] title:title];
    self.previewItem = item;
    [self.navigationController presentViewController:previewController animated:YES completion:nil];
}

-(NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller {
    return  1;
}
-(id<QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    
    return self.previewItem;
}

@end
