//
//  Form.h
//  RMA
//
//  Created by Alexander Roode on 2/25/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Address, Contact, Employee, Project;

@interface Form : NSManagedObject

@property (nonatomic, retain) NSString * checklists;
@property (nonatomic, retain) NSString * dailyType;
@property (nonatomic, retain) NSString * dispatchId;
@property (nonatomic, retain) NSString * displayName;
@property (nonatomic, retain) NSNumber * dsa;
@property (nonatomic, retain) NSString * dsaAppNumber;
@property (nonatomic, retain) NSString * dsaFileNumber;
@property (nonatomic, retain) NSString * dsaLEANumber;
@property (nonatomic, retain) NSString * hours;
@property (nonatomic, retain) NSString * localFilename;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * remarks;
@property (nonatomic, retain) NSString * scope;
@property (nonatomic, retain) NSString * shortDisplayName;
@property (nonatomic, retain) NSString * specialInstructions;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSDate * startTime;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * task;
@property (nonatomic, retain) NSString * taskDescription;
@property (nonatomic, retain) NSString * customerEmails;
@property (nonatomic, retain) Contact *contact;
@property (nonatomic, retain) Project *project;
@property (nonatomic, retain) Employee *technician;
@property (nonatomic, retain) Address *address;

@end
