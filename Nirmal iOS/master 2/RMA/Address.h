//
//  Address.h
//  RMA
//
//  Created by Alexander Roode on 2/25/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Form;

@interface Address : NSManagedObject

@property (nonatomic, retain) NSString * address1;
@property (nonatomic, retain) NSString * address2;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) Form *form;

@end
