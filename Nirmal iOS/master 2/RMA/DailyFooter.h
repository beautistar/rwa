//
//  DailyFooter.h
//  RMA
//
//  Created by Alexander Roode on 5/12/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"

@interface DailyFooter : NSObject <GRMustacheRendering>

@end
