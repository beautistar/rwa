//
//  PhotoCollectionEditor.h
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "CollectionEditor.h"

@interface PhotoCollectionEditor : CollectionEditor
+(instancetype)photoCollectionEditorWithKey:(NSString *)editorKey;
@end
