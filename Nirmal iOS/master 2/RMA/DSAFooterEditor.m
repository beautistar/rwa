//
//  DSAFooterEditor.m
//  RMA
//
//  Created by Michael Beteag on 7/3/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "DSAFooterEditor.h"
#import "FormComponentViewEditor.h"
#import "SignatureEditor.h"

@implementation DSAFooterEditor

+(DSAFooterEditor*)DSAFooterEditorWithKey:(NSString *)key {
    DSAFooterEditor *editor = [[DSAFooterEditor alloc] initWithNibName:@"DSAFooterEditor"];
    editor.validationType = validationTypeAll;
    editor.key = key;
    return editor;
}

-(BOOL)forcesNonCompliance {
    BOOL nonCompliance = NO;
    
    if (self.inspectedInAccordance.selectedSegmentIndex == 1 ||
       self.workMeetsRequirements.selectedSegmentIndex == 1 ||
       self.samplingInAccordance.selectedSegmentIndex == 1) {
        nonCompliance = YES;
    }
    return nonCompliance;
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    RadioCheckboxSet *inspectedInAccordance = [RadioCheckboxSet radioCheckboxSetFromUISegmentedControl:self.inspectedInAccordance];
    RadioCheckboxSet *workMeetsRequirements = [RadioCheckboxSet radioCheckboxSetFromUISegmentedControl:self.workMeetsRequirements];
    RadioCheckboxSet *samlingInAccordance = [RadioCheckboxSet radioCheckboxSetFromUISegmentedControl:self.samplingInAccordance];
    
    [data setObject:inspectedInAccordance forKey:@"inspectedInAccordance"];
    [data setObject:workMeetsRequirements forKey:@"workMeetsRequirements"];
    [data setObject:samlingInAccordance forKey:@"samplingInAccordance"];
    
    return data;
}

-(void)setData:(NSMutableDictionary*)data {
    RadioCheckboxSet *inspectedInAccordance = [data objectForKey:@"inspectedInAccordance"];
    RadioCheckboxSet *workMeetsRequirements = [data objectForKey:@"workMeetsRequirements"];
    RadioCheckboxSet *samlingInAccordance = [data objectForKey:@"samplingInAccordance"];
    
    self.inspectedInAccordance.selectedSegmentIndex = [inspectedInAccordance.selectedIndex intValue];
    self.workMeetsRequirements.selectedSegmentIndex = [workMeetsRequirements.selectedIndex intValue];
    self.samplingInAccordance.selectedSegmentIndex = [samlingInAccordance.selectedIndex intValue];
}

- (IBAction)nonComplianceQuestion1Changed:(id)sender {
    [self reevaluateForcedNonComplaince];
}

- (IBAction)nonComplianceQuestion2Changed:(id)sender {
    [self reevaluateForcedNonComplaince];
}

- (IBAction)nonComplianceQuestion3Changed:(id)sender {
    [self reevaluateForcedNonComplaince];
}

-(NSDictionary*)dictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    dictionary[@"inspectedInAccordance"] = self.inspectedInAccordance.selectedSegmentIndex == 0 ? @"yes" : @"no";
    dictionary[@"workMeetsRequirements"] = self.workMeetsRequirements.selectedSegmentIndex == 0 ? @"yes": self.workMeetsRequirements.selectedSegmentIndex == 1 ? @(NO) : @"N/A";
    dictionary[@"samplingInAccordance"] = self.samplingInAccordance.selectedSegmentIndex == 0 ? @"yes" : @"no";
    
    return dictionary;
}
@end
