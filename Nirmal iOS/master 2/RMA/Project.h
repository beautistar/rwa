//
//  Project.h
//  RMA
//
//  Created by Michael Beteag on 9/25/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Client, Form;

@interface Project : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * longName;
@property (nonatomic, retain) Client *client;
@property (nonatomic, retain) NSSet *forms;
@end

@interface Project (CoreDataGeneratedAccessors)

- (void)addFormsObject:(Form *)value;
- (void)removeFormsObject:(Form *)value;
- (void)addForms:(NSSet *)values;
- (void)removeForms:(NSSet *)values;

@end
