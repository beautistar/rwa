//
//  Address.m
//  RMA
//
//  Created by Alexander Roode on 2/25/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "Address.h"
#import "Form.h"


@implementation Address

@dynamic address1;
@dynamic address2;
@dynamic city;
@dynamic state;
@dynamic form;

@end
