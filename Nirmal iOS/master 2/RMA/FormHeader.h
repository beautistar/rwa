//
//  FormHeader.h
//  RMA
//
//  Created by Michael Beteag on 10/7/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FormHeader : NSObject

@property (nonatomic, retain) NSString *templateName;
@property int height;

+(FormHeader*)headerWithType:(NSString*)headerType;
-(NSString*)HTMLwithData:(NSMutableDictionary*)data;

@end
