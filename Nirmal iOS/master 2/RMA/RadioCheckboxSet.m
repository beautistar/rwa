//
//  RadioCheckboxSet.m
//  RMA
//
//  Created by Michael Beteag on 8/27/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "RadioCheckboxSet.h"

@implementation RadioCheckboxSet

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        [self setValues:[aDecoder decodeObjectForKey:@"values"]];
        [self setSelectedIndex:[aDecoder decodeObjectForKey:@"selectedIndex"]];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.values forKey:@"values"];
    [aCoder encodeObject:self.selectedIndex forKey:@"selectedIndex"];
}

+(RadioCheckboxSet*)radioCheckboxSetFromUISegmentedControl:(UISegmentedControl*)control {
    RadioCheckboxSet *set = [[RadioCheckboxSet alloc] init];
    set.values = [NSMutableArray array];
    
    for(int i = 0; i < control.numberOfSegments; i++) {
        [set.values addObject:[control titleForSegmentAtIndex:i]];
    }
    set.selectedIndex = [NSNumber numberWithInt:(int)control.selectedSegmentIndex];
    
    return set;
}

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    
    *HTMLSafe = YES;
    NSString *HTML = @"";
    
    NSString *checkbox = @"<img class=\"checkbox\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkE5RkEyQTdCODIyRTExRTNBRDE3RjlDNTVGNUZENDlDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkE5RkEyQTdDODIyRTExRTNBRDE3RjlDNTVGNUZENDlDIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTlGQTJBNzk4MjJFMTFFM0FEMTdGOUM1NUY1RkQ0OUMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QTlGQTJBN0E4MjJFMTFFM0FEMTdGOUM1NUY1RkQ0OUMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz47Hrn5AAAANElEQVR42mJkYGD4z0BFwEhLAxkpNOs/7Q38/5983zMyMo4aOGrgqIGjBhJn4OAssQECDACLvk4B0uNjagAAAABJRU5ErkJggg==\" />";
    NSString *checkedCheckbox = @"<img class=\"checkbox\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAIAAAAC64paAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkIzMzhCQUQ3ODIyRTExRTM4QTAwOUFCODlDQUQ2N0UxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkIzMzhCQUQ4ODIyRTExRTM4QTAwOUFCODlDQUQ2N0UxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjMzOEJBRDU4MjJFMTFFMzhBMDA5QUI4OUNBRDY3RTEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjMzOEJBRDY4MjJFMTFFMzhBMDA5QUI4OUNBRDY3RTEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5DukliAAAAFUlEQVR42mJgGAWjYBSMgqELAAIMAATEAAGNWsKGAAAAAElFTkSuQmCC\" />";
    
    for(int i = 0; i < self.values.count; i++) {
        NSString *checkBoxString = [self.selectedIndex intValue] == i ? checkedCheckbox : checkbox;
        HTML = [HTML stringByAppendingFormat:@"%@ %@  &nbsp;&nbsp;&nbsp;", checkBoxString, [self.values objectAtIndex:i]];
    }
    return HTML;
}
@end
