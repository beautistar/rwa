//
//  TableEditorColumn.h
//  RMA
//
//  Created by Michael Beteag on 7/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TableEditorColumn : NSObject

@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *displayName;
@property int width;
@property int maxLength;
@property BOOL editable;

+(TableEditorColumn*)columnWithKey:(NSString*)key displayName:(NSString*)displayName width:(int)width;
+(TableEditorColumn*)columnWithKey:(NSString*)key displayName:(NSString*)displayName width:(int)width maxLength:(int)maxLength;
+(TableEditorColumn*)nonEditableColumnWithKey:(NSString*)key displayName:(NSString*)displayName width:(int)width;

@end
