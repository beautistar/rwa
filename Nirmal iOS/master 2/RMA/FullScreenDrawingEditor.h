//
//  FullScreenDrawingEditor.h
//  RMA
//
//  Created by Michael Beteag on 9/23/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NICSignatureView.h"

@protocol FullScreenDrawingEditorDelegate <NSObject>
-(void)setImage:(UIImage*)image;
-(void)clear;
-(void)cancel;
@end

@interface FullScreenDrawingEditor : UIViewController
@property (weak, nonatomic) IBOutlet NICSignatureView *imageEditorView;
@property (nonatomic, assign) id<FullScreenDrawingEditorDelegate> delegate;
- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)clearButtonPressed:(id)sender;

@end
