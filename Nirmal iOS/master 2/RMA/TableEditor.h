//
//  TableEditor.h
//  RMA
//
//  Created by Michael Beteag on 7/22/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"
#import "TableEditorColumn.h"

#define kRowHeight 38
#define kVerticalPadding 4
#define kHorizontalPadding 4
#define kAddButtonWidth 30
#define kTitleHeight 38

@interface TableEditor : FormComponentEditor

@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSArray *columns;
@property (nonatomic, retain) UIButton *addButton;
@property (nonatomic, retain) NSArray *rows;
@property int unitWidth;
@property int rowCount;
@property BOOL editable;
@property int titleHeight;

- (id)initWithFrame:(CGRect)frame columns:(NSArray*)columns key:(NSString*)key;
+(TableEditor*)tableEditorWithTitle:(NSString*)title columns:(NSArray*)columns rows:(NSArray*)rows key:(NSString*)key;
@end
