//
//  FormSection.m
//  RMA
//
//  Created by Michael Beteag on 7/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormSection.h"
#import "Timesheet.h"
#import "AppData.h"
#import "FormSection+Checklists.h"
#import "FormSection+Daily.h"
#import "FormSection+Common.h"
#import "FormDivider.h"
#import "FormHeader.h"
#import "CollectionEditor.h"
#import "CheckedCheckbox.h"
#import "Checkbox.h"
#import "FormLogo.h"
#import "DailyFooter.h"

@implementation FormSection

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        [self setSectionName:[aDecoder decodeObjectForKey:@"sectionName"]];
        //[self setKey:[aDecoder decodeObjectForKey:@"key"]];
        [self setTitle:[aDecoder decodeObjectForKey:@"title"]];
        [self setTemplateName:[aDecoder decodeObjectForKey:@"templateName"]];
        [self setData:[aDecoder decodeObjectForKey:@"data"]];
        [self setTimesheetStartDate:[aDecoder decodeObjectForKey:@"timesheetStartDate"]];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    //[aCoder encodeObject:self.key forKey:@"key"];
    [aCoder encodeObject:self.sectionName forKey:@"sectionName"];
    [aCoder encodeObject:self.templateName forKey:@"templateName"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.data forKey:@"data"];
    [aCoder encodeObject:self.timesheetStartDate forKey:@"timesheetStartDate"];
}

+(FormSection*)formSectionWithEditors:(NSArray *)editors templateName:(NSString *)templateName title:(NSString*)title xmlGrouping:(NSString*)grouping xmlSubGrouping:(NSString*)subGrouping shouldOutputXml:(BOOL)shouldOutputXml {
    FormSection *section = [[FormSection alloc] init];
    section.editors = editors;
    section.templateName = templateName;
    section.title = title;
    section.xmlGrouping = grouping;
    section.xmlSubGrouping = subGrouping;
    section.shouldOutputXml = shouldOutputXml;
    return section;
}

+(FormSection*)formSectionWithEditors:(NSArray *)editors templateName:(NSString *)templateName title:(NSString *)title {
    return [self formSectionWithEditors:editors templateName:templateName title:title xmlGrouping:nil xmlSubGrouping:nil shouldOutputXml:YES];
}

+(FormSection*)formSectionWithEditors:(NSArray *)editors templateName:(NSString *)templateName title:(NSString*)title xmlGrouping:(NSString *)grouping xmlSubGrouping:(NSString*)subGrouping {
    return [self formSectionWithEditors:editors templateName:templateName title:title xmlGrouping:grouping xmlSubGrouping:subGrouping shouldOutputXml:YES];
}

+(FormSection*)formSectionWithEditors:(NSArray *)editors templateName:(NSString *)templateName title:(NSString *)title shouldOutputXml:(BOOL)shouldOutputXml {
    return [self formSectionWithEditors:editors templateName:templateName title:title xmlGrouping:nil xmlSubGrouping:nil shouldOutputXml:shouldOutputXml];
}

+(FormSection*)formSectionWithName:(NSString *)sectionName {
    
    FormSection *section;
    
    if ([sectionName isEqualToString:@"Empty"]) {
        section = [FormSection emptySection];
    } else if ([sectionName isEqualToString:@"Test"]) {
        section = [FormSection testSection];
        
    } else if ([sectionName isEqualToString:@"Drilled in Anchor Inspection"]) {
        section = [FormSection drilledInAnchorInspectionSection];
        
    } else if ([sectionName isEqualToString:@"Epoxy Adhesive Information"]) {
        section = [FormSection epoxyAdhesiveInformationSection];
        
    } else if ([sectionName isEqualToString:@"Epoxy Adhesive Inspection"]) {
        section = [FormSection epoxyAdhesiveInspectionSection];
        
    } else if ([sectionName isEqualToString:@"Torque/Pull Testing"]) {
        section = [FormSection torquePullTestingSection];
        
    } else if ([sectionName isEqualToString:@"Magnetic Particle Examination"]) {
        section = [FormSection magneticParticleExaminationSection];
        
    } else if ([sectionName isEqualToString:@"Liquid Penetrant Examination"]) {
        section = [FormSection liquidPenetrantExaminationSection];
        
    } else if ([sectionName isEqualToString:@"Ultrasonic Examination"]) {
        section = [FormSection ultrasonicExaminationSection];
        
    } else if ([sectionName isEqualToString:@"HMA Laydown"]) {
        section = [FormSection hmaLaydownSection];
        
    } else if ([sectionName isEqualToString:@"Checklists"]) {
        section = [FormSection checklistsSection];
        
    } else if ([sectionName isEqualToString:@"Daily Inspection Report"]) {
        section = [FormSection dailyInspectionSection];
        
    } else if ([sectionName isEqualToString:@"DSA Daily Inspection Report"]) {
        section = [FormSection DSAdailyInspectionSection];
        
    } else if ([sectionName isEqualToString:@"Daily Technician Report"]) {
        section = [FormSection dailyTechnicianSection];
        
    } else if ([sectionName isEqualToString:@"DSA Daily Technician Report"]) {
        section = [FormSection DSAdailyTechnicianSection];
        
    } else if ([sectionName isEqualToString:@"NonCompliance"]) {
        section = [FormSection nonComplianceSection];
        
    } else if ([sectionName isEqualToString:@"Photos"]) {
        section = [FormSection photosSection];
        
    }  else if ([sectionName isEqualToString:@"MasterForms"]) {
        section = [FormSection pdfSection];
        
    } else if ([sectionName isEqualToString:@"Standard Samples"]) {
        section = [FormSection standardSamplesSection];
        
    } else if ([sectionName isEqualToString:@"additionalRemarks"]) {
        section = [FormSection additionalRemarksSection];

    } else if ([sectionName isEqualToString:@"AddMoreSamples"]) {
        section = [FormSection addMoreSamples];
        
    }
    
    if (section != nil) {
        section.sectionName = sectionName;
        section.headerDefault = [FormHeader headerWithType:@"nonDSAHeader"];
        return section;
    }
    
    return nil;
}

// Section is enabled if at least one editor (non counting FormDividers) is enabled
-(BOOL)isEnabled {
    
    BOOL isEnabled = NO;
    for(FormComponentEditor *editor in self.editors) {
        //NSLog(@"section %@ editor %@ is enabled %d", self.sectionName, NSStringFromClass([editor class]), editor.isEnabled);
        if (![editor isKindOfClass:[FormDivider class]]) {
            isEnabled = isEnabled || editor.isEnabled;
        }
    }
    return isEnabled;
}

// Hack to hide photos and samples sections in PDF when they have no items set
-(BOOL)isHiddenInPdf {
    if (self.editors.count != 2) {
        return NO;
    }
    
    if ([self.editors[1] isKindOfClass:CollectionEditor.class]) {
        CollectionEditor *editor = self.editors[1];
        return editor.items.count == 0;
    }
    
    return NO;
}

-(NSString*)getRenderedHTML {
    if (![self isEnabled]) {
        return @"";
    }
    
    NSError *error = nil;
    
    NSString *html = [GRMustacheTemplate renderObject:[self getDataForRendering]
                                         fromResource:self.templateName
                                               bundle:nil
                                                error:&error];
    
#ifdef DEBUG
    [[AppData sharedData] logHTML:html];
#endif
    return html;
}

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    *HTMLSafe = YES;
    if (![self isEnabled]) {
        return @"";
    }
    return [GRMustacheTemplate renderObject:[self getDataForRendering]
                     fromResource:self.templateName
                           bundle:nil
                            error:NULL];
    
}
-(NSMutableDictionary*)getDataForSaving {
    return [self getDataForRendering:NO];
}

-(NSMutableDictionary*)getDataForRendering {
    return [self getDataForRendering:YES];
}

-(NSMutableDictionary*)getDataForRendering:(BOOL)forRendering {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    for(FormComponentEditor *editor in self.editors) {
        if (forRendering) {
            [data addEntriesFromDictionary:[editor getDataForRendering]];
        } else {
            [data addEntriesFromDictionary:[editor getData]];
        }
    }
    
    [data setObject:self.sectionName forKey:@"sectionName"];
    [data addEntriesFromDictionary:self.metadata];

    if (self.useSectionName) {
        NSString *displayName = self.metadata[@"displayName"];
        NSMutableArray *nameComponents = [NSMutableArray arrayWithArray:[displayName componentsSeparatedByString:@" | "]];
        if (nameComponents.count > 0) {
            nameComponents[1] = self.sectionName;
            data[@"displayName"] = [nameComponents componentsJoinedByString:@" | "];
        }
    }
    
    if ([self.sectionName isEqualToString:@"timeSheet"]) {
        //Create timesheet rendering object
        
        Timesheet *timeSheet = [Timesheet timesheetFromDictionary:data];
        [data setObject:timeSheet forKey:@"timeSheet"];
    }
    if (self.extraData != nil) {
        [data addEntriesFromDictionary:self.extraData];
    }
    
    if (forRendering) {
        [data setObject:@{@"Yes" : @"Yes",
                          @"No" : @"No",
                          @"Vertical": @"Vertical",
                          @"Horizontal": @"Horizontal",
                          @"Flat" : @"Flat",
                          @"Overhead" : @"Overhead",
                          @"Rebar" : @"Rebar",
                          @"ShotpinWireAssembly" : @"Shotpin/Wire Assembly",
                          @"Anchors" : @"Anchors",
                          @"Dynamometer" : @"Dynamometer",
                          @"TorqueWrench" : @"Torque Wrench",
                          @"HydraulicJack" : @"Hydraulic Jack",
                          @"Scale" : @"Scale",
                          @"Type1" : @"Type 1",
                          @"Type2" : @"Type 2",
                          @"One" : @"1",
                          @"B" : @"B",
                          @"I" : @"I",
                          @"F" : @"F"} forKey:@"constants"];
        
        [data setObject:[GRMustacheFilter variadicFilterWithBlock:^id(NSArray *arguments) {
            NSString *target = [arguments firstObject];
            NSString *value = arguments[1];
            
            if ([value isEqualToString:target]) {
                return [[CheckedCheckbox alloc] init];
            } else {
                return [[Checkbox alloc] init];
            }
        }] forKey:@"checkMarkIf"];
        
        [data setObject:[[FormLogo alloc] init] forKey:@"formLogo"];
        [data setObject:[[DailyFooter alloc] init] forKey:@"dailyFooter"];
    }
    
    return data;
}

-(void)setEditorData:(NSMutableDictionary*)data {
    for(FormComponentEditor *editor in self.editors) {
        [editor setData:data];
    }
}

-(NSDictionary*)dictionary {
    if (!self.shouldOutputXml || ![self isEnabled]) {
        return @{};
    }
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    for (FormComponentEditor *editor in self.editors) {
        [dictionary addEntriesFromDictionary:[editor dictionary]];
    }
    
    // Grouped sections will be a placed in a single array for each group
    if (self.xmlGrouping.length > 0) {
        return dictionary;
    }
    
    // Unique sections will be combined with the overall dictionary
    return @{self.xmlTagName ?: self.templateName : dictionary};
}
@end
