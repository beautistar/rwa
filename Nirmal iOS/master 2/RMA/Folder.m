//
//  Folder.m
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "Folder.h"
#import "Document.h"
#import "Folder.h"


@implementation Folder

@dynamic name;
@dynamic id;
@dynamic modified;
@dynamic path;
@dynamic documents;
@dynamic subfolders;
@dynamic parent;

@end
