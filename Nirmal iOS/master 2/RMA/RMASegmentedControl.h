//
//  RMASegmentedControl.h
//  RMA
//
//  Created by Michael Beteag on 6/25/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMASegmentedControl : UIView
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
- (id)initWithItems:(NSArray*)items;
-(void)selectText:(NSString*)text;
-(NSString*)text;
-(void)setOtherValue:(NSString*)otherValue;
-(void)reset;

@end
