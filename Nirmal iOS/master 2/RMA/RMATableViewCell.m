//
//  RMATableViewCell.m
//  RMA
//
//  Created by Alexander Roode on 1/29/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "RMATableViewCell.h"

@implementation RMATableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        UIView *customColorView = [[UIView alloc] init];
        customColorView.backgroundColor = [UIColor colorWithRed:17/255.0 green:150/255.0 blue:237/255.0 alpha:1.0];
        self.selectedBackgroundView = customColorView;
        self.textLabel.highlightedTextColor = [UIColor whiteColor];
    }
    return self;
}

@end
