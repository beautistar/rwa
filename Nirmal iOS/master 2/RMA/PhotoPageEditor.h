//
//  PhotoPageEditor.h
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"

@class PhotoPage;

@interface PhotoPageEditor : FormComponentEditor

+(instancetype)photoPageEditorWithPhotoPage:(PhotoPage*)photoPage itemType:(NSString *)itemType;
@end
