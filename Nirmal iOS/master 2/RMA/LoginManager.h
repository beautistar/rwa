//
//  LoginManager.h
//  RMA
//
//  Created by Michael Beteag on 4/14/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kServiceName @"net.forebrain.RMA"

@interface LoginManager : NSObject

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;

+(LoginManager*)loginManagerWithUsername:(NSString*)username password:(NSString*)password;
-(void)login;
+(NSString*)lastLoggedInUsername;
+(void)setLastLoggedInUsername:(NSString*)username;

@end
