//
//  AdditionalRemarks.h
//  RMA
//
//  Created by Michael Beteag on 10/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"
#import "Paragraph.h"

@interface AdditionalRemarks : NSObject <NSCoding, GRMustacheRendering>

@property (nonatomic, retain) Paragraph *remarksParagraph;

- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;
+ (AdditionalRemarks*)additionalRemarksWithParagraph:(Paragraph*)paragraph;
- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error;

@end
