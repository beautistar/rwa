//
//  Updater.m
//  RMA
//
//  Created by Michael Beteag on 4/14/14.
//  Copyright (c) 2014 Forebrain. All rights reserved.
//

#import "Updater.h"
#import "LoginManager.h"
#import "SSKeychain/SSKeychain.h"
#import "UpdateRequest.h"

#define kUpdateInterval 20

@implementation Updater {
    id successObserver;
    id failureObserver;
}


+(BOOL)updateNeededForCurrentUser {
    NSString *lastLoggedInUsername = [LoginManager lastLoggedInUsername];
    NSString *key = [NSString stringWithFormat:@"LastUpdateDate%@",lastLoggedInUsername];
    NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
    if(date == nil) {
        return NO;
    }
    
    if([Updater minutesSinceDate:date] > kUpdateInterval) {
        return YES;
    }
    return NO;
}

+(int)minutesSinceDate:(NSDate*)date {
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:date];
    int elapsedMinutes = interval / 60;
    return elapsedMinutes;
}

+(void)setLastUpdatedDateForCurrentUser {
    NSString *lastLoggedInUsername = [LoginManager lastLoggedInUsername];
    NSString *key = [NSString stringWithFormat:@"LastUpdateDate%@",lastLoggedInUsername];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)updateForms {
    
    NSString *lastLoggedInUsername = [LoginManager lastLoggedInUsername];
    NSString *password = [SSKeychain passwordForService:kServiceName account:lastLoggedInUsername];
    
    UpdateRequest *request = [UpdateRequest updateRequestWithUsername:lastLoggedInUsername password:password];
    [request begin];
    
    successObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"UpdateRequestDidSucceed" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [Updater setLastUpdatedDateForCurrentUser];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateDidSucceed" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:successObserver];
        [[NSNotificationCenter defaultCenter] removeObserver:failureObserver];
    }];
    
    failureObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"UpdateRequestDidFail" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateDidFail" object:note.object];
        [[NSNotificationCenter defaultCenter] removeObserver:successObserver];
        [[NSNotificationCenter defaultCenter] removeObserver:failureObserver];
    }];
}

@end
