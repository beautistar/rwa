//
//  Document+Extensions.h
//  RMA
//
//  Created by Alexander Roode on 1/8/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "Document.h"

@interface Document (Extensions)
-(NSString*)getAbsolutePath;
@end
