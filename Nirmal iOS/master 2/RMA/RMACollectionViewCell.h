//
//  RMACollectionViewCell.h
//  RMA
//
//  Created by Michael Beteag on 12/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMACollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
