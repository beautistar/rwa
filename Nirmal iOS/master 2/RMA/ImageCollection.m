//
//  ImageCollection.m
//  RMA
//
//  Created by Michael Beteag on 8/21/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "ImageCollection.h"

@implementation ImageCollection

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        [self setImages:[aDecoder decodeObjectForKey:@"images"]];
        [self setDescriptions:[aDecoder decodeObjectForKey:@"descriptions"]];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.images forKey:@"images"];
    [aCoder encodeObject:self.descriptions forKey:@"descriptions"];
}

+(ImageCollection*)imageCollectionWithImages:(NSMutableArray*)images descriptions:(NSMutableArray*)descriptions {
    ImageCollection *collection = [[ImageCollection alloc] init];
    collection.images = images;
    collection.descriptions = descriptions;
    return collection;
}

- (NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                           context:(GRMustacheContext *)context
                          HTMLSafe:(BOOL *)HTMLSafe
                             error:(NSError **)error {
    
    *HTMLSafe = YES;
    
    if(self.images.count > 0) {
        NSString *html = @""; 

        for(int i = 0; i < self.images.count; i++) {
            if(i%4 == 0) {
                /*NSDictionary *data = @{@"photoPageNumber":[NSNumber numberWithInt:(i/4+1)],
                    @"requestDate":[context valueForMustacheKey:@"requestDate"],
                    @"project":[context valueForMustacheKey:@"project"]};
                html = [html stringByAppendingString:[GRMustacheTemplate renderObject:data
                                    fromResource:@"photoPage"
                                          bundle:nil
                                           error:NULL]];*/
                html = [html stringByAppendingString:@"<div class=\"page-break\">&nbsp;</div><table class=\"photo-table first\"><tr>"];
            }
            
            if(i%2 == 0) {
                html = [html stringByAppendingString:@"<tr>"];
            }
            
            UIImage *image = [self.images objectAtIndex:i];
            NSString *description = [self.descriptions objectAtIndex:i];
            
            NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
            NSString *encodedString = [imageData base64EncodingWithLineLength:0];
            NSString *text = [NSString stringWithFormat:@"<td><img alt=\"\" src=\"data:image/jpg;base64,%@\" class=\"camera-image\" /><p class=\"image-description\">%@</p></td>", encodedString, description];
            html = [html stringByAppendingString:text];
            
            if(i == self.images.count - 1 && self.images.count % 2 == 1) {
                html = [html stringByAppendingString:@"<td></td>"];
            }
            if(i == self.images.count - 1 && (self.images.count % 4 == 1 || self.images.count % 4 == 2)) {
                html = [html stringByAppendingString:@"<tr><td></td><td></td></tr>"];
            }
            
            
            if(i%2 == 1) {
                html = [html stringByAppendingString:@"</tr>"];
            }
            
            
            if(i%4 == 3) {
                html = [html stringByAppendingString:@"</tr></table>"];
                
            }
            
        }
        return html;
    } else {
        return @"<div class=\"hidden\">aa</div>";
    }
}

@end
