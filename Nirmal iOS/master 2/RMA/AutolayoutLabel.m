//
//  AutolayoutLabel.m
//  RMA
//
//  Created by Alexander Roode on 2/18/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "AutolayoutLabel.h"

@implementation AutolayoutLabel

-(void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    if(self.preferredMaxLayoutWidth != self.bounds.size.width) {
        self.preferredMaxLayoutWidth = self.bounds.size.width;
    }
}

@end
