//
//  AdditionalRemarksEditor.h
//  RMA
//
//  Created by Michael Beteag on 10/30/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"
#import "TextAreaEditor.h"
#import "AdditionalRemarks.h"

@interface AdditionalRemarksEditor : FormComponentEditor

@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) UIButton *toggleButton;
@property (nonatomic, retain) TextAreaEditor *additionalRemarksEditor;
@property BOOL additionalRemarksOn;

+(AdditionalRemarksEditor*)additionalRemarksEditorWithKey:(NSString*)key;

@end
