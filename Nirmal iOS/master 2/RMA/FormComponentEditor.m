//
//  FormComponentEditor.m
//  RMA
//
//  Created by Michael Beteag on 7/16/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import "FormComponentEditor.h"
#import "RMATextField.h"
#import "SignatureEditor.h"
#import "RMASegmentedControl.h"
#import "RMATimeField.h"
#import "RMADateField.h"
#import "RMACheckbox.h"
#import "Constants.h"

@interface FormComponentEditor ()
@property (nonatomic, strong) UIPopoverController *popoverController;
@property (nonatomic, strong) UITextField *datePickerOutputField;
@end

@implementation FormComponentEditor

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.view.userInteractionEnabled = YES;
        self.view = self;
        self.isEnabled = YES;
    
    }
    return self;
}

-(void)textViewDidChange:(UITextView *)textView {
    textView.backgroundColor = [UIColor clearColor];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if(![textView isKindOfClass:[RMATextView class]]) {
        return YES;
    }
    int maxHeight = [[textView valueForKey:@"maxHeight"] intValue];
    
    if(maxHeight == 0) {
        return YES;
    }

    UIFont *font = [UIFont systemFontOfSize:14];
    NSString* newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    CGSize tallerSize = CGSizeMake(textView.frame.size.width+15,CGFLOAT_MAX); // pretend there's more vertical space to get that extra line to check on
    
    CGSize newSize = CGSizeMake(0, 0);
    CGRect rect = [newText boundingRectWithSize:tallerSize
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:font}
                                        context:nil];
    newSize = rect.size;

    return newSize.height <= maxHeight;
}

-(void)closeKeyboardIfOpen {
    [self.superview endEditing:NO];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if([textField isKindOfClass:[RMATimeField class]] || [textField isKindOfClass:[RMADateField class]]) {
        self.datePickerOutputField = textField;
        
        [self closeKeyboardIfOpen];
        DatePickerViewController *viewController = [[DatePickerViewController alloc] init];
        if([textField isKindOfClass:[RMATimeField class]]) {
            viewController.mode = UIDatePickerModeTime;
            if(((RMATimeField*)textField).military) {
                viewController.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
            }
        } else {
            viewController.mode = UIDatePickerModeDate;
        }
        
        self.popoverController = [[UIPopoverController alloc] initWithContentViewController:viewController];
        viewController.delegate = self;
        [self.popoverController presentPopoverFromRect:textField.frame inView:textField.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        if(textField.text.length > 0) {
            [viewController setDateFromString:textField.text];
        }
        
        return NO;
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(![textField isKindOfClass:[RMATextField class]]) {
        return YES;
    }
    
    if (((RMATextField*)textField).numeric) {
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound) {
            return NO;
        }
    }
    
    int maxWidth = [[textField valueForKey:@"maxWidth"] intValue];
    if(maxWidth > 0) {
        UIFont *font = [UIFont systemFontOfSize:14];
        CGSize newSize = CGSizeZero;
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        CGRect rect = [newText boundingRectWithSize:CGSizeMake(MAXFLOAT, 20)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:font}
                                            context:nil];
        newSize = rect.size;

        return newSize.width < maxWidth;
    }
    
    int maxLength = [[textField valueForKey:@"maxLength"] intValue];

    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= maxLength || returnKey;
}

-(void)textFieldDidChange:(UITextField*)textField {
    textField.backgroundColor = [UIColor whiteColor];
    [textField removeTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

-(void)segmentedControlDidChange:(UISegmentedControl*)segmentedControl {
    segmentedControl.tintColor = nil;
    
    // Code is removing enabler selector as well...
    //[segmentedControl removeTarget:self action:@selector(segmentedControlDidChange:) forControlEvents:UIControlEventValueChanged];

    
}

-(BOOL)requireAllFields:(NSArray*)views validationErrors:(NSMutableArray**)validationErrors referenceView:(UIView*)referenceView {
    BOOL validationSuccess = YES;
    
    UIColor *highlightColor = [UIColor colorWithRed:1.0 green:214/255.0 blue:214/255.0 alpha:1];
    
    NSMutableArray *errors = [NSMutableArray array];
    
    for (UIView *view in views) {
        if([view isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)view;
            if(!textField.enabled) {
                continue;
            }
            if(textField.text.length > 0) {
                continue;
            } else {
                validationSuccess = NO;
                FormValidationError *error = [FormValidationError FormValidationErrorWithMessage:@"Missing one or more required fields. Please enter a value for the fields highlighted in pink, then tap \"Client Review\" again"
                                                                                     scrollPoint:[referenceView convertPoint:CGPointZero fromView:view]];
                
                
                [errors addObject:error];
                view.backgroundColor = highlightColor;
                [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            }
        } else if([view isKindOfClass:[UISegmentedControl class]] || [view isKindOfClass:[RMASegmentedControl class]]) {
            UISegmentedControl *control = nil;
            
            if([view isKindOfClass:[RMASegmentedControl class]]) {
                control = ((RMASegmentedControl*)view).segmentedControl;
            } else {
                control = (UISegmentedControl*)view;
            }
            
            
            if(!control.enabled) {
                continue;
            }
            
            if(control.selectedSegmentIndex == UISegmentedControlNoSegment) {
                
                if([view isKindOfClass:[RMASegmentedControl class]]) {
                    RMASegmentedControl *rmaSegmentedControl = (RMASegmentedControl*)view;
                    if([rmaSegmentedControl text].length > 0) {
                        continue;
                    }
                }
                
                validationSuccess = NO;
                FormValidationError *error = [FormValidationError FormValidationErrorWithMessage:@"Missing one or more required fields. Please enter a value for the fields highlighted in pink, then tap \"Client Review\" again"
                                                                                     scrollPoint:[referenceView convertPoint:CGPointZero fromView:view]];
                
                
                [errors addObject:error];
                
                control.tintColor = highlightColor;
                [control addTarget:self action:@selector(segmentedControlDidChange:) forControlEvents:UIControlEventValueChanged];
                
            }
        }
        
        else if([view isKindOfClass:[UITextView class]]) {
            UITextView *textView = (UITextView*)view;
            if(!textView.editable) {
                continue;
            }
            if(!(textView.text.length > 0)) {
                validationSuccess = NO;
                FormValidationError *error = [FormValidationError FormValidationErrorWithMessage:@"Missing one or more required fields. Please enter a value for the fields highlighted in pink, then tap \"Client Review\" again" scrollPoint:[referenceView convertPoint:CGPointZero fromView:view]];
                [errors addObject:error];
                
                textView.backgroundColor = highlightColor;
            }
        } else if([view isKindOfClass:[SignatureEditor class]]) {
            SignatureEditor *editor = (SignatureEditor*)view;
            if(!editor.formSigned) {
                FormValidationError *error = [FormValidationError FormValidationErrorWithMessage:@"Please sign the form, then tap \"Client Review\" again" scrollPoint:[referenceView convertPoint:CGPointZero fromView:view]];
                [errors addObject:error];
            }
        }
    }
    if(errors.count > 0) {
        if(*validationErrors == nil) {
            *validationErrors = errors;
        } else {
            [(*validationErrors) addObjectsFromArray:errors];
        }
    }
    
    return validationSuccess;
}

-(BOOL)validates:(NSMutableArray**)validationErrors referenceView:(UIView*)referenceView {
    if(self.validationType == validationTypeNone) {
        return YES;
    }
    if(self.validationType == validationTypeAll) {
        [self requireAllFields:self.view.subviews validationErrors:validationErrors referenceView:referenceView];
        
        return *validationErrors == nil;
    } else {
        
    }
    return YES;
}
- (IBAction)toggleButtonSelected:(id)sender {
    UIButton *button = (UIButton*)sender;
    button.selected = !button.selected;
}

-(void)didEnterDate:(NSString *)date {
    [self.popoverController dismissPopoverAnimated:YES];
    self.datePickerOutputField.text = date;
    self.datePickerOutputField.backgroundColor = [UIColor whiteColor];
    self.datePickerOutputField = nil;
}

-(void)addEnablerWithText:(NSString*)text reversed:(BOOL)reversed {
    UILabel *enablerLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPaddingX,
                                                                       kPaddingY + 0.5 * kSegmentedControlBarHeight - 0.5 * kLabelHeight,
                                                                       768 - 2 * kPaddingX,
                                                                        kLabelHeight)];
    enablerLabel.text = text;
    [enablerLabel sizeToFit];
    
    NSArray *items = reversed ? @[@"No",@"Yes"] : @[@"Yes",@"No"];
    
    self.enabler = [[UISegmentedControl alloc] initWithItems:items];
    self.enabler.frame = CGRectMake(2*kPaddingX + enablerLabel.frame.size.width,
                                      kPaddingY,
                                      self.enabler.frame.size.width,
                                      self.enabler.frame.size.height);
    [self.enabler addTarget:self action:@selector(enablerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:enablerLabel];
    
    [self.view addSubview:self.enabler];
    
    [self setFormEnabled:NO];
}

-(void)enablerValueChanged:(UISegmentedControl*)enabler {
    int i = (int)enabler.selectedSegmentIndex;
    if(i == 0) {
        [self setFormEnabled:YES];
        self.isEnabled = YES;
    } else {
        [self setFormEnabled:NO];
        self.isEnabled = NO;
    }
}

-(void)setFormEnabled:(BOOL)enabled {
    self.isEnabled = enabled;
}

-(void)setEnabled:(BOOL)enabled forTagsBetween:(int)firstTag and:(int)lastTag {
    NSMutableIndexSet *indices = [NSMutableIndexSet indexSet];
    for(int i = 0; i < self.view.subviews.count; i++) {
        UIView *view = [self.view.subviews objectAtIndex:i];
        if(view.tag >= firstTag && view.tag <= lastTag) {
            [indices addIndex:i];
        }
    }
    [self setControls:[self.view.subviews objectsAtIndexes:indices] enabled:enabled];
}

-(void)setControls:(NSArray*)controls enabled:(BOOL)enabled {
    UIColor *enabledColor = [UIColor blackColor];
    UIColor *disabledColor = [UIColor lightGrayColor];
    for(UIView* subview in controls) {
        if([subview isKindOfClass:[UITextField class]]) {
            UITextField *field = (UITextField*)subview;
            field.textColor = enabled ? enabledColor : disabledColor;
            field.enabled = enabled;
            if(enabled && [field.text isEqualToString:@"N/A"]) {
                field.text = @"";
            }
        } else if([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton*)subview;
            button.enabled = enabled;
        } else if([subview isKindOfClass:[UISegmentedControl class]]) {
            UISegmentedControl *control = (UISegmentedControl*)subview;
            control.enabled = enabled;
        } else if([subview isKindOfClass:[UITextView class]]) {
            UITextView *textView = (UITextView*)subview;
            textView.textColor = enabled ? enabledColor : disabledColor;
            textView.editable = enabled;
        } else if([subview isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel*)subview;
            label.textColor = enabled ? enabledColor : disabledColor;
        }
    }
}

-(BOOL)forcesNonCompliance {
    return NO;
}

-(void)reevaluateForcedNonComplaince {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReevaluateForcedNonCompliance" object:nil];
    
}

-(NSMutableDictionary*)getSharedData {
    return nil;
}

-(NSMutableDictionary*)getData {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    for (UIView *view in self.view.subviews) {
        if (view.tag < 1) continue;
        NSString *text = @"";
        if ([view isKindOfClass:[UITextField class]]) {
            UITextField *field = (UITextField*)view;
            text = field.text;
            if (!field.enabled) {
                text = @"N/A";
            }
        } else if ([view isKindOfClass:[UITextView class]]) {
            UITextView *textView = (UITextView*)view;
            text = textView.text;
        } else if ([view isKindOfClass:[UISegmentedControl class]]) {
            UISegmentedControl *control = (UISegmentedControl*)view;
            text = [control text];
            if(!control.enabled) {
                text = @"N/A";
            }
        } else if ([view isKindOfClass:[RMACheckbox class]]) {
            RMACheckbox *checkbox = (RMACheckbox*)view;
            text = checkbox.checked ? @"1" : @"0";
        } else if ([view isKindOfClass:[UIButton class]]) {
            text = ((UIButton*)view).selected ? @"X" : @" ";
        }
        [data setObject:text forKey:[NSString stringWithFormat:@"t%li",(long)view.tag]];
    }
    
    return data;
}

-(NSMutableDictionary*)getDataForRendering {
    return [self getData];
}

-(void)setData:(NSMutableDictionary*)data {
    for(UIView *view in self.view.subviews) {
        if (view.tag < 1) continue;
        
        NSString *value = [data objectForKey:[NSString stringWithFormat:@"t%li",(long)view.tag]];
        if ([value length] == 0) continue;
        
        if ([view isKindOfClass:[UITextField class]]) {
            ((UITextField*)view).text = value;
        } else if  ([view isKindOfClass:[UITextView class]]) {
            UITextView *textView = (UITextView*)view;
            textView.text = value;
        } else if ([view isKindOfClass:[UISegmentedControl class]]) {
            UISegmentedControl *control = (UISegmentedControl*)view;
            for(int i = 0; i < [control numberOfSegments]; i++) {
                if([[control titleForSegmentAtIndex:i] isEqualToString:value]) {
                    [control setSelectedSegmentIndex:i];
                    break;
                }
            }
        } else if ([view isKindOfClass:[RMACheckbox class]]) {
            RMACheckbox *checkbox = (RMACheckbox*)view;
            checkbox.checked = [value boolValue];
        } else if ([view isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton*)view;
            if([value isEqualToString:@"X"])  {
                button.selected = YES;
            } else {
                button.selected = NO;
            }
        }
    }
}

// Export entered data as dictionary, to be converted to XML
-(NSDictionary*)dictionary {
    return [NSDictionary dictionary];
}

-(void) LoadLabSamples
{
    NSString *BaseURLString = [NSString stringWithFormat:@"http://174.127.39.132/LIMSApp_Service/ServiceDataServe.svc/Get_All_Master_Sample_Type_Lab"];
    NSURL *url = [NSURL URLWithString:BaseURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
         self.LabSamples = [[NSMutableDictionary alloc]init];
         self.LabSamples = (NSMutableDictionary *) dictionary;
         NSLog(@"%@",dictionary);
         NSLog(@"Self.LabSamples.InsideSVCall ===>>> %@",self.LabSamples);
         [self.LabSamplesTableView reloadData];
     }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"self.LabSamples.count === %lu",(unsigned long)self.LabSamples.count);
    return self.LabSamples.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSLog(@"Self.LabSamples.CellForRowAtIndexPath === %@",self.LabSamples);
    cell.textLabel.text = [self.LabSamples objectForKey:@"SampleType"];
    return cell;
}

@end
