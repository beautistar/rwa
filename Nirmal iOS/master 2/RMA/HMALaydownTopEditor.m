//
//  HMALaydownTopEditor.m
//  RMA
//
//  Created by Alexander Roode on 4/10/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "HMALaydownTopEditor.h"

@implementation HMALaydownTopEditor

-(BOOL)validates:(NSMutableArray **)validationErrors referenceView:(UIView *)referenceView {
    NSMutableArray *requiredFields = [NSMutableArray array];
    for(UIView *subview in self.view.subviews) {
        if(subview.tag >= 6 && subview.tag <= 12) {
            [requiredFields addObject:subview];
        }
    }
    
    [self requireAllFields:requiredFields validationErrors:validationErrors referenceView:referenceView];
    
    return *validationErrors == nil;
}

@end
