//
//  Photo.h
//  RMA
//
//  Created by Alexander Roode on 3/14/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Photo : NSObject <NSCoding>
@property (nonatomic, copy) NSString *caption;
@property (nonatomic, copy) UIImage *image;
@end
