//
//  FormEditorViewController.h
//  RMA
//
//  Created by Michael Beteag on 7/15/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppData.h"
#import "Form.h"
#import "FormBuilder.h"
#import "FormPreviewViewController.h"
#import "FormInfoViewController.h"
#import "FormComponentEditor.h"
#import "FormDivider.h"
#import "FormEditorPage.h"
#import "FormSection.h"
#import "ChecklistSelectionViewController.h"
#import "PageJumpTableViewController.h"

@interface FormEditorViewController : UIViewController <UIAlertViewDelegate, UIGestureRecognizerDelegate,
UIScrollViewDelegate, UIImagePickerControllerDelegate, ChecklistSelectionViewControllerDelegate, PageJumpDelegate>

@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) Form *form;
@property (nonatomic, retain) FormBuilder *formBuilder;
@property (nonatomic, retain) UIPopoverController *popoverController;
@property BOOL shouldOpenInfo;
@property int currentY;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *infoButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollViewContainer;
@property (nonatomic, retain) UITapGestureRecognizer *tapGestureRecognizer;
@property (strong, nonatomic) IBOutlet UISegmentedControl *pageSegmentedControl;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *clientReviewButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *pageBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveBarButtonItem;
@property (nonatomic, retain) NSMutableArray *editorPages;

- (IBAction)segmentedControlValueChanged:(id)sender;

- (IBAction)previewAndSubmit:(id)sender;
- (IBAction)openInfo:(id)sender;
- (IBAction)save:(id)sender;

@end
