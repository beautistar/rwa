//
//  AddChecklistViewController.h
//  RMA
//
//  Created by Alexander Roode on 1/19/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Checklist;

@protocol AddChecklistViewControllerDelegate
-(void)addChecklist:(Checklist*)checklist;
@end

@interface AddChecklistViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *checklistOptions;
@property (nonatomic, weak) id<AddChecklistViewControllerDelegate> delegate;
@end
