//
//  SubmissionManager.m
//  RMA
//
//  Created by Alexander Roode on 6/27/16.
//  Copyright © 2016 Forebrain. All rights reserved.
//

#import "SubmissionManager.h"
#import "FormBuilder.h"
#import "NonDSAFooterEditor.h"
#import "XMLDictionary.h"
#import "AFNetworking.h"
#import "Constants.h"

@implementation SubmissionManager

-(instancetype)initWithDelegate:(id<SubmissionManagerDelegate>)delegate {
    self = [super init];
    
    if (self) {
        self.delegate = delegate;
    }
    
    return self;
}

-(void)submitXml:(FormBuilder *)formBuilder {
    NSDictionary *xmlDictionary = [formBuilder dictionary];
    NSString *xml = [xmlDictionary XMLString];
    xml = [xml stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSData *xmlData = [xml dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *drNumber = formBuilder.metadata[@"drNumber"];
    NSString *employeeId = formBuilder.metadata[@"employeeId"];
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:kRMAWebServiceRootUrl]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"formData?employeeId=%@&drNumber=%@", employeeId, drNumber] relativeToURL:client.baseURL]];
    request.HTTPMethod = @"POST";
    [request setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
    request.HTTPBody = xmlData;
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate xmlSubmissionDidSucceed];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate submissionDidFail:error];
    }];
    
    [client.operationQueue addOperation:operation];
}

-(void)getMasterForms {
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://174.127.39.132/LIMSApp_Service/Service1.svc/"]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"Get_All_Master_Form" relativeToURL:client.baseURL]];
    request.HTTPMethod = @"GET";
    [request setValue:@"text/json" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate getFormsDidSucceed];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate submissionDidFail:error];
    }];
    
    [client.operationQueue addOperation:operation];
}

-(void)submitMasterForm:(NSString *)formName active:(BOOL)isActive formId:(NSString *)formId fileName:(NSString *)fileName {
    NSError *error;
    NSDictionary *dict = @{@"Form_Name": formName, @"Form_Id": formId, @"Doc_Path": fileName, @"Is_Active": @1};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://174.127.39.132/LIMSApp_Service/"]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"ServiceDataServe.svc/Insert_Update_Master_Form" relativeToURL:client.baseURL]];
    request.HTTPBody = jsonData;
    request.HTTPMethod = @"POST";
    [request setValue:@"text/json" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate formUploadDidSucceed];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate submissionDidFail:error];
    }];
    
    [client.operationQueue addOperation:operation];
}

-(void)submitHours:(FormBuilder *)formBuilder {
    int regHours = 0;
    int otHours = 0;
    NSString *project = formBuilder.metadata[@"projectId"];
    NSString *scope = formBuilder.metadata[@"scope"];
    NSString *task = formBuilder.metadata[@"task"];
    NSString *employeeId = formBuilder.metadata[@"employeeId"];
    NSString *drNumber = formBuilder.metadata[@"drNumber"];
    
    NSDictionary *footerData = [self getFooterData:formBuilder];
    regHours = [footerData[@"t105"] intValue];
    otHours = [footerData[@"t106"] intValue];
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:kHourTrackingUrl]];
    client.parameterEncoding = AFJSONParameterEncoding;
    
    [client postPath:@"hours" parameters:@{
                                           @"project": project,
                                           @"scope": scope,
                                           @"task": task,
                                           @"employeeId": employeeId,
                                           @"drNumber": drNumber,
                                           @"regularHours": @(regHours),
                                           @"overtimeHours": @(otHours)
                                           }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.delegate hoursSubmissionDidSucceed];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate submissionDidFail:error];
    }];
}

-(void)submitDRForm:(FormBuilder *)formBuilder {
    int regHours = 0;
    int otHours = 0;
    NSString *project = formBuilder.metadata[@"projectId"];
    NSString *scope = formBuilder.metadata[@"scope"];
    NSString *task = formBuilder.metadata[@"task"];
    NSString *employeeId = formBuilder.metadata[@"employeeId"];
    NSString *drNumber = formBuilder.metadata[@"drNumber"];
    
    NSDictionary *footerData = [self getFooterData:formBuilder];
    regHours = [footerData[@"t105"] intValue];
    otHours = [footerData[@"t106"] intValue];
    
    NSString *strPDFs = [[NSUserDefaults standardUserDefaults] valueForKey:drNumber];
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://174.127.39.132/LIMSApp_Service/ServiceDataServe.svc/LIMS_APP_On_DR_Submission"]];
    client.parameterEncoding = AFJSONParameterEncoding;
    
    [client postPath:@"hours" parameters:@{
                                           @"Dispatch_No": drNumber,
                                           @"Ref_Documents": @"",
                                           @"Gen_Contractor": @"",
                                           @"Sub_Contractor": @"",
                                           @"Mat_Supplier": @"",
                                           @"Reg_Hours": @(regHours),
                                           @"OT_Hours": @(otHours),
                                           @"Work_Inspected_Desc": @"",
                                           @"Additional_Remarks": @"",
                                           @"Is_Work_Inspected": @"",
                                           @"Is_Add_Photo": @"",
                                           @"Is_Contain_Samples": @"",
                                           @"Is_Contain_Master_Form": @"",
                                           @"Begin_Time": @"",
                                           @"End_Time": @"",
                                           @"Weather": @"",
                                           @"Non_Compliance_Desc": @"",
                                           @"Tech": @"",
                                           @"Master_Form_CSV": strPDFs
                                           }
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 [self.delegate hoursSubmissionDidSucceed];
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 [self.delegate submissionDidFail:error];
             }];
}

-(NSDictionary*)getFooterData:(FormBuilder*)formBuilder {
    for (FormSection *section in formBuilder.sections) {
        for (FormComponentEditor *footerEditor in section.editors) {
            if ([footerEditor isKindOfClass:[NonDSAFooterEditor class]]) {
                NSDictionary *data = [footerEditor getData];
                return data;

            }
        }
    }
    return nil;
}

@end
