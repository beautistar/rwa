//
//  AutolayoutLabel.h
//  RMA
//
//  Created by Alexander Roode on 2/18/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutolayoutLabel : UILabel

@end
