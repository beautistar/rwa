//
//  EditChecklistsButtonEditor.m
//  RMA
//
//  Created by Alexander Roode on 3/12/15.
//  Copyright (c) 2015 Forebrain. All rights reserved.
//

#import "EditChecklistsButtonEditor.h"

@implementation EditChecklistsButtonEditor

-(IBAction)editChecklistsButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EditChecklists" object:nil];
}

@end
