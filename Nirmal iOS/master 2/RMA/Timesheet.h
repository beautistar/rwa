//
//  Timesheet.h
//  RMA
//
//  Created by Michael Beteag on 12/18/13.
//  Copyright (c) 2013 Forebrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"

@interface Timesheet : NSObject <NSCoding, GRMustacheRendering>

@property (nonatomic, retain) NSDictionary *DRs;
@property (nonatomic, retain) NSDictionary *totals;

- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;
+(Timesheet*)timesheetFromDictionary:(NSDictionary*)dictionary;

-(NSString *)renderForMustacheTag:(GRMustacheTag *)tag
                          context:(GRMustacheContext *)context
                         HTMLSafe:(BOOL *)HTMLSafe
                            error:(NSError **)error;

@end
